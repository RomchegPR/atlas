#include "include/Parser/ZoneParser.h"
#include <QFile>
#include <QTextCodec>
#include <QStringList>
#include <iostream>

ZoneParser::ZoneParser():
	mFileName("")
{
}

ZoneParser::ZoneParser(QString fileName):
	mFileName( fileName )
{
}

void ZoneParser::setFileName(QString fileName)
{
    mFileName = fileName;
}

// А зачем зачитывать данные из старого файла если новый пуст?
void ZoneParser::readData(QString fileName){
    if(fileName != "") setFileName(fileName);
    QFile in(mFileName);
    in.open(QIODevice::ReadOnly);
    QVector<QString> mapIndices;
    // Набор ключей для мапа
    mapIndices << "Name" << "Points" << "Altitude" << "Description";
    //0 - name, 1 - points, 2 - altitude, 3 - description;
    QString tmp, zoneType = "";
    QMap<QString, QString> tmpMap;
    QVector<QMap<QString, QString> > tmpZones;

	// Кодеки ставяться один раз нет никакого смысла устанавливать их каждый раз
    QStringList tmpList;
    while(!in.atEnd()){
        tmp = QString(in.readLine()).toLower();
        tmp.trimmed();
//        qDebug() << tmp;
        if(tmp.size() < 2) continue;
        if(tmp.contains("границ")) continue;
        tmpList = tmp.split('\t');
        //qDebug() << tmpList.size();
        if(tmpList.size() == 1){
            //qDebug() << tmpList[0];
            if(tmpList[0].size() > 5){
                if(tmpList[0].contains(("запретны"))) {
                    //qDebug() << "запретные";
                    zoneType = "запретные зоны";
                }
                else if(tmpList[0].contains(QString("ответственности").toUtf8())){
                    //qDebug() << "ответственности";
                    zoneType = "зоны ответственности";
                }
                else{
                    //qDebug() << "ограничения";
                    zoneType = "зоны ограничения";
                }
            }
            continue;
        }
        for(int i = 1; i < tmpList.size(); i++){
            tmpList[i] = tmpList[i].simplified();
            tmpMap[mapIndices[i - 1]] = tmpList[i];
        }
        //qDebug() << zoneType;
        tmpZones.push_back(tmpMap);
        mOriginalZones[zoneType] += tmpZones;
        tmpMap.clear();
        tmpZones.clear();
    }
}

// Метод, возвращающий исходные данные, считанные из входного файла.

QMap<QString, QVector<QMap<QString, QString> > >& ZoneParser::getZones(){
    return mOriginalZones;
}

ZoneParser::~ZoneParser(){
    mOriginalZones.clear();
}
