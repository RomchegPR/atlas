#include "include/Zones/SeidelTriangulation.h"
#include "include/AirPort/Turn.h"
#include <limits.h>
using namespace std;

bool operator==(sortedPoint a, sortedPoint b)
{
    return (a.mP == b.mP && a.index == b.index);
}


/*-----------------------------------------------

Name:	setCoorditanes

Params: VertexData - массив в который записываются данные
              a,b,c - точки
              altMin, altMax - MAX и MIN высоты зоны

/*---------------------------------------------*/

void setCoorditanes(QVector < glm::vec3 > &VertexData, glm::vec3 a, glm::vec3 b, glm::vec3 c, GLdouble altMin, GLdouble altMax)
{

    Turn round;
    glm::vec3 q, w, r;
    q = round.calcRoundPointOnDist(glm::vec3(-a.x, a.y, a.z),glm::vec3(0,0,0),  -(altMin));
    w = round.calcRoundPointOnDist(glm::vec3(-b.x, b.y, b.z),glm::vec3(0,0,0),  -(altMin));
    r = round.calcRoundPointOnDist(glm::vec3(-c.x, c.y, c.z),glm::vec3(0,0,0),  -(altMin));

    VertexData.push_back(q);
    VertexData.push_back(w);
    VertexData.push_back(r);
    glm::vec3 d, e, f;

    d = round.calcRoundPointOnDist(glm::vec3(-a.x, a.y, a.z),glm::vec3(0,0,0),  -(altMax));
    e = round.calcRoundPointOnDist(glm::vec3(-b.x, b.y, b.z),glm::vec3(0,0,0),  -(altMax));
    f = round.calcRoundPointOnDist(glm::vec3(-c.x, c.y, c.z),glm::vec3(0,0,0),  -(altMax));
    VertexData.push_back(glm::vec3(d.x, d.y,  d.z));
    VertexData.push_back(glm::vec3(f.x, f.y,  f.z));
    VertexData.push_back(glm::vec3(e.x, e.y,  e.z));
}


/*-----------------------------------------------

Name:	IsReflex

Params: a,b,c - точки

Result: Проверку выпуклая вершина или нет

/*---------------------------------------------*/

bool IsReflex(glm::vec3 &a, glm::vec3 &b, glm::vec3 &c) {
    GLfloat tmp;
    tmp = (c.y-a.y)*(b.x-a.x)-(c.x-a.x)*(b.y-a.y);
    if(tmp < 0) return 1;
    else return 0;
}

/*-----------------------------------------------
Name:	pointInPolygon

Params: polygon - исходный полигон
              point - точка дл яповерки вхождения

Result: Проверка на нахождения точки в полигоне
/*---------------------------------------------*/

MonoPolygon::MonoPolygon(QVector <glm::vec3> points, GLboolean lSweep, GLboolean rSweep)
{
    // Использование указателся this тут лишнее
    // Как вообще в голову пришла идея назвать параметры с префиксом m?
    mPoints = points;
    mLsweep = lSweep;
    mRsweep = rSweep;
}


bool pointInPolygon(QVector <glm::vec3> polygon, glm::vec2 point)
{
    int i, j, nvert = polygon.size();
    bool c = false;

    // Хардкорно, я даже не стану пытаться понять что тут происходит :)
    for(i = 0, j = nvert - 1; i < nvert; j = i++)
    {
        if(((polygon[i].y >= point.y ) != (polygon[j].y >= point.y)) &&
        (point.x <= (polygon[j].x - polygon[i].x) * (point.y - polygon[i].y) / (polygon[j].y - polygon[i].y) + polygon[i].x))
        {
            c = !c;
        }
    }
    return c;
}

/*-----------------------------------------------
Name:	SortForSortedPoint

Params: array - исходные точки

Result: массив точек отсортированный по Х
/*---------------------------------------------*/

void SortForSortedPoint(QVector <sortedPoint> &array)
// Сортировка для отсортированных?
{
    for(int i = 0; i < array.size() ; i++){
        for(int j = 0; j < array.size() - 1 ; j++){
            if(array[j].mP.x >= array[j+1].mP.x){
                swap(array[j], array[j+1]);
            }
        }
    }
}

/*-----------------------------------------------
Name:	seidelTriangulation

Params: mPoints - исходные точки
              altMin, altMax - MAX и MIN высоты зоны
              mVertexData - Массив точек для отрисовки

Result: MonoPolygons - Совокупность монополигонов, из которых можно получить исходный полигон
Алгоритм можно посмотреть тут http://algolist.manual.ru/maths/geom/polygon/decompose.php
/*---------------------------------------------*/


/*-----------------------------------------------
Name:	fragmentationPolygon

Params: originalPolygon - исходный полигон с всевозможными изломами

Result: MonoPolygons - Совокупность монополигонов, из которых можно получ
Алгоритм можно посмотреть тут http://algolist.manual.ru/maths/geom/polygon/decompose.phpить исходный полигон
/*---------------------------------------------*/

void fragmentationPolygon(QVector < glm::vec3 > &originalPolygon, QVector < MonoPolygon > &MonoPolygons)
{
    MonoPolygons.push_back(MonoPolygon(originalPolygon, false, false));
    GLuint sumLFlag(1), sumRFlag(1); // можно сделать bool
    while (sumLFlag != 0)
    {
        sumLFlag = 0;
        GLuint n(1);
        for(GLuint i = 0; i < n; i++) // Пробегаем по всему массиву полигонов и ищем, где проверка на левые изгибы не пройдены
        {
            if (!MonoPolygons[i].mLsweep)
            {
                QVector <sortedPoint> sortedOriginVector;
                QVector < glm::vec3 > tmpVector;
                GLdouble MinDistance(numeric_limits<double>::infinity());
                GLint idxMinDistance(-1);

                for (int j = 0; j < MonoPolygons[i].mPoints.size(); j++)
                {

                    sortedOriginVector.push_back(sortedPoint(MonoPolygons[i].mPoints[j],j));
                }

                SortForSortedPoint(sortedOriginVector);

                for (int j = 1; j < sortedOriginVector.size(); j++) // проверка каждой вершины на наличие левого изгиба
                {
                    if (sortedOriginVector[j].mP.x < MonoPolygons[i].mPoints[((sortedOriginVector[j].index-1)+ MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size()].x &&
                            sortedOriginVector[j].mP.x < MonoPolygons[i].mPoints[((sortedOriginVector[j].index + 1) + MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size()].x)
                    {
                        for (int k = j - 1; k >= 0; k--) // находим ближайшую вершину
                        {
                            if(glm::distance(MonoPolygons[i].mPoints[sortedOriginVector[j].index], MonoPolygons[i].mPoints[sortedOriginVector[k].index]) < MinDistance &&
                                    (sortedOriginVector[k].index != (((sortedOriginVector[j].index+1)+MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size()) &&
                                     sortedOriginVector[k].index != (((sortedOriginVector[j].index-1)+MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size())))
                            {
                                MinDistance = glm::distance(MonoPolygons[i].mPoints[sortedOriginVector[j].index], MonoPolygons[i].mPoints[sortedOriginVector[k].index]);
                                idxMinDistance = k;
                            }
                        }
                        if (idxMinDistance != -1)
                        {
                            if (pointInPolygon(originalPolygon,glm::vec2((MonoPolygons[i].mPoints[sortedOriginVector[idxMinDistance].index].x + MonoPolygons[i].mPoints[sortedOriginVector[j].index].x)/2.0f,
                                                                         (MonoPolygons[i].mPoints[sortedOriginVector[idxMinDistance].index].y + MonoPolygons[i].mPoints[sortedOriginVector[j].index].y)/2.0f)))
                            {
                                if (sortedOriginVector[j].index < sortedOriginVector[idxMinDistance].index)
                                {
                                    for (int k = sortedOriginVector[j].index; k <= sortedOriginVector[idxMinDistance].index; k++)
                                    {
                                        tmpVector.push_back(MonoPolygons[i].mPoints[k]);
                                    }
                                    if(!tmpVector.empty())
                                    {
                                        MonoPolygons[i].mPoints.remove(sortedOriginVector[j].index + 1, (sortedOriginVector[idxMinDistance].index-sortedOriginVector[j].index) - 1);
                                        MonoPolygons.push_back(MonoPolygon(tmpVector, false, false));
                                        n++;
                                    }
                                }

                                if (sortedOriginVector[j].index >  sortedOriginVector[idxMinDistance].index)
                                {
                                    for (int k = sortedOriginVector[idxMinDistance].index; k <= sortedOriginVector[j].index; k++)
                                    {
                                        tmpVector.push_back(MonoPolygons[i].mPoints[k]);
                                    }
                                    if (!tmpVector.empty())
                                    {
                                        MonoPolygons[i].mPoints.remove(sortedOriginVector[idxMinDistance].index+1,(sortedOriginVector[j].index - sortedOriginVector[idxMinDistance].index)-1);
                                        MonoPolygons.push_back(MonoPolygon(tmpVector, false, false));
                                        n++;
                                    }
                                }
                            tmpVector.clear();
                            }
                        }
                        sortedOriginVector.clear();
                        for (int j = 0; j < MonoPolygons[i].mPoints.size(); j++)
                        {
                            sortedOriginVector.push_back(sortedPoint(MonoPolygons[i].mPoints[j],j));
                        }

                        SortForSortedPoint(sortedOriginVector);
                        tmpVector.clear();
                        i = 0;
                        MinDistance =  DBL_MAX;
                    }
                }
                sortedOriginVector.clear();
            }
             MonoPolygons[i].mLsweep = true;
        }
    }

    while (sumRFlag != 0)
    {
        sumRFlag = 0;

        GLint n(1);
        for (int i = 0; i  < n; i++)
        {
            if (!MonoPolygons[i].mRsweep)
            {
                QVector <sortedPoint> sortedOriginVector;
                QVector < glm::vec3 > tmpVector;
                GLdouble MinDistance(DBL_MAX);
                GLint idxMinDistance(-1);
                for (int j = 0; j < MonoPolygons[i].mPoints.size(); j++)
                {
                    sortedOriginVector.push_back(sortedPoint(MonoPolygons[i].mPoints[j],j));
                }

                SortForSortedPoint(sortedOriginVector);
                GLuint sizeOfVector;
                sizeOfVector = sortedOriginVector.size();
                for (int j = sizeOfVector - 2; j > 0; j--) // проверка каждой вершины на наличие левого изгиба
                {
//                       Проверка на тип вершины
//                       Проверка на правый изгиб
                    if (sortedOriginVector[j].mP.x > MonoPolygons[i].mPoints[((sortedOriginVector[j].index - 1) + MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size()].x &&
                            sortedOriginVector[j].mP.x > MonoPolygons[i].mPoints[((sortedOriginVector[j].index + 1) + MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size()].x)
                        {
                            for (int k = sizeOfVector - 2; k >= j+1; k--)
                            {
                                if(glm::distance(MonoPolygons[i].mPoints[sortedOriginVector[j].index], MonoPolygons[i].mPoints[sortedOriginVector[k].index]) < MinDistance &&
                                        (sortedOriginVector[k].index != (((sortedOriginVector[j].index + 1)+MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size()) &&
                                         sortedOriginVector[k].index!= (((sortedOriginVector[j].index-1)+MonoPolygons[i].mPoints.size())%MonoPolygons[i].mPoints.size())))
                                {
                                    MinDistance = glm::distance(MonoPolygons[i].mPoints[sortedOriginVector[j].index], MonoPolygons[i].mPoints[sortedOriginVector[k].index]);
                                    idxMinDistance = k;
                                }
                            }
                            if (idxMinDistance != -1)
                            {
                                if (pointInPolygon(originalPolygon,glm::vec2((MonoPolygons[i].mPoints[sortedOriginVector[idxMinDistance].index].x+MonoPolygons[i].mPoints[sortedOriginVector[j].index].x)/2.0f,
                                                                             (MonoPolygons[i].mPoints[sortedOriginVector[idxMinDistance].index].y+MonoPolygons[i].mPoints[sortedOriginVector[j].index].y)/2.0f)))
                                {
                                    if (sortedOriginVector[j].index < sortedOriginVector[idxMinDistance].index)
                                    {
                                        for (int k = sortedOriginVector[j].index; k <=sortedOriginVector[idxMinDistance].index; k++)
                                        {
                                            tmpVector.push_back(MonoPolygons[i].mPoints[k]);
                                        }
                                        if (!tmpVector.empty())
                                        {
                                            MonoPolygons[i].mPoints.remove(sortedOriginVector[j].index + 1, (sortedOriginVector[idxMinDistance].index - sortedOriginVector[j].index)-1);
                                            MonoPolygons.push_back(MonoPolygon(tmpVector, true, false));
                                            n++;
                                        }
                                    }

                                    if (sortedOriginVector[j].index > sortedOriginVector[idxMinDistance].index)
                                    {
                                        for (int k = sortedOriginVector[idxMinDistance].index; k <= sortedOriginVector[j].index; k++)
                                        {
                                            tmpVector.push_back(MonoPolygons[i].mPoints[k]);
                                        }
                                        if (!tmpVector.empty())
                                        {
                                            MonoPolygons[i].mPoints.remove(sortedOriginVector[idxMinDistance].index+1, ( sortedOriginVector[j].index -  sortedOriginVector[idxMinDistance].index)-1);
                                            MonoPolygons.push_back(MonoPolygon(tmpVector, true, false));
                                            n++;
                                        }
                                    }
                                    tmpVector.clear();
                                }
                            }
                            sortedOriginVector.clear();
                            for (int j = 0; j < MonoPolygons[i].mPoints.size(); j++)
                            {
                                sortedOriginVector.push_back(sortedPoint(MonoPolygons[i].mPoints[j],j));
                            }
                            sizeOfVector = sortedOriginVector.size();
                            j = sizeOfVector - 2;
                            SortForSortedPoint(sortedOriginVector);
                            i = 0;
                            MinDistance =  DBL_MAX;
                        }
                }
                sortedOriginVector.clear();
            }
            else
            {
                 if (MonoPolygons[i].mRsweep)(sumLFlag++);
            }
             MonoPolygons[i].mRsweep = true;
        }
    }
}

/*-----------------------------------------------
Name:	triangulation

Params: mPoints - исходные точки
              direction - направление описания точек в исходном массиве
              altMin, altMax - MAX и MIN высоты зоны
              VertexData - Массив точек для отрисовки

Result: Массив точек для отрисовки, полученный методом сканирующей линии
Алгоритм можно посмотреть тут: http://llati.upc.edu/geoc/mat1q1112/triangulacio_de_poligons_EN.pdf
/*---------------------------------------------*/

void triangulation(QVector < glm::vec3 > points, GLdouble altMin, GLdouble altMax , QVector < glm::vec3 > &VertexData)
{
    static int q;

    QVector <glm::vec3> originVector(points);
    QVector <sortedPoint> downVertices, upVertices, sortedOriginVector;
    QVector <sortedPoint> queue;
    GLuint idxMin, idxMax;
    GLboolean flagFirstPoint, flagSecondPoint; //  true - верхняя ветка, false - нижняя;
    glm::vec3 a,b,c;

    for (int i = 0; i < originVector.size(); i++)
    {
        sortedOriginVector.push_back(sortedPoint(originVector[i],i));
    }

    SortForSortedPoint(sortedOriginVector);
    idxMin = sortedOriginVector[0].index;
    idxMax = sortedOriginVector[sortedOriginVector.size()-1].index;

    int i(idxMin);
    while (i != idxMax)
    {
        upVertices.push_back(sortedPoint(originVector[i], i ));
        i = ((i+1) + originVector.size()) % originVector.size();
    }
    upVertices.push_back(sortedPoint(originVector[idxMax], idxMax ));

    i = idxMax;
    while (i != idxMin)
    {
        downVertices.push_back(sortedPoint(originVector[i], i ));
        i = ((i+1) + originVector.size()) % originVector.size();
    }
    downVertices.push_back(sortedPoint(originVector[idxMin], idxMin));

//     ПОЕХАЛИ!
//    ____|~\_
//    [4x4_|_|-]
//    (_)   (_)

        queue.push_back(sortedOriginVector[0]);
        queue.push_back(sortedOriginVector[1]);


    for (int i = 2; i < sortedOriginVector.size(); i++)
    {
        if(queue.size() > 1)
        {
            flagFirstPoint = upVertices.contains(queue[queue.size()-1]);
            flagSecondPoint = upVertices.contains(sortedOriginVector[i]);
            if (flagFirstPoint != flagSecondPoint)
            {
                if(!queue.empty())
                {
                    for (int j = 0; j < queue.size() - 1; j++)
                    {
                        setCoorditanes(VertexData,
                                       glm::vec3(sortedOriginVector[i].mP.x, sortedOriginVector[i].mP.y, sortedOriginVector[i].mP.z),
                                       glm::vec3(queue[j].mP.x, queue[j].mP.y, queue[j].mP.z),
                                       glm::vec3(queue[j + 1].mP.x, queue[j + 1].mP.y, queue[j + 1].mP.z),
                                       altMin, altMax);
                    }
                    queue.remove(0, queue.size()-1);
                    queue.push_back(sortedOriginVector[i]);
                }
            }
            if(flagFirstPoint == flagSecondPoint)
            {
                a = queue[queue.size()-2].mP;
                b = queue[queue.size()-1].mP;
                c = sortedOriginVector[i].mP;
                if (!upVertices.contains(queue[queue.size()-1]))
                {
                    if (IsReflex(a,b,c))
                    {
                        queue.push_back(sortedOriginVector[i]);
                    }
                    else
                    {
                        setCoorditanes(VertexData,
                                       glm::vec3(sortedOriginVector[i].mP.x, sortedOriginVector[i].mP.y, sortedOriginVector[i].mP.z),
                                       glm::vec3(queue[queue.size()-2].mP.x, queue[queue.size()-2].mP.y, queue[queue.size()-2].mP.z),
                                       glm::vec3(queue[queue.size()-1].mP.x, queue[queue.size()-1].mP.y, queue[queue.size()-1].mP.z),
                                       altMin, altMax);
                        queue.remove(queue.size() - 1);
                        i--;
                    }
                }
                else
                {
                    if (!IsReflex(a,b,c))
                    {
                        queue.push_back(sortedOriginVector[i]);
                    }
                    else
                    {
                        setCoorditanes(VertexData,
                                        glm::vec3(sortedOriginVector[i].mP.x, sortedOriginVector[i].mP.y, sortedOriginVector[i].mP.z),
                                        glm::vec3(queue[queue.size()-2].mP.x, queue[queue.size()-2].mP.y, queue[queue.size()-2].mP.z),
                                        glm::vec3(queue[queue.size()-1].mP.x, queue[queue.size()-1].mP.y, queue[queue.size()-1].mP.z),
                                        altMin, altMax);
                        queue.remove(queue.size() - 1);
                        i--;
                    }
                }
            }
        }
        else if(queue.size() < 2)
        {
            queue.push_back(sortedOriginVector[i]);
        }
        q++;
//        qDebug() << q;
    }
}

void seidelTriangulation(QVector < glm::vec3 > &points, GLdouble altMin, GLdouble altMax, QVector < glm::vec3 > &vertexData)
{
    QVector < MonoPolygon > MonoPolygons;
    fragmentationPolygon(points, MonoPolygons);
    for (int i = 0; i < MonoPolygons.size(); i++)
    {
        triangulation(MonoPolygons[i].mPoints, altMin, altMax, vertexData);
    }
}
