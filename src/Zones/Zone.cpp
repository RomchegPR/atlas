﻿#include "include/Zones/Zone.h"
#include <limits.h>
#include "include/AirPort/Turn.h"
#include "include/Zones/ZoneConverter.h"
#include "MainRenderModel.h"
#include <GeographicLib/TransverseMercator.hpp>
#include <GeographicLib/Gnomonic.hpp>
#include <GeographicLib/Geocentric.hpp>
#include "MainSettingsClass.h"

Zone::Zone(){
    MainRenderModel = SingleMainRenderModel::Instance();
    Status = UNCHECKED;

    SettingsClass = mainSettingsClass::Instance();
}

/*-----------------------------------------------

Name:	setName

Params: -

Result:	mName

/*---------------------------------------------*/

void Zone::setName(QString name)
{
    mName = name;
}

void Zone::setAltitude()
{
    mAltMin = mInputFlyZone.mAltMin;
    mAltMax = (mInputFlyZone.mAltMax == std::numeric_limits<double>::infinity()) ? 30000 : mInputFlyZone.mAltMax;
}


/*-----------------------------------------------

Name:	calcNormals

Params: -

Result:	mRenderData[BasicType].mNormal - Normals for light

/*---------------------------------------------*/

void Zone::calcNormals()
{
    for (int i = 0; i < mRenderData[LinesType].mVertex.size(); i++){
        mRenderData[BasicType].mNormal.push_back(glm::vec3());
    }

    for ( int i = 0 ; i < mRenderData[LinesType].mVertex.size() - 3; i += 3)
    {
        glm::vec3 v1 = mRenderData[LinesType].mVertex[i + 1] - mRenderData[LinesType].mVertex[i];
        glm::vec3 v2 = mRenderData[LinesType].mVertex[i + 2] - mRenderData[LinesType].mVertex[i];
        glm::vec3 Normal = glm::cross(v1,v2);

        glm::normalize(Normal);

        mRenderData[BasicType].mNormal[i] += Normal;
        mRenderData[BasicType].mNormal[i + 1] += Normal;
        mRenderData[BasicType].mNormal[i + 2] += Normal;
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mNormal.size() ; i++){
        glm::normalize(mRenderData[BasicType].mNormal[i]);
    }
}

Zone::~Zone(){
    mRenderData[BasicType].mVertex.clear();
    mRenderData[LinesType].mVertex.clear();
    mPointsGeocentric.clear();
}

/*-----------------------------------------------

Name:	calcCenter

Params: -

Result:	mCentrZone - координаты центральной точки

/*---------------------------------------------*/

void Zone::calcCenter()
{
    glm::vec3 sum;
    for (int i = 0; i < mRenderData[LinesType].mVertex.size(); i++)
    {
        sum.x += mRenderData[LinesType].mVertex[i].x;
        sum.y += mRenderData[LinesType].mVertex[i].y;
        sum.z += mRenderData[LinesType].mVertex[i].z;

    }

    float PointsCount = mRenderData[LinesType].mVertex.size();

     mCentrZone = glm::vec3(sum.x/PointsCount, sum.y/PointsCount, sum.z/PointsCount);
}

/*-----------------------------------------------

Name:	setType

Params: type - тип зоны (false - Зона ограничения, true - Запретная зона)

Result:	mType - устанавливает тип зоны

/*---------------------------------------------*/

void Zone::setType(TypesForZones type)
{
    mType = type;
}

void Zone::setColor(glm::vec4 color)
{
    mColor = color;
}

/*-----------------------------------------------

Name:	getType

Params: -

Result:	mType - возвращает тип зоны

/*---------------------------------------------*/

TypesForZones Zone::getType()
{
    return mType;
}

/*-----------------------------------------------

Name:	setPoints

Params: -

Result:	Sets all Points for redering

/*---------------------------------------------*/

void Zone::setPoints()
{
    mRenderData[BasicType].mVertex.clear();
    mRenderData[LinesType].mVertex.clear();

    concertInputPoints();
    setPointsForSide();
    setPointsForUpsideAndDownside();
}

void Zone::updatePointsWithNewAlt()
{
    mRenderData[BasicType].mVertex.clear();
    mRenderData[LinesType].mVertex.clear();
    updatePointsForSide();
    updatePointsForUpsideAndDownside();
    updateBuffers();
}
/*-----------------------------------------------

Name:	setPointsForSide

Params: -

Result:	Sets point for rendering sides for Zone

/*---------------------------------------------*/

void Zone::setPointsForSide()
{
    if (mPointsMercator.back() != mPointsMercator.front())
    {
        mPointsMercator.push_back(mPointsMercator.front());
    }

    setPointsForSideLinesType();

    buildMorePoints();

    setPointsForSideBasicType();

    if (mPointsMercator.back() == mPointsMercator.front())
    {
        mPointsMercator.remove(mPointsMercator.size() - 1);
    }
}

void Zone::updatePointsForSide()
{
    if (mPointsMercator.back() != mPointsMercator.front())
    {
        mPointsMercator.push_back(mPointsMercator.front());
    }

    setPointsForSideLinesType();

    setPointsForSideBasicType();

    if (mPointsMercator.back() == mPointsMercator.front())
    {
        mPointsMercator.remove(mPointsMercator.size() - 1);
    }
}



/*-----------------------------------------------

Name:	setTriangelesForRendering

Params: 3 mercator points

Result:	Add's triangle (geocentric points) to mRenderData[BasicType].mVertex

/*---------------------------------------------*/

void Zone::setTriangelesForRendering(glm::vec2 &mercatorPointA, glm::vec2 &mercatorPointB, glm::vec2 &mercatorPointC)
{
    glm::vec3 a,b,c;
    GLdouble tmpAltMin, tmpAltMax;
    tmpAltMin = mAltMin * SettingsClass->getHeightRate();
    tmpAltMax = mAltMax * SettingsClass->getHeightRate();

    a = GeoHelper::mercator2cartesian(mercatorPointA, tmpAltMin);
    b = GeoHelper::mercator2cartesian(mercatorPointB, tmpAltMin);
    c = GeoHelper::mercator2cartesian(mercatorPointC, tmpAltMin);

    mRenderData[BasicType].mVertex.push_back(convertToWorld(a));
    mRenderData[BasicType].mVertex.push_back(convertToWorld(b));
    mRenderData[BasicType].mVertex.push_back(convertToWorld(c));

    a = GeoHelper::mercator2cartesian(mercatorPointA, tmpAltMax);
    b = GeoHelper::mercator2cartesian(mercatorPointB, tmpAltMax);
    c = GeoHelper::mercator2cartesian(mercatorPointC, tmpAltMax);

    mRenderData[BasicType].mVertex.push_back(convertToWorld(a));
    mRenderData[BasicType].mVertex.push_back(convertToWorld(b));
    mRenderData[BasicType].mVertex.push_back(convertToWorld(c));
}

/*-----------------------------------------------

Name:	setPointsForUpsideAndDownside

Params: -

Result:	Sets point for rendering upside and downside for Zone

/*---------------------------------------------*/

void Zone::setPointsForUpsideAndDownside()
{
    QVector <QVector <int>> allSegments;
    QVector <int> segment;

    segment.push_back(mPointsMercator.size() - 1);
    segment.push_back(0);
    allSegments.push_back(segment);
    segment.clear();

    for (int i = 0; i < mPointsMercator.size() - 1; i++)
    {
        segment.push_back(i);
        segment.push_back(i + 1);
        allSegments.push_back(segment);
        segment.clear();
    }

    mTriangulator.DoTriangulate(mPointsMercator, &allSegments);

    for (int i = 0; i < mTriangulator.getIndexes().size(); i++)
    {
        setTriangelesForRendering(
        mTriangulator.getIndexes()[i][0],
        mTriangulator.getIndexes()[i][1],
        mTriangulator.getIndexes()[i][2]);
    }
}

void Zone::updatePointsForUpsideAndDownside()
{
    for (int i = 0; i < mTriangulator.getIndexes().size(); i++)
    {
        setTriangelesForRendering(
        mTriangulator.getIndexes()[i][0],
        mTriangulator.getIndexes()[i][1],
        mTriangulator.getIndexes()[i][2]);
    }
}

/*-----------------------------------------------

Name:	setPointsForSideBasicType

Params: -

Result: Sets points for color fill Zone

/*---------------------------------------------*/

void Zone::setPointsForSideBasicType()
{
    for (int i = mPointsMercator.size() - 2; i >= 0 ; i--)
    {
        GLdouble tmpAltMin, tmpAltMax;
        tmpAltMin = mAltMin * SettingsClass->getHeightRate();
        tmpAltMax = mAltMax * SettingsClass->getHeightRate();

        glm::vec3 a = GeoHelper::geo2cartesian(glm::vec3(GeoHelper::mercator2geo((mPointsMercator[i])), tmpAltMax), true);

        glm::vec3 b = GeoHelper::geo2cartesian(glm::vec3(GeoHelper::mercator2geo((mPointsMercator[i + 1])), tmpAltMax), true);

        glm::vec3 c = GeoHelper::geo2cartesian(glm::vec3(GeoHelper::mercator2geo((mPointsMercator[i + 1])), tmpAltMin), true);

        glm::vec3 d = GeoHelper::geo2cartesian(glm::vec3(GeoHelper::mercator2geo((mPointsMercator[i])) ,tmpAltMin), true);

        a = convertToWorld(a);
        b = convertToWorld(b);
        c = convertToWorld(c);
        d = convertToWorld(d);

        mRenderData[BasicType].mVertex.push_back(a);    // Верхняя левая
        mRenderData[BasicType].mVertex.push_back(b);    // Верхняя правая
        mRenderData[BasicType].mVertex.push_back(c);    // Нижняя правая

        mRenderData[BasicType].mVertex.push_back(c);    // Нижняя правая
        mRenderData[BasicType].mVertex.push_back(d);    // Нижняя левая
        mRenderData[BasicType].mVertex.push_back(a);    // Верхняя левая

        mRenderData[LinesType].mVertex.push_back(a);    // Верхняя левая
        mRenderData[LinesType].mVertex.push_back(b);    // Верхняя правая

        mRenderData[LinesType].mVertex.push_back(c);     // Ниняя правая
        mRenderData[LinesType].mVertex.push_back(d);     // Нижняя левая
    }
}

/*-----------------------------------------------

Name:	setPointsForSideBasicType

Params: -

Result: Sets points Lines of Zone

/*---------------------------------------------*/

void Zone::setPointsForSideLinesType()
{
    for (int i = mPointsMercator.size() - 2; i >= 0 ; i--)
    {
        GLdouble tmpAltMin, tmpAltMax;
        tmpAltMin = mAltMin * SettingsClass->getHeightRate();
        tmpAltMax = mAltMax * SettingsClass->getHeightRate();

        glm::vec3 a = GeoHelper::geo2cartesian(glm::vec3(GeoHelper::mercator2geo((mPointsMercator[i])), tmpAltMax), true);

        glm::vec3 d = GeoHelper::geo2cartesian(glm::vec3(GeoHelper::mercator2geo((mPointsMercator[i])) ,tmpAltMin), true);

        a = convertToWorld(a);
        d = convertToWorld(d);
        if (mInputFlyZone.mPointsGeographical[i].mType == SIMPLE_POINT)
        {
            mRenderData[LinesType].mVertex.push_back(a);  //  Верхняя левая
            mRenderData[LinesType].mVertex.push_back(d);  //  Нижняя левая
        }
    }
}

/*-----------------------------------------------

Name:	buildMorePoints

Params: -

Result: Check segment. Add new points in middle if segment is more then smth distance(ZoneBuilder::DISTANCE_BETWEEN_POINTS).

/*---------------------------------------------*/

void Zone::buildMorePoints()
{
    for(GLint i = 0; i < mPointsMercator.size() - 1; i++)
    {
        if (glm::distance(mPointsMercator[i], mPointsMercator[i + 1]) > ZoneBuilder::DISTANCE_BETWEEN_POINTS)
        {
            addPointsInMiddle(i);
            buildMorePoints();
        }
    }
}

/*-----------------------------------------------

Name:	addPointsInMiddle

Params: index - current index of vertex

Result: find middle point in segment

/*---------------------------------------------*/

void Zone::addPointsInMiddle(GLint index)
{
    glm::vec2 middlePoint;
    middlePoint = (mPointsMercator[index] + mPointsMercator[index + 1]);
    middlePoint /= 2;
    mPointsMercator.insert(index + 1, middlePoint);
    ZonePoint tmpGeographicalPoint(GeoHelper::mercator2geo(middlePoint), SIMPLE_POINT_ADDED_FOR_TRIANGULATION);
    mInputFlyZone.mPointsGeographical.insert(index + 1, tmpGeographicalPoint);
}

glm::vec3 Zone::getCenter(){
    return mCentrZone;
}
QString Zone::getTypeName(){
    return mType ? "Запретная Зона" : "Зона Ограничения";
}

double Zone::getAltMax(){
    return mAltMax;
}

double Zone::getAltMin(){
    return mAltMin;
}

QString Zone::getName(){
    return mName;
}

ObjectStatus Zone::getStatus() const
{
    return Status;
}

void Zone::setStatus(const ObjectStatus &value)
{
    Status = value;
}

void Zone::paint()
{

    QSet < QString > * CheckedZones = &MainRenderModel->mZoneBuilder.mCheckedZones;

    if (CheckedZones->contains(mName)){
        setStatus(CHECKED);
    } else {
        setStatus(UNCHECKED);
    }

    MainRenderModel->mShaders[LightShader].bind();

    // for Frame of Restricted
    glLineWidth(SettingsClass->getLinesWidth(LINE_ZONE_FRAME));
    mRenderData[LinesType].mVAO.bind();
    updateColor(&mRenderData[LinesType], LINES);
    glDrawArrays(GL_LINES, 0, mRenderData[LinesType].mVertex.size());
    mRenderData[LinesType].mVAO.unbind();

    MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);

    mRenderData[BasicType].mVAO.bind();
    updateColor(&mRenderData[BasicType], HALL);
    glDrawArrays(GL_TRIANGLES, 0, mRenderData[BasicType].mVertex.size());
    mRenderData[BasicType].mVAO.unbind();
}

void Zone::updateColor(RenderData *curVAO, elementsToDraw element){

    curVAO->mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    curVAO->mColorBuffer.clear();

    glm::vec3 color3;
    if (this->getStatus() == CHECKED){
        color3 = SettingsClass->getCheckedObjectColor();
    } else if (this->getStatus() == UNCHECKED){
        color3 = SettingsClass->getZonesColors(mType);
    }

    float trancpar;

    if (element == HALL){
        trancpar = SettingsClass->getObjectsTrancparency(TRANCPARENCY_FILL_ZONE);
    } else if (element == LINES) {
        trancpar = SettingsClass->getObjectsTrancparency(TRANCPARENCY_FRAME_ZONE);
    }
    glm::vec4 color4 = glm::vec4(color3, trancpar);

    curVAO->mColorBuffer.setData( sizeof(glm::vec4), &color4);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);
}

void Zone::init()
{
    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mColorBuffer.create();
    mRenderData[BasicType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    glm::vec4 color1 = glm::vec4(mColor.x,mColor.y,mColor.z, 0.1);
    mRenderData[BasicType].mVertexBuffer.setData(sizeof(glm::vec4), &color1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    mRenderData[BasicType].mVAO.unbind();

    mRenderData[LinesType].mVAO.create();
    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mVertexBuffer.create();
    mRenderData[LinesType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    if (!mRenderData[LinesType].mVertex.isEmpty()) {
        mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    }
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mColorBuffer.create();
    mRenderData[LinesType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);

    glm::vec4 color2 = glm::vec4(mColor.x,mColor.y,mColor.z, 0.5);
    mRenderData[LinesType].mVertexBuffer.setData(sizeof(glm::vec4), &color2);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    mBloor = false;

    mRenderData[LinesType].mVAO.unbind();
}

void Zone::updateBuffers (){

    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mVertexBuffer.bind();
    mRenderData[BasicType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVAO.unbind();

    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mVertexBuffer.bind();
    mRenderData[LinesType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mVAO.unbind();
}

void Zone::setFlyZone(FlyZone &flyZone)
{
    mInputFlyZone = flyZone;
}

FlyZone Zone::getFlyZone(){
    return mInputFlyZone;
}

void Zone::clear()
{
    mRenderData[BasicType].clear();
    mRenderData[LinesType].clear();
}

void    Zone::drawTriangle  (QVector < glm::vec3 > vecs)
{
    mRenderData[RenderObject::LinesType].mVertex.push_back(vecs.at(0));
    mRenderData[RenderObject::LinesType].mVertex.push_back(vecs.at(1));
    mRenderData[RenderObject::LinesType].mVertex.push_back(vecs.at(1));
    mRenderData[RenderObject::LinesType].mVertex.push_back(vecs.at(2));
    mRenderData[RenderObject::LinesType].mVertex.push_back(vecs.at(2));
    mRenderData[RenderObject::LinesType].mVertex.push_back(vecs.at(0));
}

void Zone::concertInputPoints()
{
    glm::vec2 tmpPoint;
    for (int i = 0; i < mInputFlyZone.mPointsGeographical.size(); i++)
    {
        tmpPoint = GeoHelper::geo2mercator(glm::vec2(mInputFlyZone.mPointsGeographical[i].mLatitude, mInputFlyZone.mPointsGeographical[i].mLongitude));
        if (tmpPoint.x != 0 && tmpPoint.y != 0) // Check for NaN (Mercator can return NaN after convert)
        {
            mPointsMercator.push_back(tmpPoint);

        }
    }
}

void Zone::convertPointFromMercatorToGeocentric()
{
    for (int i = 0; i < mPointsMercator.size(); i++)
    {
        mPointsGeocentric.push_back(GeoHelper::mercator2cartesian(mPointsMercator[i]));
    }
}

glm::vec3 Zone::convertToWorld( glm::vec3 &inputPoint)
{
    return glm::vec3(-inputPoint.x, inputPoint.z, inputPoint.y);
}
