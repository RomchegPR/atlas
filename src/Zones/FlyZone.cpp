#include "include/Zones/FlyZone.h"

ZonePoint::ZonePoint(){

}

ZonePoint::ZonePoint(glm::vec3 v, TypesOfPoints type)
{
    mLatitude = v.x;
    mLongitude = v.y;
    mType = type;
}

ZonePoint::ZonePoint(glm::vec2 v, TypesOfPoints type){
    mLatitude = v.x;
    mLongitude = v.y;
    mType = type;
}
