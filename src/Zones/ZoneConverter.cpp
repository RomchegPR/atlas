#include "include/Zones/ZoneConverter.h"
#include <QRegExp>
#include <cmath>
#include "math.h"
#include <limits.h>
#include <QStringList>
#include <glm/glm.hpp>
#include <iostream>
#include <iomanip>

ZoneConverter::ZoneConverter()
{
}

ZoneConverter::ZoneConverter(ZoneParser &ZP)
{
    mConvertedZones.clear();
    fill(ZP.getZones());
}

// Преобразование высот
// На вход подается строка, содержащая описание высот для зоны. На выходе получается минимальная и максимальная высоты

QList<double> ZoneConverter::convertAlt(QString &alt){
    QRegExp rx("ams*");
    rx.setPatternSyntax(QRegExp::Wildcard);
    alt.remove(rx);
    alt.remove("ft");
    alt.remove(" ");
    QStringList tmpList;
    QList<double> heightList;
    if(alt.contains("–")) tmpList = alt.split("–");
    else tmpList = alt.split("-");
    if(tmpList.size() == 1){
        if(tmpList[0].contains("gn")) tmpList[0] = "0";
        if(tmpList[0] == "unl") tmpList[0] = "-1";
        if(tmpList[0].contains("fl")) {
            tmpList[0].remove("fl");
            tmpList[0] = QString::number(tmpList[0].toDouble() * 100, 'g', 20);
        }
        heightList.append(GeoHelper::ft2metres(tmpList[0].toDouble()) - 250);
        heightList.append(GeoHelper::ft2metres(tmpList[0].toDouble()) + 250);
    }
    else{
        for(int i = 0; i < 2; i++){
            int idx = tmpList[i].indexOf('(');
            if(idx != -1) tmpList[i].remove(idx, tmpList[i].size() - idx);
        }
        for(int i = 0; i < 2; i++){
            if(tmpList[i].contains("fl")) {
                tmpList[i].remove("fl");
                tmpList[i] = QString::number(tmpList[i].toDouble() * 100, 'g', 20);
            }
            if(tmpList[i] == "gnd") heightList.append(GeoHelper::ft2metres(0));
            else if(tmpList[i] == "unl") heightList.append(GeoHelper::ft2metres(std::numeric_limits<double>::infinity()));
            else heightList.append(GeoHelper::ft2metres(tmpList[i].toDouble()));
        }
    }
    return heightList;
}

// Преобразование зоны из исходных данных

void ZoneConverter::convertPoints(QVector<ZonePoint> &points, QMap<int, double> &radius){
    QVector<ZonePoint> tmpVector;
    for(int i = 0; i < points.size(); i++){
        if(points[i].mType == SIMPLE_POINT){
            glm::vec3 decartPoint = GeoHelper::geo2cartesian(glm::vec2(GeoHelper::dms2deg(points[i].mX), GeoHelper::dms2deg(points[i].mY)));
            tmpVector.push_back(ZonePoint(decartPoint, SIMPLE_POINT));
        }
        else{
            ZonePoint startPoint = points[i - 1], endPoint = points[i + 1];
            double lat = GeoHelper::dms2rad(points[i].mX), lon = GeoHelper::dms2rad(points[i].mY);
            double startAzi = 0, endAzi = 0;
            GeoHelper::inverseProblem(lat, lon, GeoHelper::dms2rad(startPoint.mX), GeoHelper::dms2rad(startPoint.mY), startAzi);
            GeoHelper::inverseProblem(lat, lon, GeoHelper::dms2rad(endPoint.mX), GeoHelper::dms2rad(endPoint.mY), endAzi);
            glm::vec3 decartPoint;
            if(points[i].mType == COUNTER_CLOCKWISE_ARC_POINT){
                if(startAzi < endAzi){
                    double k = startAzi - 0.05;
                    while(!GeoHelper::compare(k, endAzi)){
                        glm::vec2 pt = GeoHelper::directionProblem(glm::vec2(lat, lon), k, radius[i] / 6371.0);
                        pt[0] = pt[0] * 180.0 / M_PI, pt[1] = pt[1] * 180.0 / M_PI;
                        decartPoint = GeoHelper::geo2cartesian(pt);

                        tmpVector.push_back(ZonePoint(decartPoint, COUNTER_CLOCKWISE_ARC_POINT));
                        k -= 0.05;
                        if(k < 0) k += 2 * M_PI;

                    }
                }
                else {
                    for(double k = startAzi - 0.05; k > endAzi; k -= 0.05){
                        glm::vec2 pt = GeoHelper::directionProblem(glm::vec2(lat, lon), k, radius[i] / 6371.0);
                        pt[0] = pt[0] * 180.0 / M_PI, pt[1] = pt[1] * 180.0 / M_PI;
                        decartPoint = GeoHelper::geo2cartesian(pt);
                        tmpVector.push_back(ZonePoint(decartPoint, COUNTER_CLOCKWISE_ARC_POINT));
                    }
                }
            }
            else{
                if(startAzi < endAzi){
                    for(double k = startAzi; k < endAzi; k += 0.05){
                        glm::vec2 pt = GeoHelper::directionProblem(glm::vec2(lat, lon), k, radius[i] / 6371.0);
                        pt[0] = pt[0] * 180.0 / M_PI, pt[1] = pt[1] * 180.0 / M_PI;
                        decartPoint = GeoHelper::geo2cartesian(pt);
                        tmpVector.push_back(ZonePoint(decartPoint, CLOCKWISE_ARC_POINT));
                    }
                }
                else {
                    double k = startAzi + 0.05;
                    while(!GeoHelper::compare(k, endAzi)){
                        glm::vec2 pt = GeoHelper::directionProblem(glm::vec2(lat, lon), k, radius[i] / 6371.0);
                        pt[0] = pt[0] * 180.0 / M_PI, pt[1] = pt[1] * 180.0 / M_PI;
                        decartPoint = GeoHelper::geo2cartesian(pt);
                        tmpVector.push_back(ZonePoint(decartPoint, CLOCKWISE_ARC_POINT));
                        k += 0.05;
                        if(k > 2 * M_PI) k -= 2 * M_PI;
                    }
                }
            }
        }
    }
    points = tmpVector;
}

void ZoneConverter::fill(QMap<QString, QVector<QMap<QString, QString> > > &zones){
    mOriginalZones = zones;
    QList<QString> keys = zones.keys();
    QRegExp oneChar("[a-zа-я]");
    for(int i = 0; i < keys.size(); i++){
        for(int j = 0; j < zones[keys[i]].size(); j++){
            FlyZone tmpZone;
            tmpZone.mName = zones[keys[i]][j]["Name"];
            tmpZone.mDescription = zones[keys[i]][j]["Description"];
            if(zones[keys[i]][j]["Points"].contains("окружность")){
                QStringList tmpList = zones[keys[i]][j]["Points"].split(" ");
                int idx = tmpList[2].indexOf(",");
                if(idx != -1){
                    tmpList[2][idx] = '.';
                }
                tmpZone.mRadius[0] = tmpList[2].toDouble();
                tmpList[6].remove(oneChar);
                tmpList[7].remove(oneChar);
                glm::vec3 decartPoint;
                double lat = GeoHelper::dms2rad(tmpList[6].toDouble()), lon = GeoHelper::dms2rad(tmpList[7].toDouble());
                for(int k = 0; k < 360; k+=20){
                    glm::vec2 pt = GeoHelper::directionProblem(glm::vec2(lat, lon), k * M_PI/ 180.0, tmpZone.mRadius[0] / 6371.0);
                    pt[0] = GeoHelper::rad2deg(pt[0]), pt[1] = GeoHelper::rad2deg(pt[1]);
                    decartPoint = GeoHelper::geo2cartesian(pt);
                    tmpZone.mPoints.push_back(ZonePoint(decartPoint, CLOCKWISE_ARC_POINT));
                }
            } else if(zones[keys[i]][j]["Points"].contains("часовой")){
                QStringList tmpPointsList = zones[keys[i]][j]["Points"].split(", ");
                for(int k = 0; k < tmpPointsList.size(); k++){
                    QStringList tmpPoint;
                    tmpPointsList[k].remove("геоточки ");
                    if(tmpPointsList[k].contains("исключая")) continue;
                    tmpPoint = tmpPointsList[k].split(" ");
                    if(tmpPoint.size() == 2){
                        tmpPoint[0].remove(oneChar);
                        tmpPoint[1].remove(oneChar);
                        tmpZone.mPoints.push_back(ZonePoint(glm::vec2(tmpPoint[0].toDouble(), tmpPoint[1].toDouble()), SIMPLE_POINT));
                    }
                    else{
                        TypesOfPoints direction;
                        if(tmpPointsList[k].contains("против")) direction = COUNTER_CLOCKWISE_ARC_POINT;
                        else direction = CLOCKWISE_ARC_POINT;
                        for(int idx = 0; idx < tmpPoint.size(); idx++){
                            if(tmpPoint[idx].contains("радиусом")){
                                idx++;
                                int x = tmpPoint[idx].indexOf(",");
                                if(x != -1){
                                    tmpPoint[idx][x] = '.';
                                }
                                tmpZone.mRadius[tmpZone.mPoints.size()] = tmpPoint[idx].toDouble();
                            }
                            if(tmpPoint[idx].contains("центр")){
                                idx++;
                                tmpPoint[idx].remove(oneChar);
                                tmpPoint[idx+1].remove(oneChar);
                                tmpZone.mPoints.push_back(ZonePoint(glm::vec2(tmpPoint[idx].toDouble(), tmpPoint[idx + 1].toDouble()), direction));
                            }
                            if(tmpPoint[idx].contains("до")){
                                idx++;
                                tmpPoint[idx].remove(oneChar);
                                tmpPoint[idx+1].remove(oneChar);
                                tmpZone.mPoints.push_back(ZonePoint(glm::vec2(tmpPoint[idx].toDouble(), tmpPoint[idx + 1].toDouble()), SIMPLE_POINT));
                            }
                        }
                    }
                }
                convertPoints(tmpZone.mPoints, tmpZone.mRadius);
            }
            else {
                QStringList tmpPointsList = zones[keys[i]][j]["Points"].split(", ");
                for(int k = 0; k < tmpPointsList.size(); k++){
                    QStringList tmpPoint;
                    if(tmpPointsList[k].contains("исключая")) continue;
                    tmpPoint = tmpPointsList[k].split(" ");
                    tmpPoint[0].remove(oneChar);
                    tmpPoint[1].remove(oneChar);
                    glm::vec3 point = GeoHelper::geo2cartesian(glm::vec2(GeoHelper::dms2deg(tmpPoint[0]), GeoHelper::dms2deg(tmpPoint[1])));
                    tmpZone.mPoints.push_back(ZonePoint(point, SIMPLE_POINT));
                }
            }
            QList<double> tmpH = convertAlt(zones[keys[i]][j]["Altitude"]);
            tmpZone.mAltMin = tmpH[0];
            tmpZone.mAltMax = tmpH[1];
            mConvertedZones[keys[i]].append(tmpZone);
        }
    }
}
// Метод, возвращаюший преобразованные зоны для отрисовки

QMap<QString, QVector<FlyZone> > &ZoneConverter::getZones(){
    return mConvertedZones;
}

// Методы, для дебага

void ZoneConverter::printZone(QString& name) const{
    std::cout << std::setprecision(2) << std::fixed;
    QList<QString> keys = mConvertedZones.keys();
    for(int j = 0; j < mConvertedZones[keys[0]].size(); j++){
        if(mConvertedZones[keys[0]][j].mName == name){
            std::cout << mConvertedZones[keys[0]][j].mName.toStdString() << std::endl;
            for(int i = 0; i < mConvertedZones[keys[0]][j].mPoints.size(); i++){
                std::cout << "X: " << mConvertedZones[keys[0]][j].mPoints[i].mX << " Y: " << mConvertedZones[keys[0]][j].mPoints[i].mY << std::endl;
            }
        }
    }
}

void ZoneConverter::print() const{
    std::cout << std::setprecision(2) << std::fixed;
    QList<QString> keys = mConvertedZones.keys();
    for(int i = 0; i < keys.size(); i++){
        for(int j = 0; j < mConvertedZones[keys[i]].size(); j++){
            std::cout << mConvertedZones[keys[i]][j].mName.toStdString() << std::endl;
            for(int k = 0; k < mConvertedZones[keys[i]][j].mPoints.size(); k++){
                std::cout << mConvertedZones[keys[i]][j].mPoints[k].mX << " " << mConvertedZones[keys[i]][j].mPoints[k].mY << std::endl;
            }
            std::cout << "Min Alt: " << mConvertedZones[keys[i]][j].mAltMin << " Max Alt: " << mConvertedZones[keys[i]][j].mAltMax << std::endl;
            std::cout << std::endl;
        }
    }
}

void ZoneConverter::PrintMix(QString& name) const{
    std::cout << std::setprecision(2) << std::fixed;
    QList<QString> keys = mConvertedZones.keys();
    for(int j = 0; j < mConvertedZones[keys[1]].size(); j++){
        if(mConvertedZones[keys[1]][j].mName == name){
            std::cout << mConvertedZones[keys[1]][j].mName.toStdString() << std::endl;
            for(int i = 0; i < mConvertedZones[keys[1]][j].mPoints.size(); i++){
                std::cout << "X: " << mConvertedZones[keys[1]][j].mPoints[i].mX << " Y: " << mConvertedZones[keys[1]][j].mPoints[i].mY << std::endl;
                if(mConvertedZones[keys[1]][j].mPoints[i].mType != 0) std::cout << "Radius : " << mConvertedZones[keys[1]][j].mRadius[i] << std::endl;
            }
        }
    }
}

ZoneConverter::~ZoneConverter(){

}
