#include "include/Zones/ZoneBuilder.h"
#include <QTextCodec>
#include <QStringList>
#include "include/Helper.h"

const int  ZoneBuilder::DISTANCE_BETWEEN_POINTS = 50000;

ZoneBuilder::ZoneBuilder()
{
    // Цвета для заливки зон
    mColorForZones[1] = glm::vec3(1.0, 0.0, 0.0);
    mColorForZones[0] = glm::vec3(1.0, 0.64, 0.29);
    mColorForZones[2] = glm::vec3(0.0, 0.0, 1.0);
    // Цвета для каркаса зон
    mColorForFrame = glm::vec4(1.0, 1.0, 1.0, TRANCPARENCY_FRAME_ZONE);
    mMinHeightForDraw = 0;
    mMaxHeightForDraw = 1000000;
}

void ZoneBuilder::fillData(QMap < QString, QVector < FlyZone> > &FlyZones)
{
    mZones.clear();

    QList < QString > keys = FlyZones.keys();

    for (int i = 0; i < keys.size(); i++)
    {
        for (int j = 0; j < FlyZones[keys[i]].size(); j++)
        {
            Zone tmpZone;

//            qDebug() << FlyZones[keys[i]][j].mName;
            tmpZone.setFlyZone(FlyZones[keys[i]][j]);
//            if(FlyZones[keys[i]][j].mName == "uur317"){
            tmpZone.setName(FlyZones[keys[i]][j].mName);
            tmpZone.setAltitude();
            tmpZone.setPoints();
            tmpZone.calcCenter();
            tmpZone.calcNormals();
            if(keys[i].contains("запрет")) tmpZone.setType(PRIVATE);
            else if(keys[i].contains("ответ")) tmpZone.setType(RESPONSIBLE);
            else tmpZone.setType(RESTRICTED);
            mZones.push_back(tmpZone);
//            }
        }
    }
}

void ZoneBuilder::init(){

    for (int i = 0; i < mZones.size(); i++)
    {

        if(mZones[i].getType() == 1) {
            mZones[i].setColor(glm::vec4(mColorForZones[1], TRANCPARENCY_FILL_ZONE));
        }  else if (mZones[i].getType() == 2){
            mZones[i].setColor(glm::vec4(mColorForZones[2], TRANCPARENCY_FILL_ZONE ));
        }
        else {
            mZones[i].setColor(glm::vec4(mColorForZones[0], TRANCPARENCY_FILL_ZONE));
        }

        mZones[i].init();
    }
}

void ZoneBuilder::addCheckedZone(QString ZoneName){

    if (getNames().contains(ZoneName)){
        if (mCheckedZones.contains(ZoneName)){
            mCheckedZones.remove(ZoneName);
        }else{
            mCheckedZones.insert(ZoneName);
        }
    }
}

void ZoneBuilder::draw(int i){
    glLineWidth( LINE_WIDTH );

    if (mCheckedZones.contains(mZones[i].mName)){

        mZones[i].setStatus(CHECKED);
    }
    else {
        mZones[i].setStatus(UNCHECKED);
    }

    mZones[i].paint();

}

void ZoneBuilder::setHeightRate(){
    for (int i = 0; i < mZones.size(); i++)
    {
        mZones[i].updatePointsWithNewAlt();
    }
}

GLdouble ZoneBuilder::getMaxHeightForDraw(){
    return mMaxHeightForDraw;
}
void ZoneBuilder::setmMaxHeightForDraw(GLdouble height){
    mMaxHeightForDraw = height;
}

GLdouble ZoneBuilder::getMinHeightForDraw(){
    return mMinHeightForDraw;
}
void ZoneBuilder::setMinHeightForDraw(GLdouble height){
    mMinHeightForDraw = height;
}

QVector < Zone > *ZoneBuilder::getZones(){
    return &mZones;
}
void ZoneBuilder::setColor(GLint id, glm::vec3 color){
    mColorForZones[id] = color;
}
glm::vec3 ZoneBuilder::getColor(GLint id){
    return mColorForZones[id];
}

QStringList ZoneBuilder::getNames(){
    QStringList list;

    for (int i = 0; i < mZones.size(); i++){
        list << mZones[i].getName();
    }

    return list;
}

ZoneBuilder::~ZoneBuilder(){
    for(int i = 0; i < mZones.size(); i++){
        mZones[i].clear();
    }
}
