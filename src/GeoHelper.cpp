#define _USE_MATH_DEFINES

#include "include/GeoHelper.h"
#include <GeographicLib/Constants.hpp>
#include <GeographicLib/TransverseMercator.hpp>
#include "cmath"

const float GeoHelper::FT_2_MT = 0.3048f;
const float GeoHelper::MT_2_FT = 1.0f / FT_2_MT;
const float GeoHelper::DEG_2_RAD = M_PI / 180.0f;
const float GeoHelper::RAD_2_DEG = 1.0f / DEG_2_RAD;
const float GeoHelper::MAJOR_AXIS = 6378137.0f;
const float GeoHelper::MINOR_AXIS = 6356752.3142f;
const float GeoHelper::FLATTENING = (MAJOR_AXIS - MINOR_AXIS) / MINOR_AXIS;
const float GeoHelper::ECCENTRICITY = sqrt(2.0f * FLATTENING - FLATTENING * FLATTENING);
const float GeoHelper::MINUTE_2_SECONDS = 60.0f;
const float GeoHelper::HOUR_2_SECONDS = 3600.f;
const float GeoHelper::FLATTENING_INV = 1 - FLATTENING;
const float GeoHelper::FLATTEINING_INV_SQUARE = FLATTENING_INV * FLATTENING_INV;
const float GeoHelper::LON_0 = 37.61f;
const float GeoHelper::LAT_0 = 55.75f;
const GeographicLib::Geocentric GeoHelper::GEOCENTRIC = GeographicLib::Geocentric(MAJOR_AXIS, FLATTENING);
const GeographicLib::TransverseMercator GeoHelper::MERCATOR = GeographicLib::TransverseMercator(MAJOR_AXIS, FLATTENING, GeographicLib::Constants::UTM_k0());
const GeographicLib::Geodesic GeoHelper::GEODESIC = GeographicLib::Geodesic(MAJOR_AXIS, FLATTENING);
const GeographicLib::Gnomonic GeoHelper::GNOMONIC = GeographicLib::Gnomonic();

GeoHelper::GeoHelper(){}

GeoHelper::~GeoHelper(){}

bool GeoHelper::compare(double x, double y) {
    const double eps = 0.1;

    return fabs(x-y) > eps ? 0 : 1;
}

float GeoHelper::ft2metres(float val) { return val * FT_2_MT; }

float GeoHelper::ft2metres(QString &val) { return val.toFloat() * FT_2_MT; }

float GeoHelper::deg2rad(float degree) { return degree * DEG_2_RAD; }

float GeoHelper::deg2rad(const QString &degree) { return degree.toFloat() * DEG_2_RAD; }

float GeoHelper::rad2deg(float rad) { return rad * RAD_2_DEG; }

float GeoHelper::dms2deg(float degree) {
    float degrees = floor( degree / 10000.0f );
    float minutes = floor( ( degree - degrees * 10000.0f ) / 100.0f );
    float seconds = ( degree - ( degrees * 10000.0f + minutes * 100.0f ) );
    return dms2deg(degrees, minutes, seconds);
}

QString GeoHelper::deg2dms(float dms)
{
    int degrees = ( int ) dms;
    float minutes = ( dms - degrees ) * 60;
    int seconds = ( minutes - ( int ) minutes ) * 60;

    QString degreesString = formatStringDMS( QString::number( degrees ) );
    QString minutesString = formatStringDMS( QString::number( (int)minutes ) );
    QString secondsString = formatStringDMS( QString::number( seconds ) );

    QString dmsString = degreesString + "°" + minutesString + "′" + secondsString + " ";
    return dmsString;
}

QString GeoHelper::formatStringDMS(QString value){
    if( value.size() == 1 ) {
        value.insert( 0, '0' );
    }
    return value;
}

QString GeoHelper::rad2dms(QString value){
    float dms = rad2deg( value.toFloat() );
    return deg2dms(dms);
}

float GeoHelper::dms2deg(float degrees, float minutes, float seconds) { return degrees + minutes / MINUTE_2_SECONDS + seconds / HOUR_2_SECONDS; }

float GeoHelper::dms2deg(const QString &value) { return dms2deg(value.toFloat()); }

float GeoHelper::dms2rad(float degree) { return dms2deg(degree) * DEG_2_RAD; }

float GeoHelper::dms2rad(const QString &value) { return dms2deg(value.toFloat()) * DEG_2_RAD; }

float GeoHelper::dms2rad(float degrees, float minutes, float seconds) { return dms2deg(degrees, minutes, seconds) * DEG_2_RAD; }

glm::vec3 GeoHelper::geo2cartesian(QString &lat, QString &lon, bool isRad) {
    return geo2cartesian(glm::vec2(dms2deg(lat.toFloat()), dms2deg(lon.toFloat())), isRad);
}

// geodetic -> ecef

// TODO move all to radians

glm::vec3 GeoHelper::geo2cartesian(glm::vec3 point, bool isRad) {

    double lat = point.x, lon = point.y, h = point.z;
    double x, y, z;
    if(isRad){
        GEOCENTRIC.Forward(rad2deg(lat), rad2deg(lon), h, x, y, z);
    } else {
        GEOCENTRIC.Forward(lat, lon, h, x, y, z);
    }
    return glm::vec3(x, y, z);
}

glm::vec3 GeoHelper::geo2cartesianGL(glm::vec3 point, bool isRad) {

    glm::vec3 noGLpoint = geo2cartesian(point, isRad);
    glm::vec3 GLpoint = glm::vec3(-noGLpoint.x, noGLpoint.z, noGLpoint.y);
    return GLpoint;
}

glm::vec3 GeoHelper::geo2cartesian(glm::vec2 point, bool isRad) {
    return geo2cartesian(glm::vec3(point, 0.0), isRad);
    /*float x = MAJOR_AXIS * lon;
    float y = MAJOR_AXIS * log( tan( M_PI / 4 + lat / 2 ) * pow( 1 - ECCENTRICITY * sin ( lat ) / ( 1 + ECCENTRICITY * sin( lat ) ), ( ECCENTRICITY / 2.0 ) ) );
    return glm::vec2(x, y);*/
}

glm::vec3 GeoHelper::currentRadius(float val){
    double clat = cos(DEG_2_RAD * val);
    double slat = sin(DEG_2_RAD * val);
    double dsq = 1.0 - ECCENTRICITY * ECCENTRICITY * slat * clat;
    double d = sqrt(dsq);
    double rn = MAJOR_AXIS / d;
    double rm = rn * (1.0 - ECCENTRICITY * ECCENTRICITY) / dsq;
    double rho = rn * clat;
    double z = (1.0 - ECCENTRICITY * ECCENTRICITY) * rm * slat;
    double rsq = rho * rho + z * z;
    double r = sqrt(rsq);
    return glm::vec3(r, rn, rm);
}

float GeoHelper::radiusEarth(float lat){
    return currentRadius(lat).x;
}

// перевод широты из геодезической в геоцентрическую

float GeoHelper::geocentic2geodetic(glm::vec2 point){
    double flatgc = point.x;
    double altkm = point.y;
    double ecc = ECCENTRICITY;
    double esq = ecc * ecc;
    double altnow = altkm;
    glm::vec3 rrnrm = currentRadius(flatgc);
    double rn = rrnrm.y;
    double ratio = 1 - esq * rn / (rn + altnow);
    double tlat = tan(DEG_2_RAD * flatgc) / ratio;
    double flatgd = RAD_2_DEG * atan(tlat);
    rrnrm = currentRadius(flatgd);
    rn = rrnrm.y;
    ratio = 1 - esq * rn / (rn + altnow);
    flatgd = RAD_2_DEG * atan(tlat);
    return flatgd;
}

// Спизжено отсюда http://www.oc.nps.edu/oc2902w/coord/llhxyz.htm

glm::vec2 GeoHelper::cartesian2geo(glm::vec3 point){
    double x = point.x, y = point.y, z = point.z;
    double lat, lon, h;
    GEOCENTRIC.Reverse(x, y, z, lat, lon, h);

    return glm::vec2(deg2rad(lat) , deg2rad(lon));

    /*double flat, flon, altkm;
    double eps = 10e-10;
    double esq = ECCENTRICITY * ECCENTRICITY;
    double x = point.x;
    double y = point.y;
    double z = point.z;
    double rp = sqrt(x * x + y * y + z * z);
    double flatgc = asin(z / rp) / DEG_2_RAD;
    double testVal = abs(x) + abs(y);

    if(testVal < eps){
        flon = 0.0;
    }
    else{
        flon = atan2(y,x) / DEG_2_RAD;
    }
    if(flon < 0.0){
        flon += 360.0;
    }
    double p = sqrt(x*x + y*y);
    if( p < eps){
        flat = 90.0;
        if(z < 0.0){
            flat = -90.0;
        }
        altkm = rp - radiusEarth(flat);
        return glm::vec2(flat, flon);
    }

    double rnow = radiusEarth(flatgc);
    altkm = rp - rnow;
    flat = geocentic2geodetic(glm::vec2(flatgc, altkm));
    glm::vec3 rrnrm = currentRadius(flat);
    double rn = rrnrm.y;
    for(int count = 0; count < 10; count++){
        double slat = sin(DEG_2_RAD * flat);
        double tangd = (z + rn * esq * slat) / p;
        double flatn = atan(tangd) / DEG_2_RAD;
        double dlat = flatn - flat;
        flat = flatn;
        double clat = cos(DEG_2_RAD * flat);
        rrnrm = currentRadius(flat);
        rn = rrnrm.y;
        altkm = (p / clat) - rn;
        if(abs(dlat) < eps){
            break;
        }
    }

    return glm::vec2(flat * DEG_2_RAD , flon * DEG_2_RAD);*/
}

double GeoHelper::heightAboveSeaLevel(glm::vec3 &point){
    double x = point.x, y = point.y, z = point.z;
    double lat, lon, h;
    GEOCENTRIC.Reverse(x, y, z, lat, lon, h);

    return h;
}

glm::vec2 GeoHelper::directionProblem(glm::vec2 point, double angle, double dist) {
    /*double lat, lon;
    GEODESIC.Direct(rad2deg(point.x), rad2deg(point.y), rad2deg(angle), dist, lat, lon);
    return glm::vec2(deg2rad(lat), deg2rad(lon));*/
    glm::vec2 pt2;
    glm::vec2 pt1 = point;
    glm::vec2 pt;
    glm::vec3 x;
    pt[0] = M_PI_2 - dist;
    pt[1] = M_PI - angle;
    spherToCart(x, pt);			// сферические -> декартовы
    rotate(x, pt1[0] - M_PI_2, 1);	// первое вращение
    rotate(x, -pt1[1], 2);		// второе вращение
    cartToSpher(x, pt2);
    return pt2;
}

void GeoHelper::spherToCart(glm::vec3 &pt1, glm::vec2 &pt2) {
    double p = cos(pt2[0]);
    pt1[2] = sin(pt2[0]);
    pt1[1] = p * sin(pt2[1]);
    pt1[0] = p * cos(pt2[1]);
}

float GeoHelper::inverseProblem(double x1, double y1, double x2, double y2, double &azimuth) {
    /*double dist, azimuth1, azimuth2;
    GEODESIC.Inverse(rad2deg(x1), rad2deg(y1), rad2deg(x2), rad2deg(y2), dist, azimuth1, azimuth2);
    azimuth = azimuth1;
    return dist;*/

    double dist = 0;
    glm::vec2 pt1 = glm::vec2(x1,y1);
    glm::vec2 pt2 = glm::vec2(x2,y2);
    sphereInverse(pt1, pt2, azimuth, dist);
    return dist;
}

void GeoHelper::sphereInverse(glm::vec2 &pt1, glm::vec2 &pt2, double &azi, double &dist) {
    glm::vec3 x;
    glm::vec2 pt;
    spherToCart(x, pt2);			// сферические -> декартовы
    rotate(x, pt1[1], 2);			// первое вращение
    rotate(x, M_PI_2 - pt1[0], 1);	// второе вращение
    cartToSpher(x, pt);	     		// декартовы -> сферические
    azi = M_PI - pt[1];
    dist = M_PI_2 - pt[0];
}

void GeoHelper::rotate(glm::vec3 &pt, double a, int i) {
    int j = (i + 1) % 3, k = (i - 1) % 3;
    double sinA = sin(a), cosA = cos(a), xj = pt[j] * cosA + pt[k] * sinA;
    pt[k] = -pt[j] * sinA + pt[k] * cosA;
    pt[j] = xj;
}

float GeoHelper::cartToSpher(glm::vec3 &pt1, glm::vec2 &pt2) {
    double p = hypot(pt1[0], pt1[1]);
    pt2[1] = atan2(pt1[1], pt1[0]);
    pt2[0] = atan2(pt1[2], p);
    return hypot(p, pt1[2]);
}

glm::vec2 GeoHelper::geo2mercator(const glm::vec2 &point)
{
    double x, y;
    MERCATOR.Forward(LON_0, point.x, point.y, x, y);
    return glm::vec2(x, y);

    /*glm::vec2 decartPoint;
    decartPoint.x = MAJOR_AXIS * point.y;
    decartPoint.y = - MAJOR_AXIS * log( tan( M_PI_4 + point.x / 2.0 ) );
    return decartPoint;*/
}

glm::vec2 GeoHelper::mercator2geo(const glm::vec2 &point){
    double lat, lon;
    MERCATOR.Reverse(LON_0, point.x, point.y, lat, lon);
    return glm::vec2(deg2rad(lat), deg2rad(lon));
}

glm::vec3 GeoHelper::mercator2cartesian(const glm::vec2 &point, float alt){
    return geo2cartesian(glm::vec3(mercator2geo(point), alt), true);
}

glm::vec3 GeoHelper::mercator2cartesian(const glm::vec2 &point){
    return geo2cartesian(mercator2geo(point), true);
}

glm::vec2 GeoHelper::gnomonic2geo(const glm::vec2 &point){
    double x, y;
    GNOMONIC.Forward(LAT_0, LON_0, rad2deg(point.x), rad2deg(point.y), x, y);
    return glm::vec2(x, y);
}

glm::vec2 GeoHelper::geo2gnomonic(const glm::vec2 &point){
    double lat, lon;
    GNOMONIC.Reverse(LAT_0, LON_0, point.x, point.y, lat, lon);
    return glm::vec2(deg2rad(lat), deg2rad(lon));
}
