#include "include/CubeMap.h"
#include "MainRenderModel.h"
#include "QCoreApplication"

CubeMap::CubeMap():
    mSize(25000000.0f)
{
}

void CubeMap::init(){

    mRenderData[BasicType].mVertex <<
      glm::vec3(-mSize,  mSize, -mSize) <<
      glm::vec3(-mSize, -mSize, -mSize) <<
      glm::vec3( mSize, -mSize, -mSize) <<
      glm::vec3( mSize, -mSize, -mSize) <<
      glm::vec3( mSize,  mSize, -mSize) <<
      glm::vec3(-mSize,  mSize, -mSize) <<

      glm::vec3(-mSize, -mSize,  mSize) <<
      glm::vec3(-mSize, -mSize, -mSize) <<
      glm::vec3(-mSize,  mSize, -mSize) <<
      glm::vec3(-mSize,  mSize, -mSize) <<
      glm::vec3(-mSize,  mSize,  mSize) <<
      glm::vec3(-mSize, -mSize,  mSize) <<

      glm::vec3( mSize, -mSize, -mSize) <<
      glm::vec3( mSize, -mSize,  mSize) <<
      glm::vec3( mSize,  mSize,  mSize) <<
      glm::vec3( mSize,  mSize,  mSize) <<
      glm::vec3( mSize,  mSize, -mSize) <<
      glm::vec3( mSize, -mSize, -mSize) <<

      glm::vec3(-mSize, -mSize,  mSize) <<
      glm::vec3(-mSize,  mSize,  mSize) <<
      glm::vec3( mSize,  mSize,  mSize) <<
      glm::vec3( mSize,  mSize,  mSize) <<
      glm::vec3( mSize, -mSize,  mSize) <<
      glm::vec3(-mSize, -mSize,  mSize) <<

      glm::vec3(-mSize,  mSize, -mSize) <<
      glm::vec3( mSize,  mSize, -mSize) <<
      glm::vec3( mSize,  mSize,  mSize) <<
      glm::vec3( mSize,  mSize,  mSize) <<
      glm::vec3(-mSize,  mSize,  mSize) <<
      glm::vec3(-mSize,  mSize, -mSize) <<

      glm::vec3(-mSize, -mSize, -mSize) <<
      glm::vec3(-mSize, -mSize,  mSize) <<
      glm::vec3( mSize, -mSize, -mSize) <<
      glm::vec3( mSize, -mSize, -mSize) <<
      glm::vec3(-mSize, -mSize,  mSize) <<
      glm::vec3( mSize, -mSize,  mSize);

    mRenderData[BasicType].mTextureIndex <<
            glm::vec2(0.0f, 0.0f) <<
            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(0.0f, 0.0f) <<

            glm::vec2(0.0f, 0.0f) <<
            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(0.0f, 0.0f) <<

            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(0.0f, 0.0f) <<
            glm::vec2(1.0f, 0.0f) <<

            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(0.0f, 0.0f) <<
            glm::vec2(1.0f, 0.0f) <<

            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(0.0f, 0.0f) <<
            glm::vec2(0.0f, 1.0f) <<

            glm::vec2(0.0f, 1.0f) <<
            glm::vec2(1.0f, 1.0f) <<
            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(1.0f, 0.0f) <<
            glm::vec2(0.0f, 0.0f) <<
            glm::vec2(0.0f, 1.0f);

    for (int i = 0 ; i < mRenderData[BasicType].mVertex.size(); i++){
        mRenderData[BasicType].mNormal.push_back(glm::vec3(0));
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mIndices.size() ; i += 3) {

        int Index0 = mRenderData[BasicType].mIndices[i];
        int Index1 = mRenderData[BasicType].mIndices[i + 1];
        int Index2 = mRenderData[BasicType].mIndices[i + 2];

        glm::vec3 v1 = mRenderData[BasicType].mVertex[Index1] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 v2 = mRenderData[BasicType].mVertex[Index2] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 Normal = glm::cross(v1,v2);

        glm::normalize(Normal);

        mRenderData[BasicType].mNormal[Index0] += Normal;
        mRenderData[BasicType].mNormal[Index1] += Normal;
        mRenderData[BasicType].mNormal[Index2] += Normal;
    }

    QString fileName = QCoreApplication::applicationDirPath ()  + "/res/Space/Space1.bmp";

    mFaces.push_back(fileName);
    mFaces.push_back(fileName);
    mFaces.push_back(fileName);
    mFaces.push_back(fileName);
    mFaces.push_back(fileName);
    mFaces.push_back(fileName);

    // Setup skybox VAO

    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mColorBuffer.create();
    mRenderData[BasicType].mColorBuffer.bind();
    glEnableVertexAttribArray(0);
    glm::vec4 mColor = glm::vec4(1.0f, 0.0f, 1.0f, 1.0f);
    mRenderData[BasicType].mColorBuffer.setData(sizeof(glm::vec4), &mColor);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 1);

    mRenderData[BasicType].mTextureBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mTextureBuffer.create();
    mRenderData[BasicType].mTextureBuffer.bind();
    glEnableVertexAttribArray(2);
    mRenderData[BasicType].mTextureBuffer.setData(mRenderData[BasicType].mTextureIndex.size() * sizeof(glm::vec2), &mRenderData[BasicType].mTextureIndex.front());
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(3);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mNormal.size() * sizeof(glm::vec3), &mRenderData[BasicType].mNormal.front());
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    loadCubemap();

    mRenderData[BasicType].mVAO.unbind();

    MainRenderModel = SingleMainRenderModel::Instance();



}

void CubeMap::paint(){

    mRenderData[BasicType].mVAO.bind();
    glActiveTexture(GL_TEXTURE0);
    mRenderData[BasicType].mTexture.bind();

    glDrawArrays(GL_TRIANGLES, 0, 36);

    mRenderData[BasicType].mVAO.unbind();

}

void CubeMap::loadCubemap()
{
    QImage image(mFaces[0]);

    mRenderData[BasicType].mTexture.create(Texture::Texture_2D);
    mRenderData[BasicType].mTexture.bind();
    mRenderData[BasicType].mTexture.setImage2D(image);
    mRenderData[BasicType].mTexture.generateMipmaps();
    mRenderData[BasicType].mTexture.setWrap(Texture::Repeat, Texture::Repeat);
    mRenderData[BasicType].mTexture.setFilter(Texture::Linear, Texture::LinearMipMapNearest);

////      color = texture(skybox, TexCoords);

//    mRenderData[BasicType].mTexture.create(Texture::Texture_CubeMap);
//    mRenderData[BasicType].mTexture.bind();

//    QImage image(mFaces[0]);

//    mRenderData[BasicType].mTexture.setCubeMap(Texture::PositiveX,
//                                               Texture::RGB,
//                                               image.width(),
//                                               image.height(),
//                                               InputData::BGRA,
//                                               InputData::dtUnsignedByte,
//                                               image.bits(),
//                                               0,
//                                               0);

//    image.load(mFaces[1]);

//    mRenderData[BasicType].mTexture.setCubeMap(Texture::NegativeX,
//                                               Texture::RGB,
//                                               image.width(),
//                                               image.height(),
//                                               InputData::BGRA,
//                                               InputData::dtUnsignedByte,
//                                               image.bits(),
//                                               0,
//                                               0);

//    image.load(mFaces[2]);

//    mRenderData[BasicType].mTexture.setCubeMap(Texture::PositiveY,
//                                               Texture::RGB,
//                                               image.width(),
//                                               image.height(),
//                                               InputData::BGRA,
//                                               InputData::dtUnsignedByte,
//                                               image.bits(),
//                                               0,
//                                               0);

//    image.load(mFaces[3]);


//    mRenderData[BasicType].mTexture.setCubeMap(Texture::NegativeY,
//                                               Texture::RGB,
//                                               image.width(),
//                                               image.height(),
//                                               InputData::BGRA,
//                                               InputData::dtUnsignedByte,
//                                               image.bits(),
//                                               0,
//                                               0);

//    image.load(mFaces[4]);


//    mRenderData[BasicType].mTexture.setCubeMap(Texture::PositiveZ,
//                                               Texture::RGB,
//                                               image.width(),
//                                               image.height(),
//                                               InputData::BGRA,
//                                               InputData::dtUnsignedByte,
//                                               image.bits(),
//                                               0,
//                                               0);

//    image.load(mFaces[5]);


//    mRenderData[BasicType].mTexture.setCubeMap(Texture::NegativeZ,
//                                               Texture::RGB,
//                                               image.width(),
//                                               image.height(),
//                                               InputData::BGRA,
//                                               InputData::dtUnsignedByte,
//                                               image.bits(),
//                                               0,
//                                               0);


//    mRenderData[BasicType].mTexture.generateMipmaps();

//    mRenderData[BasicType].mTexture.setWrap(Texture::ClampToEdge, Texture::ClampToEdge, Texture::ClampToEdge);
//    mRenderData[BasicType].mTexture.setFilter(Texture::Linear, Texture::Linear);

////    mRenderData[BasicType].mTexture.generateMipmaps();
////    mRenderData[BasicType].mTexture.setWrap(Texture::Repeat, Texture::Repeat);
////    mRenderData[BasicType].mTexture.setFilter(Texture::Linear, Texture::LinearMipMapNearest);

//    mRenderData[BasicType].mTexture.unbind();

}

void CubeMap::clear(){

    mFaces.clear();

    for(QMap<RenderObjectType, RenderData >::Iterator it = mRenderData.end() - 1; it != mRenderData.begin() - 1; it--){
            it.value().clear();
    }
}
