#include "include/Lamp.h"
#include "MainRenderModel.h"
#include <MainSettingsClass.h>

Lamp::Lamp():
    lightPos(1.2f, 1.0f, 2.0f)
{
    SettingsClass = mainSettingsClass::Instance();

}

void Lamp::init(){

    mCamera = Camera::Instance();

    std::vector <float> materialAmbient ;
    materialAmbient.push_back(1.0f) ; materialAmbient.push_back(0.5f) ; materialAmbient.push_back(0.31f);
    std::vector <float> materialDiffuse;
    materialDiffuse.push_back( 1.0f) ; materialDiffuse.push_back(0.5f); materialDiffuse.push_back(0.33f);
    std::vector <float> materialSpecular;
    materialSpecular.push_back( 0.5f ); materialSpecular.push_back( 0.5f ) ; materialSpecular.push_back( 0.5f );
    std::vector <float> materialShine;
    materialShine.push_back( 64.0f);

    MainRenderModel->mShaders[LightShader].bind();
    MainRenderModel->mShaders[LightShader].setUniformArrayf("material.specular", 3, &materialSpecular);
    MainRenderModel->mShaders[LightShader].setUniformArrayf("material.shininess", 1, &materialShine);

    std::vector <float> lightAmbient ;
    lightAmbient.push_back(0.8f) ; lightAmbient.push_back(0.8f) ; lightAmbient.push_back(0.8f);
    std::vector <float> lightDiffuse;
    lightDiffuse.push_back(0.9f) ; lightDiffuse.push_back(0.9f); lightDiffuse.push_back(0.9f);
    std::vector <float> lightSpecular;
    lightSpecular.push_back( 0.3f ); lightSpecular.push_back(0.3f) ; lightSpecular.push_back(0.3f);
    std::vector <float> lightDir;
    lightDir.push_back( 0.3f ); lightDir.push_back(-1.3f) ; lightDir.push_back(0.3f);

    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.ambient", 3, &lightAmbient);
    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.diffuse", 3, &lightDiffuse);
    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.specular", 3, &lightSpecular);
//    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.direction", 3, &lightDir);


    //depending on distance

    materialShine[0] = 1.0f;
    MainRenderModel->mShaders[LightShader].setUniformArrayf( "light.constant", 1, &materialShine);
    materialShine[0] = 0.000000003;
    MainRenderModel->mShaders[LightShader].setUniformArrayf( "light.linear", 1, &materialShine);
    materialShine[0] = 0.00000000001;
    MainRenderModel->mShaders[LightShader].setUniformArrayf( "light.quadratic", 1, &materialShine);


    MainRenderModel->mShaders[LightShader].setUniformValue("material.diffuse", 0.5f);
    MainRenderModel->mShaders[LightShader].unbind();

    GLfloat vertices[] = {
            -0.5f, -0.5f, -0.5f,
             0.5f, -0.5f, -0.5f,
             0.5f,  0.5f, -0.5f,
             0.5f,  0.5f, -0.5f,
            -0.5f,  0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,

            -0.5f, -0.5f,  0.5f,
             0.5f, -0.5f,  0.5f,
             0.5f,  0.5f,  0.5f,
             0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f,  0.5f,
            -0.5f, -0.5f,  0.5f,

            -0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f, -0.5f,
            -0.5f, -0.5f,  0.5f,
            -0.5f,  0.5f,  0.5f,

             0.5f,  0.5f,  0.5f,
             0.5f,  0.5f, -0.5f,
             0.5f, -0.5f, -0.5f,
             0.5f, -0.5f, -0.5f,
             0.5f, -0.5f,  0.5f,
             0.5f,  0.5f,  0.5f,

            -0.5f, -0.5f, -0.5f,
             0.5f, -0.5f, -0.5f,
             0.5f, -0.5f,  0.5f,
             0.5f, -0.5f,  0.5f,
            -0.5f, -0.5f,  0.5f,
            -0.5f, -0.5f, -0.5f,

            -0.5f,  0.5f, -0.5f,
             0.5f,  0.5f, -0.5f,
             0.5f,  0.5f,  0.5f,
             0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f,  0.5f,
            -0.5f,  0.5f, -0.5f
        };




    // Then, we set the light's VAO (VBO stays the same. After all, the vertices are the same for the light object (also a 3D cube))

    glGenVertexArrays(1, &lightVAO);
    glBindVertexArray(lightVAO);

    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // We only need to bind to the VBO (to link it with glVertexAttribPointer), no need to fill it; the VBO's data already contains all we need.
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Set the vertex attributes (only position data for the lamp))
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

}

void Lamp::paint(){

    MainRenderModel->mShaders[LampShader].bind();

    glm::mat4 model = glm::mat4();

    model = glm::translate(model,  glm::vec3( mCamera->getEye().x*0.9,  mCamera->getEye().y*0.9,  mCamera->getEye().z*0.9 ));

    model = glm::scale(model, glm::vec3(400.2f)); // Make it a smaller cube

    MainRenderModel->mShaders[LampShader].setUniformMatrix("model", model);
    MainRenderModel->mShaders[LampShader].setUniformMatrix("view", MainRenderModel->mViewMatrix);
    MainRenderModel->mShaders[LampShader].setUniformMatrix("projection", MainRenderModel->mProjectionMatrix);


    glBindVertexArray(lightVAO);
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glBindVertexArray(0);


    //    glm::vec3 lightColor;
    //    lightColor.x = sin(QDateTime::currentDateTime().time().second() * 2.0f);
    //    lightColor.y = sin(QDateTime::currentDateTime().time().second() * 0.7f);
    //    lightColor.z = sin(QDateTime::currentDateTime().time().second() * 1.3f);

    //    glm::vec3 diffuseColor = lightColor   * glm::vec3(0.9f); // Decrease the influence
    //    glm::vec3 ambientColor = diffuseColor * glm::vec3(0.77f); // Low influence

    //    MainRenderModel->mShaders[LightShader].setUniformArray3f("light.ambient", ambientColor);
    //    MainRenderModel->mShaders[LightShader].setUniformArray3f("light.diffuse", diffuseColor);

}

void Lamp::update(){

    glm::vec3 SunPos = SettingsClass->getLight()->getPosition();
    float koef = SettingsClass->getLight()->getDistKoef();

    SunPos = glm::vec3(SunPos.x * koef, SunPos.y * koef, SunPos.z * koef);


    std::vector <float> materialShine;

    materialShine.push_back( SettingsClass->getLight()->getMaterialShine());

    MainRenderModel->mShaders[LightShader].bind();
    MainRenderModel->mShaders[LightShader].setUniformArray3f("light.position", SunPos);
    MainRenderModel->mShaders[LightShader].setUniformArrayf("material.shininess", 1, &materialShine);

    float val = SettingsClass->getLight()->getLightAmbient();
    std::vector <float> lightAmbient ;
    lightAmbient.push_back(val) ; lightAmbient.push_back(val) ; lightAmbient.push_back(val);
    std::vector <float> lightDiffuse;
    val = SettingsClass->getLight()->getLightDiffuse();
    lightDiffuse.push_back(val) ; lightDiffuse.push_back(val); lightDiffuse.push_back(val);
    std::vector <float> lightSpecular;
    val = SettingsClass->getLight()->getLightSpecular();
    lightSpecular.push_back( val); lightSpecular.push_back(val) ; lightSpecular.push_back(val);
    std::vector <float> lightDir;
    lightDir.push_back(SettingsClass->getLight()->getLightDir().x);
    lightDir.push_back(SettingsClass->getLight()->getLightDir().y);
    lightDir.push_back(SettingsClass->getLight()->getLightDir().z);

    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.ambient", 3, &lightAmbient);
    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.diffuse", 3, &lightDiffuse);
    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.specular", 3, &lightSpecular);
//    MainRenderModel->mShaders[LightShader].setUniformArrayf("light.direction", 3, &lightDir);


    //depending on distance

    materialShine[0] = SettingsClass->getLight()->getLightShineConstant();
    MainRenderModel->mShaders[LightShader].setUniformArrayf( "light.constant", 1, &materialShine);
    materialShine[0] = SettingsClass->getLight()->getLightShineLinear();
    MainRenderModel->mShaders[LightShader].setUniformArrayf( "light.linear", 1, &materialShine);
    materialShine[0] = SettingsClass->getLight()->getLightShineQuadratic();
    MainRenderModel->mShaders[LightShader].setUniformArrayf( "light.quadratic", 1, &materialShine);


    MainRenderModel->mShaders[LightShader].setUniformValue("material.diffuse", SettingsClass->getLight()->getMaterialDiffuse());
    MainRenderModel->mShaders[LightShader].unbind();
}

void Lamp::setLightPos(glm::vec3 pos){
    lightPos = pos;
}
