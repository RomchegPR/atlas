#define _USE_MATH_DEFINES

#include "include/Model/Model.h"
#include <glm/gtc/type_ptr.hpp>
#include <QDebug>
#include "MainRenderModel.h"
//#include <assimp/Importer.hpp>
//#include <assimp/postprocess.h>
//#include <assimp/scene.h>
//#include <assimp/postprocess.h>
using namespace std;

void Model::init(GLchar* path){
    MainRenderModel = SingleMainRenderModel::Instance();

    this->loadModel(path);

    mModelMatrix = glm::translate(mModelMatrix, glm::vec3(80000.0f, 0.0f, 0.0f)); // Translate it down a bit so it's at the center of the scene
    mModelMatrix = glm::scale(mModelMatrix, glm::vec3(20.2f, 20.2f, 20.2f));	// It's a bit too big for our scene, so scale it down
    mModelMatrix = glm::rotate(mModelMatrix, float(-M_PI/2), glm::vec3(0,0,1));	// It's a bit too big for our scene, so scale it down

}

// Draws the model, and thus all its meshes
void Model::Draw()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    MainRenderModel->mShaders[ModelsShader].bind();

    MainRenderModel->mShaders[ModelsShader].setUniformMatrix("projection", MainRenderModel->mProjectionMatrix);
    MainRenderModel->mShaders[ModelsShader].setUniformMatrix("view", MainRenderModel->mViewMatrix);
    MainRenderModel->mShaders[ModelsShader].setUniformMatrix("model", mModelMatrix);

    for(GLuint i = 0; i < this->meshes.size(); i++)
        this->meshes[i].paint();

    MainRenderModel->mShaders[ModelsShader].unbind();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DEPTH_TEST);
}

void Model::loadModel(string path)
{
    // Read file via ASSIMP
//    Assimp::Importer importer;
//    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
//    // Check for errors
//    if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
//    {
//        cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
//        return;
//    }
//    // Retrieve the directory path of the filepath
//    this->directory = path.substr(0, path.find_last_of('/'));

//    // Process ASSIMP's root node recursively
//    this->processNode(scene->mRootNode, scene);
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
//void Model::processNode(aiNode* node, const aiScene* scene)
//{
//    // Process each mesh located at the current node
//    for(GLuint i = 0; i < node->mNumMeshes; i++)
//    {
//        // The node object only contains indices to index the actual objects in the scene.
//        // The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
//        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
//        this->meshes.push_back(this->processMesh(mesh, scene));
//    }
//    // After we've processed all of the meshes (if any) we then recursively process each of the children nodes
//    for(GLuint i = 0; i < node->mNumChildren; i++)
//    {
//        this->processNode(node->mChildren[i], scene);
//    }

//}

//Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
//{
//    // Data to fill
//    vector<Vertex> vertices;
//    vector<GLuint> indices;
//    vector<TextureData> textures;

//    // Walk through each of the mesh's vertices
//    for(GLuint i = 0; i < mesh->mNumVertices; i++)
//    {
//        Vertex vertex;
//        glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
//        // Positions
//        vector.x = mesh->mVertices[i].x;
//        vector.y = mesh->mVertices[i].y;
//        vector.z = mesh->mVertices[i].z;
//        vertex.Position = vector;
//        // Normals
//        vector.x = mesh->mNormals[i].x;
//        vector.y = mesh->mNormals[i].y;
//        vector.z = mesh->mNormals[i].z;
//        vertex.Normal = vector;
//        // Texture Coordinates
//        if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
//        {
//            glm::vec2 vec;
//            // A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
//            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
//            vec.x = mesh->mTextureCoords[0][i].x;
//            vec.y = mesh->mTextureCoords[0][i].y;
//            vertex.TexCoords = vec;
//        }
//        else
//            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
//        vertices.push_back(vertex);
//    }
//    // Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
//    for(GLuint i = 0; i < mesh->mNumFaces; i++)
//    {
//        aiFace face = mesh->mFaces[i];
//        // Retrieve all indices of the face and store them in the indices vector
//        for(GLuint j = 0; j < face.mNumIndices; j++)
//            indices.push_back(face.mIndices[j]);
//    }
//    // Process materials
//    if(mesh->mMaterialIndex >= 0)
//    {
//        aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
//        // We assume a convention for sampler names in the shaders. Each diffuse texture should be named
//        // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
//        // Same applies to other texture as the following list summarizes:
//        // Diffuse: texture_diffuseN
//        // Specular: texture_specularN
//        // Normal: texture_normalN

//        // 1. Diffuse maps
//        vector<TextureData> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
//        textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
//        // 2. Specular maps
//        vector<TextureData> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
//        textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
//    }

//    // Return a mesh object created from the extracted mesh data
//    return Mesh(vertices, indices, textures);
//}

//vector<TextureData> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName)
//{
//    vector<TextureData> textures;
//    for(GLuint i = 0; i < mat->GetTextureCount(type); i++)
//    {
//        aiString str;
//        mat->GetTexture(type, i, &str);
//        // Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
//        GLboolean skip = false;
//        for(GLuint j = 0; j < textures_loaded.size(); j++)
//        {
////            qDebug() << textures_loaded[j].path << str.C_Str();
//            if(textures_loaded[j].path == str.C_Str())
//            {
////                qDebug() << textures_loaded[j].path;
//                textures.push_back(textures_loaded[j]);
//                skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
//                break;
//            }
//        }

//        if(!skip)
//        {   // If texture hasn't been loaded already, load it
//            qDebug() << str.C_Str();
//            TextureData texture;
//            texture.id = TextureFromFile(str.C_Str(), this->directory);
//            texture.type = typeName;
//            texture.path = str.C_Str();
//            textures.push_back(texture);
//            this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
//        }
//    }
//    return textures;
//}

GLint Model::TextureFromFile(const char* path, string directory){

         //Generate texture ID and load texture data
        string filename = string(path);
        QString LowFileName = filename.c_str();
//        LowFileName = LowFileName.toLower();
//        filename = directory + '/' + LowFileName.toStdString();
        filename = directory + '/' + filename;
        GLuint textureID;
        glGenTextures(1, &textureID);

        QString QfileName = filename.c_str();
        QImage image(QfileName);

        if (image.width() == 0) qDebug() << "FUCK!" << QfileName;

        // Assign texture to ID
        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width(), image.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, image.bits());
        glGenerateMipmap(GL_TEXTURE_2D);

        // Parameters
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glBindTexture(GL_TEXTURE_2D, 0);

        return textureID;

}

void Model::rotateWithMouse(QMouseEvent &e, GLfloat &LastX, GLfloat &LastY){

    if(mPitch > 89.0f)
      mPitch =  89.0f;
    if(mPitch < -89.0f)
      mPitch = -89.0f;

    double xoffset = e.x() - LastX;
    double yoffset = LastY - e.y(); // Reversed since y-coordinates range from bottom to top

//    LastX = e.x();
//    LastY = e.y();

    GLfloat sensitivity = 0.00001f;

    mPitch += xoffset *  sensitivity;
    mYaw += yoffset * sensitivity;

    if (xoffset < 0)
    mModelMatrix = glm::rotate(mModelMatrix, mPitch, glm::vec3 (0,1,0));
    else
    mModelMatrix = glm::rotate(mModelMatrix, -mPitch, glm::vec3 (0,1,0));

    if (yoffset > 0)
    mModelMatrix = glm::rotate(mModelMatrix, mYaw, glm::vec3 (1,0,0));
    else
    mModelMatrix = glm::rotate(mModelMatrix, -mYaw, glm::vec3 (1,0,0));



}
