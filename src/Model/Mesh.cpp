#include "include/Model/Mesh.h"
#include "MainRenderModel.h"

Mesh::Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<TextureData> textures)
{
    this->vertices = vertices;
    this->indices = indices;
    this->textures = textures;

    // Now that we have all the required data, set the vertex buffers and its attribute pointers.
    this->init();
    MainRenderModel = SingleMainRenderModel::Instance();
}

void Mesh::init(){

    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mElementBuffer.create();
    mRenderData[BasicType].mElementBuffer.bind();
    mRenderData[BasicType].mElementBuffer.setData(this->indices.size() * sizeof(GLuint), &this->indices[0]);

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    mRenderData[BasicType].mVertexBuffer.setData(this->vertices.size() * sizeof(Vertex), &this->vertices[0]);

    // Set the vertex attribute pointers
    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));
    // Vertex Texture Coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

    mRenderData[BasicType].mVAO.unbind();
}

void Mesh::paint(){
    // Bind appropriate textures
    GLuint diffuseNr = 1;
    GLuint specularNr = 1;
    for(GLuint i = 0; i < this->textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i); // Active proper texture unit before binding
        // Retrieve texture number (the N in diffuse_textureN)
        stringstream ss;
        string number;
        string name = this->textures[i].type;
        if(name == "texture_diffuse")
            ss << diffuseNr++; // Transfer GLuint to stream
        else if(name == "texture_specular")
            ss << specularNr++; // Transfer GLuint to stream
        number = ss.str();
        string nameNumber = name + number;

        MainRenderModel->mShaders[ModelsShader].setUniformValue(nameNumber.c_str(), (int)i);
        // And finally bind the texture
        glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
    }

    // Also set each mesh's shininess property to a default value (if you want you could extend this to another mesh property and possibly change this value)
//    glUniform1f(glGetUniformLocation(shader.mShaderProgram[3], "material.shininess"), 16.0f);

    // Draw mesh


    mRenderData[BasicType].mVAO.bind();
    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
    mRenderData[BasicType].mVAO.unbind();


    // Always good practice to set everything back to defaults once configured.
    for (GLuint i = 0; i < this->textures.size(); i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}
