#include "include/TreeWidgetModel.h"

#include "include/AirPort/AirPort.h"
#include "include/Zones/FlyZone.h"
#include <QTextCodec>
#include "MainRenderModel.h"

#include <QPushButton>

TreeWidgetModel::TreeWidgetModel()
{
    mainRenderModel = SingleMainRenderModel::Instance();
}

void TreeWidgetModel::linkWidget(QTreeWidget *Widget){
    if (Widget != NULL)
    this->pWidget = Widget;
}

void TreeWidgetModel::addAirPort(AirPortType airPortType,QTreeWidget *pWidget ){

   QTreeWidgetItem* SaintPetersburgRPI = createChild("St. Petersburg", "RPI", "RPI");
   pWidget->insertTopLevelItem(0, SaintPetersburgRPI);
   pWidget->insertTopLevelItem(0, SaintPetersburgRPI);

    for(QMap<AirPortType, AirPort>::Iterator it = mainRenderModel->mAirPort.begin(); it != mainRenderModel->mAirPort.end(); it++){

        AirPort *CurAirport = &it.value();

         QTreeWidgetItem* AirportItem = createChild(CurAirport->getName(), "SVO", "NITA");

         QTreeWidgetItem* STARItem = createChild( "STAR", "", "НИТА");
         QTreeWidgetItem* SIDItem = createChild( "SID", "", "НИТА");
         QTreeWidgetItem* APRCHItem = createChild( "APRCH", "", "НИТА");

         if (it.key() == airPortType && airPortType == LED){
            addChild(SaintPetersburgRPI, AirportItem);
            addChild(AirportItem, STARItem);
            addChild(AirportItem, SIDItem);
            addChild(AirportItem, APRCHItem);
         } else if (it.key() != AIRWAYS && it.key() == airPortType){

//            addChild(MoscowRPI, AirportItem);

//            addChild(AirportItem, STARItem);
//            addChild(AirportItem, SIDItem);
//            addChild(AirportItem, APRCHItem);

         } else {
             continue;
         }

         for (int i = 0; i < CurAirport->mProcedures.size(); i++){

             QTreeWidgetItem* Procedure = createChild(CurAirport->mProcedures[i].getName(), "", "");

             switch (CurAirport->mProcedures[i].getType()){
                 case STAR : {
                     addChild(STARItem, Procedure);
                     break;
                 }
                 case DEPARTURE : {
                     addChild(SIDItem, Procedure);
                     break;
                 }
                 case APRCH: {
                     addChild(APRCHItem, Procedure);
                     break;
                 }

             }
         }
    }
}

void TreeWidgetModel::fillWithData(QTreeWidget *pWidget){

    //QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8")); //изменения
    qDebug() << 1;
    QTreeWidgetItem* MoscowRPI = createChild("Moscow", "RPI", "RPI");
    QTreeWidgetItem* SaintPetersburgRPI = createChild("St. Petersburg", "RPI", "RPI");
    QTreeWidgetItem* Russia = createChild("Russia", "", "");
    QTreeWidgetItem* AirWays = createChild("AIRWAYS", "", "");
    QTreeWidgetItem* RPIs = createChild("RPI", "", "");


    addChild(pWidget->invisibleRootItem(), MoscowRPI);
    addChild(pWidget->invisibleRootItem(), SaintPetersburgRPI);
    addChild(pWidget->invisibleRootItem(), Russia);

    pWidget->insertTopLevelItem(0, MoscowRPI);
    pWidget->insertTopLevelItem(0, Russia);
    pWidget->insertTopLevelItem(0, SaintPetersburgRPI);
    addChild(Russia, AirWays);
    addChild(Russia, RPIs);

    for(QMap<AirPortType, AirPort>::Iterator it = mainRenderModel->mAirPort.begin(); it != mainRenderModel->mAirPort.end(); it++){
         AirPort *CurAirport = &it.value();

         QTreeWidgetItem* AirportItem = createChild(CurAirport->getName(), "SVO", "NITA");

         QTreeWidgetItem* STARItem = createChild( "STAR", "", "НИТА");
         QTreeWidgetItem* SIDItem = createChild( "SID", "", "НИТА");
         QTreeWidgetItem* APRCHItem = createChild( "APRCH", "", "НИТА");

         if (it.key() == LED){
            addChild(SaintPetersburgRPI, AirportItem);
         }

         if (it.key() != AIRWAYS){
            addChild(MoscowRPI, AirportItem);

            addChild(AirportItem, STARItem);
            addChild(AirportItem, SIDItem);
            addChild(AirportItem, APRCHItem);
         }

         for (int i = 0; i < CurAirport->mProcedures.size(); i++){

             QTreeWidgetItem* Procedure = createChild(CurAirport->mProcedures[i].getName(), "", "");

             switch (CurAirport->mProcedures[i].getType()){
                 case STAR : {
                     addChild(STARItem, Procedure);
                     break;
                 }
                 case DEPARTURE : {
                     addChild(SIDItem, Procedure);
                     break;
                 }
                 case APRCH: {
                     addChild(APRCHItem, Procedure);
                     break;
                 }
                 case AIRWAY: {
                     addChild(AirWays, Procedure);
                     break;
                 }

             }
         }
    }

    QTreeWidgetItem* Zones = createChild( "Зоны", "", "НИТА");
    addChild(SaintPetersburgRPI, Zones);

    QTreeWidgetItem* FullRestrictedZones = createChild( "Запретные", "", "НИТА");
    addChild(Zones, FullRestrictedZones);

    QTreeWidgetItem* LimitationRestrictedZones = createChild( "Ограничение", "", "НИТА");
    addChild(Zones, LimitationRestrictedZones);


    QTreeWidgetItem* MoscowTMA = createChild("TMA", "Moscow", "TMA");

    addChild(MoscowRPI, MoscowTMA);

    for (int i = 0; i < mainRenderModel->mZoneBuilder.mZones.size(); i++)
    {
        QTreeWidgetItem* Zone = createChild(mainRenderModel->mZoneBuilder.mZones[i].mName, "", "");

        if (mainRenderModel->mZoneBuilder.mZones[i].getType() == PRIVATE){
            addChild(FullRestrictedZones, Zone);
        } else if (mainRenderModel->mZoneBuilder.mZones[i].getType() == RESPONSIBLE){

            QString name = mainRenderModel->mZoneBuilder.mZones[i].mName;

            if (name.contains("mtma") || name.contains("MFIR") || name.contains("MSEC")){
                addChild(MoscowTMA, Zone);
            } else {
                addChild(RPIs, Zone);
            }
        }
        {
            addChild(LimitationRestrictedZones, Zone);
        }

    }

}

void TreeWidgetModel::addChild(QTreeWidgetItem *parent, QTreeWidgetItem *child){
    parent->addChild(child);
}

QTreeWidgetItem* TreeWidgetModel::createChild(QString Name1, QString Name2, QString toolTip){

    QTreeWidgetItem *child = new QTreeWidgetItem();

    child->setText(0, Name1);
    child->setText(1, Name2);
    child->setToolTip(1, toolTip);
    child->setCheckState(0, Qt::Unchecked);
    child->setFlags(child->flags() | Qt::ItemIsSelectable);
    child->sortChildren(0, Qt::AscendingOrder);


    return child;
}

void TreeWidgetModel::itemChange(QTreeWidgetItem* item){
    Qt::CheckState state = item->checkState(0);

    for ( int i = 0; i < item->childCount(); i++ ) {
        QTreeWidgetItem* child = item->child(i);
            child->setCheckState(0, state);
    }
}
