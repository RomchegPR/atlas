#include "include/Barrier.h"
#include "include/Helper.h"
#include "include/GeoHelper.h"
#include <glm/gtx/rotate_vector.hpp>
#include "MainRenderModel.h"
#include <MainSettingsClass.h>

Barrier::Barrier()
{
    mColor = glm::vec4(1,0,0,1);
    mHeightRate = 10;
    mCubeSize = 500.0f;

    MainRenderModel = SingleMainRenderModel::Instance();
    SettingsClass = mainSettingsClass::Instance();

}

void Barrier::calc(){

    mRenderData[BasicType].mVertex.clear();
    mRenderData[LinesType].mVertex.clear();
    mRenderData[LinesType].mIndices.clear();
    mRenderData[BasicType].mNormal.clear();
    mHeightVec.clear();


    mHeightPoint =  Helper::CalcPointOnDistance(mPosition,
                                                glm::vec3(0,0,0),
                                                -mAltitude);


    mRenderData[LinesType].mVertex.push_back(mHeightPoint);
    mRenderData[LinesType].mVertex.push_back(mPosition);

    glm::vec3 normHeightVec = glm::normalize(mHeightPoint - mPosition);
    normHeightVec = glm::vec3(normHeightVec.x * 100, normHeightVec.y * 100, normHeightVec.z * 100);

    for (int i = 0; i < 36; i++){
        mHeightVec.push_back(normHeightVec);
    }

    mRenderData[LinesType].mIndices.push_back(0); mRenderData[LinesType].mIndices.push_back(1);


    mRenderData[BasicType].mVertex << glm::vec3( -mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize, -mCubeSize)
                                   << glm::vec3( -mCubeSize,  mCubeSize, -mCubeSize)
                                   << glm::vec3( -mCubeSize, -mCubeSize, -mCubeSize)

                                   << glm::vec3( -mCubeSize, -mCubeSize,  mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize,  mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize, -mCubeSize,  mCubeSize)

                                   << glm::vec3( -mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize,  mCubeSize, -mCubeSize)
                                   << glm::vec3( -mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3( -mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3( -mCubeSize, -mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize,  mCubeSize,  mCubeSize)

                                   << glm::vec3(  mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize,  mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize,  mCubeSize)

                                   << glm::vec3( -mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize,  mCubeSize)
                                   << glm::vec3(  mCubeSize, -mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize, -mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize, -mCubeSize, -mCubeSize)

                                   << glm::vec3( -mCubeSize,  mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize, -mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3(  mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize,  mCubeSize,  mCubeSize)
                                   << glm::vec3( -mCubeSize,  mCubeSize, -mCubeSize);

}

void Barrier::calcNormal(){
    for (int i = 0 ; i < mRenderData[BasicType].mVertex.size(); i++){
        mRenderData[BasicType].mNormal.push_back(glm::vec3(0));
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mVertex.size() ; i += 3) {

        int Index0 = i;
        int Index1 = i + 1;
        int Index2 = i + 2;

        glm::vec3 v1 = mRenderData[BasicType].mVertex[Index1] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 v2 = mRenderData[BasicType].mVertex[Index2] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 Normal = glm::cross(v1,v2);

        glm::normalize(Normal);

        mRenderData[BasicType].mNormal[Index0] += Normal;
        mRenderData[BasicType].mNormal[Index1] += Normal;
        mRenderData[BasicType].mNormal[Index2] += Normal;
    }
}


void Barrier::init(){

    calc();
    calcNormal();
    mRenderData[LinesType].mVAO.create();
    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mElementBuffer.create();
    mRenderData[LinesType].mElementBuffer.bind();
    mRenderData[LinesType].mElementBuffer.setData(mRenderData[LinesType].mIndices.size() * sizeof(GLuint), &mRenderData[LinesType].mIndices.front());

    mRenderData[LinesType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mVertexBuffer.create();
    mRenderData[LinesType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);


    mRenderData[LinesType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mColorBuffer.create();
    mRenderData[LinesType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    mRenderData[LinesType].mColorBuffer.setData(sizeof(glm::vec4), &mColor);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);



    mRenderData[LinesType].mVAO.unbind();

    glm::mat4 translate = glm::translate(glm::mat4(1.f), glm::vec3(mHeightPoint.x, mHeightPoint.y, mHeightPoint.z));


    for (int i = 0; i < mRenderData[BasicType].mVertex.size(); i++){
        mRenderData[BasicType].mVertex[i] =  glm::vec3(translate * glm::vec4(mRenderData[BasicType].mVertex[i], 1.0f));
    }

    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*) 0);

    mRenderData[BasicType].mHeightVecBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mHeightVecBuffer.create();
    mRenderData[BasicType].mHeightVecBuffer.bind();
    glEnableVertexAttribArray(5);
    mRenderData[BasicType].mHeightVecBuffer.setData(mHeightVec.size() * sizeof(glm::vec3), &mHeightVec.front());
    glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*) 0);

    mRenderData[BasicType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mColorBuffer.create();
    mRenderData[BasicType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    mRenderData[BasicType].mColorBuffer.setData(sizeof(glm::vec4), &mColor);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(3);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mNormal.size() * sizeof(glm::vec3), &mRenderData[BasicType].mNormal.front());
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVAO.unbind();
}

void Barrier::paint(){

//    updateColor();

    MainRenderModel->mShaders[LightShader].setUniformValue("useHeightMap", false);
    MainRenderModel->mShaders[LightShader].setUniformValue("useHeightVec", true);
    MainRenderModel->mShaders[LightShader].setUniformValue("heightRate", mHeightRate);

    mRenderData[BasicType].mVAO.bind();
    glDrawArrays(GL_TRIANGLES, 0, 36);
    mRenderData[BasicType].mVAO.unbind();

    mRenderData[LinesType].mVAO.bind();
    glDrawElements(GL_LINES, mRenderData[LinesType].mIndices.size(), GL_UNSIGNED_INT, 0);
    mRenderData[LinesType].mVAO.unbind();
    MainRenderModel->mShaders[LightShader].setUniformValue("useHeightVec", false);

}

void Barrier::update(){

    calc();

    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mElementBuffer.bind();
    mRenderData[LinesType].mElementBuffer.clear();
    mRenderData[LinesType].mElementBuffer.setData(mRenderData[LinesType].mIndices.size() * sizeof(GLuint), &mRenderData[LinesType].mIndices.front());

    mRenderData[LinesType].mVertexBuffer.bind();
    mRenderData[LinesType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mVAO.unbind();

    glm::mat4 translate = glm::translate(glm::mat4(1.f), glm::vec3(mHeightPoint.x, mHeightPoint.y, mHeightPoint.z));

    for (int i = 0; i < mRenderData[BasicType].mVertex.size(); i++){
        mRenderData[BasicType].mVertex[i] =  glm::vec3(translate * glm::vec4(mRenderData[BasicType].mVertex[i], 1.0f));
    }

    mRenderData[BasicType].mVAO.bind();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*) 0);

    mRenderData[BasicType].mVAO.unbind();
}

void Barrier::updateColor(){

    mRenderData[BasicType].mVAO.bind();
    mRenderData[BasicType].mColorBuffer.bind();
    mRenderData[BasicType].mColorBuffer.clear();
    glEnableVertexAttribArray(1);
    glm::vec4 color = glm::vec4(SettingsClass->getBarrierColor(), SettingsClass->getBarrierTrancparency());
    mRenderData[BasicType].mColorBuffer.setData(sizeof(glm::vec4), &color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);
    mRenderData[BasicType].mVAO.unbind();


    mRenderData[LinesType].mVAO.bind();
    mRenderData[LinesType].mColorBuffer.bind();
    mRenderData[LinesType].mColorBuffer.clear();
    glEnableVertexAttribArray(1);
    mRenderData[LinesType].mColorBuffer.setData(sizeof(glm::vec4), &color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);
    mRenderData[LinesType].mVAO.unbind();
}

void Barrier::clear(){

}

double Barrier::getAltitude() const
{
    return mAltitude;
}

void Barrier::setAltitude(const double value)
{
    mAltitude = value;
}

glm::vec3 Barrier::getPosition() const
{
    return mPosition;
}

void Barrier::setPosition(const glm::vec3 &value)
{
    mPosition = value;
}

void Barrier::setPosition( const Waypoint &value )
{

    QString lat = QString::number(value.lat);
    QString lon = QString::number(value.lon);

    glm::vec3 geoCoords = GeoHelper::geo2cartesian(lat, lon, false);

    mPosition = glm::vec3(-geoCoords.x, geoCoords.z, geoCoords.y);

}

void Barrier::setPositionGeo( glm::vec3 pos )
{
    glm::vec3 geoCoords = GeoHelper::geo2cartesianGL(pos, true);
    mPosition = geoCoords;

}

glm::vec3 Barrier::getCenter(){
    return mHeightPoint;
}

QString Barrier::getName() const
{
    return mName;
}

void Barrier::setName(const QString &value)
{
    mName = value;
}

glm::vec4 Barrier::getLightColor() const
{
    return mLightColor;
}

void Barrier::setLightColor(const glm::vec4 &value)
{
    mLightColor = value;
}

glm::vec4 Barrier::getColor() const
{
    return mColor;
}

void Barrier::setColor(const glm::vec4 &value)
{
    mColor = value;
}

float Barrier::getHeightRate() const
{
    return mHeightRate * 10;
}

void Barrier::setHeightRate(float value)
{
    mHeightRate = value;

//    mHeightPoint =  Helper::CalcPointOnDistance(mPosition,
//                                                glm::vec3(0,0,0),
//                                                -mHeightRate*mAltitude);

//    mRenderData[LinesType].mVertex.clear();
//    mRenderData[BasicType].mVertex.clear();
//    mRenderData[LinesType].mIndices.clear();

//    init();
}
