#include "include/ViewOption.h"
#include "include/ColorFrame.h"
#include "ui_ViewOption.h"
#include "include/Headers.h"
#include <QColorDialog>
#include <QTextCodec>

#include "MainSettingsClass.h"
#include "MainRenderModel.h"
#include "Camera.h"

ViewOption::ViewOption(Ui::MainWindow* mainWindow) :
    ui(new Ui::ViewOption)
{
    mainRenderModel = SingleMainRenderModel::Instance();
    mCamera = Camera::Instance();
    SettingsClass = mainSettingsClass::Instance();
    setDefault();

}

void ViewOption::setDefault(){
    glm::vec3 rZoneColor = SettingsClass->getZonesColors(RESTRICTED);
    glm::vec3 pZoneColor = SettingsClass->getZonesColors(PRIVATE);
    glm::vec3 sZoneColor = SettingsClass->getZonesColors(RESPONSIBLE);

    glm::vec4 cAPRCH = glm::vec4(SettingsClass->getProceduresColors(APRCH), SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL));
    glm::vec4 cSID = glm::vec4(SettingsClass->getProceduresColors(DEPARTURE), SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL));
    glm::vec4 cSTAR = glm::vec4(SettingsClass->getProceduresColors(STAR), SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL));

    if (mainRenderModel->mAirPort.size() > 0) {

        ui->setupUi(this);
        ui->frame->setObjectName("frame1");
        ui->frame->setColor(SettingsClass->getProceduresColors(STAR));
        ui->frame_2->setObjectName("frame2");
        ui->frame_2->setColor(SettingsClass->getProceduresColors(APRCH));
        ui->frame_3->setObjectName("frame3");
        ui->frame_3->setColor(SettingsClass->getProceduresColors(DEPARTURE));
        ui->frame_4->setObjectName("frame4");
        ui->frame_4->setColor(SettingsClass->getZonesColors(RESTRICTED));
        ui->frame_5->setObjectName("frame5");
        ui->frame_5->setColor(SettingsClass->getZonesColors(PRIVATE));
        ui->frame_6->setObjectName("frame6");
        ui->frame_6->setColor(SettingsClass->getZonesColors(RESPONSIBLE));
        ui->FrameObjectsChoosedColor->setObjectName("ChoosedColorProcedureFrame");
        ui->FrameObjectsChoosedColor->setColor(SettingsClass->getCheckedObjectColor());
        ui->BarrierColorFrame->setObjectName("BarrierColorFrame");
        ui->BarrierColorFrame->setColor(SettingsClass->getBarrierColor());

        ui->ColorLegendFrame->setObjectName("ColorLegendFrame");
        ui->ColorLegendFrame->setColor(SettingsClass->getLegendFrameColor());
        ui->ColorLegendText->setObjectName("ColorLegendText");
        ui->ColorLegendText->setColor(SettingsClass->getLegendTextColor());

    }

    /*sid, star, aprch */
    ui->frame->setType("STAR");
    ui->frame_2->setType("APRCH");
    ui->frame_3->setType("SID");
    ui->frame_4->setType("UUP");
    ui->frame_5->setType("UUR");
    ui->frame_6->setType("RID");
    ui->FrameObjectsChoosedColor->setType("ChoosedColorProcedureFrameT");
    ui->BarrierColorFrame->setType("BarrierColorFrame");
    ui->ColorLegendFrame->setType("ColorLegendFrame");
    ui->ColorLegendText->setType("ColorLegendText");


    QString color = "#frame1{ background-color: rgb(" + QString::number(cSTAR.x * 255.0f) + "," + QString::number(cSTAR.y * 255.0f) + "," + QString::number(cSTAR.z * 255.0f) + "); }";
    ui->frame->setStyleSheet(color);
    color = "#frame2 { background-color: rgb(" + QString::number(cAPRCH.x * 255.0f) + "," + QString::number(cAPRCH.y * 255.0f) + "," + QString::number(cAPRCH.z * 255.0f) + "); }";
    ui->frame_2->setStyleSheet(color);
    color = "#frame3 { background-color: rgb(" + QString::number(cSID.x * 255.0f) + "," + QString::number(cSID.y * 255.0f) + "," + QString::number(cSID.z * 255.0f) + "); }";
    ui->frame_3->setStyleSheet(color);
    color = "#frame4 { background-color: rgb(" + QString::number(rZoneColor.x * 255.0f) + "," + QString::number(rZoneColor.y * 255.0f) + "," + QString::number(rZoneColor.z * 255.0f) + "); }";
    ui->frame_4->setStyleSheet(color);
    color = "#frame5 { background-color: rgb(" + QString::number(pZoneColor.x * 255.0f) + "," + QString::number(pZoneColor.y * 255.0f) + "," + QString::number(pZoneColor.z * 255.0f) + "); }";
    ui->frame_5->setStyleSheet(color);
    color = "#frame6 { background-color: rgb(" + QString::number(sZoneColor.x * 255.0f) + "," + QString::number(sZoneColor.y * 255.0f) + "," + QString::number(sZoneColor.z * 255.0f) + "); }";
    ui->frame_6->setStyleSheet(color);
    color = "#ChoosedColorProcedureFrame { background-color: rgb(" + QString::number(SettingsClass->getCheckedObjectColor().x * 255.0f) + "," + QString::number(SettingsClass->getCheckedObjectColor().y * 255.0f) + "," + QString::number(SettingsClass->getCheckedObjectColor().z * 255.0f) + "); }";
    ui->FrameObjectsChoosedColor->setStyleSheet(color);
    color = "#BarrierColorFrame { background-color: rgb(" + QString::number(SettingsClass->getBarrierColor().x * 255.0f) + "," + QString::number(SettingsClass->getBarrierColor().y * 255.0f) + "," + QString::number(SettingsClass->getBarrierColor().z * 255.0f) + "); }";
    ui->BarrierColorFrame->setStyleSheet(color);

    color = "#ColorLegendFrame { background-color: rgb(" + QString::number(SettingsClass->getLegendFrameColor().x* 255.0f) + "," + QString::number(SettingsClass->getLegendFrameColor().y * 255.0f) + "," + QString::number(SettingsClass->getLegendFrameColor().z * 255.0f) + "); }";
    ui->ColorLegendFrame->setStyleSheet(color);
    color = "#ColorLegendText { background-color: rgb(" + QString::number(SettingsClass->getLegendTextColor().x* 255.0f) + "," + QString::number(SettingsClass->getLegendTextColor().y * 255.0f) + "," + QString::number(SettingsClass->getLegendTextColor().z * 255.0f) + "); }";
    ui->ColorLegendText->setStyleSheet(color);

    isFilePathChanged = false;

    ui->SliderTrancparencyProceduresLines->setValue(SettingsClass->getObjectsTrancparency(TRANCPARENCY_LINE) * 100.0f);
    ui->SliderTrancparencyZonesLines->setValue(SettingsClass->getObjectsTrancparency(TRANCPARENCY_FRAME_ZONE) * 100.0f);
    ui->SliderTrancparencyProcedures->setValue(SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL) * 100.0f);
    ui->SliderTrancparencyZones->setValue(SettingsClass->getObjectsTrancparency(TRANCPARENCY_FILL_ZONE) * 100.0f);

    ui->SliderProcedureLineWidthBase->setValue(SettingsClass->getLinesWidth(LINE_BASE) * 20.0f);
    ui->SliderProcedureLineWidthTrajectory->setValue(SettingsClass->getLinesWidth(LINE_TRAJECTORY) * 20.0f);
    ui->SliderProcedureLineWidthRams->setValue(SettingsClass->getLinesWidth(LINE_RAM) * 20.0f);
    ui->SliderZoneLineWidthTrajectory->setValue(SettingsClass->getLinesWidth(LINE_ZONE_FRAME) * 20.0f);

    ui->SliderLightDistKoef->setValue((SettingsClass->getLight()->getDistKoef() - 1) * 100.0f);
    ui->SliderLightAmbient->setValue((SettingsClass->getLight()->getLightAmbient()) * 100.0f);
    ui->SliderLightDiffuse->setValue((SettingsClass->getLight()->getLightDiffuse()) * 100.0f);
    ui->SliderLightSpecular->setValue((SettingsClass->getLight()->getLightSpecular()) * 100.0f);
    ui->SliderMaterialShine->setValue((SettingsClass->getLight()->getMaterialShine()));

    ui->LightPositionXBox->setValue(SettingsClass->getLight()->getPosition().x);
    ui->LightPositionYBox->setValue(SettingsClass->getLight()->getPosition().y);
    ui->LightPositionZBox->setValue(SettingsClass->getLight()->getPosition().z);

    ui->LightDirXBox->setValue(SettingsClass->getLight()->getLightDir().x);
    ui->LightDirXBox->setValue(SettingsClass->getLight()->getLightDir().y);
    ui->LightDirXBox->setValue(SettingsClass->getLight()->getLightDir().z);

    ui->LightShineConstantBox->setValue(SettingsClass->getLight()->getLightShineConstant());
    ui->LightShineLinearBox->setValue(SettingsClass->getLight()->getLightShineLinear());
    ui->LightShineQuadraticBox->setValue(SettingsClass->getLight()->getLightShineQuadratic());
    ui->MaterialDiffuseBox->setValue(SettingsClass->getLight()->getMaterialDiffuse());

    ui->SliderBarrierTrancparency->setValue(SettingsClass->getBarrierTrancparency() * 100.0f);
    ui->SliderBarrierSize->setValue(SettingsClass->getBarrierSize());
    ui->SliderTextSize->setValue(SettingsClass->getLegendTextSize());
    ui->SliderLegendTrancparency->setValue(SettingsClass->getObjectsTrancparency(TRANCPARENCY_LEGEND) * 100.0f);

    ui->SpinBoxTextSize->setValue(ui->SliderTextSize->value());
    ui->spinBoxLegendTrancparency->setValue(ui->SliderLegendTrancparency->value());

    ui->spinBoxLegendTrancparency->setValue(ui->SliderLegendTrancparency->value());
    ui->spinBoxLightDistKoef->setValue(ui->SliderLightDistKoef->value());
    ui->doubleSpinBoxLightAmbient->setValue(ui->SliderLightAmbient->value());
    ui->doubleSpinBoxLightDiffuse->setValue(ui->SliderLightDiffuse->value());
    ui->doubleSpinBoxLightSpecular->setValue(ui->SliderLightSpecular->value());
    ui->doubleSpinBoxMaterialShine->setValue(ui->SliderMaterialShine->value());
    ui->spinBoxTrancparencyProcedures->setValue(ui->SliderTrancparencyProcedures->value());
    ui->spinBoxTrancparencyProceduresLines->setValue(ui->SliderTrancparencyProceduresLines->value());
    ui->spinBoxProcedureLineWidthBase->setValue(ui->SliderProcedureLineWidthBase->value());
    ui->spinBoxProcedureLineWidthTrajectory->setValue(ui->SliderProcedureLineWidthTrajectory->value());
    ui->spinBoxProcedureLineWidthRams->setValue(ui->SliderProcedureLineWidthRams->value());
    ui->spinBoxTrancparencyZones->setValue(ui->SliderTrancparencyZones->value());
    ui->spinBoxTrancparencyZonesLines->setValue(ui->SliderTrancparencyZonesLines->value());
    ui->spinBoxZoneLineWidthTrajectory->setValue(ui->SliderZoneLineWidthTrajectory->value());
    ui->spinBoxBarrierTrancparency->setValue(ui->SliderBarrierTrancparency->value());
    ui->spinBoxBarrierSize->setValue(ui->SliderBarrierSize->value());
    ui->filePathEarthTextureLine->setText(mainRenderModel->mTextureEarthFileName);
}

ViewOption::~ViewOption()
{
    delete ui;
}

void ViewOption::on_EarthTexture_clicked()
{
    QString str = QFileDialog::getOpenFileName(0, "Open Dialog", "");
    ui->filePathEarthTextureLine->setText(str);
    mainRenderModel->mTextureEarthFileName = str;
    isFilePathChanged = true;
}

void ViewOption::on_HeighmapTexture_clicked()
{
    QString str = QFileDialog::getOpenFileName(0, "Open Dialog", "");
    ui->filePathHeightMapLine->setText(str);
    mainRenderModel->mTextureHeightMapFileName = str;
    isFilePathChanged = true;
}

void ViewOption::on_useSettingsButton_clicked()
{

    SettingsClass->setProceduresColors(STAR,ui->frame->getColor() );
    SettingsClass->setProceduresColors(APRCH,ui->frame_2->getColor() );
    SettingsClass->setProceduresColors(DEPARTURE,ui->frame_3->getColor() );

    SettingsClass->setZonesColors(RESTRICTED,ui->frame_4->getColor() );
    SettingsClass->setZonesColors(PRIVATE,ui->frame_5->getColor() );
    SettingsClass->setZonesColors(RESPONSIBLE,ui->frame_6->getColor() );

    SettingsClass->setCheckedObjectColor(ui->FrameObjectsChoosedColor->getColor() );

    SettingsClass->setBarrierColor(ui->BarrierColorFrame->getColor());

//    qDebug() << ui->ColorLegendFrame->getColor().x << ui->ColorLegendFrame->getColor().y << ui->ColorLegendFrame->getColor().z;
    SettingsClass->setLegendFrameColor(ui->ColorLegendFrame->getColor());
    SettingsClass->setLegendTextColor(ui->ColorLegendText->getColor());


    if (isFilePathChanged) {
        mainRenderModel->mEarth.updateEarthTextures();
    }

    glm::vec3 position = glm::vec3(ui->LightPositionXBox->value(), ui->LightPositionYBox->value(), ui->LightPositionZBox->value());
    glm::vec3 direction = glm::vec3(ui->LightDirXBox->value(), ui->LightDirYBox->value(), ui->LightDirZBox->value());

    SettingsClass->getLight()->setPosition(position);
    SettingsClass->getLight()->setLightDir(direction);
    SettingsClass->getLight()->setLightShineConstant(ui->LightShineConstantBox->value());
    SettingsClass->getLight()->setLightShineLinear(ui->LightShineLinearBox->value());
    SettingsClass->getLight()->setLightShineQuadratic(ui->LightShineQuadraticBox->value());
    SettingsClass->getLight()->setMaterialDiffuse(ui->MaterialDiffuseBox->value());

    SettingsClass->setBarrierSize(ui->SliderBarrierSize->value());

}

void ViewOption::on_OkButton_clicked()
{
    this->close();
}

void ViewOption::on_SliderTrancparencyZones_valueChanged(int value)
{
    SettingsClass->setObjectsTrancparency(TRANCPARENCY_FILL_ZONE, value);
    ui->spinBoxTrancparencyZones->setValue(value);
}

void ViewOption::on_SliderTrancparencyProcedures_valueChanged(int value)
{
    SettingsClass->setObjectsTrancparency(TRANCPARENCY_HALL, value);
    ui->spinBoxTrancparencyProcedures->setValue(value);
}

void ViewOption::on_SliderTrancparencyProceduresLines_valueChanged(int value)
{
    SettingsClass->setObjectsTrancparency(TRANCPARENCY_LINE, value);
    ui->spinBoxTrancparencyProceduresLines->setValue(value);
}

void ViewOption::on_SliderTrancparencyZonesLines_valueChanged(int value)
{
    SettingsClass->setObjectsTrancparency(TRANCPARENCY_FRAME_ZONE, value);
    ui->spinBoxTrancparencyZonesLines->setValue(value);
}

void ViewOption::on_SliderProcedureLineWidthBase_valueChanged(int value)
{
    SettingsClass->setLinesWidth(LINE_BASE, value);
    ui->spinBoxProcedureLineWidthBase->setValue(value);
}

void ViewOption::on_SliderProcedureLineWidthTrajectory_valueChanged(int value)
{
    SettingsClass->setLinesWidth(LINE_TRAJECTORY, value);
    ui->spinBoxProcedureLineWidthTrajectory->setValue(value);
}

void ViewOption::on_SliderProcedureLineWidthRams_valueChanged(int value)
{
    SettingsClass->setLinesWidth(LINE_RAM, value);
    ui->spinBoxProcedureLineWidthRams->setValue(value);
}

void ViewOption::on_SliderZoneLineWidthTrajectory_valueChanged(int value)
{
    SettingsClass->setLinesWidth(LINE_ZONE_FRAME, value);
    ui->spinBoxZoneLineWidthTrajectory->setValue(value);
}

void ViewOption::on_SliderLightDistKoef_valueChanged(int value)
{
    SettingsClass->getLight()->setDistKoef(value/100.0f + 1.0f);
    ui->spinBoxLightDistKoef->setValue(value);
}

void ViewOption::on_SliderLightAmbient_valueChanged(int value)
{
    SettingsClass->getLight()->setLightAmbient(value/100.0f);
    ui->doubleSpinBoxLightAmbient->setValue(value);
}

void ViewOption::on_SliderLightDiffuse_valueChanged(int value)
{
    SettingsClass->getLight()->setLightDiffuse(value/100.0f);
    ui->doubleSpinBoxLightDiffuse->setValue(value);
}

void ViewOption::on_SliderLightSpecular_valueChanged(int value)
{
    SettingsClass->getLight()->setLightSpecular(value/100.0f);
    ui->doubleSpinBoxLightSpecular->setValue(value);
}

void ViewOption::on_SliderMaterialShine_valueChanged(int value)
{
    SettingsClass->getLight()->setMaterialShine(value);
    ui->doubleSpinBoxMaterialShine->setValue(value);
}

void ViewOption::on_SliderBarrierTrancparency_valueChanged(int value)
{
    SettingsClass->setBarrierTrancparency(value/100.0f);
    ui->spinBoxBarrierTrancparency->setValue(value);
}

void ViewOption::on_SliderTextSize_valueChanged(int value)
{
    SettingsClass->setLegendTextSize(value);
    ui->SpinBoxTextSize->setValue(value);
}
void ViewOption::on_SliderLegendTrancparency_valueChanged(int value)
{
    SettingsClass->setObjectsTrancparency(TRANCPARENCY_LEGEND, value);
    ui->spinBoxLegendTrancparency->setValue(value);
}

void ViewOption::on_setDefaultButton_clicked()
{
    SettingsClass->setDefault();
    setDefault();
}

void ViewOption::on_EarthGridCheckBox_clicked(bool checked)
{
    SettingsClass->setIsDrawEarthGrid(checked);
}

void ViewOption::on_SpinBoxTextSize_valueChanged(int arg1)
{
    ui->SliderTextSize->setValue(arg1);
}

void ViewOption::on_spinBoxLegendTrancparency_valueChanged(int arg1)
{
    ui->SliderLegendTrancparency->setValue(arg1);
}

void ViewOption::on_spinBoxLightDistKoef_valueChanged(int arg1)
{
    ui->SliderLightDistKoef->setValue(arg1);
}

void ViewOption::on_doubleSpinBoxLightAmbient_valueChanged(double arg1)
{
    ui->SliderLightAmbient->setValue(arg1);
}

void ViewOption::on_doubleSpinBoxLightDiffuse_valueChanged(double arg1)
{
    ui->SliderLightDiffuse->setValue(arg1);
}

void ViewOption::on_doubleSpinBoxLightSpecular_valueChanged(double arg1)
{
    ui->SliderLightSpecular->setValue(arg1);
}

void ViewOption::on_doubleSpinBoxMaterialShine_valueChanged(double arg1)
{
    ui->SliderMaterialShine->setValue(arg1);
}

void ViewOption::on_spinBoxTrancparencyProcedures_valueChanged(int arg1)
{
    ui->SliderTrancparencyProcedures->setValue(arg1);
}

void ViewOption::on_spinBoxTrancparencyProceduresLines_valueChanged(int arg1)
{
    ui->SliderTrancparencyProceduresLines->setValue(arg1);
}

void ViewOption::on_spinBoxProcedureLineWidthBase_valueChanged(int arg1)
{
    ui->SliderProcedureLineWidthBase->setValue(arg1);
}

void ViewOption::on_spinBoxProcedureLineWidthTrajectory_valueChanged(int arg1)
{
    ui->SliderProcedureLineWidthTrajectory->setValue(arg1);
}

void ViewOption::on_spinBoxProcedureLineWidthRams_valueChanged(int arg1)
{
    ui->SliderProcedureLineWidthRams->setValue(arg1);
}

void ViewOption::on_spinBoxTrancparencyZones_valueChanged(int arg1)
{
    ui->SliderTrancparencyZones->setValue(arg1);
}

void ViewOption::on_spinBoxTrancparencyZonesLines_valueChanged(int arg1)
{
    ui->SliderTrancparencyZonesLines->setValue(arg1);
}

void ViewOption::on_spinBoxZoneLineWidthTrajectory_valueChanged(int arg1)
{
    ui->SliderZoneLineWidthTrajectory->setValue(arg1);
}

void ViewOption::on_spinBoxBarrierTrancparency_valueChanged(int arg1)
{
    ui->SliderBarrierTrancparency->setValue(arg1);
}

void ViewOption::on_spinBoxBarrierSize_valueChanged(int arg1)
{
    ui->SliderBarrierSize->setValue(arg1);
}
