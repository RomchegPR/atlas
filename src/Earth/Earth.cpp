#define _USE_MATH_DEFINES
#include <cmath>

#include "include/Earth/Earth.h"
#include "QImage"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include "include/Helper.h"
#include "include/GeoHelper.h"
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/quaternion.hpp>
#include <limits.h>
#include "MainRenderModel.h"
#include <QDir>
#include "MainSettingsClass.h"
#include "QCoreApplication"

Earth::Earth()
{
    mRateCount = 100; // почему то надо кратные 10, чтоб снизу норм рисовалось...
    mDistToReDraw = 10000;
    mRadiusEarth = GeoHelper::MINOR_AXIS/100;
    mEarthCenter = glm::vec3(0.0f, 0.0f, 0.0f);

    mLastCameraPos = glm::vec3(0, 0, 0);

    mColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

    MainRenderData = SingleMainRenderModel::Instance();
    mCamera = Camera::Instance();

    mHeightRate = 10000;

    SettingsClass = mainSettingsClass::Instance();
}

void Earth::createEarth(){

    int k = 0;

    QVector <QVector < int > > EarthBasisIndeces;

    for (int i = 0 ; i <= mRateCount; i++){
        EarthBasisIndeces.push_back(QVector < int> ());
        for (int j = 0; j <= mRateCount; j++){
            EarthBasisIndeces[i].push_back(k);

            double x = mEarthCenter.x + GeoHelper::MAJOR_AXIS*sin(2*M_PI/mRateCount*(i))*cos(2*M_PI/mRateCount*j );
            double y = mEarthCenter.y + GeoHelper::MINOR_AXIS*cos(2*M_PI/mRateCount*(i));
            double z = mEarthCenter.z + GeoHelper::MAJOR_AXIS*sin(2*M_PI/mRateCount*(i))*sin(2*M_PI/mRateCount*j );

            if (j == 0 ||  i == mRateCount / 4) {
                if (mRenderData[LinesType].mVertex.size() == 0) {
                    mRenderData[LinesType].mVertex.push_back(glm::vec3(x+1,y+1,z+1));
                } else {
                    mRenderData[LinesType].mVertex.push_back(glm::vec3(x+1,y+1,z+1));
                    mRenderData[LinesType].mVertex.push_back(glm::vec3(x+1,y+1,z+1));
                }

                mRenderData[LinesType].mIndices.push_back(mRenderData[LinesType].mVertex.size());
                mRenderData[LinesType].mIndices.push_back(mRenderData[LinesType].mVertex.size());
            }

            mRenderData[BasicType].mTextureIndex.push_back(glm::vec2 (
                                                                (mRateCount - j)*1.0f / mRateCount,
                                                                ( i )*2.0f / mRateCount
                                                            )
                                                  );

            mRenderData[BasicType].mVertex.push_back(glm::vec3(x,y,z));
            mRenderData[BasicType].mNormal.push_back(glm::vec3(0));

            k++;
        }
    }

    for (int i = 0 ; i < mRateCount; i++){
        for (int j = 0; j < mRateCount; j++){

            mRenderData[BasicType].mIndices.push_back(EarthBasisIndeces[i][j]);
            mRenderData[BasicType].mIndices.push_back(EarthBasisIndeces[i][j+1]);
            mRenderData[BasicType].mIndices.push_back(EarthBasisIndeces[i+1][j+1]);

            mRenderData[BasicType].mIndices.push_back(EarthBasisIndeces[i+1][j+1]);
            mRenderData[BasicType].mIndices.push_back(EarthBasisIndeces[i+1][j]);
            mRenderData[BasicType].mIndices.push_back(EarthBasisIndeces[i][j]);

        }
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mIndices.size() ; i += 3) {

        int Index0 = mRenderData[BasicType].mIndices[i];
        int Index1 = mRenderData[BasicType].mIndices[i + 1];
        int Index2 = mRenderData[BasicType].mIndices[i + 2];

        glm::vec3 v1 = mRenderData[BasicType].mVertex[Index1] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 v2 = mRenderData[BasicType].mVertex[Index2] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 Normal = glm::cross(v1,v2);

        glm::normalize(Normal);

        mRenderData[BasicType].mNormal[Index0] += Normal;
        mRenderData[BasicType].mNormal[Index1] += Normal;
        mRenderData[BasicType].mNormal[Index2] += Normal;
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mNormal.size() ; i++) {
        glm::normalize(mRenderData[BasicType].mNormal[i]);
    }

    preloadParts();
}

void Earth::preloadParts(){

    mEarthPartsDir = SettingsClass->getApplicationDirPath() +  "/res/EarthParts/Textures/";

    QDir myDirTextures(mEarthPartsDir);

    mEarthPartsFiles = myDirTextures.entryList(QDir::NoDotAndDotDot |
                                                QDir::System |
                                                QDir::Hidden  |
                                                QDir::AllDirs |
                                                QDir::Files,
                                                QDir::DirsFirst);

    if (mEarthPartsFiles.size() == 0){
        qDebug() << "ERROR! NO Earthpart Texture";
    }


    for (int i = 0; i < mEarthPartsFiles.size(); i++){
        QString fileName = mEarthPartsFiles.at(i).left(mEarthPartsFiles.at(i).indexOf(".jpg"));



             if (fileName != "1" && !fileName.contains("bmp")){


                QStringList Coords = fileName.split("_");
                QStringList start = Coords.at(0).split(";");
                QStringList end = Coords.at(1).split(";");

                float Startlat = start.at(0).toFloat();
                float Startlon = start.at(1).toFloat();

                float Endlat = end.at(0).toFloat();
                float Endlon = end.at(1).toFloat();

                glm::vec3 centerPart = GeoHelper::geo2cartesian(glm::vec2((Startlat + Endlat) / 2, (Startlon + Endlon)/2), false);

                centerPart.x /= 100;
                centerPart.y /= 100;
                centerPart.z /= 100;

                double size = fabs(Startlat - Endlat) * fabs(Startlon - Endlon);

                double dist = glm::distance(centerPart, mCamera->getEye()) / size / GeoHelper::MAJOR_AXIS * 1000 ;

                float level = (10/(size));
                if (level <= 0) {
                    level = 1;
                } else {
                    level *= 2;
                }
                qDebug() << size << level;

                createEarthPart(Startlat, Startlon, Endlat, Endlon, mEarthPartsDir + mEarthPartsFiles.at(i), 3000/size);
//                qDebug() << fileName ;
        }
    }
}

void Earth::checkEarthPart(){

    for (int i = 0; i < mEarthPartsFiles.size(); i++){

        QString fileName = mEarthPartsFiles.at(i).left(mEarthPartsFiles.at(i).indexOf(".jpg"));

            if (fileName != "1" && !fileName.contains("bmp")){
                QStringList Coords = fileName.split("_");
                QStringList start = Coords.at(0).split(";");
                QStringList end = Coords.at(1).split(";");


                float Startlat = start.at(0).toFloat();
                float Startlon = start.at(1).toFloat();

                float Endlat = end.at(0).toFloat();
                float Endlon = end.at(1).toFloat();

                glm::vec3 centerPart = GeoHelper::geo2cartesian(glm::vec2((Startlat + Endlat) / 2, (Startlon + Endlon)/2), false);
                centerPart.x /= 100;
                centerPart.y /= 100;
                centerPart.z /= 100;
                double size = fabs(Startlat - Endlat) * fabs(Startlon - Endlon);
                int level = 100/size;
                centerPart = glm::vec3(-centerPart.x, centerPart.z, centerPart.y);

//                double dist = glm::distance(centerPart, mCamera->getEye()/glm::vec3(100)) / size / GeoHelper::MAJOR_AXIS * 1000 ;
                double dist = glm::distance(centerPart, mCamera->getEye()/glm::vec3(100)) ;


//                qDebug() << glm::distance(centerPart, mCamera->getEye()) / (GeoHelper::MAJOR_AXIS / 1000) ;

                if (glm::distance(centerPart, mCamera->getEye()) / (GeoHelper::MAJOR_AXIS / 1000) < 0.4f) {
                    mAllPartsVisible = true;
                    createEarthPart(Startlat, Startlon, Endlat, Endlon, mEarthPartsDir + mEarthPartsFiles.at(i), level);
                } else if (glm::distance(centerPart, mCamera->getEye()) / (GeoHelper::MAJOR_AXIS / 1000) >= 0.7f) {
                    mAllPartsVisible = false;
                    hideEarthPart(mEarthPartsDir + mEarthPartsFiles.at(i));
                }
        }
    }
}


void Earth::createEarthPart(float Startlat, float Startlon, float Endlat, float Endlon, QString PathToImage, float level){

    for (int i = 0; i < mEarthParts.size(); i++){
        if (mEarthParts[i].PathToImage == PathToImage || mAllPartsVisible) {
            mEarthParts[i].setVisible(true);
//            return;
        }
    }

    if (!mAllPartsVisible){
        if (mEarthParts.size() > 5) return;

        EarthPart temp;

        temp.setCenter(&mEarthCenter);
        temp.setRadiusEarth(&mRadiusEarth);
        temp.setRateCount(mRateCount);

        temp.setPartCoords(90.0 - Startlat, 180.0 - Startlon, 90.0 - Endlat, 180.0 - Endlon);

        temp.setEarthRenderData(&mRenderData[BasicType]);
        temp.setImage(PathToImage);
        temp.setLevel(level);
        temp.init();
        temp.setVisible(true);

        mEarthParts.push_back(temp);
    }
}


void Earth::hideEarthPart(QString PathToImage){
    for (int i = 0; i < mEarthParts.size(); i++){
        if (mEarthParts[i].PathToImage == PathToImage) {
            mEarthParts[i].setVisible(false);
            return;
        }
    }
}


void Earth::init(){

    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mElementBuffer.create();
    mRenderData[BasicType].mElementBuffer.bind();
    mRenderData[BasicType].mElementBuffer.setData(mRenderData[BasicType].mIndices.size() * sizeof(GLuint), &mRenderData[BasicType].mIndices.front());

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mColorBuffer.create();
    mRenderData[BasicType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    mRenderData[BasicType].mColorBuffer.setData(sizeof(glm::vec4), &mColor);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 1);

    mRenderData[BasicType].mTextureBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mTextureBuffer.create();
    mRenderData[BasicType].mTextureBuffer.bind();
    glEnableVertexAttribArray(2);
    mRenderData[BasicType].mTextureBuffer.setData(mRenderData[BasicType].mTextureIndex.size() * sizeof(glm::vec2), &mRenderData[BasicType].mTextureIndex.front());
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(3);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mNormal.size() * sizeof(glm::vec3), &mRenderData[BasicType].mNormal.front());
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    QImage image(MainRenderData->mTextureEarthFileName);

    mRenderData[BasicType].mTexture.create(Texture::Texture_2D);
    mRenderData[BasicType].mTexture.bind();
    mRenderData[BasicType].mTexture.setImage2D(image);
    mRenderData[BasicType].mTexture.generateMipmaps();
    mRenderData[BasicType].mTexture.setWrap(Texture::Repeat, Texture::Repeat);
    mRenderData[BasicType].mTexture.setFilter(Texture::Linear, Texture::LinearMipMapNearest);

    QImage image2(MainRenderData->mTextureHeightMapFileName);

    heightmap.create(Texture::Texture_2D);
    heightmap.bind();
    heightmap.setImage2D(image2);
    heightmap.generateMipmaps();
    heightmap.setWrap(Texture::Repeat, Texture::Repeat);
    heightmap.setFilter(Texture::Linear, Texture::LinearMipMapNearest);

    mRenderData[BasicType].mVAO.unbind();

    mRenderData[LinesType].mVAO.create();
    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mElementBuffer.create();
    mRenderData[LinesType].mElementBuffer.bind();
    mRenderData[LinesType].mElementBuffer.setData(mRenderData[LinesType].mIndices.size() * sizeof(GLuint), &mRenderData[LinesType].mIndices.front());

    mRenderData[LinesType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mVertexBuffer.create();
    mRenderData[LinesType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mColorBuffer.create();
    mRenderData[LinesType].mColorBuffer.bind();
    glEnableVertexAttribArray(0);
    glm::vec4 color = glm::vec4(1,0,0,1);
    mRenderData[LinesType].mColorBuffer.setData(sizeof(glm::vec4), &color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 1);

    mRenderData[LinesType].mVAO.unbind();

    for (int i = 0; i < mEarthParts.size(); i++){
        mEarthParts[i].init();

    }
}

void Earth::updateEarthTextures(){

    QImage image(MainRenderData->mTextureEarthFileName);

    mRenderData[BasicType].mTexture.create(Texture::Texture_2D);
    mRenderData[BasicType].mTexture.bind();
    mRenderData[BasicType].mTexture.setImage2D(image);
    mRenderData[BasicType].mTexture.generateMipmaps();
    mRenderData[BasicType].mTexture.setWrap(Texture::Repeat, Texture::Repeat);
    mRenderData[BasicType].mTexture.setFilter(Texture::Linear, Texture::LinearMipMapNearest);

    QImage image2(MainRenderData->mTextureHeightMapFileName);

    heightmap.create(Texture::Texture_2D);
    heightmap.bind();
    heightmap.setImage2D(image2);
    heightmap.generateMipmaps();
    heightmap.setWrap(Texture::Repeat, Texture::Repeat);
    heightmap.setFilter(Texture::Linear, Texture::LinearMipMapNearest);
}

void Earth::paint(){

//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_BACK);

//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    mRenderData[BasicType].mVAO.bind();
    glActiveTexture(GL_TEXTURE1);
    heightmap.bind();

    MainRenderData->mShaders[LightShader].setUniformValue("heightmap", 1);
    MainRenderData->mShaders[LightShader].setUniformValue("useHeightMap", false);
    MainRenderData->mShaders[LightShader].setUniformValue("heightRate", mHeightRate);

    MainRenderData->mShaders[LightShader].setUniformValue("minHeight", MIN_ALTITUDO);
    MainRenderData->mShaders[LightShader].setUniformValue("maxHeight", MAX_ALTITUDO);

    glActiveTexture(GL_TEXTURE0);
    mRenderData[BasicType].mTexture.bind();
    glDrawElements(GL_TRIANGLES, mRenderData[BasicType].mIndices.size()/2, GL_UNSIGNED_INT, 0);
    mRenderData[BasicType].mTexture.unbind();

    if (SettingsClass->getIsDrawEarthGrid()){
        glDrawElements(GL_LINES, mRenderData[BasicType].mIndices.size()/2, GL_UNSIGNED_INT, 0);

        mRenderData[LinesType].mVAO.bind();
        glDrawArrays(GL_LINES, 0,  mRenderData[LinesType].mIndices.size());
        mRenderData[LinesType].mVAO.unbind();
    }

    mRenderData[BasicType].mVAO.unbind();

    checkEarthPart();

    for (int i = 0; i < mEarthParts.size(); i++){
        if (mEarthParts[i].getIsTextureFound()){
            mEarthParts[i].paint();
        }else{
            qDebug() << "ERROR! No Earthpart Texture";
        }
    }


//    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
//    glDisable(GL_CULL_FACE);
//    glCullFace(GL_FRONT_AND_BACK);

}

void Earth::clear(){

    mRenderData[BasicType].clear();

    for (int i = 0; i < mEarthParts.size(); i++)
        mEarthParts[i].clear();

    mEarthParts.clear();
}

void Earth::setEarthCenter(const glm::vec3 &center){
    mEarthCenter = center;
}

void Earth::setEarthRadius(const int &radius){
    mRadiusEarth = radius;
}

void    Earth::setColor ( glm::vec4 color ){
    mColor = color;
}

void Earth::setHeightRate( float rate ){
    mHeightRate = rate * 10000;
}

glm::vec3 Earth::getCenter(){
    return mEarthCenter;
}

glm::vec3 Earth::getIntersectPointWithRay(){
    return mIntersectedPoint;
}

double Earth::getRadius(){
    return mRadiusEarth;
}
