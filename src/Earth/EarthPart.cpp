#define _USE_MATH_DEFINES

#include "include/Earth/EarthPart.h"
#include <cmath>
#include <QDebug>
#include <QGLFormat>
#include "include/GeoHelper.h"
#include <QThread>
#include "MainRenderModel.h"
#include "MainSettingsClass.h"

EarthPart::EarthPart(){
    mLevel = 0;
    isVisible = false;
    isTextureFound = true;

    MainRenderData = SingleMainRenderModel::Instance();
    SettingsClass = mainSettingsClass::Instance();
}

void EarthPart::init( ){

    calc();

    for ( int i = 0 ; i < pEarthRenderData[BasicType].mIndices.size() ; i += 3) {

        int Index0 = pEarthRenderData[BasicType].mIndices[i];
        int Index1 = pEarthRenderData[BasicType].mIndices[i + 1];
        int Index2 = pEarthRenderData[BasicType].mIndices[i + 2];

        glm::vec3 v1 = mRenderData[BasicType].mVertex[Index1] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 v2 = mRenderData[BasicType].mVertex[Index2] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 Normal = glm::cross(v1,v2);

        Normal = glm::normalize(Normal);

        mRenderData[BasicType].mNormal[Index0] += Normal;
        mRenderData[BasicType].mNormal[Index1] += Normal;
        mRenderData[BasicType].mNormal[Index2] += Normal;
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mNormal.size() ; i++) {
        glm::normalize(mRenderData[BasicType].mNormal[i]);
    }


    mCenter = mRenderData[BasicType].mVertex[mRenderData[BasicType].mVertex.size()/2];

    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mElementBuffer.create();
    mRenderData[BasicType].mElementBuffer.bind();
    if (pEarthRenderData[BasicType].mIndices.size() > 0)
        mRenderData[BasicType].mElementBuffer.setData(pEarthRenderData[BasicType].mIndices.size() * sizeof(GLuint), &pEarthRenderData[BasicType].mIndices.front());

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    if (pEarthRenderData[BasicType].mVertex.size() > 0)
        mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mTextureBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mTextureBuffer.create();
    mRenderData[BasicType].mTextureBuffer.bind();
    glEnableVertexAttribArray(2);
        if (pEarthRenderData[BasicType].mTextureIndex.size() > 0)
    mRenderData[BasicType].mTextureBuffer.setData(mRenderData[BasicType].mTextureIndex.size() * sizeof(glm::vec2), &mRenderData[BasicType].mTextureIndex.front());
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(3);
        if (pEarthRenderData[BasicType].mNormal.size() > 0)
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mNormal.size() * sizeof(glm::vec3), &mRenderData[BasicType].mNormal.front());
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    QImage image(PathToImage);
    if (image.width() != 0 && image.height() != 0){
        isTextureFound = true;
    }

    QPoint center = image.rect().center();
    QMatrix matrix;
    matrix.translate(center.x(), center.y());
    matrix.rotate(-90);
    image = image.transformed(matrix);

    mRenderData[BasicType].mTexture.create(Texture::Texture_2D);
    mRenderData[BasicType].mTexture.bind();
    mRenderData[BasicType].mTexture.setImage2D(image);
    mRenderData[BasicType].mTexture.generateMipmaps();
    mRenderData[BasicType].mTexture.setWrap(Texture::Repeat, Texture::Repeat);
    mRenderData[BasicType].mTexture.setFilter(Texture::Linear, Texture::LinearMipMapNearest);

    mRenderData[BasicType].mVAO.unbind();

    QString heightFileDir ;

    heightFileDir = SettingsClass->getApplicationDirPath() + "/res/EarthParts/Heights/";
    QString heightFile = PathToImage.right(PathToImage.lastIndexOf("/"));

    while (heightFile.contains("/")){
        heightFile = heightFile.right(heightFile.size() - heightFile.indexOf("/") - 1);
    }

    QImage image5(heightFileDir + heightFile);

    QPoint center1 = image5.rect().center();
    QMatrix matrix1;
    matrix1.translate(center1.x(), center1.y());
    matrix1.rotate(-90);
    image5 = image5.transformed(matrix1);

    mHeightMap.create(Texture::Texture_2D);
    mHeightMap.bind();
    mHeightMap.setImage2D(image5);
    mHeightMap.generateMipmaps();
    mHeightMap.setWrap(Texture::Repeat, Texture::Repeat);
    mHeightMap.setFilter(Texture::Linear, Texture::LinearMipMapNearest);

    isVisible = true;

}

void EarthPart::calc(){

    mRateCount = 100;
    int k = 0;

    double deltaLongit = fabs(mFinishLongit - mLongit);
    double deltaLatit = fabs(mFinishLatit - mLatit);

    mLatit += 0.018;
    if (deltaLongit < 1)    mLatit +=0.0075;

    for (int i = 0 ; i <= mRateCount; i++){
        for (int j = 0; j <= mRateCount; j++){

            double x = mEarthCenter->x + mLevel + GeoHelper::MAJOR_AXIS*sin(2*M_PI/(mRateCount)*(((double)j/mRateCount)*deltaLatit + mLatit))*cos(2*M_PI/(mRateCount)*(((double)i/mRateCount)*deltaLongit + mLongit ));
            double y = mEarthCenter->y + mLevel + GeoHelper::MINOR_AXIS*cos(2*M_PI/(mRateCount)*(((double)j/(mRateCount))*deltaLatit + mLatit));
            double z = mEarthCenter->z + mLevel + GeoHelper::MAJOR_AXIS*sin(2*M_PI/(mRateCount)*(((double)j/mRateCount)*deltaLatit + mLatit))*sin(2*M_PI/(mRateCount)*(((double)i/mRateCount)*deltaLongit + mLongit ));

//            glm::vec2 polar = GeoHelper::cartesian2geo(glm::vec3(-x,z,y));
//            polar.x = GeoHelper::rad2deg(polar.x);
//            polar.y = GeoHelper::rad2deg(polar.y);
//            qDebug() << polar.x << polar.y;


            mRenderData[BasicType].mVertex.push_back( glm::vec3(x,y,z));
            mRenderData[BasicType].mNormal.push_back(glm::vec3(0));
            mRenderData[BasicType].mTextureIndex.push_back(glm::vec2 (
                                                                ( j - mRateCount)*1.0f / mRateCount,
                                                                ( i )*1.0f / mRateCount
                                                                     )
                                                            );
            k++;
        }
    }
}

void EarthPart::paint( ){

    if (isVisible){
        glActiveTexture(GL_TEXTURE1);
        mHeightMap.bind();

        if (PathToImage.contains("57;35")){
            MainRenderData->mShaders[LightShader].setUniformValue("useHeightMap", true);

            MainRenderData->mShaders[LightShader].setUniformValue("heightmap", 1);

            MainRenderData->mShaders[LightShader].setUniformValue("minHeight", 73);
            MainRenderData->mShaders[LightShader].setUniformValue("maxHeight", 380);

        } else if (PathToImage.contains("55.99")){

            MainRenderData->mShaders[LightShader].setUniformValue("useHeightMap", true);

            MainRenderData->mShaders[LightShader].setUniformValue("heightmap", 1);

            MainRenderData->mShaders[LightShader].setUniformValue("minHeight", 250);
            MainRenderData->mShaders[LightShader].setUniformValue("maxHeight", 530);

        } else{
            MainRenderData->mShaders[LightShader].setUniformValue("useHeightMap", false);

            MainRenderData->mShaders[LightShader].setUniformValue("heightmap", 1);

            MainRenderData->mShaders[LightShader].setUniformValue("minHeight", 0);
            MainRenderData->mShaders[LightShader].setUniformValue("maxHeight", 0);
        }


        mRenderData[BasicType].mVAO.bind();
        glActiveTexture(GL_TEXTURE0);
        mRenderData[BasicType].mTexture.bind();
        glDrawElements(GL_TRIANGLES, pEarthRenderData[BasicType].mIndices.size(), GL_UNSIGNED_INT, 0);
        mRenderData[BasicType].mVAO.unbind();
    }

}

void EarthPart::clear() {
    RenderObject::clear();
}

void EarthPart::setEarthRenderData(RenderData *data)
{
    pEarthRenderData = data;
}

void EarthPart::setCenter(glm::vec3 *center){
    mEarthCenter = center;
}

void EarthPart::setRadiusEarth(double *value)
{
    mRadiusEarth = value;
}

void EarthPart::setModel( glm::mat4 *model ){
    pModelMatrix = model;
}

void EarthPart::setRateCount ( int count ){
    mRateCount = count;
}

void EarthPart::setImage(QString path)
{
    PathToImage = path;
}

void EarthPart::setLevel(float level){
    mLevel = level;
}

glm::vec3 EarthPart::getPartCenter(){
    return mCenter;
}

bool EarthPart::getIsTextureFound() const
{
    return isTextureFound;
}

void EarthPart::setIsTextureFound(bool value)
{
    isTextureFound = value;
}

void EarthPart::setPartCoords(float startLat, float startLon, float finishLat, float finishLon){

    // start and end Lon swap because of texture vertexes - from right to left

    mLatit = startLat * mRateCount / 360;
    mLongit = finishLon * mRateCount / 360;

    mFinishLatit = finishLat * mRateCount / 360;
    mFinishLongit = startLon * mRateCount / 360;


}

void EarthPart::setVisible(bool value){
    isVisible = value;
}
