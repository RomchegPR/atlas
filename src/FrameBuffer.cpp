#include "include/FrameBuffer.h"
#include "include/RenderObject/Texture.h"

#include "iostream"

FrameBuffer::FrameBuffer()
{
}

void FrameBuffer::init(Mode mode){
    mMode = mode;
}

bool FrameBuffer::create(){
    if (glIsFramebuffer(mId) == GL_TRUE)
        return true;

    glGenFramebuffers(1, &mId);
    glBindFramebuffer(mMode, mId);
    mOk = (glIsFramebuffer(mId) == GL_TRUE);
    glBindFramebuffer( mMode, 0 );
    return mOk;
}

bool FrameBuffer::bind(){
    glBindFramebuffer(mMode, mId);
}

bool FrameBuffer::unbind(){
    glBindFramebuffer(mMode, 0);
}

void FrameBuffer::remove(){
    if ( glIsFramebuffer(mId) ){
        glDeleteFramebuffers(1, &mId);
        mOk = false;
    }
}

void FrameBuffer::setTexture(AttachmentPoint attachment, Texture texture, GLuint mipMapLevel){
//    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture.id(), 0);
    glFramebufferTexture2D(mMode, attachment, texture.target(), texture.id(), mipMapLevel);
}

void FrameBuffer::checkStatus(){
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;
}

