#include "include/Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <include/GeoHelper.h>
#include <math.h>

#include "include/Helper.h"
#include "MainSettingsClass.h"

const int Camera::SMOOTH_ROTATION = 10;
const int Camera::SPEED_ROTATION = 10;

Camera::Camera(QObject *parent): QObject(parent)
{
    SettingsClass = mainSettingsClass::Instance();
    setDefaultParameters();
    connect(&mActionTimer, SIGNAL(timeout()), SLOT(allRotationWithTimer()));
}

void Camera::setDefaultParameters()
{
    mAxisRotationAroundEarthZ = glm::vec3(0,0,1);
    mAxisRotationAroundEarthY = glm::vec3(0,1,0);
    mAxisRotationAroundEarthX = glm::vec3(0,0,1);
    mCameraSpeed = 50.0f;
    mEye = glm::vec3(-51259.2, 94862.7, 40093.2);
    mCenter = glm::vec3(0.0f, 0.0f, 0.0f);
    mCameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
    mAngleVerticalCamera = 0.0f;
    mAngleCameraUpNorth = 0.0f;
    mFlagRotationToTheNorth = false;
    mFlagRotationVertical = false;
    mFlagRotationToPoint = false;
    mAllAxes = glm::mat4(1.0f);

    mPointRotateAround = glm::normalize(mEye) * GeoHelper::MINOR_AXIS / 100.0f;
    mNorthPole = glm::normalize(glm::vec3(0,1,0)) * GeoHelper::MINOR_AXIS;
    mCenter = mPointRotateAround;
    mAxisRotationAroundPointOnEarthX = mAxisRotationAroundEarthX;
    mAxisRotationAroundPointOnEarthY = mAxisRotationAroundEarthY;
    mAxisRotationAroundPointOnEarthZ = glm::cross(mCameraUp, glm::normalize(mEye - mCenter));
    mCameraUp = glm::normalize(glm::cross(mAxisRotationAroundPointOnEarthZ, -mEye));
    mInclineSensitivity = glm::distance(mEye, mPointRotateAround)/100 / SettingsClass->getCameraSpeed();
    mSensitivityRotationAroundPointOnEarth = glm::distance(mEye, mPointRotateAround)/100 / SettingsClass->getCameraSpeed();
    mSensitivityRotationAroundEarth = mSensitivityRotationAroundPointOnEarth/150000 / SettingsClass->getCameraSpeed();
}

Camera::~Camera()
{

}

Camera* Camera::Instance()
{
   static Camera    instance;
   return &instance;
}

//COMMENT: All moves of camera divided to 2 separate motion: 1) it is motion by some orbit around center of Earth 2) it is motion around some point on the surface of Earth

/*-----------------------------------------------

Name:	rotateWithMouse

Params:	e - MouseEvent (moving)
        xOffset, yOffset - difference between mouse coordinates

Result:	Rotate camera while mouse is moving.

-----------------------------------------------*/

void Camera::rotateWithMouseAroundPoint(GLint &xOffset, GLint &yOffset)
{
    glm::mat4 allAxes =  glm::mat4(1.0f);
    mPointRotateAround = glm::normalize(mPointRotateAround) * GeoHelper::MINOR_AXIS / 100.0f;
    glm::mat4 TranslationMatrix = glm::translate(-mPointRotateAround);
    mInclineSensitivity = glm::distance(mEye, glm::vec3(0,0,0))/(10000000.0f * SettingsClass->getCameraSpeed());
    mSensitivityRotationAroundPointOnEarth = mInclineSensitivity / (2.0f * SettingsClass->getCameraSpeed());
    mAxisRotationAroundPointOnEarthY = mPointRotateAround;
    mAxisRotationAroundPointOnEarthZ = glm::cross(glm::normalize(mEye), glm::normalize(mCameraUp));

    if (fabs(xOffset) > fabs(yOffset))
    {
        allAxes = glm::rotate(allAxes, mSensitivityRotationAroundPointOnEarth * xOffset, mAxisRotationAroundPointOnEarthY);
        mEye = glm::vec3(allAxes * glm::vec4(mEye, 1.0));
        mCameraUp = glm::vec3(allAxes * glm::vec4(mCameraUp, 1.0));
        mCenter = glm::vec3(allAxes * glm::vec4(mCenter, 1.0));
    }
    else
    {
        // Calculate angle between mCenter and mEye around Earth
        GLfloat angle =  mInclineSensitivity * yOffset;         //We will move camera for this angle
        glm::vec3 tmpCamera, tmpCenter;

        allAxes = glm::rotate(allAxes, angle, mAxisRotationAroundPointOnEarthZ);
        tmpCamera = mEye;
        tmpCamera = glm::vec3(allAxes * (TranslationMatrix * glm::vec4(tmpCamera, 1.0)));
        tmpCamera = glm::vec3(glm::inverse(TranslationMatrix) * glm::vec4(tmpCamera, 1.0));

        tmpCenter = mCenter;
        tmpCenter = glm::vec3(allAxes * (TranslationMatrix * glm::vec4(tmpCenter, 1.0)));
        tmpCenter = glm::vec3(glm::inverse(TranslationMatrix) * glm::vec4(tmpCenter, 1.0));

        glm::mat4 TranslationMatrix1 = glm::translate(-mCenter );
        glm::vec3 tmpVector = glm::normalize(TranslationMatrix1 * glm::vec4(mEye, 1.0));

        GLfloat tmpVerticalAngle = glm::orientedAngle(glm::normalize(mPointRotateAround), tmpVector, glm::normalize(mAxisRotationAroundPointOnEarthZ))+ angle;

        if ( tmpVerticalAngle > 0.0f || tmpVerticalAngle < -M_PI_2)  // If Angle > 90 degreec nothing to do
        {
            return;
        }

        // Cheking our Camera is in Earth or not.

        if(!isPointInEllipse(GeoHelper::MAJOR_AXIS, GeoHelper::MINOR_AXIS, tmpCamera * glm::vec3(100)))
        {
            // We chacked angle and it's OK! We can move our camera.
            mAngleVerticalCamera = tmpVerticalAngle;
            mCenter = tmpCenter;
            mEye = tmpCamera;
            mCameraUp = glm::rotate(mCameraUp, angle, mAxisRotationAroundPointOnEarthZ);
        }
    }
}

void Camera::rotateWithMouseAroundObjectPoint(GLint &xOffset, GLint &yOffset)
{
    glm::mat4 allAxes =  glm::mat4(1.0f);
    glm::mat4 TranslationMatrix = glm::translate(-mPointObjectRotateAround);
    mInclineSensitivity = glm::distance(mEye, glm::vec3(0,0,0))/(10000000.0f * SettingsClass->getCameraSpeed());
    mSensitivityRotationAroundPointOnEarth = mInclineSensitivity / (2.0f * SettingsClass->getCameraSpeed());
    mAxisRotationAroundPointOnEarthY = mPointObjectRotateAround;
    mAxisRotationAroundPointOnEarthZ = glm::cross(glm::normalize(mEye), glm::normalize(mCameraUp));

    if (fabs(xOffset) > fabs(yOffset))
    {
        allAxes = glm::rotate(allAxes, mSensitivityRotationAroundPointOnEarth * xOffset, mAxisRotationAroundPointOnEarthY);
        mEye = glm::vec3(allAxes * glm::vec4(mEye, 1.0));
        mCameraUp = glm::vec3(allAxes * glm::vec4(mCameraUp, 1.0));;
        mCenter = glm::vec3(allAxes * glm::vec4(mCenter, 1.0));
    }
    else
    {
        // Calculate angle between mCenter and mEye around Earth
        GLfloat angle =  mInclineSensitivity * yOffset;         //We will move camera for this angle
        glm:: vec3 tmpCamera, tmpCenter;

        allAxes = glm::rotate(allAxes, angle, mAxisRotationAroundPointOnEarthZ);
        tmpCamera = mEye;
        tmpCamera = glm::vec3(allAxes * (TranslationMatrix * glm::vec4(tmpCamera, 1.0)));
        tmpCamera = glm::vec3(glm::inverse(TranslationMatrix) * glm::vec4(tmpCamera, 1.0));

        tmpCenter = mCenter;
        tmpCenter = glm::vec3(allAxes * (TranslationMatrix * glm::vec4(tmpCenter, 1.0)));
        tmpCenter = glm::vec3(glm::inverse(TranslationMatrix) * glm::vec4(tmpCenter, 1.0));

        glm::mat4 TranslationMatrix1 = glm::translate(-mCenter );
        glm::vec3 tmpVector = glm::normalize(TranslationMatrix1 * glm::vec4(mEye, 1.0));

        GLfloat tmpVerticalAngle = glm::orientedAngle(glm::normalize(mPointObjectRotateAround), tmpVector, glm::normalize(mAxisRotationAroundPointOnEarthZ))+ angle;


        if ( tmpVerticalAngle > 0.0f || tmpVerticalAngle < -M_PI_2)  // If Angle > 90 degreec nothing to do
        {
            return;
        }

        // Cheking our Camera is in Earth or not.

        if(!isPointInEllipse(GeoHelper::MAJOR_AXIS, GeoHelper::MINOR_AXIS, tmpCamera * glm::vec3(100)))
        {
            // We chacked angle and it's OK! We can move our camera.
            mAngleVerticalCamera = tmpVerticalAngle;
            mCenter = tmpCenter;
            mEye = tmpCamera;
            mCameraUp = glm::rotate(mCameraUp, angle, mAxisRotationAroundPointOnEarthZ);
        }
    }
}

/*-----------------------------------------------

Name:	startRotationToTheNorthPole

Params:	none

Result:	Start ActionTimer and calculated anfle which need rotate.

-----------------------------------------------*/

void Camera::startRotationToTheNorthPole()
{
    mActionTimer.start(SMOOTH_ROTATION);
    mFlagRotationToTheNorth = true;
    glm::vec3 normalizedFirstPoint = glm::normalize(mCameraUp);
    glm::vec3 normalizedSecondPoint = glm::normalize(mNorthPole);

    double angle1, angle2;

    angle1 = glm::orientedAngle( normalizedSecondPoint, glm::normalize(mAxisRotationAroundEarthZ), glm::normalize(mEye));
    angle2 = glm::orientedAngle( normalizedFirstPoint, glm::normalize(mAxisRotationAroundEarthZ), glm::normalize(mEye));
    mAngleCameraUpNorth = (angle2 - angle1);
    mPartOfAngleNorth = mAngleCameraUpNorth / SPEED_ROTATION;
}

/*-----------------------------------------------

Name:	rotationToTheNorthPole

Params:	none

Result:	rotated Camera

-----------------------------------------------*/

void Camera::rotationToTheNorthPole()
{
    static int k = 0;
    mCameraUp = glm::rotate(mCameraUp, mPartOfAngleNorth, glm::normalize(mEye));
    mCenter = glm::rotate(mCenter, mPartOfAngleNorth, glm::normalize(mEye));
    mEye = glm::rotate(mEye, mPartOfAngleNorth, glm::normalize(mEye));
    k++;

    if (k == SPEED_ROTATION)
    {
        k = 0;
        mFlagRotationToTheNorth = false;
        mActionTimer.stop();
        if(fabs(mAngleCameraUpNorth) > 0.02f)
        {
            startRotationToTheNorthPole();
        }
    }
}

/*-----------------------------------------------

Name:	startRotationToPoint

Params:	none

Result:	Start ActionTimer and calculated anfle which need rotate.

-----------------------------------------------*/

void Camera::startRotationToPoint(glm::vec3 &point)
{
    mActionTimer.start(SMOOTH_ROTATION);
    mFlagRotationToPoint = true;
    mPointRotateAround = point;

    glm::vec3 normalizedFirstPoint = glm::normalize(mEye);
    glm::vec3 normalizedSecondPoint = glm::normalize(point);

// Rotation around EarthY Axis

    double angle1, angle2;

    angle1 = glm::orientedAngle( normalizedSecondPoint, glm::normalize(mAxisRotationAroundEarthZ), glm::normalize(mAxisRotationAroundEarthY));
    angle2 = glm::orientedAngle( normalizedFirstPoint, glm::normalize(mAxisRotationAroundEarthZ), glm::normalize(mAxisRotationAroundEarthY));
    mPartofAngleToPointY = (angle2 - angle1) /SPEED_ROTATION;

    angle1 = glm::orientedAngle( normalizedSecondPoint, glm::normalize(mAxisRotationAroundEarthY), glm::normalize(mAxisRotationAroundEarthZ));
    angle2 = glm::orientedAngle( normalizedFirstPoint, glm::normalize(mAxisRotationAroundEarthY), glm::normalize(mAxisRotationAroundEarthZ));
    mPartofAngleToPointZ = (angle2 - angle1) / SPEED_ROTATION;
}

/*-----------------------------------------------

Name:	rotationToPoint

Params:	none

Result:	rotated Camera.

-----------------------------------------------*/

void Camera::rotationToPoint()
{
    static int k = 0;
    k++;

    glm::mat4 allAxes = glm::mat4(1.0f);
    allAxes = glm::rotate(allAxes, (float)mPartofAngleToPointY, mAxisRotationAroundEarthY);
    mEye = glm::vec3(allAxes * glm::vec4(mEye, 1.0f));
    mCenter = glm::vec3(allAxes * glm::vec4(mCenter, 1.0f));
    mCameraUp = glm::vec3(allAxes * glm::vec4(mCameraUp, 1.0f));

    allAxes = glm::mat4(1.0f);

    mAxisRotationAroundEarthY = mCameraUp;

    allAxes = glm::rotate(allAxes, (float)mPartofAngleToPointZ, mAxisRotationAroundEarthZ);
    mEye = glm::vec3(allAxes * glm::vec4(mEye, 1.0f));
    mCenter = glm::vec3(allAxes * glm::vec4(mCenter, 1.0f));
    mCameraUp = glm::vec3(allAxes * glm::vec4(mCameraUp, 1.0f));

    mAxisRotationAroundEarthZ = (glm::cross(glm::normalize(mEye - mCenter), glm::normalize(mCameraUp)));
    mAxisRotationAroundEarthY = (glm::cross(mEye, mAxisRotationAroundEarthZ));

    if (k == SPEED_ROTATION)
    {
        k = 0;
        mFlagRotationToPoint = false;
        mActionTimer.stop();
        if(fabs(mPartofAngleToPointY) > 0.002f && fabs(mPartofAngleToPointZ) > 0.002f)
        {
            startRotationToPoint(mPointRotateAround);
        }
    }
}

/*-----------------------------------------------

Name:	startRotationToVerticalView

Params:	none

Result:	Start ActionTimer and calculated anfle which need rotate.

-----------------------------------------------*/

void Camera::startRotationToVerticalView()
{
    mFlagRotationVertical = true;
    if(mAngleVerticalCamera != 0)
    {
        mActionTimer.start(SMOOTH_ROTATION);
    }
    mPartOfAngleVertical = - mAngleVerticalCamera / SPEED_ROTATION;
}


void Camera::rotationToVerticalView()
{
    if(mAngleVerticalCamera > - 0.1f)
    {
        rotateAroundPointVertical(-mAngleVerticalCamera);
        if (mAngleCameraUpNorth != 0.0f)
        {
            mFlagRotationToTheNorth = true;
        }
        else
        {
            mActionTimer.stop();
        }
        mFlagRotationVertical = false;
        mAngleVerticalCamera = 0.0f;
    }
    else
    {
        rotateAroundPointVertical(mPartOfAngleVertical);
    }
}

/*-----------------------------------------------

Name:	rotateAroundPointVertical

Params:	none

Result:	rotated Camera.

-----------------------------------------------*/

void Camera::rotateAroundPointVertical(GLdouble angle)
{
    glm::mat4 allAxes =  glm::mat4(1.0f);
    glm::mat4 TranslationMatrix = glm::translate(-mCenter);
    mInclineSensitivity = glm::distance(mEye, glm::vec3(0,0,0))/(10000000 * SettingsClass->getCameraSpeed());
    mSensitivityRotationAroundPointOnEarth = mInclineSensitivity / (2 * SettingsClass->getCameraSpeed());
    mAxisRotationAroundPointOnEarthY = mCenter;
    mAxisRotationAroundPointOnEarthZ = glm::cross(glm::normalize(mEye), glm::normalize(mCameraUp));

    glm::vec3 tmpCamera;
    allAxes = glm::rotate(allAxes, (float)angle, mAxisRotationAroundPointOnEarthZ);
    tmpCamera = mEye;
    tmpCamera = glm::vec3(allAxes * (TranslationMatrix * glm::vec4(tmpCamera, 1.0)));
    mEye = glm::vec3(glm::inverse(TranslationMatrix) * glm::vec4(tmpCamera, 1.0));

    mCameraUp = glm::vec3(allAxes * glm::vec4(mCameraUp, 1.0));
    mAngleVerticalCamera += angle;
}

void Camera::allRotationWithTimer()
{
    if (mFlagRotationVertical)
    {
        rotationToVerticalView();
    }
    if (mFlagRotationToTheNorth)
    {
        rotationToTheNorthPole();
    }

    if (mFlagRotationToPoint)
    {
        rotationToPoint();
    }

    GLfloat speed = 21 - SettingsClass->getCameraSpeed(); // Because 10 - max value of slider
    if (mFlagRotationAroundEarth == LEFT)
    {
        rotateCameraAroundEarth(speed, 0.0f);
    }

    if (mFlagRotationAroundEarth == RIGHT)
    {
         rotateCameraAroundEarth(-speed, 0.0f);
    }

    if (mFlagRotationAroundEarth == UP)
    {
        rotateCameraAroundEarth(0.0f, speed);
    }

    if (mFlagRotationAroundEarth == DOWN)
    {
        rotateCameraAroundEarth(0.0f, -speed);
    }
}

/*-----------------------------------------------

Name:	rotateCameraAroundEarthAxis (with real point on the Earth's surface)

Params:	e - MouseEvent (moving)
         Axis1 - Rotation Axis
         Axis2 - For Calculation deflection angle
         secondPoint - Intersection point Ray and Earth
         normalizedFirstPoint, normalizedSecondPoint - Normalized point of intersection
         allAxes - Rotation matrix

Result:	Rotate camera while mouse is moving by some orbit.

-----------------------------------------------*/
void Camera::rotateCameraAroundEarthAxis(glm::vec3 &Axis1, glm::vec3 &Axis2, glm::vec3 &secondPoint,glm::vec3 &normalizedFirstPoint, glm::vec3 normalizedSecondPoint)
{
    double angle, angle1, angle2;
    glm::mat4 allAxes = glm::mat4(1.0f);

    angle1 = glm::orientedAngle( normalizedSecondPoint, glm::normalize(Axis2), glm::normalize(Axis1));
    angle2 = glm::orientedAngle( normalizedFirstPoint, glm::normalize(Axis2), glm::normalize(Axis1));
    angle =  -(angle2 - angle1);
    secondPoint = glm::rotate(secondPoint, (float)angle, Axis1);

    allAxes = glm::rotate(allAxes, (float)angle, Axis1);
    mEye = glm::vec3(allAxes * glm::vec4(mEye, 1.0f));
    mCenter = glm::vec3(allAxes * glm::vec4(mCenter, 1.0f));
    mCameraUp = glm::vec3(allAxes * glm::vec4(mCameraUp, 1.0f));
    mPointRotateAround = glm::vec3(allAxes * glm::vec4(mPointRotateAround, 1.0f));
}

/*-----------------------------------------------

Name:	rotateCameraAroundEarth (with real point on the Earth's surface)

Params:	e - MouseEvent (moving)
         xOffset, yOffset - difference between mouse coordinates
         secondPoint - Intersection point Ray and Earth

Result:	Rotate camera while mouse is moving by some orbit.

-----------------------------------------------*/

void Camera::rotateCameraAroundEarth(glm::vec3 &secondPoint)
{
    glm::vec3 normalizedFirstPoint = glm::normalize(mLastPos);
    glm::vec3 normalizedSecondPoint = glm::normalize(secondPoint);

// Rotation around EarthY Axis
    rotateCameraAroundEarthAxis(mAxisRotationAroundEarthY, mAxisRotationAroundEarthZ, secondPoint, normalizedFirstPoint, normalizedSecondPoint);

// Rotation around EarthZ Axis
    mAxisRotationAroundEarthY = glm::normalize(mCameraUp);
    rotateCameraAroundEarthAxis(mAxisRotationAroundEarthZ, mAxisRotationAroundEarthY, secondPoint, normalizedFirstPoint, normalizedSecondPoint);

    mAxisRotationAroundEarthZ = (glm::cross(glm::normalize(mEye - mCenter), glm::normalize(mCameraUp)));
    mAxisRotationAroundEarthY = (glm::cross(mEye, mAxisRotationAroundEarthZ));
}

/*-----------------------------------------------

Name:	rotateCameraAroundEarth

Params:	e - MouseEvent (moving)
        xOffset, yOffset - difference between mouse coordinates

Result:	Rotate camera while mouse is moving by some orbit.

-----------------------------------------------*/

void Camera::rotateCameraAroundEarth(GLint xOffset, GLint yOffset)
{
    glm::vec3 tmpPoint = glm::normalize(mEye)* GeoHelper::MINOR_AXIS / glm::vec3(100);
    mSensitivityRotationAroundEarth = glm::distance(mEye, tmpPoint)/50000000;
    glm::mat4 allAxes =  glm::mat4(1.0f);

    mAxisRotationAroundEarthZ = (glm::cross(glm::normalize(mEye - mCenter), glm::normalize(mCameraUp)));
    mAxisRotationAroundEarthY = glm::normalize(glm::cross(mEye, mAxisRotationAroundEarthZ));

    allAxes = glm::rotate(allAxes, mSensitivityRotationAroundEarth * xOffset, mAxisRotationAroundEarthY);
    allAxes = glm::rotate(allAxes, mSensitivityRotationAroundEarth * yOffset, mAxisRotationAroundEarthZ);

    mAllAxes = glm::rotate(mAllAxes, mSensitivityRotationAroundEarth * xOffset, mAxisRotationAroundEarthY);
    mAllAxes = glm::rotate(mAllAxes, mSensitivityRotationAroundEarth * yOffset, mAxisRotationAroundEarthZ);

    mLastPos = glm::vec3(allAxes * glm::vec4(mLastPos, 1.0f));
    mEye = glm::vec3(allAxes * glm::vec4(mEye, 1.0f));
    mCameraUp = glm::vec3(allAxes * glm::vec4(mCameraUp, 1.0f));
    mCenter = glm::vec3(allAxes * glm::vec4(mCenter, 1.0f));
    mPointRotateAround = glm::vec3(allAxes * glm::vec4(mPointRotateAround, 1.0f));
}

/*-----------------------------------------------

Name:	moveCameraWithKey

Params:	key - pressed key

Result:	if pressed UP, DOWN . . . camera will
        move to this way.

-----------------------------------------------*/

void Camera::moveCameraWithKey(int key, GLfloat speed)
{
    switch (key)
    {
        case Qt::Key_Left:
            rotateCameraAroundEarth(speed, 0.0f);
            break;
        case Qt::Key_Right:
            rotateCameraAroundEarth(-speed, 0.0f);
            break;
        case Qt::Key_Up:
            rotateCameraAroundEarth(0.0f, speed);
            break;
        case Qt::Key_Down:
            rotateCameraAroundEarth(0.0f, -speed);
            break;
    }
}

/*-----------------------------------------------

Name:	zoom

Params:	value - zoom value

Result:	Zoom to Point

-----------------------------------------------*/

void Camera::zoom(int value)
{
    stopAllMoves();
    glm::vec3 tmpVec = mEye - mCenter;
    glm::vec3 tmpPoint = glm::normalize(mEye) * GeoHelper::MINOR_AXIS / glm::vec3(100);
    mZoomSensitivity = glm::distance(mEye, tmpPoint)/(10.0f - SettingsClass->getCameraSpeed());
    tmpVec = glm::normalize(tmpVec);
    glm::vec3 tmpCamera = mEye;

    if (value > 0){
            tmpCamera -= tmpVec * mZoomSensitivity;
        }else{
            tmpCamera += tmpVec * mZoomSensitivity;
    }

    glm::vec3 tmpVec1 = tmpCamera - mCenter;

    if(glm::length(tmpVec * mZoomSensitivity) > glm::length(tmpVec1))
    {
        mCenter -= tmpVec * mZoomSensitivity;
        return;

    }

    GLfloat distanceCameraToCenter = glm::distance(tmpCamera, glm::vec3(0));
    if(!isPointInEllipse(GeoHelper::MAJOR_AXIS, GeoHelper::MINOR_AXIS, tmpCamera * glm::vec3(100))
       && distanceCameraToCenter < 150000 )
    {
        mEye = tmpCamera;
    }
    mSensitivityRotationAroundEarth = mZoomSensitivity/(200000 * SettingsClass->getCameraSpeed());
}

/*-----------------------------------------------

Name:	isPointInEllipse

Params:	bigAxle -
        smallAxle
        checkPoint

Result:	Checking is that point in ellipsw or not. true - yes, Point is in ellipse, false - no.

-----------------------------------------------*/

GLboolean Camera::isPointInEllipse(GLfloat bigAxle, GLfloat smallAxle, glm::vec3 checkPoint)
{
    //Большая и малые полуоси
    GLfloat a = bigAxle, b = smallAxle;
    //Эксцентриситет
    GLfloat e = sqrt(1 - b / a);
    //Фокальное расстояние
    GLfloat c = a * e;
    //Центр эллипса
    glm::vec3 center = glm::vec3(0,0,0);
    //Фокусы
    glm::vec3 f1 = glm::vec3(center.x - c, center.y, 0), f2 = glm::vec3(center.x + c, center.y, 0);
    //Расстояния от точки до фокусов
    GLdouble d1 = glm::distance(f1, checkPoint);
    GLdouble d2 = glm::distance(f2, checkPoint);

    if (round(d1 + d2) > round(2 * a))
    {
        return false;
    }
    else
    {
        if(round(d1 + d2) < round(2 * a)) return true;
    }
}

/*-----------------------------------------------

Name:	setCameraSpeed

Params:	x - value to add to camera Speed

Result:	change camera speed.

-----------------------------------------------*/

void Camera::setCameraSpeed(GLfloat x)
{
    mCameraSpeed = 50*(x+1);
}

void Camera::setCenter(glm::vec3 center)
{
    mCenter = center;
}

void Camera::setEye(glm::vec3 newEyePosition)
{
    mEye = newEyePosition;
}

void Camera::setCameraUp(glm::vec3 newCameraUp)
{
    mCameraUp = newCameraUp;
}

void Camera::setPointRotateAround(glm::vec3 &newPoint)
{
    if (newPoint == newPoint) // Check "NULL" point
    {
        mPointRotateAround = newPoint;
    }
}

void Camera::setPointObjectRotateAround(glm::vec3 &newPoint)
{
    if (newPoint == newPoint) // Check "NULL" point
    {
        mPointObjectRotateAround = newPoint;
    }
}

/*-----------------------------------------------

Name:	getLookAt

Params:	none

Result:	returns current LookAt matrix.

-----------------------------------------------*/

glm::mat4 Camera::getLookAt()
{
    return glm::lookAt(
        mEye,
        mCenter,
        mCameraUp);
}

/*-----------------------------------------------

Name:	stopAllMoves

Params:	none

Result:	reset all flags -> stops all Moves with timer.

-----------------------------------------------*/

void Camera::stopAllMoves()
{
    mFlagRotationToPoint = false;
    mFlagRotationToTheNorth = false;
    mFlagRotationVertical = false;
    mActionTimer.stop();
    mFlagRotationAroundEarth = STOP;
}

void Camera::startRotationAroundEarth(ButtonPressedType button)
{
    mActionTimer.start();
    mFlagRotationAroundEarth = button;
}

glm::vec3 Camera::getEye()
{
    return mEye;
}

glm::vec3 Camera::getCenter()
{
    return mCenter;
}

glm::vec3 Camera::getCameraUp()
{
    return mCameraUp;
}

glm::vec3 Camera::getPointRotateAround()
{
    return mPointRotateAround * glm::vec3(100);
}

glm::vec3 Camera::getLastPos()
{
    return mLastPos;
}

void Camera::setLastPos(glm::vec3 &value)
{
    mLastPos = value;
}

GLdouble Camera::getVerticalCameraAngle()
{
    return mAngleVerticalCamera;
}

glm::vec3 Camera::getAxisRotationAroundEarthZ()
{
    return mAxisRotationAroundEarthZ;
}
