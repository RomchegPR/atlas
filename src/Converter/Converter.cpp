
#include "include/Converter/Converter.h"
#include <QStringList>

Converter::Converter()
{
}

Converter::Converter(Parser &parser)
{
    fill(parser.getPoints());
}

// Заполнение исходных данных из парсера

void Converter::fill(QMap <QString, QVector<ProcedureData > > &procedures)
{
    mOriginalProcedures = procedures;
    QVector<QString> mapIndices1;
    mapIndices1 << "Waypoint ID" << "Waypoint" << "P/T" << "Fly Over" << "Course" << "Turn Direction" << "Altitude" << "Dist/Time" << "Speed Limit";
    // 0 - name, 1 - Waypoint, 2 - pathTerminated, 3 - flyOver, 4 - course, 5 - turnDirection, 6 - altitude, 7 - distPerTime, 8 - speedLimit;
    QVector<QString> mapIndices2;
    QList<QString> keys = procedures.keys();
    mapIndices2 << "Waypoint ID" << "Latitude" << "Longitude" << "P/T" << "Fly Over" << "Course" << "Turn Direction" << "Min Altitude" << "Max Altitude" << "Dist/Time" << "Speed Limit" << "Z";
    // 0 - name, 1 - latitude, 2 - longitude, 3 - pathTerminated, 4 - flyOver, 5 - course, 6 - turnDirection, 7 - min altitude, 8 - max altitude, 9 - distPerTime, 10 - speedLimit;
    for(int key = 0; key < keys.size(); key++){
        for(int i = 0; i < mOriginalProcedures[keys[key]].size(); i++){
            QString procedureName = mOriginalProcedures[keys[key]][i].name;
            QVector<QMap<QString, QString> > tmpPoints;
            for(int j = 0; j < mOriginalProcedures[keys[key]][i].points.size(); j++){
                QMap<QString, QString> tmpMap;
                for(int k1 = 0, k2 = 0; k2 < mapIndices2.size() - 1; k1++, k2++){
                    if(k2 == 1){
                        if(mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]] == ""){
                            tmpMap[mapIndices2[k2++]] = mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]];
                            tmpMap[mapIndices2[k2]] = mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]];
                        }
                        else{
                            QStringList tmp = splitWP(mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]]);
                            tmpMap[mapIndices2[k2++]] = tmp[0];
                            tmpMap[mapIndices2[k2]] = tmp[1];
                        }
                    }
                    else if( k2 == 7){
                        if(mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]].contains("By ATC")){
                            tmpMap[mapIndices2[k2++]] = tmpPoints[tmpPoints.size() - 1]["Min Altitude"];
                            tmpMap[mapIndices2[k2]] = tmpPoints[tmpPoints.size() - 1]["Max Altitude"];
                        }
                        else{
                            QStringList tmp = splitAlt(mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]]);

                            tmpMap[mapIndices2[k2++]] = tmp[0];
                            tmpMap[mapIndices2[k2]] = tmp[1];
                        }
                    }
                    else if (k2 == 10){
                        tmpMap[mapIndices2[k2]] = mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]].toLower().remove('k').remove('-').replace(',','.');
                    }
                    else{
                        tmpMap[mapIndices2[k2]] = mOriginalProcedures[keys[key]][i].points[j][mapIndices1[k1]];
                    }
                }
                tmpPoints.push_back(tmpMap);

            }

            mProcedures[keys[key]].push_back({procedureName, tmpPoints});
        }
    }
}

double Converter::calculateGradient(QString type, int procIdx, int pointIdx, QString alt)
{
    int pointIdx2 = 0;
    for(int i = pointIdx + 1; i < mProcedures[type][procIdx].points.size(); i++){
        if(mProcedures[type][procIdx].points[i][alt] != ""){
            pointIdx2 = i;
            break;
        }
    }
    if(pointIdx2 == 0) return 0.1;
    double distance = 0;
    QString tmpAlt = mProcedures[type][procIdx].points[pointIdx2][alt];
    QString tmpAlt2 = mProcedures[type][procIdx].points[pointIdx - 1][alt];
    double deltaHeight = GeoHelper::ft2metres(tmpAlt.remove("-").remove("+").toDouble() - tmpAlt2.remove("-").remove("+").toDouble());;
    double azi = 0;
    //for(int i = pointIdx - 1; i < pointIdx2 - 1; i++){
    distance += GeoHelper::inverseProblem(GeoHelper::dms2rad(mProcedures[type][procIdx].points[pointIdx - 1]["Latitude"]),
            GeoHelper::dms2rad(mProcedures[type][procIdx].points[pointIdx - 1]["Longitude"]),
            GeoHelper::dms2rad(mProcedures[type][procIdx].points[pointIdx2]["Latitude"]),
            GeoHelper::dms2rad(mProcedures[type][procIdx].points[pointIdx2]["Longitude"]),
            azi) * GeoHelper::MAJOR_AXIS / 1000.0f;
    //}
    return deltaHeight/(distance * 1000);
}

// Разделение строки с координатами на 2 строки

QStringList Converter::splitWP(QString& wp)
{
    QStringList coords = wp.split('/');
    coords[0] = coords[0].remove('N');
    coords[1] = coords[1].remove('E');
    return coords;
}

QMap <QString, QVector<ProcedureData > >& Converter::getPoints()
{
    return mProcedures;
}

// Конвертирование высоты, на вход подается тип процедуры.

void Converter::convertAlt(QString type)
{
    for(int i = 0; i < mProcedures[type].size(); i++){
        for(int j = 0; j < mProcedures[type][i].points.size(); j++){
            QString min = mProcedures[type][i].points[j]["Min Altitude"];
            QString max = mProcedures[type][i].points[j]["Max Altitude"];
            if(type == "STAR" && j == 0 && max == ""){
                max = QString::number(100 * 270);
            }
            if(type == "SID" && j == 0) {
                if(min.contains('+')){
                    max = min.remove('+');
                }
            }
            if(type == "APRCH" && j == 0){
                double maxAlt = 0;
                for(int i1 = 0; i1 < mProcedures["STAR"].size(); i1++){
                    if(mProcedures["STAR"][i1].points[mProcedures["STAR"][i1].points.size() - 1]["Waypoint ID"] == mProcedures[type][i].points[j]["Waypoint ID"]){
                        maxAlt = mProcedures["STAR"][i].points[mProcedures["STAR"][i].points.size() - 1]["Max Altitude"].toDouble();
                    }
                }
                max = QString::number(maxAlt);
            }

            double grad = 0;
            if(min.contains('+')){
                min = min.remove('+');
                if(max == ""){
                    double azi = 0;
                    grad = calculateGradient(type, i, j, QString("Max Altitude"));
                    double distance = GeoHelper::inverseProblem(GeoHelper::dms2rad(mProcedures[type][i].points[j - 1]["Latitude"]),
                            GeoHelper::dms2rad(mProcedures[type][i].points[j - 1]["Longitude"]),
                            GeoHelper::dms2rad(mProcedures[type][i].points[j]["Latitude"]),
                            GeoHelper::dms2rad(mProcedures[type][i].points[j]["Longitude"]),
                            azi) * GeoHelper::MAJOR_AXIS / 1000.0f;

                    double deltaH = distance * grad * 1000 / 0.3048;
                    max = QString::number(mProcedures[type][i].points[j - 1]["Max Altitude"].toDouble() + deltaH);

                    if(max.toDouble() < min.toDouble() || max.toDouble() - min.toDouble() < 50.0f){
                        grad = 0.1;
                        deltaH = distance * grad * 1000 / 0.3048;
                        max = QString::number(mProcedures[type][i].points[j - 1]["Max Altitude"].toDouble() + deltaH);

                        float ind = j + 1;
                        for(int k = ind; k < mProcedures[type][i].points.size(); k++){
                            if(mProcedures[type][i].points[k]["Max Altitude"] != ""){
                                ind = k;
                                break;
                            }
                        }
                        if(max.toDouble() > mProcedures[type][i].points[ind]["Max Altitude"].toDouble()){
                            max = mProcedures[type][i].points[ind]["Max Altitude"];
                        }
                        if(max.toDouble() < min.toDouble()){
                            max = QString::number( min.toFloat() + 250.0f);

                        }
                        //max = min;
                    }
                }
            }
            if(max.contains('-')){
                max = max.remove('-');
                if(min == ""){
                    double azi = 0;
                    grad = calculateGradient(type, i, j, QString("Min Altitude"));
                    double distance = GeoHelper::inverseProblem(GeoHelper::dms2rad(mProcedures[type][i].points[j]["Latitude"]),
                            GeoHelper::dms2rad(mProcedures[type][i].points[j]["Longitude"]),
                            GeoHelper::dms2rad(mProcedures[type][i].points[j-1]["Latitude"]),
                            GeoHelper::dms2rad(mProcedures[type][i].points[j-1]["Longitude"]),
                            azi) * GeoHelper::MAJOR_AXIS / 1000.0f;
                    double deltaH = distance * grad * 1000 / 0.3048;
                    min = QString::number(mProcedures[type][i].points[j - 1]["Min Altitude"].toDouble() + deltaH);
                    if(max.toDouble() < min.toDouble()){
                        min = QString::number( max.toFloat() - 250.0f);
                    }
                }
            }

            if( max == min ){
                min = QString::number( min.toFloat() - 250.0f);
                max = QString::number( max.toFloat() + 250.0f);
            }

            mProcedures[type][i].points[j]["Min Altitude"] = min;
            mProcedures[type][i].points[j]["Max Altitude"] = max;

        }
    }
}

// Разделение строки с высотами на 2 строки (минимальную и максимальную)

QStringList Converter::splitAlt(QString &alt)
{
    QStringList tmp;
    if(alt == ""){
        tmp.push_back("0");
        tmp.push_back("630");
    }
    if(alt.contains('-')){
        tmp = alt.split('-');
        if(tmp[0] == "" || tmp[1] == ""){
            tmp[0] = "";
            tmp[1] = alt;
        }
    }
    else{
        if(!alt.contains("+")){
            if(alt.contains("FL")) alt = QString::number(alt.remove("FL").toDouble() * 100);
            tmp.append(QString::number(alt.toDouble() - 250));
            tmp.append(QString::number(alt.toDouble() + 250));
        }
        else{
            tmp.append(alt);
            tmp.append("");
        }
    }
    for(int i = 0; i < tmp.size(); i++){
        QString sign = "";
        if(tmp[i].contains("FL")){
            if(tmp[i] != ""){
                if(tmp[i].contains('-')) sign = "-";
                if(tmp[i].contains('+')) sign = "+";
                tmp[i] = QString::number(tmp[i].remove("FL").remove(sign).toDouble() * 100) + sign;
            }
        }
    }
    return tmp;
}

void Converter::clear()
{
    mOriginalProcedures.clear();
    mProcedures.clear();
    mFileName = "";
}

QMap<QString, QVector<QVector<Waypoint> > > Converter::getStruct()
{
    QMap <QString, QVector<QVector <Waypoint> > > tmpMap;
    QList<QString> keys = mProcedures.keys();

    for(int key = 0; key < keys.size(); key++){

        tmpMap[keys[key]].resize(mProcedures[keys[key]].size());

        for(int i = 0; i < tmpMap[keys[key]].size(); i++){

            for(int j = 0; j < mProcedures[keys[key]][i].points.size(); j++){

                double x = mProcedures[keys[key]][i].points[j]["Latitude"].toDouble();
                double y = mProcedures[keys[key]][i].points[j]["Longitude"].toDouble();
                double z = mProcedures[keys[key]][i].points[j]["z"].toDouble();
                double minAlt = mProcedures[keys[key]][i].points[j]["Min Altitude"].toDouble();
                double maxAlt = mProcedures[keys[key]][i].points[j]["Max Altitude"].toDouble();
                QString procedureName = mProcedures[keys[key]][i].name;
                QString pointName = mProcedures[keys[key]][i].points[j]["Waypoint ID"];
                double course = mProcedures[keys[key]][i].points[j]["Course"].replace(',','.').toDouble();
                double speed = mProcedures[keys[key]][i].points[j]["Speed Limit"].toDouble();
                GLboolean flyOver = (mProcedures[keys[key]][i].points[j]["Fly Over"] == "Y") ? true : false;
                QStringList point = splitWP(mOriginalProcedures[keys[key]][i].points[j]["Waypoint"]);
                double lat = GeoHelper::dms2rad(point[0]);
                double lon = GeoHelper::dms2rad(point[1]);
                tmpMap[keys[key]][i].append({x, y, z, minAlt, maxAlt, course, speed, lat, lon, flyOver, procedureName, pointName});

            }
        }
    }
    return tmpMap;
}

// Метод, в котором порисходит конвертирование исходных данных

void Converter::convert()
{
    convertAlt("STAR");
    convertAlt("SID");
    convertAlt("APRCH");
    QList<QString> keys = mProcedures.keys();
    for(int key = 0; key < keys.size(); key++){
        for(int i = 0; i < mProcedures[keys[key]].size(); i++){
            for(int j = 0; j < mProcedures[keys[key]][i].points.size(); j++){
                glm::vec3 point = GeoHelper::geo2cartesian(mProcedures[keys[key]][i].points[j]["Latitude"], mProcedures[keys[key]][i].points[j]["Longitude"]);
                point = glm::vec3(-point.x, point.z, point.y);
                mProcedures[keys[key]][i].points[j]["Latitude"] = QString::number(point[0], 'g', 20);
                mProcedures[keys[key]][i].points[j]["Longitude"] = QString::number(point[1], 'g', 20);
                mProcedures[keys[key]][i].points[j]["Min Altitude"] = QString::number(GeoHelper::ft2metres(mProcedures[keys[key]][i].points[j]["Min Altitude"]), 'g', 20);
                mProcedures[keys[key]][i].points[j]["Max Altitude"] = QString::number(GeoHelper::ft2metres(mProcedures[keys[key]][i].points[j]["Max Altitude"]), 'g', 20);
                mProcedures[keys[key]][i].points[j]["z"] = QString::number(point[2], 'g', 20);
            }
        }
    }
}

void Converter::print(QString type)
{
    for(int k = 0; k < mProcedures[type].size(); k++){
        for(QVector<QMap<QString, QString> >::iterator i = mProcedures[type][k].points.begin(); i != mProcedures[type][k].points.end(); i++){
            QMap<QString, QString>::const_iterator j = i->constBegin();
            while(j != i->constEnd()){
                j++;
            }
        }
    }
}

void Converter::print(QString type, int k)
{
    for(QVector<QMap<QString, QString> >::iterator i = mProcedures[type][k].points.begin(); i != mProcedures[type][k].points.end(); i++){
        QMap<QString, QString>::const_iterator j = i->constBegin();
        while(j != i->constEnd()){
            j++;
        }
    }
}

Converter::~Converter()
{
    clear();
}
