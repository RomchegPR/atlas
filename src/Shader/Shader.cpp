#include "include/Shader/Shader.h"

#include <QFile>
#include <QDebug>

Shader::Shader( Type type ):
	mID		( 0 ),
	mType	( type )
{
}

Shader::~Shader()
{
}

bool Shader::create( )
{
	mID = glCreateShader( mType );
	return bool( glIsShader( mID ) );
}

bool Shader::create( QString fileName )
{
	return create( ) && compileSourceFile( fileName );
}

void Shader::remove()
{
	if ( glIsShader( mID ) ){
		glDeleteShader( mID );
		mID = 0;
	}
}

bool Shader::compileSourceFile( QString fileName )
{
	QFile file( fileName);
	file.open(QIODevice::ReadOnly);

	if ( !file.isOpen( ) ){
		QString errMsg = QString( "Can not open shader source file: %1" ).arg( fileName );
//		qCritical( errMsg.toAscii( ) );
		return 0;
	}

	QByteArray data = file.readAll( );
	char* dataPtr = data.data( );

	glShaderSource( mID, 1, (const GLchar**)&dataPtr, NULL );
	glCompileShader( mID );

	GLint compiled = 0;
	glGetShaderiv( mID, GL_COMPILE_STATUS, &compiled );

	mCompiled = ( compiled == GL_TRUE );

	if ( !mCompiled ){
		GLint length;
		GLchar* log;
		glGetShaderiv( mID, GL_INFO_LOG_LENGTH, &length );
		log = new char[length];
		glGetShaderInfoLog( mID, length, &length, log );
		QString errMsg = QString( "Error while compiling shader: \n %1" ).arg( log );
//		qCritical( errMsg.toAscii() );
		delete[] log;
	}
	return mCompiled;
}

bool Shader::isCompiled() const
{
	return mCompiled;
}

Shader::Type Shader::type() const
{
	return mType;
}

GLuint Shader::id() const
{
	return mID;
}

