#include "include/Shader/ShaderProgram.h"

#include <QFile>
#include <QDebug>
#include <QMatrix4x4>
#include "glm/gtc/type_ptr.hpp"
#include <functional>

ShaderProgram::ShaderProgram():
	mId( 0 ),
	mLinked( false )
{
}

ShaderProgram::~ShaderProgram( )
{
}

GLuint ShaderProgram::programId()
{
	return mId;
}

bool ShaderProgram::create( )
{
	// return false is already created
	if ( glIsProgram( mId ) )
		return false;

    mPrivateShaders.clear();

	mId = glCreateProgram( );
	mLinked = false;

	return ( glIsProgram( mId ) == GL_TRUE );
}

void ShaderDeleteHelper( Shader* s )
{
	s->remove();
	delete s;
}

bool ShaderProgram::create( QString vertexShaderSourceFile, QString fragmentShaderSourceFile )
{
//	 return false is already created
    if ( glIsProgram( mId ) )
        return false;

	if ( !create( ) )
		return false;


    boost::shared_ptr<Shader> vs( new Shader( Shader::Vertex   ), &ShaderDeleteHelper );
    boost::shared_ptr<Shader> fs( new Shader( Shader::Fragment ), &ShaderDeleteHelper );
	if (  vs->create( vertexShaderSourceFile   ) &&
		  fs->create( fragmentShaderSourceFile ) ){
		addShader( vs.get() );
		addShader( fs.get() );
		mPrivateShaders.clear();
		mPrivateShaders.push_back( vs );
		mPrivateShaders.push_back( fs );
		if ( link() ){
			return true;
		}
	}
	glDeleteProgram ( mId );
	mId = 0;
	return false;
}

void ShaderProgram::remove()
{
	glDeleteProgram( mId );
	mPrivateShaders.clear();
}

void ShaderProgram::addShader( Shader *shader )
{
	glAttachShader( mId, shader->id() );
	mLinked = false;
}

void ShaderProgram::detachShader( Shader* shader )
{
	mLinked = false;
	if ( glIsShader( shader->id() ) )
		glDetachShader( mId, shader->id() );
}


void ShaderProgram::detachShader( )
{
    mLinked = false;
//    for (int i = 0; i < mPrivateShaders.size(); i++){

//        if ( glIsShader( mPrivateShaders.->id() ) )
//            glDetachShader( mId, shader->id() );
//    }
}


bool ShaderProgram::link()
{
	if ( mLinked )
		return mLinked;

	GLint linked;
	glLinkProgram(mId);
	glGetProgramiv(mId, GL_LINK_STATUS, &linked );
	if ( !linked ) {
		GLint length;
		GLchar* log;
		glGetProgramiv(mId, GL_INFO_LOG_LENGTH, &length );
		log = new char[length];
		glGetProgramInfoLog(mId, length, &length, log);
//		std::cerr << "Error: " << log << std::endl;
		QString errMsg = QString("Error while linking shader program: \n %1").arg(log);
		delete[] log;
//		qCritical( errMsg.toAscii() );
	}

	mLinked = static_cast<bool>(linked);

	return ( mLinked );
}

bool ShaderProgram::isLinked( )
{
	return mLinked;
}

void ShaderProgram::bind()
{
	glUseProgram( mId );
}

void ShaderProgram::unbind()
{
	glUseProgram( 0 );
}

void ShaderProgram::setUniformValue( const char* name, int value )
{
	GLint loc = uniformLocation( name );

	if ( loc < 0)
		return;

	glUniform1i( loc, value );
}

void ShaderProgram::setUniformValue( const char* name, float value)
{
	GLint loc = uniformLocation( name );

	if ( loc < 0 )
		return;

	glUniform1f( loc, value );
}

void ShaderProgram::setUniformArrayfv( const char* name, int size, const float *data )
{
	GLint loc = uniformLocation( name );

	if ( loc < 0)
		return;

	switch ( size ){
	case 4:
		glUniform4fv( loc, 1, data );
		break;
	case 3:
		glUniform3fv( loc, 1, data );
		break;
	case 2:
		glUniform2fv( loc, 1, data);
		break;
	default:
		glUniform1fv( loc, size, data );
	}
}

void ShaderProgram::setUniformArrayf( const char* name, int size, std::vector <float> *data)
{
    GLint loc = uniformLocation( name );

    if ( loc < 0)
        return;

    switch ( size ){
    case 4:
        glUniform4f( loc, data->at(0), data->at(1), data->at(2), data->at(3));
        break;
    case 3:
        glUniform3f( loc, data->at(0), data->at(1), data->at(2) );
        break;
    case 2:
        glUniform2f( loc, data->at(0), data->at(1));
        break;
    case 1:
        glUniform1f( loc, data->at(0));
        break;
    }
}

void ShaderProgram::setUniformArray3f( const char* name,  glm::vec3 data)
{
    GLint loc = uniformLocation( name );

    if ( loc < 0)
        return;

    glUniform3f( loc, data.x, data.y, data.z );

}

void ShaderProgram::setUniformMatrix( const char* name, const QMatrix4x4& m )
{
    // Transform data from matrix from qreal to GL_FLOAT
//    GLfloat* floatData;
//    if ( sizeof(qreal) == sizeof(GLfloat) ){
//        floatData = (GLfloat*)m.constData();
//    }else{
//        const qreal* qrealData = m.constData();
//        floatData = new GLfloat[16];
//        for (int i = 0; i < 16; i++)
//        {
//            floatData[i] = static_cast<GLfloat>(qrealData[i]);
//        }
//    }

//    GLint loc = uniformLocation(name);

//    if ( loc >= 0)
//        glUniformMatrix4fv(loc, 1, GL_FALSE, floatData);

//    if ( sizeof(qreal) != sizeof(GLfloat) )
//    {
//        delete[] floatData;
//    }
}

void ShaderProgram::setUniformMatrix( const char* name, const glm::mat4 m )
{
//    // Transform data from matrix from qreal to GL_FLOAT
//    GLfloat* floatData;
//    if ( sizeof(qreal) == sizeof(GLfloat) ){
//        floatData = (GLfloat*)m.constData();
//    }else{
//        const qreal* qrealData = m.constData();
//        floatData = new GLfloat[16];
//        for (int i = 0; i < 16; i++)
//        {
//            floatData[i] = static_cast<GLfloat>(qrealData[i]);
//        }
//    }

    GLint loc = uniformLocation(name);

    if ( loc >= 0)
        glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m));

//    if ( sizeof(qreal) != sizeof(GLfloat) )
//    {
//        delete[] floatData;
//    }
}

GLint ShaderProgram::uniformLocation( const char *name )
{
	if ( !mLinked ){
		qWarning( "Program is not linked" );
		return -1;
	}
	GLint loc = glGetUniformLocation( mId, name );
	if ( -1 == loc ){
		QString str = QString("Can not find uniform location named %1").arg(name);
        qDebug( str.toLatin1() );
		return -1;
	}
	return loc;
}

GLint ShaderProgram::attributeLocation( const char* name )
{
	if (!mLinked){
		qWarning("Program is not linked");
		return -1;
	}
	GLint loc = glGetAttribLocation( mId, name );
	if ( -1 == loc ){
		QString str = QString("Can not find attribute location named %1").arg( name );
        qDebug( str.toLatin1() );
		return -1;
	}
	return loc;
}

void ShaderProgram::bindAttribLocation( const char *name, GLuint location )
{
	glBindAttribLocation( mId, location, name );
}
