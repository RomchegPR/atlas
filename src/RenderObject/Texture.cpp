#include "include/RenderObject/Texture.h"

Texture::Texture()
{

}

Texture::~Texture()
{

}

GLuint Texture::id() const
{
    return mID;
}

bool Texture::create( Target target)
{
    glGenTextures( 1, &mID );
    mTarget = target;
    bind();
    unbind();
    return ( glIsTexture( mID ) == GL_TRUE );
}

Texture::Target Texture::target()
{
    return mTarget;
}

void Texture::bind()
{
    glBindTexture( mTarget, mID );
}

void Texture::unbind()
{
    glBindTexture( mTarget, 0 );
}

void Texture::unbind( Target target)
{
    glBindTexture( target, 0 );
}

void Texture::setImage2D( const QImage& img )
{
    InputData::Format inputFormat;
    InputData::Type inputType;

    switch ( img.format() ){
    case QImage::Format_Invalid:
    case QImage::Format_ARGB32:
        inputFormat = InputData::RGBA;
        inputType = InputData::dtUnsignedByte;
    break;
    case QImage::Format_RGB32:
        inputFormat = InputData::BGRA;
        inputType = InputData::dtUnsignedByte;
    break;
    case QImage::Format_RGB16:
        inputFormat = InputData::RGB;
        inputType = InputData::dtUnsignedShort565;
    break;

    case QImage::Format_RGB555:
        inputFormat = InputData::RGB;
        inputType = InputData::dtUnsignedShort5551;
    break;

    case QImage::Format_RGB888:
        inputFormat = InputData::RGB;
        inputType = InputData::dtUnsignedInt8888;
    break;

    case QImage::Format_RGB444:
        inputFormat = InputData::RGB;
        inputType = InputData::dtUnsignedShort4444;
    break;

    case QImage::Format_Mono:
    case QImage::Format_MonoLSB:
    case QImage::Format_Indexed8:
    case QImage::Format_ARGB32_Premultiplied:
    case QImage::Format_ARGB8565_Premultiplied:
    case QImage::Format_RGB666:
    case QImage::Format_ARGB6666_Premultiplied:
    case QImage::Format_ARGB8555_Premultiplied:
    case QImage::Format_ARGB4444_Premultiplied:
        throw "Unsupported image format!";
    break;
    default:
        throw "Unknown image format!";
    break;
    }

    glTexImage2D( mTarget, 0, GL_RGB, img.width(), img.height(), 0, inputFormat, inputType, img.bits() );
}

void Texture::setImage2D( TextureFormat format, int width, int height, InputData::Format inputFormat, InputData::Type inputType, uchar *data, int miplevel, int border )
{
    glTexImage2D( mTarget, miplevel, format, width, height, border, inputFormat, inputType, data );
}

void Texture::setImage1D( Texture::TextureFormat format, int width, InputData::Format inputFormat, InputData::Type inputType, uchar *data, int miplevel )
{
    glTexImage1D( mTarget, miplevel, format, width, 0, inputFormat, inputType, data );
}

void Texture::setCubeMap( Texture::CubeMapSide side, Texture::TextureFormat format, int width, int height, InputData::Format inputFormat, InputData::Type inputType, uchar *data, int miplevel, int border )
{
    glTexImage2D( side, miplevel, format, width, height, border, inputFormat, inputType, data );
}

void Texture::setFilter(FilterMode min, FilterMode mag)
{
    glTexParameteri( mTarget, GL_TEXTURE_MIN_FILTER, min );
    glTexParameteri( mTarget, GL_TEXTURE_MAG_FILTER, mag );
}

void Texture::setWrap( WrapMode s )
{
    glTexParameteri( mTarget, GL_TEXTURE_WRAP_S, s );
}

void Texture::setWrap( WrapMode s, WrapMode t)
{
    glTexParameteri( mTarget, GL_TEXTURE_WRAP_S, s );
    glTexParameteri( mTarget, GL_TEXTURE_WRAP_T, t );

}

void Texture::setWrap( WrapMode s, WrapMode t, WrapMode r )
{
    glTexParameteri( mTarget, GL_TEXTURE_WRAP_S, s );
    glTexParameteri( mTarget, GL_TEXTURE_WRAP_T, t );
    glTexParameteri( mTarget, GL_TEXTURE_WRAP_R, r );
}

void Texture::setBaseLevel(int level)
{
    glTexParameteri( mTarget, GL_TEXTURE_BASE_LEVEL, level );
}

void Texture::setMaxLevel(int level)
{
    glTexParameteri( mTarget, GL_TEXTURE_MAX_LEVEL, level );
}

void Texture::generateMipmaps()
{
    glGenerateMipmap( mTarget );
}

void Texture::copyTexImage2D( TextureFormat format, int x, int y, int width, int height, int miplevel)
{
    glCopyTexImage2D( mTarget, miplevel, format, x, y, width, height, 0 );
}


void Texture::remove()
{
    glDeleteTextures( 1, &mID );
    mID = 0;
}
