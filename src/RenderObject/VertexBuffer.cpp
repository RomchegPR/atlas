#include "include/RenderObject/VertexBuffer.h"
#include "MainRenderModel.h"

VertexBuffer::VertexBuffer(Type type, UsagePattern usage):
    mId(0),
    mOk(false),
    mTarget( type ),
    mUsagePattern( usage )
{
}

void VertexBuffer::init(Type type, UsagePattern usage){

    mId = 0;
    mOk = false;
    mTarget = type;
    mUsagePattern = usage;
}

VertexBuffer::~VertexBuffer()
{
}

bool VertexBuffer::create()
{
    if (glIsBuffer(mId) == GL_TRUE)
        return true;

    glGenBuffers(1, &mId);
    glBindBuffer(mTarget, mId);
    mOk = (glIsBuffer(mId) == GL_TRUE);
    glBindBuffer( mTarget, 0 );
    return mOk;
}

bool VertexBuffer::isCreated()
{
    return mOk;
}

void VertexBuffer::remove()
{
    if ( glIsBuffer(mId) ){
        glDeleteBuffers(1, &mId);
        mOk = false;
    }
}

GLuint VertexBuffer::getID()
{
    return mId;
}

void VertexBuffer::bind()
{
    glBindBuffer(mTarget, mId);
}

void VertexBuffer::unbind()
{
    glBindBuffer(mTarget, 0);
}

void VertexBuffer::setData( ptrdiff_t size, const void* ptr )
{
    glBufferData( mTarget, size, ptr, mUsagePattern );
}

void VertexBuffer::clear()
{
    glBufferData( mTarget, 0, NULL, 0 );
}

void VertexBuffer::setSubData( ptrdiff_t offset, ptrdiff_t size, const void* ptr)
{
    glBufferSubData(mTarget, offset, size, ptr);
}

void VertexBuffer::getSubData( ptrdiff_t offset, ptrdiff_t size, void* ptr )
{
    glGetBufferSubData(mTarget, offset, size, ptr);
}

void* VertexBuffer::map( AccessMode access )
{
    return glMapBuffer( mTarget, access );
}

bool VertexBuffer::unmap()
{
    return (glUnmapBuffer(mTarget) == GL_TRUE);
}

int VertexBuffer::size()
{
    int value = 0;
    glGetBufferParameteriv( mTarget, GL_BUFFER_SIZE, &value );
    return value;
}


