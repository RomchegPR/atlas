#include "include/RenderObject/VertexArray.h"

VertexArray::VertexArray():
    mID( 0 )
{
}

VertexArray::~VertexArray()
{
}

bool VertexArray::create()
{
    if (glIsVertexArray(mID))
        return true;

    glGenVertexArrays ( 1, &mID );
    bind();
    unbind();
    return isCreated( );
}

bool VertexArray::isCreated()
{
    return ( glIsVertexArray ( mID ) == GL_TRUE );
}

GLuint VertexArray::getID()
{
    return mID;
}

void VertexArray::remove()
{
    glDeleteVertexArrays ( 1, &mID );
}

void VertexArray::bind()
{
    glBindVertexArray( mID );
}

void VertexArray::unbind()
{
    glBindVertexArray( 0 );
}

