#define _USE_MATH_DEFINES
#include <cmath>

#include "include/Helper.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <QVector>
#include "glm/glm.hpp"
#include "GL/glew.h"


Helper::Line3D::Line3D(glm::vec3 start, glm::vec3 end){

    x1 = start.x;
    y1 = start.y;
    z1 = start.z;

    a = end.x - start.x;
    b = end.y - start.y;
    c = end.z - start.z;

}

double Helper::dist(glm::vec3 a, glm::vec3 b){
    return std::sqrt(std::pow(b.z - a.z, 2) + std::pow(b.x - a.x, 2) + std::pow(b.y - a.y, 2));
}

double cosec(double alpha){

    return 1/sin(alpha);
}

double ctg(double alpha){
    return 1/tan(alpha);
}

double  Helper::DegreeToRadians(double degree){
    return degree*M_PI/180.0f;
}

double RadiansToDegree(double radian){
    return radian*180.0f/M_PI;
}

double meterToFoot(double meter){
    return meter*0.304;
}

double kt_hToKm_H(double kt_h){
    return kt_h*0.53;
}

double Helper::Km_HTokt_h(double km_h){
    return km_h*1.8519f;
}

double Helper::calcLUR(Waypoint wp1, Waypoint wp2, Waypoint wp3, ProcedureType type, double r){ // type  - aprch, sid

    double latitudeWP1 = wp1.lat; //phi
    double latitudeWP2 = wp2.lat;
    double latitudeWP3 = wp3.lat;
    double longitudeWP1 = wp1.lon; //lamda
    double longitudeWP2 = wp2.lon;
    double longitudeWP3 = wp3.lon;

    // start - center

    double catangB1_startToCenter = tan(latitudeWP2)*cos(latitudeWP1)*cosec(longitudeWP2 - longitudeWP1) -
            sin(latitudeWP1)*ctg(longitudeWP2 - longitudeWP1 );

    double Beta1_startToCenter = RadiansToDegree(atan(1/catangB1_startToCenter)); //B1 // угол разворота

    if (longitudeWP2 > longitudeWP1) {
        if (Beta1_startToCenter < 0) Beta1_startToCenter = 180 + Beta1_startToCenter;
    }
    if (longitudeWP2 < longitudeWP1) {
        if (Beta1_startToCenter > 0) Beta1_startToCenter = 180 + Beta1_startToCenter;
        if (Beta1_startToCenter < 0) Beta1_startToCenter = 360 + Beta1_startToCenter;
    }

    // center - end

    double s0 = tan(latitudeWP3);
    double s1 = cos(latitudeWP2);
    double s2 = cosec(longitudeWP3 - longitudeWP2);
    double s3 = sin(latitudeWP2);
    double s4 = ctg(longitudeWP3 - longitudeWP2);

    double catangB1_centerToEnd = tan(latitudeWP3)*cos(latitudeWP2)*cosec(longitudeWP3 - longitudeWP2) -
            sin(latitudeWP2)*ctg(longitudeWP3 - longitudeWP2);

    double Beta1_centerToEnd = RadiansToDegree(atan(1/catangB1_centerToEnd)); //B1

    if (longitudeWP3 > longitudeWP2) {
        if (Beta1_centerToEnd < 0) Beta1_centerToEnd = 180 + Beta1_centerToEnd;
    }
    if (longitudeWP3 < longitudeWP2) {
        if (Beta1_centerToEnd > 0) Beta1_centerToEnd = 180 + Beta1_centerToEnd;
        if (Beta1_centerToEnd < 0) Beta1_centerToEnd = 360 + Beta1_centerToEnd;
    }

    double alphaAns = 0;

    if (Beta1_centerToEnd - Beta1_startToCenter > 180){
        alphaAns = Beta1_centerToEnd - Beta1_startToCenter - 360;
    }else
    {
        if (Beta1_startToCenter - Beta1_centerToEnd > 180){
            alphaAns = Beta1_centerToEnd - Beta1_startToCenter + 360;
        }else
        {
            alphaAns = Beta1_centerToEnd - Beta1_startToCenter;
        }
    }


//    qDebug() << fabs(r * tan(DegreeToRadians(abs(alphaAns))/2.0f)) << fabs(r * tan(DegreeToRadians(fabs(alphaAns))/2.0f));

    double LUR = fabs(r * tan(DegreeToRadians(abs(alphaAns))/2.0f)); // линейное упреждение разворота

    return LUR;
}

double Helper::calcTAS(Waypoint &wp){

    double iAS = wp.speed; // or from point limits    //// indicated Air Speed, Приборная скорость
    double H = ((wp.maxAlt - wp.minAlt) / 2  + wp.minAlt) / 0.3048f; // Высота пролета точки H в футах

    if (iAS == 0) iAS = 250;
    return iAS * 171233 * pow(((288) - 0.00198f * H), 0.5f) / pow((288 - 0.00198f * H), 2.628f); // в узлах!
}

double Helper::calcR(Waypoint wp, double roll){  // roll - Применяемый крен

    if (roll == 0){ //если не задан крен

        if (wp.type == STAR || wp.type == APRCH){
            roll = 25;
        }else if (wp.maxAlt < 1000){
            roll = 15;
        }else if (wp.maxAlt < 3000) {
            roll = 20;
        }else roll = 25;

    }

    double TAS; // истинную воздушную скорость (TAS – True Air Speed)

    TAS = calcTAS(wp); // в узлах!

    double R = (3431*tan(Helper::DegreeToRadians(roll))) / (M_PI * TAS); // Угловая скорость разворота (R) в °/с

    double r =  TAS/(20*M_PI*R) * 1.852; // Радиус разворота для данного значения R в км.

    return r;
}


/* *
 * if you have line segment, initiate with two point, and you need point on this line
 * on distance from start - use it
 *
*/
glm::vec3 Helper::CalcPointOnDistance(glm::vec3 start, glm::vec3 end, double distance){

    double coeff = distance / (dist(start, end) - distance);
    double x = (start.x + coeff*end.x) / (1 + coeff);
    double z = (start.z + coeff*end.z) / (1 + coeff);
    double y = (start.y + coeff*end.y) / (1 + coeff);

    return glm::vec3(x, y, z);
}


double Helper::determinantMat3(QVector <glm::vec3> matr) {

    double ans = 0;
    double temp; // сомножитель
    temp = matr[0].x*matr[1].y*matr[2].z; ans += temp;
    temp = matr[0].y*matr[1].z*matr[2].x; ans += temp;
    temp = matr[0].z*matr[1].x*matr[2].y; ans += temp;
    temp = matr[0].z*matr[1].y*matr[2].x; ans -= temp;
    temp = matr[0].y*matr[1].x*matr[2].z; ans -= temp;
    temp = matr[0].x*matr[1].z*matr[2].y; ans -= temp;

    return ans;
}

void Helper::debugVec3(glm::vec3 tmp)
{
    qDebug() << tmp.x << tmp.y << tmp.z;
}
