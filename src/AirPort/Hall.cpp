#include "include/AirPort/Hall.h"
#include <glm/gtx/transform.hpp>
#include <cmath>
#include "include/Helper.h"
#include "include/GeoHelper.h"
#include "MainRenderModel.h"
#include "MainSettingsClass.h"

Hall::Hall(){
    mRenderData[HallObjectType];
    mRenderData[HallLinesObjectType];
    mRenderData[HallLinesTrajectoryObjectType];
    mRenderData[HallLinesRamObjectType];
    mRenderData[HallGroundLinesProjectionObjectType];
    mRenderData[HallGroundLinesObjectType];

    MainRenderModel = SingleMainRenderModel::Instance();
    SettingsClass = mainSettingsClass::Instance();

    mHeightRate = 10.0f;
    mFillTrancparency = 10;

    pStartTurn = NULL;
    pEndTurn = NULL;
}

void Hall::paint( )
{
    if (*elemToDraw & LINES) {
        glLineWidth(SettingsClass->getLinesWidth(LINE_TRAJECTORY));
        drawLines(&mRenderData[HallLinesTrajectoryObjectType], LINES);
    }

    MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);

    if (*elemToDraw & LINES) {
        glLineWidth(SettingsClass->getLinesWidth(LINE_RAM));
        drawLines(&mRenderData[HallLinesObjectType],LINES);
    }


    if (*elemToDraw & HALL) {
        drawTriangles(&mRenderData[HallObjectType], HALL);
    }
}

void Hall::init()
{
    initVAO(&mRenderData[HallObjectType], glm::vec4(glm::vec3(mColor), SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL) / 100.0f));
    initVAO(&mRenderData[HallLinesObjectType], glm::vec4(glm::vec3(mColor), TRANCPARENCY_LINE/ 100.0f));
    initVAO(&mRenderData[HallLinesTrajectoryObjectType], glm::vec4(glm::vec3(mColor), 0.8f));
    initVAO(&mRenderData[HallLinesRamObjectType], glm::vec4(glm::vec3(mColor), TRANCPARENCY_LINE_RAM/ 100.0f));
    initVAO(&mRenderData[HallGroundLinesObjectType], glm::vec4(0.7f, 0.7f, 0.7f, TRANCPARENCY_LINE_GROUND/ 100.0f));
    initVAO(&mRenderData[HallGroundLinesProjectionObjectType], glm::vec4(glm::vec3(mColor), TRANCPARENCY_LINE/ 100.0f));
}

/*-----------------------------------------------

Name:	buildHallFromPoints

Params:	StartWayPoint - wayPoint of start
        EndWayPoint - waypoint od end
        r - radius of hall
        color - color of hall

Result:	main func of the class.
        prezent hall as 12 triangles
        calculate for them points and
        add points to VertexData, sets Indicies
        and Color

        0-7 points for drawning Hall
        8-9 points - StartWayPoint and EndWayPoint
        10-11 points is EndWayPoint on max Height and
        on min Height

-----------------------------------------------*/

void Hall::buildHallFromPoints(Waypoint StartWayPoint, Waypoint EndWayPoint, bool isUpdate){

//    // calculation points

    mStartWaypoint = StartWayPoint;
    mEndWaypoint = EndWayPoint;

    mRenderData[HallObjectType].mVertex.clear();
    mRenderData[HallObjectType].mNormal.clear();
    mRenderData[HallObjectType].mIndices.clear();

    setPoints(StartWayPoint, EndWayPoint, isUpdate);



    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(a0)); //1 Ld  left of the box            2-------6     / \Y
    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(a1));  // Rd                            / |     /|      |
    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(a2)); // RU                            /  |    / |      |  /\Z
    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(a3)); // LU                           3-------7  |      |  /
                                                                              //                  | a|    | b|      | /
    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(b0)); //2 LD down of the box          |  1----|--5   ---0 ---->X
    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(b1)); // RD                           | /     | /      /|
    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(b2)); // RU                           |/      |/      / |
    mRenderData[HallObjectType].mVertex.push_back(glm::vec3(b3)); // LD                           0-------4

    mRenderData[HallObjectType].mVertex.push_back(a); mRenderData[HallObjectType].mVertex.push_back(b);

    for (int i = 0; i < mRenderData[HallObjectType].mVertex.size(); i++){
        mRenderData[HallObjectType].mNormal.push_back(glm::vec3());
    }

    mRenderData[HallObjectType].mIndices.clear();

//    mRenderData[HallObjectType].mIndices.push_back(0); mRenderData[HallObjectType].mIndices.push_back(1); mRenderData[HallObjectType].mIndices.push_back(2); //left
//    mRenderData[HallObjectType].mIndices.push_back(2); mRenderData[HallObjectType].mIndices.push_back(3); mRenderData[HallObjectType].mIndices.push_back(1);

    mRenderData[HallObjectType].mIndices.push_back(0); mRenderData[HallObjectType].mIndices.push_back(1); mRenderData[HallObjectType].mIndices.push_back(4); //down
    mRenderData[HallObjectType].mIndices.push_back(4); mRenderData[HallObjectType].mIndices.push_back(5); mRenderData[HallObjectType].mIndices.push_back(1);

    mRenderData[HallObjectType].mIndices.push_back(3); mRenderData[HallObjectType].mIndices.push_back(7); mRenderData[HallObjectType].mIndices.push_back(5); // nearest for you
    mRenderData[HallObjectType].mIndices.push_back(3); mRenderData[HallObjectType].mIndices.push_back(5); mRenderData[HallObjectType].mIndices.push_back(1);

    mRenderData[HallObjectType].mIndices.push_back(6); mRenderData[HallObjectType].mIndices.push_back(2); mRenderData[HallObjectType].mIndices.push_back(0); // back
    mRenderData[HallObjectType].mIndices.push_back(0); mRenderData[HallObjectType].mIndices.push_back(4); mRenderData[HallObjectType].mIndices.push_back(6);

    mRenderData[HallObjectType].mIndices.push_back(2); mRenderData[HallObjectType].mIndices.push_back(6); mRenderData[HallObjectType].mIndices.push_back(7); // up
    mRenderData[HallObjectType].mIndices.push_back(7); mRenderData[HallObjectType].mIndices.push_back(2); mRenderData[HallObjectType].mIndices.push_back(3);

    for ( int i = 0 ; i < mRenderData[HallObjectType].mIndices.size() ; i += 3) {

        int Index0 = mRenderData[HallObjectType].mIndices[i];
        int Index1 = mRenderData[HallObjectType].mIndices[i + 1];
        int Index2 = mRenderData[HallObjectType].mIndices[i + 2];

        glm::vec3 v1 = mRenderData[HallObjectType].mVertex[Index1] - mRenderData[HallObjectType].mVertex[Index0];
        glm::vec3 v2 = mRenderData[HallObjectType].mVertex[Index2] - mRenderData[HallObjectType].mVertex[Index0];
        glm::vec3 Normal = glm::cross(v1,v2);

        glm::normalize(Normal);

        mRenderData[HallObjectType].mNormal[Index0] += Normal;
        mRenderData[HallObjectType].mNormal[Index1] += Normal;
        mRenderData[HallObjectType].mNormal[Index2] += Normal;
    }

    for ( int i = 0 ; i < mRenderData[HallObjectType].mNormal.size() ; i++) {
        glm::normalize(-mRenderData[HallObjectType].mNormal[i]);
    }

    mCenterHall = glm::vec3((a.x + b.x)/2, (a.y + b.y)/(2), (a.z + b.z)/2);
}

/*-----------------------------------------------

Name:	setPoints

Params: StartWayPoint - wayPoint of start
        EndWayPoint - waypoint od end
        r - radius of hall

Result:	main calculation!
        use angle between Ox - Oz to rotate
        with Rotation Matrix Hall

        first calculate points perpendicular
        to ground and them rotate Ox-OZ

-----------------------------------------------*/

void Hall::setPoints(Waypoint StartWayPoint, Waypoint EndWayPoint, bool isUpdate){

    if (!isUpdate){
        glm::vec3 StartPoint, EndPoint;

        StartPoint = glm::vec3(StartWayPoint.x, StartWayPoint.y, StartWayPoint.z);
        EndPoint   = glm::vec3(EndWayPoint.x, EndWayPoint.y, EndWayPoint.z);

        setStartPoint(StartPoint);
        setEndPoint(EndPoint);
    }

    glm::vec2 geoCoord = GeoHelper::cartesian2geo(glm::vec3(-StartWayPoint.x, StartWayPoint.z, StartWayPoint.y));
    glm::vec3 newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mHeightRate * StartWayPoint.minAlt), true);
    StartDownPoint = newCartCoord;

    geoCoord = GeoHelper::cartesian2geo(glm::vec3(-EndWayPoint.x, EndWayPoint.z, EndWayPoint.y));
    newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mHeightRate * EndWayPoint.minAlt), true);
    EndDownPoint = newCartCoord;

    geoCoord = GeoHelper::cartesian2geo(glm::vec3(-StartWayPoint.x, StartWayPoint.z, StartWayPoint.y));
    newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mHeightRate * StartWayPoint.maxAlt), true);
    StartUpPoint = newCartCoord;

    geoCoord = GeoHelper::cartesian2geo(glm::vec3(-EndWayPoint.x, EndWayPoint.z, EndWayPoint.y));
    newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mHeightRate * EndWayPoint.maxAlt), true);
    EndUpPoint = newCartCoord;

    glm::vec3 Normal = glm::normalize(glm::cross(glm::vec3(StartUpPoint - EndDownPoint), glm::vec3(StartUpPoint - glm::vec3(0,0,0))));

    geoCoord = GeoHelper::cartesian2geo(glm::vec3(-StartWayPoint.x, StartWayPoint.z, StartWayPoint.y));
    newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mHeightRate*(StartWayPoint.maxAlt + StartWayPoint.minAlt)/2), true);
    a = newCartCoord;

    geoCoord = GeoHelper::cartesian2geo(glm::vec3(-EndWayPoint.x, EndWayPoint.z, EndWayPoint.y));
    newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mHeightRate*(EndWayPoint.maxAlt + EndWayPoint.minAlt)/2), true);
    b = newCartCoord;

    if (pStartTurn != NULL){

        glm::vec3 mStartBigRoundTurnBack = pStartTurn->getBigRound().back();
        glm::vec3 mStartSmallRoundTurnBack = pStartTurn->getSmallRound().back();
        glm::vec3 mStartCentralRoundTurnBack = pStartTurn->getRoundPoints().back();

        double mStartTurnHeightUp = pStartTurn->getEndHeightCurUp();
        double mStartTurnHeightDown = pStartTurn->getEndHeightCurDown();

        //up

        glm::vec2 geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mStartBigRoundTurnBack.x, mStartBigRoundTurnBack.z, mStartBigRoundTurnBack.y));
        glm::vec3 newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mStartTurnHeightUp), true);

        a3 = newCartCoord;

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mStartSmallRoundTurnBack.x, mStartSmallRoundTurnBack.z, mStartSmallRoundTurnBack.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mStartTurnHeightUp), true);

        a2 = newCartCoord;

        // down

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mStartBigRoundTurnBack.x, mStartBigRoundTurnBack.z, mStartBigRoundTurnBack.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mStartTurnHeightDown), true);

        a1 = newCartCoord;

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mStartSmallRoundTurnBack.x, mStartSmallRoundTurnBack.z, mStartSmallRoundTurnBack.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mStartTurnHeightDown), true);

        a0 = newCartCoord;

        //center

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mStartCentralRoundTurnBack.x, mStartCentralRoundTurnBack.z, mStartCentralRoundTurnBack.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord,( mStartTurnHeightUp + mStartTurnHeightDown) / 2), true);

        a = newCartCoord;

        if (!pStartTurn->getIsLeftTurn() && pEndTurn == NULL){
            swap(a2,a3);
            swap(a1,a0);
        }

        if (!pStartTurn->getIsLeftTurn() && pEndTurn != NULL && pEndTurn->getIsLeftTurn()){
            swap(a2,a3);
            swap(a1,a0);
        }


    } else{

        a0 = Helper::CalcPointOnDistance(-Normal + StartDownPoint, StartDownPoint, mWidth/2);
        a1 = Helper::CalcPointOnDistance(Normal + StartDownPoint, StartDownPoint, mWidth/2);
        a2 = Helper::CalcPointOnDistance(-Normal + StartUpPoint, StartUpPoint, mWidth/2);
        a3 = Helper::CalcPointOnDistance(Normal + StartUpPoint, StartUpPoint, mWidth/2);

    }

    if (pEndTurn != NULL){

        glm::vec3 mEndBigRoundTurn0 = pEndTurn->getBigRound()[0];
        glm::vec3 mEndSmallRoundTurn0 = pEndTurn->getSmallRound()[0];
        glm::vec3 mEndCentralRoundTurn0 = pEndTurn->getRoundPoints()[0];

        double mEndTurnHeightUp = pEndTurn->getStartHeightCurUp();
        double mEndTurnHeightDown = pEndTurn->getStartHeightCurDown();

        // up

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mEndBigRoundTurn0.x, mEndBigRoundTurn0.z, mEndBigRoundTurn0.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mEndTurnHeightUp), true);

        b3 = newCartCoord;

        glm::vec2 geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mEndSmallRoundTurn0.x, mEndSmallRoundTurn0.z, mEndSmallRoundTurn0.y));
        glm::vec3 newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, mEndTurnHeightUp), true);

        b2 = newCartCoord;

        //down

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mEndBigRoundTurn0.x, mEndBigRoundTurn0.z, mEndBigRoundTurn0.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord,mEndTurnHeightDown), true);

        b1 = newCartCoord;

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mEndSmallRoundTurn0.x, mEndSmallRoundTurn0.z, mEndSmallRoundTurn0.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord,mEndTurnHeightDown ), true);

        b0 = newCartCoord;

        if (!pEndTurn->getIsLeftTurn() && pStartTurn == NULL){
            swap(b2,b3);
            swap(b1,b0);
        }

        if (pStartTurn != NULL && pStartTurn->getIsLeftTurn() && !pEndTurn->getIsLeftTurn()){
            swap(b2,b3);
            swap(b1,b0);
        }

        //center

        geoCoord = GeoHelper::cartesian2geo(glm::vec3(-mEndCentralRoundTurn0.x, mEndCentralRoundTurn0.z, mEndCentralRoundTurn0.y));
        newCartCoord = GeoHelper::geo2cartesianGL(glm::vec3(geoCoord,( mEndTurnHeightUp + mEndTurnHeightDown) / 2), true);

        b = newCartCoord;

    } else{
        b0 = Helper::CalcPointOnDistance(-Normal + EndDownPoint, EndDownPoint, mWidth/2);
        b1 = Helper::CalcPointOnDistance(Normal + EndDownPoint, EndDownPoint, mWidth/2);
        b2 = Helper::CalcPointOnDistance(-Normal + EndUpPoint, EndUpPoint, mWidth/2 );
        b3 = Helper::CalcPointOnDistance(Normal + EndUpPoint, EndUpPoint, mWidth/2);

//        if (!pEndTurn->getIsLeftTurn()){
//            swap(b2,b3);
//            swap(b1,b0);
//        }

    }

    mAltMaxStart = StartWayPoint.maxAlt;
    mAltMinStart = StartWayPoint.minAlt;

    mAltMaxEnd = EndWayPoint.maxAlt;
    mAltMinEnd = EndWayPoint.minAlt;
}

/*-----------------------------------------------

Name:	buildMoreRam

Params:	StartWayPoint - wayPoint of start
        EndWayPoint - waypoint od end
        r - radius of hall
        color - color of hall

Result:	calculate new points, where will be
        Frames and then build them
        on recursion method


-----------------------------------------------*/

void Hall::buildMoreRam(Waypoint StartWayPoint, Waypoint EndWayPoint,  GLdouble r){

//    setPoints(StartWayPoint,EndWayPoint, r);

//    // using recursion to draw more Frames

//    if (abs(glm::length(a-b)) > DISTANCE_BETWEEN_RAMS)
//    {
//        buildMoreRam(StartWayPoint, findMiddle(StartWayPoint, EndWayPoint),r); // left part of segment
//        buildMoreRam(findMiddle(StartWayPoint, EndWayPoint), EndWayPoint,r); // right part of segment
//    }
//    else{
//        setPoints(StartWayPoint, findMiddle(StartWayPoint, EndWayPoint), r); // calculating new points for building Frames

//        int SizeVertexDataLines = mVAOLinesRam.Vertex.size();

//        mVAOLinesRam.Vertex.push_back(glm::vec3(a0));
//        mVAOLinesRam.Vertex.push_back(glm::vec3(a1));
//        mVAOLinesRam.Vertex.push_back(glm::vec3(a2));
//        mVAOLinesRam.Vertex.push_back(glm::vec3(a3));

//        mVAOLinesRam.Vertex.push_back(glm::vec3(b0));
//        mVAOLinesRam.Vertex.push_back(glm::vec3(b1));
//        mVAOLinesRam.Vertex.push_back(glm::vec3(b2));
//        mVAOLinesRam.Vertex.push_back(glm::vec3(b3));

//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 0); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 1);
//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 1); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 3);
//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 2); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 3);
//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 2); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 0);

//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 4); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 5);
//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 5); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 7);
//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 6); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 7);
//        mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 6); mVAOLinesRam.Indices.push_back(SizeVertexDataLines + 4);

//    }
}

/*-----------------------------------------------

Name:	buildLines

Params: color - color of hall

Result:	use earlier calculated points for
        setting VertexData, Indices ...

-----------------------------------------------*/

void Hall::buildLines(){


//    mRenderData[HallLinesObjectType].Vertex.push_back(glm::vec3(a.x-2830000, a.y - 5260000, a.z + 2170000));
//    mRenderData[HallLinesObjectType].Vertex.push_back(glm::vec3(b.x-2830000, b.y - 5260000, b.z + 2170000));

//    mRenderData[HallLinesObjectType].Vertex.push_back(a);
//    mRenderData[HallLinesObjectType].Vertex.push_back(b);

    mRenderData[HallLinesObjectType].mVertex.clear();
    mRenderData[HallLinesObjectType].mIndices.clear();

    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(a0));
    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(a1));
    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(a2));
    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(a3));

    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(b0));
    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(b1));
    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(b2));
    mRenderData[HallLinesObjectType].mVertex.push_back(glm::vec3(b3));


//    mRenderData[HallLinesObjectType].mIndices.push_back(0); mRenderData[HallLinesObjectType].mIndices.push_back(1);
//    mRenderData[HallLinesObjectType].mIndices.push_back(1); mRenderData[HallLinesObjectType].mIndices.push_back(3);
//    mRenderData[HallLinesObjectType].mIndices.push_back(2); mRenderData[HallLinesObjectType].mIndices.push_back(3);
//    mRenderData[HallLinesObjectType].mIndices.push_back(2); mRenderData[HallLinesObjectType].mIndices.push_back(0);

//    mRenderData[HallLinesObjectType].mIndices.push_back(4); mRenderData[HallLinesObjectType].mIndices.push_back(5);
//    mRenderData[HallLinesObjectType].mIndices.push_back(5); mRenderData[HallLinesObjectType].mIndices.push_back(7);
//    mRenderData[HallLinesObjectType].mIndices.push_back(6); mRenderData[HallLinesObjectType].mIndices.push_back(7);
//    mRenderData[HallLinesObjectType].mIndices.push_back(6); mRenderData[HallLinesObjectType].mIndices.push_back(4);

    mRenderData[HallLinesObjectType].mIndices.push_back(3); mRenderData[HallLinesObjectType].mIndices.push_back(7);
    mRenderData[HallLinesObjectType].mIndices.push_back(2); mRenderData[HallLinesObjectType].mIndices.push_back(6);
    mRenderData[HallLinesObjectType].mIndices.push_back(0); mRenderData[HallLinesObjectType].mIndices.push_back(4);
    mRenderData[HallLinesObjectType].mIndices.push_back(1); mRenderData[HallLinesObjectType].mIndices.push_back(5);
}

/*-----------------------------------------------

Name:	buildTrajectory

Params: StartWayPoint - wayPoint of start
        EndWayPoint - waypoint od end
        color - color of hall

Result:	calculated new points on GROUND for
        setting mVertexData, mIndices ...

-----------------------------------------------*/
void Hall::buildTrajectory(Waypoint StartWayPoint, Waypoint EndWayPoint){

    mRenderData[HallLinesTrajectoryObjectType].mVertex.clear();
    mRenderData[HallLinesTrajectoryObjectType].mIndices.clear();

    mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(a);
    mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(b);

    mRenderData[HallLinesTrajectoryObjectType].mIndices.push_back(0);
    mRenderData[HallLinesTrajectoryObjectType].mIndices.push_back(1);

}


/*-----------------------------------------------

Name:	findMiddle

Params:	StartWayPoint - wayPoint of start
        EndWayPoint - waypoint od end

Result:	waypoint in the middle between start
        and end points

-----------------------------------------------*/

Waypoint Hall::findMiddle (Waypoint StartWayPoint, Waypoint EndWayPoint){
    Waypoint temp;
    temp.x = (EndWayPoint.x + StartWayPoint.x) / 2;
    temp.y = (EndWayPoint.y + StartWayPoint.y) / 2;
    temp.maxAlt = (EndWayPoint.maxAlt + StartWayPoint.maxAlt) / 2;
    temp.minAlt = (EndWayPoint.minAlt + StartWayPoint.minAlt) / 2;
    return temp;
}

void Hall::init(glm::vec3 &color){
    initVAO(&mRenderData[HallObjectType], glm::vec4(color, SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL) / 100.0f));

    mRenderData[HallObjectType].mVAO.bind();
    mRenderData[HallObjectType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);

    mRenderData[HallObjectType].mVertexBuffer.create();
    mRenderData[HallObjectType].mVertexBuffer.bind();
    glEnableVertexAttribArray(3);
    if (mRenderData[HallObjectType].mNormal.size() > 0)
        mRenderData[HallObjectType].mVertexBuffer.setData(mRenderData[HallObjectType].mNormal.size() * sizeof(glm::vec3), &mRenderData[HallObjectType].mNormal.front());
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    mRenderData[HallObjectType].mVAO.unbind();

    initVAO(&mRenderData[HallLinesObjectType], glm::vec4(color, TRANCPARENCY_LINE/ 100.0f));
    initVAO(&mRenderData[HallLinesTrajectoryObjectType], glm::vec4(color, TRANCPARENCY_LINE_TRAJECTORY/ 100.0f));
    initVAO(&mRenderData[HallLinesRamObjectType], glm::vec4(color, TRANCPARENCY_LINE_RAM/ 100.0f));
    initVAO(&mRenderData[HallGroundLinesObjectType], glm::vec4(0.7f, 0.7f, 0.7f, TRANCPARENCY_LINE_GROUND/ 100.0f));
    initVAO(&mRenderData[HallGroundLinesProjectionObjectType], glm::vec4(color, TRANCPARENCY_LINE/ 100.0f));

}

void Hall::draw(HallBitmask elemToDraw){

    if (elemToDraw & HALL) {
        drawTriangles(&mRenderData[HallObjectType], HALL);
    }
    if (elemToDraw & LINES) {
        glLineWidth(LINE_WIDTH_RAM);
        drawLines(&mRenderData[HallLinesObjectType], LINES);
    }
    if (elemToDraw & LINES_TRAJECTORY) {
        glLineWidth(LINE_WIDTH);
        drawLines(&mRenderData[HallLinesTrajectoryObjectType], LINES_TRAJECTORY);
    }
    if (elemToDraw & LINES_RAM) {
        glLineWidth(LINE_WIDTH_RAM);
        drawLines(&mRenderData[HallLinesRamObjectType], LINES_RAM);
    }
    if (elemToDraw & LINES_GROUND) {
        glLineWidth( 3.8f);
        drawLines(&mRenderData[HallGroundLinesObjectType], LINES_GROUND);
    }
    if (elemToDraw & LINES_PROJECTION) {
        glLineWidth( 0.9f);
        drawLines(&mRenderData[HallGroundLinesProjectionObjectType],LINES_PROJECTION);
    }
}

void Hall::drawTriangles(RenderData *curVAO, elementsToDraw elem){

    curVAO->mVAO.bind();
    updateColor(&mRenderData[HallObjectType], elem);
    glDrawElementsInstanced(GL_TRIANGLES, curVAO->mIndices.size(), GL_UNSIGNED_INT, 0, 1);
    curVAO->mVAO.unbind();
}

void Hall::drawLines(RenderData *curVAO, elementsToDraw elem){
    curVAO->mVAO.bind();
    updateColor(&mRenderData[HallLinesObjectType], elem);
    glDrawElements(GL_LINES, curVAO->mIndices.size(), GL_UNSIGNED_INT, 0);
    curVAO->mVAO.unbind();
}

void Hall::updateColor(RenderData *curVAO, elementsToDraw element){

    curVAO->mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    ProcedureType type = pProcedure->getType();

    glm::vec3 color3;
    if (pProcedure->getStatus() == CHECKED){
        color3 = SettingsClass->getCheckedObjectColor();
    } else if (pProcedure->getStatus() == UNCHECKED){
        color3 = SettingsClass->getProceduresColors(type);
    }

    float trancpar;

    if (element == HALL){
        trancpar = SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL);
    } else if (element == LINES) {
        trancpar = SettingsClass->getObjectsTrancparency(TRANCPARENCY_LINE);
    }
    glm::vec4 color4 = glm::vec4(color3, trancpar);

    curVAO->mColorBuffer.setData( sizeof(glm::vec4), &color4);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

}

double Hall::getWidth() const
{
    return mWidth;
}

void Hall::setWidth(double width)
{
    mWidth = width;
}

void Hall::initVAO(RenderData *VAO, glm::vec4 color){

    VAO->mVAO.create();
    VAO->mVAO.bind();

    VAO->mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    VAO->mElementBuffer.create();
    VAO->mElementBuffer.bind();
    if (VAO->mIndices.size() > 0)
        VAO->mElementBuffer.setData(VAO->mIndices.size() * sizeof(GLuint), &VAO->mIndices.front());

    VAO->mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    VAO->mVertexBuffer.create();
    VAO->mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    if (VAO->mVertex.size() > 0)
        VAO->mVertexBuffer.setData(VAO->mVertex.size() * sizeof(glm::vec3), &VAO->mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    VAO->mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    VAO->mColorBuffer.create();
    VAO->mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    VAO->mColorBuffer.setData( sizeof(glm::vec4), &color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    VAO->mVAO.unbind();
}


void Hall::updateRenderPoints( ){
    buildHallFromPoints(mStartWaypoint, mEndWaypoint, true);

    buildLines();
    buildTrajectory(mStartWaypoint, mEndWaypoint);
}

void Hall::updateBuffers( ){

    mRenderData[HallObjectType].mVAO.bind();
    mRenderData[HallObjectType].mVertexBuffer.bind();
    mRenderData[HallObjectType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    if (mRenderData[HallObjectType].mVertex.size() > 0)
        mRenderData[HallObjectType].mVertexBuffer.setData(mRenderData[HallObjectType].mVertex.size() * sizeof(glm::vec3), &mRenderData[HallObjectType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[HallObjectType].mVAO.unbind();

    mRenderData[HallLinesObjectType].mVAO.bind();
    mRenderData[HallLinesObjectType].mVertexBuffer.bind();
    mRenderData[HallLinesObjectType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    if (mRenderData[HallLinesObjectType].mVertex.size() > 0)
        mRenderData[HallLinesObjectType].mVertexBuffer.setData(mRenderData[HallLinesObjectType].mVertex.size() * sizeof(glm::vec3), &mRenderData[HallObjectType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[HallLinesTrajectoryObjectType].mVAO.unbind();
    mRenderData[HallLinesTrajectoryObjectType].mVAO.bind();
    mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.bind();
    mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    if (mRenderData[HallLinesTrajectoryObjectType].mVertex.size() > 0)
        mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.setData(mRenderData[HallLinesTrajectoryObjectType].mVertex.size() * sizeof(glm::vec3), &mRenderData[HallLinesTrajectoryObjectType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[HallLinesTrajectoryObjectType].mVAO.unbind();
}


void Hall::setColor(glm::vec4 color){

    mColor = color;

    mRenderData[HallObjectType].mVAO.bind();
    mRenderData[HallObjectType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    mRenderData[HallObjectType].mColorBuffer.clear();
    mRenderData[HallObjectType].mColorBuffer.setData( sizeof(glm::vec4), &color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[HallObjectType].mVAO.unbind();

}


void Hall::setParentProcedure ( Procedure *parent ){
    pProcedure = parent;
}

Procedure * Hall::getParentProcedure ( ){
    return pProcedure;
}

void Hall::setDrawMask( HallBitmask *value ){
    elemToDraw = value;
}

HallBitmask * Hall::getDrawMask(){
    return elemToDraw;
}

glm::vec3 Hall::getCenter(){
    return mCenterHall;
}

void Hall::setHeightRate(const float value){
    mHeightRate = value;
}

float Hall::getHeightRate() const {
    return mHeightRate;
}

float Hall::setFillTrancparency(float value){
    this->mFillTrancparency = value;
}

void Hall::setSpeed( float startSpeed, float endSpeed ){
    mStartSpeed = startSpeed == 0 ? 250 : startSpeed;
    mEndSpeed = endSpeed == 0 ? 250 : endSpeed;
}

double Hall::getStartSpeed( ){
    return mStartSpeed;
}

double Hall::getEndSpeed( ){
    return mEndSpeed ;
}

double Hall::getStartAltMin( ){
    return mAltMinStart ;
}

double Hall::getEndAltMin( ){
    return mAltMinEnd;
}

double Hall::getStartAltMax( ){
    return mAltMaxStart ;
}

double Hall::getEndAltMax( ){
    return mAltMaxEnd ;
}

void Hall::setStartPoint( glm::vec3 point){
    mStartHall = point;
}

void Hall::setEndPoint( glm::vec3 point){
    mEndHall = point;
}

glm::vec3 Hall::getStartPoint( ){
    return mStartHall;
}

glm::vec3 Hall::getEndPoint( ){
    return mEndHall;
}

void    Hall::setTurns   (Turn *StartTurn, Turn *EndTurn, bool isUpdate){

    if (!isUpdate){
        this->pStartTurn = StartTurn;
        this->pEndTurn = EndTurn;
    }else {
        if (this->pStartTurn != NULL){
            this->pStartTurn = StartTurn;
        }
        if (this->pEndTurn != NULL){
            this->pEndTurn = EndTurn;
        }
    }
}

Hall::~Hall(){

}
