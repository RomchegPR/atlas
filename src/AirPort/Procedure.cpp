#include "include/AirPort/Procedure.h"
#include "include/Helper.h"
#include "include/GeoHelper.h"

Procedure::Procedure(ProcedureType type, ProcedureMode mode, QString name){
    mType = type;
    mMode = mode;
    mName = name;
    isChecked = false;
    Status = UNCHECKED;
    mHeightRate = 10.0;

}

void Procedure::addHall(Waypoint &start, Waypoint &center, Waypoint &end)
{
    mHalls.push_back(createHall(start, center));
    mHalls.push_back(createHall(center, end));
}

void Procedure::addWayPoint(Waypoint &point){

    mWayPoints.push_back(point);
}

/*-----------------------------------------------

Name:	addHall

Params:	start, end - Air Points

Result:	simply build hall from one point to other.
        very usefull for Debug !

-----------------------------------------------*/

Hall Procedure::createHall(Waypoint &start, Waypoint &end, Turn *StartTurn, Turn *EndTurn){

    // creating new Hall and getting pointer to that
    Hall tempHall = Hall();
    tempHall.setHeightRate(mHeightRate);
    tempHall.setSpeed(start.speed, end.speed);
    if (mType == AIRWAY)
        tempHall.setWidth(10000.0f);
    else
        tempHall.setWidth(RNAV1 * 2);
    tempHall.setTurns(StartTurn, EndTurn);

    // calculate different parts of Halls
    tempHall.buildHallFromPoints(start, end);
    tempHall.buildLines();
    tempHall.buildTrajectory(start, end);


    return tempHall;
}

Hall Procedure::createHall(Waypoint &start, Waypoint &end){

    // creating new Hall and getting pointer to that
    Hall tempHall = Hall();
    tempHall.setHeightRate(mHeightRate);
    tempHall.setSpeed(start.speed, end.speed);

    // calculate different parts of Halls
    tempHall.buildHallFromPoints(start, end);
    tempHall.buildLines();
    tempHall.buildTrajectory(start, end);

    return tempHall;
}



/*-----------------------------------------------

Name:	init

Params:	-

Result:	initialize VAO, creating Buffers for procedure
        and Calculate FLY_BY, FLY_OVER (calc)

-----------------------------------------------*/

void Procedure::init()
{
    // calculation Path with turns (FLY_BY, FLY_OVER)

    calc();

    // uncomment for building original halls, without any turns
//    for (int i = 0; i < mWayPoints.size() - 1; i++){
//        addHall(mWayPoints[i], mWayPoints[i+1]);
//    }

    // init buffers

    for (int i = 0; i < mTurns.size(); i++){
        mTurns[i].init();
        mTurns[i].setParentProcedure(this);
    }

    // setting color
    for (int i = 0; i < mHalls.size(); i++){
        mHalls[i].setColor(glm::vec4(mColorFill, TRANCPARENCY_HALL / 100.0f));
        mHalls[i].setDrawMask(&mDrawMask);
        mHalls[i].init();
        mHalls[i].setParentProcedure(this);
    }

    for (int i = 0; i < mRenderData[HallGroundLinesProjectionObjectType].mVertex.size(); i++){
        mRenderData[HallGroundLinesProjectionObjectType].mIndices.push_back(i);
    }

    for (int i = 0; i < mRenderData[HallGroundLinesObjectType].mVertex.size(); i++){
        mRenderData[HallGroundLinesObjectType].mIndices.push_back(i);
        mRenderData[HallGroundLinesObjectType].mIndices.push_back(i-1);

        mRenderData[HallLinesTrajectoryObjectType].mIndices.push_back(i);
        mRenderData[HallLinesTrajectoryObjectType].mIndices.push_back(i-1);
    }

    initVAO(&mRenderData[HallGroundLinesProjectionObjectType], glm::vec4(0.6, 0.6, 0.6, 0.7f));
    initVAO(&mRenderData[HallGroundLinesObjectType],  glm::vec4(0.5, 0.5, 0.5, 0.9f));
    initVAO(&mRenderData[HallLinesTrajectoryObjectType], glm::vec4(mColorFill, 0.7f));
}

void Procedure::initVAO(RenderData *VAO, glm::vec4 color){

    VAO->mVAO.create();
    VAO->mVAO.bind();

    VAO->mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    VAO->mElementBuffer.create();
    VAO->mElementBuffer.bind();
    if (VAO->mIndices.size() > 0)
        VAO->mElementBuffer.setData(VAO->mIndices.size() * sizeof(GLuint), &VAO->mIndices.front());

    VAO->mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    VAO->mVertexBuffer.create();
    VAO->mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    if (VAO->mVertex.size() > 0)
        VAO->mVertexBuffer.setData(VAO->mVertex.size() * sizeof(glm::vec3), &VAO->mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    VAO->mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    VAO->mColorBuffer.create();
    VAO->mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    VAO->mColorBuffer.setData( sizeof(glm::vec4), &color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    VAO->mVAO.unbind();
}

void Procedure::paint( int index )
{

    if (index < mTurns.size())
    {
        mTurns[index].paint();
    }

    mHalls[index].paint();

}

void Procedure::paint()
{
    if (mDrawMask & LINES_PROJECTION) {
        glLineWidth( 2.2f);
        mRenderData[HallGroundLinesProjectionObjectType].mVAO.bind();
        glDrawElements(GL_LINES, mRenderData[HallGroundLinesProjectionObjectType].mIndices.size(), GL_UNSIGNED_INT, 0);
        mRenderData[HallGroundLinesProjectionObjectType].mVAO.unbind();
        glLineWidth( 1 );
    }

    if (mDrawMask & LINES_GROUND) {
        glLineWidth( 2.9f);
        mRenderData[HallGroundLinesObjectType].mVAO.bind();
        glDrawElements(GL_LINES, mRenderData[HallGroundLinesObjectType].mIndices.size(), GL_UNSIGNED_INT, 0);
        mRenderData[HallGroundLinesObjectType].mVAO.unbind();
        glLineWidth( 1 );
    }

    if (mDrawMask & LINES_TRAJECTORY) {
        glLineWidth( 2.0f);
        mRenderData[HallLinesTrajectoryObjectType].mVAO.bind();
        glDrawElements(GL_LINES, mRenderData[HallLinesTrajectoryObjectType].mIndices.size(), GL_UNSIGNED_INT, 0);
        mRenderData[HallLinesTrajectoryObjectType].mVAO.unbind();
        glLineWidth( 1 );
    }
}

/*-----------------------------------------------

Name:	calc

Params:	-

Result:	Very important function!!!
        from saved Air Points build new Procedure with
        turns, new correct points.

        initiate "Rounding", calculate and save Vertexes and Indexes
        of Turn.

-----------------------------------------------*/

void Procedure::calc(){

    // keeps position of original Air Point
    glm::vec3 CurStart;
    glm::vec3 CurCenter;
    glm::vec3 CurEnd;

    // keeps AirPoints, that obtained after calculation turns - turns displace portions of AirPoints
    Waypoint StartWaypoint, EndWaypoint;

    StartWaypoint = mWayPoints[0];

    // keeps position of original Air Point
    CurStart  = glm::vec3(0);
    CurCenter = glm::vec3(0);
    CurEnd    = glm::vec3(0);

    // keeps AirPoints, that obtained after calculation turns - turns displace portions of AirPoints
//    Waypoint StartWaypoint, EndWaypoint;

    StartWaypoint = mWayPoints[0];

    // we need to calc turn, and for it we need 3 points!
    for (int i = 0; i < mWayPoints.size() - 2; i++){

        CurStart   = Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z), glm::vec3(0,0,0), mHeightRate*(mWayPoints[i].maxAlt + mWayPoints[i].minAlt)/2);
        CurCenter  = Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z), glm::vec3(0,0,0), mHeightRate*(mWayPoints[i+1].maxAlt + mWayPoints[i+1].minAlt)/2 );
        CurEnd     = Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z), glm::vec3(0,0,0), mHeightRate*(mWayPoints[i+2].maxAlt + mWayPoints[i+2].minAlt)/2 );

        mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z));
        mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i].maxAlt + mWayPoints[i].minAlt)/2));

        mRenderData[HallGroundLinesObjectType].mVertex.push_back(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z));
        mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i].maxAlt + mWayPoints[i].minAlt)/2));

        if (i == mWayPoints.size() - 3) {

            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z));
            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+1].maxAlt + mWayPoints[i+1].minAlt)/2));

            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z));
            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+2].maxAlt + mWayPoints[i+2].minAlt)/2));

            mRenderData[HallGroundLinesObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z));
            mRenderData[HallGroundLinesObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z));

            mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+1].maxAlt + mWayPoints[i+1].minAlt)/2));
            mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+2].maxAlt + mWayPoints[i+2].minAlt)/2));
        }

        double r   = Helper::calcR(mWayPoints[i+1]);
        double LTA = Helper::calcLUR(mWayPoints[i], mWayPoints[i+1], mWayPoints[i+2], mType, r)*1000;
        double c   = std::sqrt(LTA*LTA/1000000 + r*r); // needs for calc TurnCenter in FLY_BY

        TurnTypes type = mWayPoints[i+1].mFlyOver ? FLY_OVER : FLY_BY;

        if (mType == AIRWAY){

          mHalls.push_back(createHall(mWayPoints[i], mWayPoints[i+1], NULL, NULL));
          if (i == mWayPoints.size() - 3) {
              mHalls.push_back(createHall(mWayPoints[i+1], mWayPoints[i+2], NULL, NULL));
          }

          continue;
        }

        // init Turn (save points in RoundingClass)
        Turn turn(CurStart, CurCenter, CurEnd, LTA, type);

        turn.setWidth(RNAV1 * 2);
        turn.setRadius(r);
        turn.setDistCenter(c);
        turn.setWayPoints(mWayPoints[i], mWayPoints[i+1], mWayPoints[i+2] );
        turn.setHeightRate(mHeightRate);
        turn.calcHeightsStart();
        turn.calcHeightsEnd();


        turn.calcBasicTrajectory();
        turn.setColor(mColorFill);
        turn.calcAllRenderPoint();
        turn.calcTurnDiretion();

        //after Turn calculation we have trajectory of turn
        QVector<glm::vec3> turnPoints = turn.getRoundPoints();

        /* in general, the construction of Procedure look so:
        *
        *   1) build hall from CurStart to TurnStart
        *   2) build turn
        *   3) build hall from TurnEnd to CurEnd
        *
        *   so we need to have correct points of TurnStart and TurnEnd
        *   for building next part of Procedure (next i, i+1, i+2)
        *
        *   // if no turn - build CurStart-CurCenter // CurCenter-CurEnd
        *
        **/

        // if it is first hall
        if (i == 0) {
            StartWaypoint = mWayPoints[i];
            Waypoint point;
            if (turnPoints.size() > 0){
                EndWaypoint = vec2waypoint(turnPoints[0], mWayPoints[i+1].minAlt , mWayPoints[i+1].maxAlt, mWayPoints[i+1]);
                point = vec2waypoint(turnPoints[0], mWayPoints[i+1].minAlt, mWayPoints[i+1].maxAlt, mWayPoints[i+1]);
            }else{
                 EndWaypoint = mWayPoints[i+1];
            }

            if (turn.getSmallRound().size() > 0){
                mHalls.push_back(createHall(StartWaypoint, EndWaypoint, NULL, &turn));
            } else{
                 mHalls.push_back(createHall(StartWaypoint, EndWaypoint, NULL, NULL));
            }


            if (turnPoints.size() > 0)
                 StartWaypoint = vec2waypoint(turnPoints[turnPoints.size() - 1], mWayPoints[i+1].minAlt, mWayPoints[i+1].maxAlt, mWayPoints[i+1]);
            else
                 StartWaypoint = mWayPoints[i+1];
        }
        else {
            if (turn.getIsNoTurn()){
                if (mTurns.last().getSmallRound().size() > 0){
                    mHalls.push_back(createHall(StartWaypoint, mWayPoints[i+1], &mTurns[mTurns.size() - 1], NULL));
                }else{
                    mHalls.push_back(createHall(StartWaypoint, mWayPoints[i+1]));
                }
                StartWaypoint = mWayPoints[i+1];
            }
            else{
                if (turnPoints.size() > 0){
                    EndWaypoint =  vec2waypoint(turnPoints[0], mWayPoints[i+1].minAlt , mWayPoints[i+1].maxAlt, mWayPoints[i+1]);

                    if (mTurns.last().getSmallRound().size() > 0)
                        mHalls.push_back(createHall(StartWaypoint, EndWaypoint, &mTurns[mTurns.size() - 1], &turn));
                    else {
                        mHalls.push_back(createHall(StartWaypoint, EndWaypoint, NULL, &turn));
                    }
                    StartWaypoint = vec2waypoint(turnPoints[turnPoints.size() - 1], mWayPoints[i+1].minAlt, mWayPoints[i+1].maxAlt, mWayPoints[i+1]);
                }
                else {
                    EndWaypoint = mWayPoints[i+1];
                    if (mTurns.last().getSmallRound().size() > 0){
                        mHalls.push_back(createHall(StartWaypoint, EndWaypoint, &mTurns[mTurns.size() - 1], NULL));
                    } else {
                        mHalls.push_back(createHall(StartWaypoint, EndWaypoint, NULL, NULL));
                    }

                    StartWaypoint = EndWaypoint;
                }
            }

            // if it is last points of Procedure
            if (i == mWayPoints.size() - 3 ){
                if (turn.getSmallRound().size() > 0){
                    mHalls.push_back(createHall(StartWaypoint, mWayPoints[i+2], &turn, NULL));
                } else {
                    mHalls.push_back(createHall(StartWaypoint, mWayPoints[i+2], NULL, NULL));
                }

            }
        }

        mTurns.push_back(turn);

    }

}


/*-----------------------------------------------

Name:	vec2waypoint

Params:	temp - coords of point on the ground
        MinAlt, MaxAlt - heights
        example- other info for new waypoint

Result:	translate coords of point on the ground and Heights
        to Waypoint

-----------------------------------------------*/

Waypoint Procedure::vec2waypoint(glm::vec3 temp, double MinAlt, double MaxAlt, Waypoint &example){
    Waypoint ans = example;

    glm::vec3 PointOnEarth = Helper::CalcPointOnDistance(temp, glm::vec3(0,0,0), mHeightRate*(MaxAlt + MinAlt)/2);

    ans.x = PointOnEarth.x;
    ans.y = PointOnEarth.y;
    ans.z = PointOnEarth.z;

    ans.minAlt = MinAlt;
    ans.maxAlt = MaxAlt;

    return ans;
}

bool Procedure::getIsChecked() const
{
    return isChecked;
}

void Procedure::setIsChecked(bool value)
{
    isChecked = value;
}

void Procedure::draw()
{

    for (int i = 0; i < mHalls.size(); i++){
        mHalls[i].draw(mDrawMask);
    }
}


void Procedure::draw(HallBitmask mask)
{
//    for (int i = 0; i < mRenderData[LinesType].size(); i++){
//        drawLines(mRenderData[LinesType][i]);
//    }

//    for (int i = 0; i < mRenderData[BasicType].size(); i++){
//        drawTriangles(mRenderData[BasicType][i]);
//    }

//    for (int i = 0; i < mHalls.size(); i++){
//        mHalls[i].draw(mask);
//    }
}

void Procedure::draw(int ind, HallBitmask mask)
{

//    if (ind < mRenderData[BasicType].size())
//    {
//        drawLines(mRenderData[LinesType][ind]);
//        drawTriangles(mRenderData[BasicType][ind]);
//    }

//    mHalls[ind].draw(mask);
}

QString Procedure::getName() const
{
    return mName;
}

void Procedure::setName(const QString &value)
{
    mName = value;
}

ProcedureType Procedure::getType() const
{
    return mType;
}

void Procedure::setType(const ProcedureType &value)
{
    mType = value;
}

glm::vec3 Procedure::getColorFrame() const
{
    return mColorFrame;
}

void Procedure::setColorFrame(const glm::vec3 &value)
{
    mColorFrame = value;
}

ProcedureMode Procedure::getMode() const
{
    return mMode;
}

int Procedure::getCountHalls() const{
    return mHalls.size();
}

glm::vec3 Procedure::getHallCenter(int i){
    return mHalls[i].getCenter();
}

void Procedure::setMode(const ProcedureMode &value)
{
    mMode = value;
}

void Procedure::setColorFill(const glm::vec3 &value)
{
    mColorFill = value;
}

void Procedure::setDrawMask(HallBitmask value, bool fool)
{
    if (fool){
        mDrawMask = value & (mDrawMask | value);
    } else {
        mDrawMask = mDrawMask ^ value;
    }

}

HallBitmask Procedure::getDrawMask()
{
    return mDrawMask;
}

void Procedure::setFillTrancparency(const float value){
    for (int i = 0; i < mHalls.size(); i++){
        mHalls[i].setFillTrancparency(value);
    }

//    for (int i = 0; i < mTurns.size(); i++){
//        mTurns[i].setFillTrancparency(value);
//    }
}

ObjectStatus Procedure::getStatus() const
{
    return Status;
}

void Procedure::setStatus(const ObjectStatus &value)
{
    Status = value;
}

void Procedure::setHeightRate(const float value){
    mHeightRate = value;

    mRenderData[HallGroundLinesProjectionObjectType].mVertex.clear();
    mRenderData[HallGroundLinesObjectType].mVertex.clear();
    mRenderData[HallLinesTrajectoryObjectType].mVertex.clear();

    glm::vec3 CurStart  = glm::vec3(0);
    glm::vec3 CurCenter = glm::vec3(0);
    glm::vec3 CurEnd    = glm::vec3(0);

    for (int i = 0; i < mWayPoints.size() - 2; i++){

        CurStart   = Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z), glm::vec3(0,0,0), mHeightRate*(mWayPoints[i].maxAlt + mWayPoints[i].minAlt)/2);
        CurCenter  = Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z), glm::vec3(0,0,0), mHeightRate*(mWayPoints[i+1].maxAlt + mWayPoints[i+1].minAlt)/2 );
        CurEnd     = Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z), glm::vec3(0,0,0), mHeightRate*(mWayPoints[i+2].maxAlt + mWayPoints[i+2].minAlt)/2 );

        mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z));
        mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i].maxAlt + mWayPoints[i].minAlt)/2));

        mRenderData[HallGroundLinesObjectType].mVertex.push_back(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z));
        mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i].x, mWayPoints[i].y, mWayPoints[i].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i].maxAlt + mWayPoints[i].minAlt)/2));

        if (i == mWayPoints.size() - 3) {

            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z));
            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+1].maxAlt + mWayPoints[i+1].minAlt)/2));

            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z));
            mRenderData[HallGroundLinesProjectionObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+2].maxAlt + mWayPoints[i+2].minAlt)/2));

            mRenderData[HallGroundLinesObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z));
            mRenderData[HallGroundLinesObjectType].mVertex.push_back(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z));

            mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+1].x, mWayPoints[i+1].y, mWayPoints[i+1].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+1].maxAlt + mWayPoints[i+1].minAlt)/2));
            mRenderData[HallLinesTrajectoryObjectType].mVertex.push_back(Helper::CalcPointOnDistance(glm::vec3(mWayPoints[i+2].x, mWayPoints[i+2].y, mWayPoints[i+2].z), glm::vec3(0,0,0), -mHeightRate*(mWayPoints[i+2].maxAlt + mWayPoints[i+2].minAlt)/2));
        }
    }

    initVAO(&mRenderData[HallGroundLinesProjectionObjectType], glm::vec4(0.6, 0.6, 0.6, 0.7f));
    initVAO(&mRenderData[HallGroundLinesObjectType],  glm::vec4(0.5, 0.5, 0.5, 0.9f));
    initVAO(&mRenderData[HallLinesTrajectoryObjectType], glm::vec4(mColorFill, 0.7f));


    for (int i = 0; i < mTurns.size(); i++){
        mTurns[i].setHeightRate(mHeightRate);
        mTurns[i].calcAllRenderPoint();
        mTurns[i].updateBuffers();
    }


    for (int i = 0; i < mHalls.size(); i++){

        if (i == 0){
            mHalls[i].setTurns(NULL, &mTurns[i], true);
        }else if (i != mHalls.size() - 1){
            mHalls[i].setTurns(&mTurns[i - 1], &mTurns[i], true);
        } else{
            mHalls[i].setTurns(&mTurns[i - 1], NULL, true);
        }

        mHalls[i].setHeightRate(mHeightRate);
        mHalls[i].updateRenderPoints();
        mHalls[i].updateBuffers();
    }

}

float Procedure::getHeightRate() const {
    return mHeightRate;
}


void Procedure::clear(){

    for (int i = 0; i < mHalls.size(); i++){
        mHalls[i].clear();
    }

    for (int i = 0; i < mTurns.size(); i++){
        mTurns[i].clear();
    }

}
