#include "include/AirPort/Parser.h"
#include <QTextCodec>
#include "QFile"
#include <QStringList>
#include <iostream>

Parser::Parser() :
    mFileName("")
{
    mProcedures.clear();
}

Parser::Parser(QString fileName) :
    mFileName(fileName)
{
    mProcedures.clear();
}

Parser::~Parser(){
    mPoints.clear();
    mProcedures.clear();
}

void Parser::setFileName(QString fileName){
    mFileName = fileName;
}

QMap <QString, QVector<ProcedureData > >& Parser::getPoints(){
    return mProcedures;
}

void Parser::readData(const QString fileName){
	// Такой фрагмент я уже видел в другом месте
    QTextCodec *russianCodec = QTextCodec::codecForName("Windows-1251");
    QTextCodec::setCodecForTr(russianCodec);
    QTextCodec::setCodecForCStrings(russianCodec);
    QTextCodec::setCodecForLocale(russianCodec);

    QMap<QString, QVector<QString> > points;


    QFile sourceFile("://res/Points.txt");
//    QFile sourceFile(":/moscow/res/Anton/Points_tmp.txt");
    sourceFile.open(QIODevice::ReadOnly);
    QString tmpString;
    QVector <QString> tmpVector;
    while(!sourceFile.atEnd()){
        tmpString = sourceFile.readLine();
        if(tmpString.size() > 0){
            QStringList tmpList = tmpString.split("\t");
            QString latStr = tmpList[1];
            QString lonStr = tmpList[2];
            tmpVector << latStr << lonStr;
            points[tmpList[0]] = tmpVector;
        }
        tmpVector.clear();
    }
    bool newProcedure = true;
    QVector<QString> mapIndices;
    // This vector used for filling map from input file;
    mapIndices << "P/T" << "Waypoint ID" << "Fly Over" << "Course" << "Dist/Time" << "Turn Direction" << "Altitude" << "Speed Limit" << "Specification";
    // 0 - name, 1 - Waypoint, 2 - pathTerminated, 3 - flyOver, 4 - course, 5 - turnDirection, 6 - altitude, 7 - distPerTime, 8 - speedLimit;
    if(!fileName.isEmpty()) {
        setFileName(fileName);
    }
    QFile in(fileName);
    in.open(QIODevice::ReadOnly);
    QString tmpVal;
    QString procedureName;
    QString procedureType = "";
    QVector<QMap<QString, QString> > tmpPoints;
    while(!in.atEnd()){

        QMap<QString, QString> tmpMap;
        tmpVal = in.readLine();
        tmpVal.remove('\n');
        if(tmpVal == "STAR" || tmpVal == "SID" || tmpVal == "APRCH"){
            if(tmpVal != procedureType){
                procedureType = tmpVal;
            }
            continue;
        }
        if(newProcedure){
            procedureName = tmpVal;
            tmpVal = in.readLine();
            newProcedure = false;
        }
        else if(tmpVal.size() <= 10){
            mProcedures[procedureType].push_back({procedureName, tmpPoints});
            tmpPoints.clear();
            newProcedure = true;
        }
        else{
            QStringList tmpList = tmpVal.split('\t');
            if(tmpList[1].contains("HM") || tmpList[1].contains("CA") || tmpList[1].contains("VA")) continue;
                for(int i = 0, k = 1; i < mapIndices.size(); i++, k++){
                    if(k == 2){
                        tmpList[k] = tmpList[k].toUtf8();
                        tmpMap["Waypoint"] = points[tmpList[k]][0] + "/" + points[tmpList[k]][1];
                    }
                    if(mapIndices[i] == "Altitude") tmpList[k].remove("A");
                    if(tmpList[k] == "") tmpList[k] = "-";
                    tmpMap[mapIndices[i]] = tmpList[k];
                }
            }
        if(!tmpMap.isEmpty()) tmpPoints.push_back(tmpMap);
    }
    mProcedures[procedureType].push_back({procedureName, tmpPoints});
    in.close();
}

