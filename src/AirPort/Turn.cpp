#define _USE_MATH_DEFINES
#include <cmath>

#include "include/AirPort/Turn.h"
#include "include/GeoHelper.h"
#include <algorithm>
#include "include/Zones/Zone.h"
#include "MainRenderModel.h"
#include "MainSettingsClass.h"

Turn::Turn(glm::vec3 start, glm::vec3 middle, glm::vec3 end, double LTA, TurnTypes type){

    mLTA = LTA;
    EPS  = 10;

    mStart = start;
    mCenter = middle;
    mEnd = end;

    mTurnType = type;


    mRenderData[BasicType];
    mRenderData[LinesType];

    mHeightRate = 10.0f;

    MainRenderModel = SingleMainRenderModel::Instance();
    SettingsClass = mainSettingsClass::Instance();

}

double Turn::dist(glm::vec3 a, glm::vec3 b){
    return std::sqrt(std::pow(b.z - a.z, 2) + std::pow(b.x - a.x, 2) + std::pow(b.y - a.y, 2));
}

/*-----------------------------------------------

Name:	calcRoundPointOnDist

Params:	start, end - point of line
        lta - dist to translation

Result:	calculate new point on line after start
        on line

-----------------------------------------------*/

glm::vec3 Turn::calcRoundPointOnDist(glm::vec3 start, glm::vec3 end, double lta){

    double coeff = lta / (dist(start, end) - lta);

    double x = (start.x + coeff*end.x) / (1 + coeff);
    double z = (start.z + coeff*end.z) / (1 + coeff);
    double y = (start.y + coeff*end.y) / (1 + coeff);

    return glm::vec3(x, y, z);
}

/*-----------------------------------------------

Name:	calcCenter3D

Params:	-

Result:	calculate Turn Center For FLY_BY

-----------------------------------------------*/

glm::vec3 Turn::calcCenter3D(){

    glm::vec2 polarStart = GeoHelper::cartesian2geo(glm::vec3(mWaypointStart.x, mWaypointStart.y, mWaypointStart.z));
    glm::vec2 polarEnd = GeoHelper::cartesian2geo(glm::vec3(mWaypointEnd.x, mWaypointEnd.y, mWaypointEnd.z));
    glm::vec2 polarCenter = GeoHelper::cartesian2geo(glm::vec3(mWaypointCenter.x, mWaypointCenter.y, mWaypointCenter.z));

    double azimuthCenterToStart, azimuthCenterToEnd;

    //for getting Azimuths

    GeoHelper::inverseProblem(polarCenter.x, polarCenter.y,
                              polarStart.x, polarStart.y,
                              azimuthCenterToStart);

    GeoHelper::inverseProblem(polarCenter.x, polarCenter.y,
                              polarEnd.x, polarEnd.y,
                              azimuthCenterToEnd);

    //calc start and end turn points

    glm::vec3 GeoStartRoundPoint = glm::vec3(GeoHelper::directionProblem( polarCenter,
                                                                          azimuthCenterToStart,
                                                                          ( mLTA / GeoHelper::MAJOR_AXIS )
                                                                        ),
                                              mHeightRate*(mWaypointCenter.maxAlt + mWaypointCenter.minAlt)/2);

    mStartRoundPoint3D =  GeoHelper::geo2cartesianGL(GeoStartRoundPoint, true);

    glm::vec3 GeoEndRoundPoint = glm::vec3(GeoHelper::directionProblem( polarCenter,
                                                                        azimuthCenterToEnd,
                                                                        ( mLTA / GeoHelper::MAJOR_AXIS )
                                                                       ),
                                              mHeightRate*(mWaypointCenter.maxAlt + mWaypointCenter.minAlt)/2);

    mEndRoundPoint3D =  GeoHelper::geo2cartesianGL(GeoEndRoundPoint, true);

    // don't try to understand :)
    glm::vec3 middlePointBetween = (mStartRoundPoint3D + mEndRoundPoint3D);

    middlePointBetween.x /= 2;
    middlePointBetween.y /= 2;
    middlePointBetween.z /= 2;

    glm::vec3 GeoCenterTrajectory = glm::vec3(GeoHelper::directionProblem( polarCenter,
                                                                azimuthCenterToStart,
                                                                0
                                                                ),
                                            mHeightRate*(mWaypointCenter.maxAlt + mWaypointCenter.minAlt)/2);

    glm::vec3 CenterTrajectory =  GeoHelper::geo2cartesianGL(GeoCenterTrajectory, true);

    mCenter = CenterTrajectory;

    glm::vec3 Center = calcRoundPointOnDist(CenterTrajectory, middlePointBetween, mDistCenter * 1000);

    // Height on Start and End on Turn is not same as on Center

    distFromEarthCenter = glm::distance(CenterTrajectory, glm::vec3(0)) - mHeightRate*(mWaypointCenter.maxAlt + mWaypointCenter.minAlt)/2;

    mStartHeight = glm::distance(glm::vec3(0), mStartRoundPoint3D) - distFromEarthCenter;
    mEndHeight = glm::distance(glm::vec3(0), mEndRoundPoint3D) - distFromEarthCenter;

    return Center;
}

bool Turn::getIsLeftTurn() const
{
    return isLeftTurn;
}

double Turn::getWidth() const
{
    return mWidth;
}

void Turn::setWidth(double width)
{
    mWidth = width;
}

double Turn::getEndHeightCurDown() const
{
    return EndHeightCurDown;
}

double Turn::getStartHeightCurDown() const
{
    return StartHeightCurDown;
}

double Turn::getEndHeightCurUp() const
{
    return EndHeightCurUp;
}

double Turn::getStartHeightCurUp() const
{
    return StartHeightCurUp;
}

double Turn::getDopHCenterStart() const
{
    return dopHCenterStart;
}

void Turn::setDopHCenterStart(double value)
{
    dopHCenterStart = value;
}

double Turn::getStartHeightDown() const
{
    return mStartHeightDown;
}

void Turn::setStartHeightDown(double startHeightDown)
{
    mStartHeightDown = startHeightDown;
}

double Turn::getStartHeightUp() const
{
    return mStartHeightUp;
}

void Turn::setStartHeightUp(double startHeightUp)
{
    mStartHeightUp = startHeightUp;
}

double Turn::getDopHDownStart() const
{
    return dopHDownStart;
}

void Turn::setDopHDownStart(double value)
{
    dopHDownStart = value;
}

double Turn::getDopHUpStart() const
{
    return dopHUpStart;
}

void Turn::setDopHUpStart(double value)
{
    dopHUpStart = value;
}

/*-----------------------------------------------

Name:	calcFlyOver

Params:	original points

Result:	1) calculate Round  that begins from center
        2) calculate Round that begins from end
        3) need reverce 2*,and only after that add 1* and 2

-----------------------------------------------*/

QVector <glm::vec3> Turn::calcFlyOver(Waypoint wp1, Waypoint wp2, Waypoint wp3){

    double r1 = Helper::calcR(wp2, 25);
    double r2 = Helper::calcR(wp2, 15);
    double L1, L2, L3, L4, L5;

    QVector <glm::vec3> pointsOfRound;
    QVector <glm::vec3> pointOfSmallRound;
    QVector <glm::vec3> pointOfBigRound;

    glm::vec3 CurStart   = Helper::CalcPointOnDistance(glm::vec3(-wp1.x, wp1.z, wp1.y), glm::vec3(0,0,0), mHeightRate*(wp1.maxAlt + wp1.minAlt)/2 );
    glm::vec3 CurCenter  = Helper::CalcPointOnDistance(glm::vec3(-wp2.x, wp2.z, wp2.y), glm::vec3(0,0,0), mHeightRate*(wp2.maxAlt + wp2.minAlt)/2 );
    glm::vec3 CurEnd     = Helper::CalcPointOnDistance(glm::vec3(-wp3.x, wp3.z, wp3.y), glm::vec3(0,0,0), mHeightRate*(wp3.maxAlt + wp3.minAlt)/2 );

    glm::vec2 polarStart = GeoHelper::cartesian2geo(CurStart);
    glm::vec2 polarEnd = GeoHelper::cartesian2geo(CurEnd);
    glm::vec2 polarCenter = GeoHelper::cartesian2geo(CurCenter);

    double azimuthStartCenter, azimuthEndCenter, theta;
    double alpha = GeoHelper::deg2rad(30);

    GeoHelper::inverseProblem(polarCenter.x, polarCenter.y, polarStart.x, polarStart.y, azimuthStartCenter);
    GeoHelper::inverseProblem(polarEnd.x, polarEnd.y, polarCenter.x, polarCenter.y, azimuthEndCenter);

    theta = fabs(azimuthEndCenter - azimuthStartCenter);
    if (theta > M_PI) theta = 2*M_PI - theta;

    L1 = (r1 * 1000) * sin(theta);
    L2 = (r1 * 1000) * cos(theta) * tan(alpha);
    L3 = (r1 * 1000) *  (1 / sin(alpha) - 2 * cos(theta) / sin(GeoHelper::deg2rad(90 - 30)));
    L4 = (r2 * 1000) * tan(alpha/2);

    double V = Helper::calcTAS(wp2) * 1.852; // км/ч
    L5 = 10 * V/3600 * 1000; // М

    if ((L1 + L2 + L3 + L4 + L5) > glm::distance(CurCenter, CurEnd)) {
        qDebug() << "Not Correct Stabilization Distation FLY_OVER !!! Have" << glm::distance(CurCenter, CurEnd) << "need" << (L1 + L2 + L3 + L4 + L5);
        return pointsOfRound;
    }

    double FirstTurnRadius = r1 * 1000 / GeoHelper::MAJOR_AXIS;
    double SecondTurnRadius = r2 * 1000 / GeoHelper::MAJOR_AXIS;

    glm::vec3 GeoFirstCenter = (glm::vec3(GeoHelper::directionProblem(polarCenter, azimuthStartCenter - M_PI/2, FirstTurnRadius), wp2.maxAlt));
    glm::vec3 FirstCenter1 =  GeoHelper::geo2cartesianGL(GeoFirstCenter, true);

    glm::vec3 GeoFirstCenter2 = (glm::vec3(GeoHelper::directionProblem(polarCenter, azimuthStartCenter + M_PI/2, FirstTurnRadius), wp2.maxAlt));
    glm::vec3 FirstCenter2 =  GeoHelper::geo2cartesianGL(GeoFirstCenter2, true);

    glm::vec3 FirstCenter;

    if (glm::distance(FirstCenter1, CurEnd) > glm::distance(FirstCenter2, CurEnd)){
        FirstCenter = FirstCenter1;
    }
    else {
        FirstCenter = FirstCenter2;
        GeoFirstCenter = GeoFirstCenter2;
    }

    double azimuthCenter_FirstTurnCenter;
    double radiusFirstTurn = GeoHelper::inverseProblem(GeoFirstCenter.x, GeoFirstCenter.y, polarCenter.x, polarCenter.y, azimuthCenter_FirstTurnCenter);

    double angle = azimuthCenter_FirstTurnCenter;
    double AngleStep = 0.01;

    double curHeight = mStartHeight;
    double HeightStep = (mEndHeight - mStartHeight) / (fabs(M_PI/2 + M_PI/6)/AngleStep);

    double AngleVal = 0;

    double firstTurnAngle;

    if (theta < M_PI/2)
        firstTurnAngle = M_PI/6 + M_PI/2 + acos(fabs(L1/(r1*1000)));
    else
        firstTurnAngle = M_PI/6 + M_PI/2 - acos(fabs(L1/(r1*1000)));

    // first point of turn, need to to determine the direction of rotation
    // if distation from start to turn will increase, direction is correct!
    glm::vec3 FirstPointOfTurn(glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoFirstCenter), angle, radiusFirstTurn), curHeight));
    FirstPointOfTurn = GeoHelper::geo2cartesianGL(FirstPointOfTurn, true);
    double distStartToTurn = glm::distance(CurStart, FirstPointOfTurn);

    bool angleIsCorrect = true;
    bool isFirstStep = true;

    angle += AngleStep;

    // first turn
    while (AngleVal < firstTurnAngle){

        pointsOfRound.push_back(glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoFirstCenter), angle, radiusFirstTurn), curHeight));
        pointsOfRound[pointsOfRound.size() - 1] = GeoHelper::geo2cartesianGL(pointsOfRound[pointsOfRound.size() - 1], true);

        pointOfSmallRound.push_back(glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoFirstCenter), angle, radiusFirstTurn - mWidth/(2000.0f * 6371.0f)), curHeight));
        pointOfSmallRound[pointOfSmallRound.size() - 1] = GeoHelper::geo2cartesianGL(pointOfSmallRound[pointOfSmallRound.size() - 1], true);

        pointOfBigRound.push_back(glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoFirstCenter), angle, radiusFirstTurn + mWidth/(2000.0f * 6371.0f)), curHeight));
        pointOfBigRound[pointOfBigRound.size() - 1] = GeoHelper::geo2cartesianGL(pointOfBigRound[pointOfBigRound.size() - 1], true);

        curHeight += HeightStep;

        if (isFirstStep && (glm::distance(CurStart, pointsOfRound[0]) < distStartToTurn)) {
            angleIsCorrect = false;
        }

        // after understanding direction - start again turn building from start angle
        if (isFirstStep)
        {
            pointsOfRound.clear();
            pointOfSmallRound.clear();
            pointOfBigRound.clear();

            angle = azimuthCenter_FirstTurnCenter;
        }

        isFirstStep = false;

        if (angleIsCorrect)
            angle -= AngleStep;
        else
            angle += AngleStep;

        AngleVal += AngleStep;

        if(angle >= 2 * M_PI){
            angle -= 2 * M_PI;
        }
    }

    //second turn

    double azimuthCenter_End, azimuthSecondTurnStart, azimuth7;

    // azimuthCenter_End == azimuthEndCenter

    GeoHelper::inverseProblem(polarEnd.x, polarEnd.y, polarCenter.x, polarCenter.y, azimuthCenter_End);

    double fromEndToSecondTurn =  L2 + L3 + L4 + L1;

    glm::vec3 GeoSecondCenterProjection = (glm::vec3(GeoHelper::directionProblem(polarCenter, azimuthEndCenter + M_PI, fromEndToSecondTurn / GeoHelper::MAJOR_AXIS), wp3.maxAlt));
    GeoHelper::cartesian2geo(GeoSecondCenterProjection);

    glm::vec3 GeoSecondCenter = (glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoSecondCenterProjection), azimuthCenter_End + M_PI/2, SecondTurnRadius), wp3.maxAlt));
    glm::vec3 SecondCenter1 =  GeoHelper::geo2cartesianGL(GeoSecondCenter, true);

    glm::vec3 GeoSecondCenter2 = (glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoSecondCenterProjection), azimuthCenter_End - M_PI/2, SecondTurnRadius), wp3.maxAlt));
    glm::vec3 SecondCenter2 =  GeoHelper::geo2cartesianGL(GeoSecondCenter2, true);

    glm::vec3 SecondCenter;

    if (glm::distance(SecondCenter1, FirstCenter) < glm::distance(SecondCenter2, FirstCenter)){
        SecondCenter = SecondCenter1;
    }
    else {
        SecondCenter = SecondCenter2;
        GeoSecondCenter = GeoSecondCenter2;
    }

    AngleVal = 0;

    GeoHelper::inverseProblem(GeoSecondCenter.x, GeoSecondCenter.y, GeoSecondCenterProjection.x, GeoSecondCenterProjection.y, azimuthSecondTurnStart);

    angle = azimuthSecondTurnStart;

    QVector <glm::vec3> pointsOfRound2;
    QVector <glm::vec3> pointOfSmallRound2;
    QVector <glm::vec3> pointOfBigRound2;

    glm::vec2 polarSecondCenter = GeoHelper::cartesian2geo(SecondCenter);

    double radiusSecondTurn = GeoHelper::inverseProblem(GeoSecondCenter.x, GeoSecondCenter.y, GeoSecondCenterProjection.x, GeoSecondCenterProjection.y, azimuth7);

    FirstPointOfTurn = glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoSecondCenter), angle, radiusSecondTurn), curHeight);
    FirstPointOfTurn = GeoHelper::geo2cartesianGL(FirstPointOfTurn, true);

    distStartToTurn = glm::distance(CurEnd, FirstPointOfTurn);

    isFirstStep = true;

    angle += AngleStep;

    //second turn

    while (AngleVal < M_PI/6){

        pointsOfRound2.push_back(glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoSecondCenter), angle, radiusSecondTurn), curHeight));
        pointsOfRound2[pointsOfRound2.size() - 1] = GeoHelper::geo2cartesianGL(pointsOfRound2[pointsOfRound2.size() - 1], true);

        pointOfSmallRound2.push_back(glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoSecondCenter), angle, radiusSecondTurn - mWidth/(2000.0f * 6371.0f)), curHeight));
        pointOfSmallRound2[pointOfSmallRound2.size() - 1] = GeoHelper::geo2cartesianGL(pointOfSmallRound2[pointOfSmallRound2.size() - 1], true);

        pointOfBigRound2.push_back(glm::vec3(GeoHelper::directionProblem(glm::vec2(GeoSecondCenter), angle, radiusSecondTurn + mWidth/(2000.0f * 6371.0f)), curHeight));
        pointOfBigRound2[pointOfBigRound2.size() - 1] = GeoHelper::geo2cartesianGL(pointOfBigRound2[pointOfBigRound2.size() - 1], true);

        curHeight += HeightStep;

        if (isFirstStep && (glm::distance(CurEnd, pointsOfRound2[0]) < distStartToTurn)) {
            angleIsCorrect = false;
        }

        if (isFirstStep)
        {
            pointsOfRound2.clear();
            pointOfSmallRound2.clear();
            pointOfBigRound2.clear();

            angle = azimuthSecondTurnStart;
        }

        isFirstStep = false;

        if (angleIsCorrect)
            angle -= AngleStep;
        else
            angle += AngleStep;

        AngleVal += AngleStep;

        if(angle >= 2 * M_PI){
            angle -= 2 * M_PI;
        }
    }

    for (int i = pointsOfRound2.size() - 1; i >= 0; i--){
        pointsOfRound.push_back(pointsOfRound2[i]);
        pointOfSmallRound.push_back(pointOfBigRound2[i]);
        pointOfBigRound.push_back(pointOfSmallRound2[i]);
    }

    if (theta > 0.1)
        isNoTurn = false;
    else
        isNoTurn = true;

    mSmallRound = pointOfSmallRound;
    mBigRound = pointOfBigRound;

    return pointsOfRound;
}

void Turn::calcTurnDiretion(){

    double startDirection  = mWaypointCenter.mCourse;
    double centerDirection = mWaypointEnd.mCourse;

    if (startDirection == 0 || centerDirection == 0){

        double az1, az2;

        GeoHelper::GEODESIC.Inverse(mWaypointStart.lat,mWaypointStart.lon, mWaypointCenter.lat, mWaypointCenter.lon, az1, az2 );
        startDirection = az1;
        az1 = 0; az2 = 0;
        GeoHelper::GEODESIC.Inverse(mWaypointCenter.lat,mWaypointCenter.lon, mWaypointEnd.lat, mWaypointEnd.lon, az1, az2 );
        centerDirection = az1;

    }

    float deltaDirection;
    if (centerDirection - startDirection > 180.0f){
        deltaDirection = centerDirection-startDirection-360;
    } else {
        if (startDirection-centerDirection > 180){
            deltaDirection = centerDirection-startDirection+360;
        } else {
            deltaDirection = centerDirection-startDirection;
        }
    }

    if (deltaDirection > 0){
        isLeftTurn = false;
    } else{
        isLeftTurn = true;
    }


//    if (isLeftTurn){
//        qDebug() << "Left" << startDirection << centerDirection << mWaypointCenter.mName;
//    }else {
//        qDebug() << "Right" << startDirection << centerDirection << mWaypointCenter.mName;
//    }



}

/*-----------------------------------------------

Name:	calcFlyBy

Params:	original points

Result:	1) calculate Round  that begins from center
        2) calculate Round that begins from end
        3) need reverce 2*,and only after that add 1* and 2

-----------------------------------------------*/

QVector <glm::vec3> Turn::calcFlyBy(glm::vec3 start, glm::vec3 center, glm::vec3 end){

    glm::vec2 polarStart = GeoHelper::cartesian2geo(start);
    glm::vec2 polarEnd = GeoHelper::cartesian2geo(end);
    glm::vec2 polarCenter = GeoHelper::cartesian2geo(center);

    double azimuthStartCenter, azimuthEndCenter;
    double AngleStep = 0.025;

    double radius = GeoHelper::inverseProblem(polarCenter.x, polarCenter.y, polarStart.x, polarStart.y, azimuthStartCenter);
    GeoHelper::inverseProblem(polarCenter.x, polarCenter.y, polarEnd.x, polarEnd.y, azimuthEndCenter);

    radius = mRadiusTurn / GeoHelper::MINOR_AXIS * 1000;

    double angle = azimuthStartCenter;

    QVector <glm::vec3> pointsOfRound;
    QVector <glm::vec3> pointOfSmallRound;
    QVector <glm::vec3> pointOfBigRound;

    double curHeight = (mStartHeightUp + mStartHeightDown) / 2;
    double HeightStep = (((mEndHeightUp + mEndHeightDown)/2) - ((mStartHeightUp + mStartHeightDown)/2)) / (fabs(azimuthEndCenter - azimuthStartCenter)/AngleStep);
    bool correctTurn = true;

    glm::vec2 polarMCenter = GeoHelper::cartesian2geo(mCenter);
//    glm::vec2 polarMCenter = GeoHelper::cartesian2geo(glm::vec3(mWaypointCenter.x, mWaypointCenter.y, mWaypointCenter.z));

    double azimuthStartCenterTrajectory, azimuthCenterTrajectoryEnd;
    GeoHelper::inverseProblem(polarStart.x, polarStart.y, polarMCenter.x, polarMCenter.y, azimuthStartCenterTrajectory);
    GeoHelper::inverseProblem(polarMCenter.x, polarMCenter.y, polarEnd.x, polarEnd.y, azimuthCenterTrajectoryEnd);

    isNoTurn = false;

    double countAngle = 0;

    // first build in one direction

    if (!(fabs(azimuthStartCenterTrajectory - azimuthCenterTrajectoryEnd) < 0.025)) {

        while (fabs(angle - azimuthEndCenter) > 0.025){

            pointsOfRound.push_back(glm::vec3(GeoHelper::directionProblem(polarCenter, angle, radius), curHeight));
            pointsOfRound[pointsOfRound.size() - 1] = GeoHelper::geo2cartesianGL(pointsOfRound[pointsOfRound.size() - 1], true);

            pointOfSmallRound.push_back(glm::vec3(GeoHelper::directionProblem(polarCenter, angle, radius - mWidth/(2000.0f * 6371.0f)), curHeight));
            pointOfSmallRound[pointOfSmallRound.size() - 1] = GeoHelper::geo2cartesianGL(pointOfSmallRound[pointOfSmallRound.size() - 1], true);

            pointOfBigRound.push_back(glm::vec3(GeoHelper::directionProblem(polarCenter, angle, radius + mWidth/(2000.0f * 6371.0f)), curHeight));
            pointOfBigRound[pointOfBigRound.size() - 1] = GeoHelper::geo2cartesianGL(pointOfBigRound[pointOfBigRound.size() - 1], true);

            countAngle += AngleStep;

            if (countAngle > M_PI){
                correctTurn = false;
                break;
            }


            curHeight += HeightStep;

            angle += AngleStep;

            if(angle >= 2 * M_PI){
                angle -= 2 * M_PI;
            }
        }


        // if not correct direction build in other direction
        if (!correctTurn){

            pointsOfRound.clear();
            pointOfSmallRound.clear();
            pointOfBigRound.clear();
            curHeight = mStartHeight;
            angle = azimuthStartCenter;

            while(fabs(angle - azimuthEndCenter) > 0.025){
                pointsOfRound.push_back(glm::vec3(GeoHelper::directionProblem(polarCenter, angle, radius), curHeight));
                pointsOfRound[pointsOfRound.size() - 1] = GeoHelper::geo2cartesianGL(pointsOfRound[pointsOfRound.size() - 1], true);

                pointOfSmallRound.push_back(glm::vec3(GeoHelper::directionProblem(polarCenter, angle, radius - mWidth/(2000.0f * 6371.0f)), curHeight));
                pointOfSmallRound[pointOfSmallRound.size() - 1] = GeoHelper::geo2cartesianGL(pointOfSmallRound[pointOfSmallRound.size() - 1], true);

                pointOfBigRound.push_back(glm::vec3(GeoHelper::directionProblem(polarCenter, angle, radius + mWidth/(2000.0f * 6371.0f)), curHeight));
                pointOfBigRound[pointOfBigRound.size() - 1] = GeoHelper::geo2cartesianGL(pointOfBigRound[pointOfBigRound.size() - 1], true);

                curHeight += HeightStep;

                angle -= AngleStep;

                if(angle <= 0){
                    angle += 2 * M_PI;
                }
            }
        }

    } else{

        isNoTurn = true;
    }

    mSmallRound = pointOfSmallRound;
    mBigRound = pointOfBigRound;


    return pointsOfRound;
}

void Turn::calcBasicTrajectory(){

    glm::vec3 center = calcCenter3D();
    mCenterTurn = center;

    if (mTurnType == FLY_BY){
        mTurn = calcFlyBy(mStartRoundPoint3D, center, mEndRoundPoint3D);
    }
    if (mTurnType == FLY_OVER){
        mTurn = calcFlyOver(mWaypointStart, mWaypointCenter, mWaypointEnd);
    }
}

void Turn::calcHeightsStart(){

    // up

    glm::vec3 start = glm::vec3(mWaypointStart.x, mWaypointStart.y, mWaypointStart.z);
    glm::vec3 center = glm::vec3(mWaypointCenter.x, mWaypointCenter.y, mWaypointCenter.z);

    double distHUp = glm::distance(start, center);
    float deltaHup = mWaypointStart.maxAlt - mWaypointCenter.maxAlt;

    dopHUpStart = mLTA * (deltaHup / distHUp); // LTA * tg alpha

    mStartHeightUp = dopHUpStart + mWaypointCenter.maxAlt;

    // down

    double distHdown = glm::distance(start, center);
    float deltaHdown = mWaypointStart.minAlt - mWaypointCenter.minAlt;

    dopHDownStart = mLTA * (deltaHdown / distHdown);

    mStartHeightDown = mWaypointCenter.minAlt + dopHDownStart;


    if (mTurn.size() > 0) {
        double distHCenter = glm::distance(mTurn[0], mTurn[mTurn.size() / 2]);
        float deltaHCenter = (mWaypointStart.maxAlt + mWaypointStart.minAlt)/2 - (mWaypointCenter.maxAlt + mWaypointCenter.minAlt)/2;

        dopHCenterStart = mLTA * (deltaHCenter / distHCenter);

    }

}

void Turn::calcHeightsEnd(){

    // up

    glm::vec3 center = glm::vec3(mWaypointCenter.x, mWaypointCenter.y, mWaypointCenter.z);
    glm::vec3 end = glm::vec3(mWaypointEnd.x, mWaypointEnd.y, mWaypointEnd.z);

    double distHUp = glm::distance(end, center);
    float deltaHup = mWaypointCenter.maxAlt - mWaypointEnd.maxAlt;

    dopHUpEnd = mLTA * (deltaHup / distHUp);

    mEndHeightUp = mWaypointCenter.maxAlt - dopHUpEnd ;

    // down

    double distHdown = glm::distance(end, center);
    float deltaHdown = mWaypointCenter.minAlt  - mWaypointEnd.minAlt;

    dopHDownEnd = mLTA * (deltaHdown / distHdown);
    mEndHeightDown = mWaypointCenter.minAlt - (dopHDownEnd);

}

void Turn::calcAllRenderPoint(){

    int indI = 0;   // last used index of hall!

    mRenderData[LinesType].mVertex.clear();
    mRenderData[LinesType].mIndices.clear();

    mRenderData[HallLinesTrajectoryObjectType].mVertex.clear();
    mRenderData[HallLinesTrajectoryObjectType].mIndices.clear();

    mRenderData[BasicType].mVertex.clear();
    mRenderData[BasicType].mIndices.clear();
    mRenderData[BasicType].mNormal.clear();



    if (mTurn.size() > 0 && !getIsNoTurn()){  // central line (in center of Hall)

        // need to offset start Index of next Line.
        indI = mTurn.size();

        double heightStepUp =  ((mEndHeightUp - mStartHeightUp) / mTurn.size()) * mHeightRate ;
        double heightStepDown = ((mEndHeightDown - mStartHeightDown) / mTurn.size()) * mHeightRate  ;

        StartHeightCurUp = (mStartHeightUp) * mHeightRate ;
        StartHeightCurDown =  (mStartHeightDown) * mHeightRate;

        EndHeightCurUp = StartHeightCurUp + heightStepUp * (mTurn.size() - 2);
        EndHeightCurDown = StartHeightCurDown + heightStepDown *(mTurn.size() - 2);

        //central Line Of Turn
        addLineVertex(&mTurn, LinesType, (StartHeightCurUp + StartHeightCurDown)/2, (heightStepUp + heightStepDown) / 2, 0);
        addLineVertex(&mTurn, HallLinesTrajectoryObjectType, (StartHeightCurUp + StartHeightCurDown)/2, (heightStepUp + heightStepDown) / 2, 0);

        // Up! lines of Turn

        if (mSmallRound.size() > 0){

            addLineVertex(&mSmallRound, LinesType, StartHeightCurUp, heightStepUp, indI);
        }

        indI += mSmallRound.size();

        if (mBigRound.size() > 0){

             addLineVertex(&mBigRound, LinesType, StartHeightCurUp, heightStepUp, indI);
        }

        indI += mSmallRound.size();

        // Down lines of Turn

        if (mSmallRound.size() > 0){

            addLineVertex(&mSmallRound, LinesType, StartHeightCurDown, heightStepDown, indI);
        }

        indI += mBigRound.size();

        if (mBigRound.size() > 0){

             addLineVertex(&mBigRound, LinesType, StartHeightCurDown, heightStepDown, indI);
        }

        int startSmallUp = mTurn.size();
        int startBigUp = mTurn.size()*2;
        int startSmallDown = mTurn.size()*3;
        int startBigDown = mTurn.size()*4;

        int ind = 0;

        for (int i = 0; i < mTurn.size() - 1; i += 1){
            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + startSmallDown]);  //0
            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + 1 + startSmallDown]); //1
            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + 1 + startBigDown]); //2
            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + startBigDown]); //3

            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + startSmallUp]);  //4
            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + 1 + startSmallUp]);  //5
            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + 1 + startBigUp]);  //6
            mRenderData[BasicType].mVertex.push_back(mRenderData[LinesType].mVertex[i + startBigUp]); //7

            //down
            mRenderData[BasicType].mIndices.push_back(ind);
            mRenderData[BasicType].mIndices.push_back(ind + 1);
            mRenderData[BasicType].mIndices.push_back(ind + 2);

            mRenderData[BasicType].mIndices.push_back(ind + 2);
            mRenderData[BasicType].mIndices.push_back(ind);
            mRenderData[BasicType].mIndices.push_back(ind + 3);

            //up
            mRenderData[BasicType].mIndices.push_back(ind + 4);
            mRenderData[BasicType].mIndices.push_back(ind + 5);
            mRenderData[BasicType].mIndices.push_back(ind + 6);

            mRenderData[BasicType].mIndices.push_back(ind + 6);
            mRenderData[BasicType].mIndices.push_back(ind + 4);
            mRenderData[BasicType].mIndices.push_back(ind + 7);

            // small side

            mRenderData[BasicType].mIndices.push_back(ind);
            mRenderData[BasicType].mIndices.push_back(ind + 1);
            mRenderData[BasicType].mIndices.push_back(ind + 5);

            mRenderData[BasicType].mIndices.push_back(ind + 5);
            mRenderData[BasicType].mIndices.push_back(ind + 4);
            mRenderData[BasicType].mIndices.push_back(ind);

            // big side

            mRenderData[BasicType].mIndices.push_back(ind + 3);
            mRenderData[BasicType].mIndices.push_back(ind + 2);
            mRenderData[BasicType].mIndices.push_back(ind + 6);

            mRenderData[BasicType].mIndices.push_back(ind + 6);
            mRenderData[BasicType].mIndices.push_back(ind + 7);
            mRenderData[BasicType].mIndices.push_back(ind + 3);

            ind += 8;
        }

    }

    for ( int i = 0; i < mRenderData[BasicType].mVertex.size(); i++ ) {
        mRenderData[BasicType].mNormal.push_back(glm::vec3());
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mIndices.size() ; i += 3) {

        int Index0 = mRenderData[BasicType].mIndices[i];
        int Index1 = mRenderData[BasicType].mIndices[i + 1];
        int Index2 = mRenderData[BasicType].mIndices[i + 2];

        glm::vec3 v1 = mRenderData[BasicType].mVertex[Index1] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 v2 = mRenderData[BasicType].mVertex[Index2] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 Normal = glm::cross(v1,v2);

        glm::normalize(Normal);

        mRenderData[BasicType].mNormal[Index0] += Normal;
        mRenderData[BasicType].mNormal[Index1] += Normal;
        mRenderData[BasicType].mNormal[Index2] += Normal;
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mNormal.size() ; i++) {
        glm::normalize(mRenderData[BasicType].mNormal[i]);
    }
}

void  Turn::addLineVertex(QVector<glm::vec3> *turn, double Height, int idOffset){

    for (int i = 1; i < turn->size(); i++){
        mRenderData[LinesType].mVertex.push_back(Helper::CalcPointOnDistance(turn->at(i), glm::vec3(0,0,0), Height));
        mRenderData[LinesType].mIndices.push_back(i - 1 + idOffset);
        mRenderData[LinesType].mIndices.push_back(i + idOffset);
    }
}

void  Turn::addLineVertex(QVector<glm::vec3> *turn, RenderObjectType type, double Height, double HeightStep, int idOffset){

    glm::vec2 geoCoord = GeoHelper::cartesian2geo(glm::vec3(-turn->at(0).x, turn->at(0).z, turn->at(0).y));
    mRenderData[type].mVertex.push_back(GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, Height), true));

    for (int i = 1; i < turn->size(); i++){

        glm::vec2 geoCoord = GeoHelper::cartesian2geo(glm::vec3(-turn->at(i).x, turn->at(i).z, turn->at(i).y));

        mRenderData[type].mVertex.push_back( GeoHelper::geo2cartesianGL(glm::vec3(geoCoord, Height), true));
        mRenderData[type].mIndices.push_back(i - 1 + idOffset);
        mRenderData[type].mIndices.push_back(i + idOffset);

        Height += HeightStep;
    }
}


void Turn::init(){

    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mElementBuffer.create();
    mRenderData[BasicType].mElementBuffer.bind();
    if (mRenderData[BasicType].mIndices.size() > 0)
        mRenderData[BasicType].mElementBuffer.setData(mRenderData[BasicType].mIndices.size() * sizeof(GLuint), &mRenderData[BasicType].mIndices.front());

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    if (mRenderData[BasicType].mVertex.size() > 0)
        mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mColorBuffer.create();
    mRenderData[BasicType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    glm::vec4 color1 = glm::vec4(mColor, TRANCPARENCY_HALL/100.0f);
    mRenderData[BasicType].mColorBuffer.setData(sizeof(glm::vec4), &color1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

//    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
//    mRenderData[BasicType].mVertexBuffer.create();
//    mRenderData[BasicType].mVertexBuffer.bind();
//    glEnableVertexAttribArray(3);
//    if (mRenderData[BasicType].mNormal.size() > 0)
//        mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mNormal.size() * sizeof(glm::vec3), &mRenderData[BasicType].mNormal.front());
//    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVAO.unbind();

    mRenderData[LinesType].mVAO.create();
    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mElementBuffer.create();
    mRenderData[LinesType].mElementBuffer.bind();
    if (mRenderData[LinesType].mIndices.size() > 0)
        mRenderData[LinesType].mElementBuffer.setData(mRenderData[LinesType].mIndices.size() * sizeof(GLuint), &mRenderData[LinesType].mIndices.front());

    mRenderData[LinesType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mVertexBuffer.create();
    mRenderData[LinesType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    if (mRenderData[LinesType].mVertex.size() > 0)
        mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mColorBuffer.create();
    mRenderData[LinesType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    glm::vec4 color2 = glm::vec4(mColor,TRANCPARENCY_LINE/100.0f);
    mRenderData[LinesType].mColorBuffer.setData(sizeof(glm::vec4), &color2);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    mRenderData[LinesType].mVAO.unbind();


    mRenderData[HallLinesTrajectoryObjectType].mVAO.create();
    mRenderData[HallLinesTrajectoryObjectType].mVAO.bind();

    mRenderData[HallLinesTrajectoryObjectType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[HallLinesTrajectoryObjectType].mElementBuffer.create();
    mRenderData[HallLinesTrajectoryObjectType].mElementBuffer.bind();
    if (mRenderData[HallLinesTrajectoryObjectType].mIndices.size() > 0)
        mRenderData[HallLinesTrajectoryObjectType].mElementBuffer.setData(mRenderData[HallLinesTrajectoryObjectType].mIndices.size() * sizeof(GLuint), &mRenderData[HallLinesTrajectoryObjectType].mIndices.front());

    mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.create();
    mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    if (mRenderData[HallLinesTrajectoryObjectType].mVertex.size() > 0)
        mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.setData(mRenderData[HallLinesTrajectoryObjectType].mVertex.size() * sizeof(glm::vec3), &mRenderData[HallLinesTrajectoryObjectType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[HallLinesTrajectoryObjectType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[HallLinesTrajectoryObjectType].mColorBuffer.create();
    mRenderData[HallLinesTrajectoryObjectType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    glm::vec4 color = glm::vec4(mColor,0.8f);
    mRenderData[HallLinesTrajectoryObjectType].mColorBuffer.setData(sizeof(glm::vec4), &color );
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    mRenderData[HallLinesTrajectoryObjectType].mVAO.unbind();

}

void Turn::paint(){

    if (pProcedure->getDrawMask() & LINES) {

        mRenderData[HallLinesTrajectoryObjectType].mVAO.bind();
        updateColor(&mRenderData[LinesType], LINES);
        glDrawElements(GL_LINES, mRenderData[HallLinesTrajectoryObjectType].mIndices.size(), GL_UNSIGNED_INT, 0);
        mRenderData[HallLinesTrajectoryObjectType].mVAO.unbind();
    }

    MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);


    if (pProcedure->getDrawMask() & LINES) {

        mRenderData[LinesType].mVAO.bind();
        updateColor(&mRenderData[LinesType], LINES);
        glDrawElements(GL_LINES, mRenderData[LinesType].mIndices.size(), GL_UNSIGNED_INT, 0);
        mRenderData[LinesType].mVAO.unbind();
    }

    if (pProcedure->getDrawMask() & HALL) {

        mRenderData[BasicType].mVAO.bind();
        updateColor(&mRenderData[BasicType], HALL);
        glDrawElementsInstanced(GL_TRIANGLES, mRenderData[BasicType].mIndices.size(), GL_UNSIGNED_INT, 0, 1);
        mRenderData[BasicType].mVAO.unbind();

    }
}

void Turn::updateColor(RenderData *curVAO, elementsToDraw element){

    curVAO->mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    curVAO->mColorBuffer.clear();

    ProcedureType type = pProcedure->getType();
    glm::vec3 color3;
    if (pProcedure->getStatus() == CHECKED){
        color3 = SettingsClass->getCheckedObjectColor();
    } else if (pProcedure->getStatus() == UNCHECKED){
        color3 = SettingsClass->getProceduresColors(type);
    }

    float trancpar;

    if (element == HALL){
        trancpar = SettingsClass->getObjectsTrancparency(TRANCPARENCY_HALL);
    } else if (element == LINES) {
        trancpar = SettingsClass->getObjectsTrancparency(TRANCPARENCY_LINE);
    }

    glm::vec4 color4 = glm::vec4(color3, trancpar);

    curVAO->mColorBuffer.setData(sizeof(glm::vec4), &color4);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);
}

void Turn::updateBuffers(){
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mElementBuffer.bind();
    if (mRenderData[BasicType].mIndices.size() > 0)
        mRenderData[BasicType].mElementBuffer.setData(mRenderData[BasicType].mIndices.size() * sizeof(GLuint), &mRenderData[BasicType].mIndices.front());

    mRenderData[BasicType].mVertexBuffer.bind();
    mRenderData[BasicType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    if (mRenderData[BasicType].mVertex.size() > 0)
        mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVAO.unbind();

    mRenderData[LinesType].mVAO.bind();


    mRenderData[LinesType].mElementBuffer.bind();
    if (mRenderData[LinesType].mIndices.size() > 0)
        mRenderData[LinesType].mElementBuffer.setData(mRenderData[LinesType].mIndices.size() * sizeof(GLuint), &mRenderData[LinesType].mIndices.front());

    mRenderData[LinesType].mVertexBuffer.bind();
    mRenderData[LinesType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    if (mRenderData[LinesType].mVertex.size() > 0)
        mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mVAO.unbind();

    mRenderData[HallLinesTrajectoryObjectType].mVAO.bind();


    mRenderData[HallLinesTrajectoryObjectType].mElementBuffer.bind();
    if (mRenderData[HallLinesTrajectoryObjectType].mIndices.size() > 0)
        mRenderData[HallLinesTrajectoryObjectType].mElementBuffer.setData(mRenderData[HallLinesTrajectoryObjectType].mIndices.size() * sizeof(GLuint), &mRenderData[HallLinesTrajectoryObjectType].mIndices.front());

    mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.bind();
    mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.clear();
    glEnableVertexAttribArray(0);
    if (mRenderData[HallLinesTrajectoryObjectType].mVertex.size() > 0)
        mRenderData[HallLinesTrajectoryObjectType].mVertexBuffer.setData(mRenderData[HallLinesTrajectoryObjectType].mVertex.size() * sizeof(glm::vec3), &mRenderData[HallLinesTrajectoryObjectType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[HallLinesTrajectoryObjectType].mVAO.unbind();
}


void Turn::setWayPoints(Waypoint start, Waypoint center, Waypoint end){
    mWaypointStart = start;
    mWaypointCenter = center;
    mWaypointEnd = end;
}

void Turn::SetStartPointForHeight ( Waypoint point ){
    mWaypointStartHeight = point;
}

QVector<glm::vec3> Turn::getRoundPoints(){
    return mTurn;
}

glm::vec3 Turn::getStartPoint(){
    return mStartRoundPoint3D;
}

glm::vec3 Turn::getEndPoint(){
    return mEndRoundPoint3D;
}

glm::vec3 Turn::getCenterOfRound(){

    return mCenterTurn;
}

QVector<glm::vec3> Turn::getSmallRound(){
    return mSmallRound;
}

QVector<glm::vec3> Turn::getBigRound(){
    return mBigRound;
}

double Turn::getEndHeight(){
    return mEndHeight;
}

double Turn::getStartHeight(){
    return mStartHeight;
}

void Turn::setEndHeight(double h){
     mEndHeight = h;
}

void Turn::setStartHeight(double h){
     mStartHeight = h;
}

void Turn::setRadius(double r){
    this->mRadiusTurn = r;
}

void Turn::setDistCenter(double c){
    this->mDistCenter = c;
}

void Turn::setHeight(double h){
    this->mCenterHeight = h;
}

bool Turn::getIsNoTurn() const
{
    return isNoTurn;
}

void Turn::setIsNoTurn(bool value)
{
    isNoTurn = value;
}

void Turn::setColor(glm::vec3 color){
    this->mColor = color;
}

void Turn::setHeightRate(const float value){
    mHeightRate = value;
}

float Turn::getHeightRate() const {
    return mHeightRate;
}

void Turn::setParentProcedure ( Procedure *parent ){
    pProcedure = parent;
}

Procedure * Turn::getParentProcedure (  ){
    return pProcedure;
}
