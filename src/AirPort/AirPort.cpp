#include "include/AirPort/AirPort.h"

AirPort::AirPort(){
    mColorForChecked = glm::vec3(1.0f,0.0f,1.0f);
    mProcedureColors[STAR] = glm::vec3(1.0f, 1.0f, 0.0f);
    mProcedureColors[DEPARTURE] = glm::vec3(0.8f, 0.8f, 0.8f);
    mProcedureColors[APRCH] = glm::vec3(0.0f, 1.0f, 0.0f);

    mHeightRate = 10;
    mDrawMask = 0;
}


float AirPort::TRANCPARENCY_HALL = 10;


/*-----------------------------------------------

Name:	fillData

Params:	QMap<QString, QVector<QVector<Waypoint> > >
        - all points of airport


Result:	create new Procedure, fill it with points and
        add this procedure to mProcedures.

-----------------------------------------------*/

void AirPort::fillData(QMap<QString, QVector<QVector<Waypoint> > > &airPoints)
{
    QList<QString> keys = airPoints.keys();

    for(int key = 0; key < keys.size(); key++){

        for (int i = 0; i < airPoints[keys[key]].size(); i++){

            if (airPoints[keys[key]][i].size() == 0) {qDebug() << "Empty AirPoints!"; break;}

//            if (!airPoints[keys[key]][i][0].mProcedureName.contains("LARIN 1 E")){
//                continue;
//            }

            // setting color for Procedure
            glm::vec3 mColorFill;

            ProcedureType type;

            if (key == 0) {
                type = APRCH;
                mColorFill = mProcedureColors[APRCH];
            }
            if (key == 1) {
                type = DEPARTURE;
                mColorFill = mProcedureColors[DEPARTURE];
            }
            if (key == 2){
                type = STAR;
                mColorFill = mProcedureColors[STAR];
            }

            //creating and setting main data for procedure
            Procedure newProcedure(airPoints[keys[key]][i][0].type, airPoints[keys[key]][i][0].mode, airPoints[keys[key]][i][0].mProcedureName);

            newProcedure.setColorFill(mColorFill);
            newProcedure.setDrawMask(mDrawMask, true);

            if (airPoints[keys[key]][i][0].type == AIRWAY){
                type = AIRWAY;
            }

            newProcedure.setType(type);

            // add Air Points to Procedure

            for (int j = 0; j < airPoints[keys[key]][i].size(); j++){
                newProcedure.addWayPoint(airPoints[keys[key]][i][j]);
            }


            // saving Procedure in Airport
            mProcedures.push_back(newProcedure);

        }
    }
}

/*-----------------------------------------------

Name:	init

Params:	-

Result:	after all Procedures have AirPoints it necessary
        to calculate all turns (FLY_OVER, FLY_BY) and other data
        for rendering.

        Every Procedure in Airport will have all data for
        further rendering.

-----------------------------------------------*/

void AirPort::init()
{
    for (int i = 0; i < mProcedures.size(); i++){
        mProcedures[i].init();
    }
}

/*-----------------------------------------------

Name:	draw

Params:	-

Result:	draw all Procedures

-----------------------------------------------*/

void AirPort::draw(){

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < mProcedures.size(); i++){
        mProcedures[i].draw(mDrawMask);
    }

    glDisable(GL_BLEND);
}

/*-----------------------------------------------

Name:	draw

Params:	name - need to Draw Procedure

Result:	draw only Procedures, that have name
        from input param

-----------------------------------------------*/

void AirPort::draw(QString name){

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < mProcedures.size(); i++){

        if (mProcedures[i].getName() == name){

            mProcedures[i].draw(mDrawMask);
        }
    }

    glDisable(GL_BLEND);
}

/*-----------------------------------------------

Name:	draw

Params:	name - need to Draw Procedure
        id - number of Hall of Procedure

Result:	draw only part of Procedures, that have name
        from input param and number.

        it necessary to correct distance sorting (drawing)

-----------------------------------------------*/

void AirPort::draw(QString name, int id){

    for (int i = 0; i < mProcedures.size(); i++){

        if (mProcedures[i].getName() == name){

//            mProcedures[i].draw(id, mDrawMask);
            mProcedures[i].paint(id);
        }
    }
}

Hall* AirPort::getCursorOnHall () {
    return &mCursorOnHall;
}

/*-----------------------------------------------

Name:	addCheckedProcedure

Params:	name - Procedure name

Result:	set reversed flag for IsChecked for Procedure.
        if was CHECKED - set UNCHECKED
        if was UNCHECKED - set CHECKED
        and set correct color

-----------------------------------------------*/

bool AirPort::addCheckedProcedure(QString name){

    for (int i = 0; i < mProcedures.size(); i++){

        if (mProcedures[i].getName() == name){

            if (mProcedures[i].getStatus() == CHECKED){

                mProcedures[i].setStatus(UNCHECKED);

            }else {

                mProcedures[i].setStatus(CHECKED);
            }

            return true;
        }
    }

    return false;
}

void AirPort::setDrawMask(const HallBitmask &value)
{
    mDrawMask = mDrawMask ^ value;

    for (int i = 0; i < mProcedures.size(); i++){

        mProcedures[i].setDrawMask(mDrawMask, true);

    }
}

HallBitmask AirPort::getDrawMask() const
{
    return mDrawMask;
}


QString AirPort::getName() const
{
    return mName;
}

void AirPort::setName(const QString &value)
{
    mName = value;
}

QStringList AirPort::getProcedureNames() const{

   QStringList list;

   for (int i = 0; i < mProcedures.size(); i++){

      list << mProcedures[i].getName();

   }

   return list;
}

void AirPort::setHeightRate(const float value){
    mHeightRate = value;

    for (int i = 0; i < mProcedures.size(); i++){

       mProcedures[i].setHeightRate(mHeightRate);

    }
}

float AirPort::getHeightRate() const {
    return mHeightRate;
}

void AirPort::clear(){
    for (int i = 0; i < mProcedures.size(); i++){
        mProcedures[i].clear();
    }
}

void AirPort::setFillTrancparency(float val){
    TRANCPARENCY_HALL = val;

    for (int i = 0; i < mProcedures.size(); i++){
       mProcedures[i].setFillTrancparency(TRANCPARENCY_HALL);
    }

}

float AirPort::getFillTrancparency(){
    return TRANCPARENCY_HALL;
}
