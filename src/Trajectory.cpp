#include "include/Trajectory.h"
#include <glm/gtc/type_ptr.hpp>

Trajectory::Trajectory()
{
}

void Trajectory::print(){
    for(int i = 0; i < mCoords.size(); i++)
    {
        qDebug() << mCoords[i].x << mCoords[i].y << mCoords[i].z;
    }
}

void Trajectory::init(glm::mat4 model, glm::mat4 view, glm::mat4 proj){
//    mModel = &model;
//    mView = &view;
//    mProj = &proj;
//    const GLchar* vertexShaderSource = "#version 420 core\n"
//            "layout (location = 0) in vec3 position;\n"
//            "layout (location = 1) in vec4 color;\n"
//            "out vec4 ourColor;\n"
//            "uniform mat4 model;\n"
//            "uniform mat4 view;\n"
//            "uniform mat4 projection;\n"
//            "void main()\n"
//            "{\n"
//            "gl_Position = projection * view * model * vec4(position, 1.0f);\n"
//            "ourColor = color;\n"
//            "}\0";

//        const GLchar* fragmentShaderSource = "#version 420 core\n"
//            "in vec3 ourColor;\n"
//            "out vec4 color;\n"
//            "void main()\n"
//            "{\n"
//            "color = ourColor;\n"
//            "}\n\0";

//        mVertexShader = glCreateShader(GL_VERTEX_SHADER);
//        glShaderSource(mVertexShader, 1, &vertexShaderSource, NULL);
//        glCompileShader(mVertexShader);

//        mFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
//        glShaderSource(mFragmentShader, 1, &fragmentShaderSource, NULL);
//        glCompileShader(mFragmentShader);

//        mShaderProgram = glCreateProgram();
//        glAttachShader(mShaderProgram, mFragmentShader);
//        glAttachShader(mShaderProgram, mVertexShader);
//        glLinkProgram(mShaderProgram);

        glGenVertexArrays(1, &mVAO);

        glGenBuffers(2, mVBO);
        glBindVertexArray(mVAO);
        glBindBuffer(GL_ARRAY_BUFFER, mVBO[0]);
            glEnableVertexAttribArray(0);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * mCoords.size(), mCoords.data(), GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

        glBindBuffer(GL_ARRAY_BUFFER, mVBO[1]);
            glEnableVertexAttribArray(1);
        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * mColors.size(), mColors.data(), GL_STATIC_DRAW);
        glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

//        mModelID = glGetUniformLocation(mShaderProgram, "model");
//        mProjID = glGetUniformLocation(mShaderProgram, "projection");
//        mViewID = glGetUniformLocation(mShaderProgram, "view");
}

void Trajectory::draw()
{
//    glUseProgram(mShaderProgram);

//    glUniformMatrix4fv(mModelID, 1, GL_FALSE, glm::value_ptr(*mModel));
//    glUniformMatrix4fv(mProjID, 1, GL_FALSE, glm::value_ptr(*mProj));
//    glUniformMatrix4fv(mViewID, 1, GL_FALSE, glm::value_ptr(*mView));

    glBindVertexArray(mVAO);
//    glEnableVertexAttribArray(0);
//    glEnableVertexAttribArray(1);


    glDrawArrays(GL_LINES, 0, mCoords.size());
//    glDisableVertexAttribArray(0);
//    glDisableVertexAttribArray(1);
    glBindVertexArray(0);
}

void Trajectory::addNewPoint(glm::vec3 newPoint)
{
//    mCoords.push_back(mCoords.back());
    mCoords.push_back(newPoint);
    mColors.push_back(glm::vec4(1.0f));
//    mColors.push_back(glm::vec4(1.0f));
    glBindVertexArray(mVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO[0]);
        glEnableVertexAttribArray(0);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * mCoords.size(), mCoords.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glBindBuffer(GL_ARRAY_BUFFER, mVBO[1]);
        glEnableVertexAttribArray(1);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * mColors.size(), mColors.data(), GL_STATIC_DRAW);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
