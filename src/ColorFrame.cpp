#include "include/ColorFrame.h"
#include <QColorDialog>

ColorFrame::ColorFrame(QObject *parent)
{
    this->setWindowFlags(Qt::WindowStaysOnTopHint);
}

ColorFrame::~ColorFrame(){

}

void ColorFrame::setColor(glm::vec3 color){
    mColor = color;
}

glm::vec3 ColorFrame::getColor(){
    return mColor;
}

void ColorFrame::setType(QString type){
    this->mType = type;
}

void ColorFrame::mousePressEvent(QMouseEvent *event){
    QColor color = QColorDialog::getColor();
    if(color.isValid()){

        color = color.toRgb();
        mColor.x = color.red() / 255.0f;
        mColor.y = color.green() / 255.0f;
        mColor.z = color.blue() / 255.0f;
        QString name = this->objectName();

        QString colorCSS = "#" + name + "{ background-color: rgb(" + QString::number(mColor.x * 255.0f) + "," + QString::number(mColor.y * 255.0f) + "," + QString::number(mColor.z * 255.0f) + "); }";
        this->setStyleSheet(colorCSS);

        emit ChangeColorFromViewOption(mColor, mType);

    }
}
