#include "include/Ray.h"
#include <QDebug>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/intersect.hpp>
#include "MainRenderModel.h"

Ray::Ray()
{
    MainRenderModel = SingleMainRenderModel::Instance();
}

/*-----------------------------------------------

Name:	createRay

Params: mouseX - Position of mouse on the screen mouseY and mouseX, GLint widgetW, GLint widgetH, glm::mat4 projection, glm::mat4 model, glm::mat4 view

Result:  create Ray.

/*---------------------------------------------*/

void Ray::createRay(GLuint mouseX, GLuint mouseY, GLint widgetW, GLint widgetH)
{
    glm::vec4 viewport = glm::vec4(0, 0, widgetW, widgetH);
    glm::vec3 wincoord = glm::vec3(mouseX, widgetH - mouseY/*widgetW / 2, widgetH / 2*/, 0.0);
    mFirstPoint = glm::unProject(wincoord, MainRenderModel->mViewMatrix * MainRenderModel->mModelMatrix, MainRenderModel->mProjectionMatrix, viewport);

    wincoord = glm::vec3(mouseX, widgetH - mouseY, 1.0f);
    mSecondPoint = glm::unProject(wincoord, MainRenderModel->mViewMatrix * MainRenderModel->mModelMatrix, MainRenderModel->mProjectionMatrix, viewport);
}

GLboolean Ray::checkProcedureTriangle(Hall hall, int i1, int i2, int i3)
{
    //creating and filling with points Triangle
    glm::vec3 pointOfIntersection;
    if (hall.mRenderData[Hall::HallObjectType].mVertex.size() > 0)
    {
        return glm::intersectLineTriangle(mFirstPoint,
                                    glm::normalize(mSecondPoint - mFirstPoint),
                                    hall.mRenderData[Hall::HallObjectType].mVertex[i1],
                                    hall.mRenderData[Hall::HallObjectType].mVertex[i2],
                                    hall.mRenderData[Hall::HallObjectType].mVertex[i3],
                                    pointOfIntersection);
    }
    return false;
}

GLboolean Ray::hasIntersection(int i1, int i2, int i3, glm::vec3 &point, QVector <glm::vec3> &restrictedData){
    return glm::intersectLineTriangle(mFirstPoint,
                                       glm::normalize(mSecondPoint - mFirstPoint),
                                       restrictedData[ i1 ],
                                       restrictedData[ i2 ],
                                       restrictedData[ i3 ],
                                       point);
}

void Ray::checkClickObject(QSet < QString > *NameObj)
{
    //Zones
    mPossibleZone.clear();
    mPossibleProcedure.clear();
    mPossibleBarrier.clear();
    mPossibleAirPlane.clear();
    mClickedZone = NULL;

    QVector < Zone > *zones = MainRenderModel->mZoneBuilder.getZones();

    for(int i = 0; i < zones->size(); i++)
    {
        if (NameObj->contains(zones->at(i).mName))
        {
            const QVector <glm::vec3> &restrictedData = zones->at(i).mRenderData[RenderObject::BasicType].mVertex;
            for (int j = 0; j < restrictedData.size() - 2; j += 3)
            {
                glm::vec3 pointOfIntersection;
                if (glm::intersectLineTriangle(mFirstPoint,
                                               glm::normalize(mSecondPoint - mFirstPoint),
                                               restrictedData[j],
                                               restrictedData[j + 1],
                                               restrictedData[j + 2],
                                               pointOfIntersection))
                {
                    mPossibleZone.push_back(zones->at(i).mName);
                    mClickedZone = const_cast< Zone* >(&zones->at(i));
                    break;
                }
            }
        }
    }

    for (int i = 0; i < MainRenderModel->mBarriers.size(); i++){

        const QVector <glm::vec3> &restrictedData = MainRenderModel->mBarriers[i].mRenderData[RenderObject::BasicType].mVertex;

        glm::vec3 tmp, tmp1;

        if (NameObj->contains(MainRenderModel->mBarriers[i].getName()))
        {

            if (glm::intersectRaySphere(mFirstPoint,
                                        glm::normalize(mSecondPoint - mFirstPoint ),
                                        MainRenderModel->mBarriers[i].getCenter(),
                                        5000,
                                        tmp,
                                        tmp1))
            {

                mPossibleBarrier.push_back(MainRenderModel->mBarriers[i].getName());
                mClickedBarrier = const_cast< Barrier* >(&MainRenderModel->mBarriers[i]);
                break;
            }
        }
    }

    for(QMap < QString,  AirPlane >::Iterator it = MainRenderModel->mAirPlanes.begin(); it != MainRenderModel->mAirPlanes.end(); it++){

        AirPlane *currentAirplane = &it.value();

        glm::vec3 tmp, tmp1;

        if (NameObj->contains(currentAirplane->getName()))
        {
            if (glm::intersectRaySphere(mFirstPoint,
                                        glm::normalize(mSecondPoint - mFirstPoint ),
                                        currentAirplane->getCurCoord3D(),
                                        5000,
                                        tmp,
                                        tmp1))
            {
                mPossibleAirPlane = currentAirplane->getName();
                mClickedAirPlane = currentAirplane;
                break;
            }
        }
    }

    //Procedures
    for(QMap<AirPortType, AirPort>::Iterator it = MainRenderModel->mAirPort.begin(); it != MainRenderModel->mAirPort.end(); it++)
    {

        AirPort *currentAirport = &it.value();

        QString nameFindProcedures = "";

        QVector < int > indexes;
        indexes << 1 << 5 << 4
                << 1 << 0 << 4
                << 2 << 6 << 7
                << 3 << 2 << 7
                << 1 << 2 << 3
                << 0 << 1 << 3
                << 6 << 5 << 7
                << 7 << 5 << 4
                << 3 << 7 << 4
                << 0 << 4 << 3
                << 6 << 5 << 2
                << 2 << 5 << 1;

        for(int i = 0; i < currentAirport->mProcedures.size(); i++)
        {
            // if NameObj contains Procedures name - that procedure is displayed. so it is active. so you can choose it
            if (NameObj->contains(currentAirport->mProcedures[i].getName()))
            {
                // if nameFindProcedures - intersected procedure was found

                // check all side of all halls of Procedures
                // turns (FLY_BY, FLY_OVER) - a not cliclable! they not check for intersection


                for (int j = 0; j < currentAirport->mProcedures[i].mHalls.size(); j++)
                {
                    QVector <glm::vec3> &restrictedData = currentAirport->mProcedures[i].mHalls[j].mRenderData[RenderObject::HallObjectType].mVertex;
                    for (int k = 0; k < restrictedData.size() - 7; k += 7)
                    {
                        glm::vec3 pointOfIntersection;
                        for(int ind = 0; ind < indexes.size() - 3; ind += 3 ){
                            if(hasIntersection(k + indexes[ ind ],
                                               k + indexes[ ind + 1 ],
                                               k + indexes[ ind + 2 ],
                                               pointOfIntersection,
                                               restrictedData))
                            {
                                mPossibleProcedure = currentAirport->mProcedures[i].getName();
                                mClickedHall = const_cast< Hall* >(&currentAirport->mProcedures[i].mHalls[j]);

                                return;
                            }
                        }
                    }
                }

                for (int j = 0; j < currentAirport->mProcedures[i].mTurns.size(); j++)
                {
                    const QVector <glm::vec3> &restrictedData = currentAirport->mProcedures[i].mTurns[j].mRenderData[RenderObject::BasicType].mVertex;
                    for (int k = 0; k < restrictedData.size() - 3; k++)
                    {
                        glm::vec3 pointOfIntersection;
                        if (glm::intersectLineTriangle(mFirstPoint,
                                                       glm::normalize(mSecondPoint - mFirstPoint),
                                                       restrictedData[k],
                                                       restrictedData[k + 1],
                                                       restrictedData[k + 2],
                                                       pointOfIntersection))
                            {
                                mPossibleProcedure = currentAirport->mProcedures[i].getName();
    //                                mClickedProcedure = currentAirport->mProcedures[i].mTurns[j];
                                break;
                            }
                    }
                }
            }
        }
    }
}

QVector < QString > Ray::getPossibleZones()
{
    return mPossibleZone;
}

Zone* Ray::getClickedZones()
{
    return mClickedZone;
}

Hall* Ray::getClickedHall()
{
    return mClickedHall;
}

QString Ray::getPossibleProcedures()
{
    return mPossibleProcedure;
}

Barrier* Ray::getClickedBarrier()
{
    return mClickedBarrier;
}

QString Ray::getPossibleBarrier()
{
    return mPossibleBarrier;
}

QString Ray::getPossibleAirPlane()
{
    return mPossibleAirPlane;
}

AirPlane* Ray::getClickedAirPlane(){
    return mClickedAirPlane;
}
