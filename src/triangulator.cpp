#include "include/Triangulator.h"
#include "include/Zones/ZoneBuilder.h"

QVector<QVector<int> > Trianglator::getFaces() const
{
    return faces;
}

QVector <glm::vec2> Trianglator::getVerts() const
{
    return verts;
}

bool Trianglator::DoTriangulate(QVector < glm::vec2 > &vctVertsSource, QVector < QVector <int> >  *segments)
{
    indexes.clear();
    bool bOk = false;
    // Инициализация ввода триангуляции
    triangulateio in_tr;
    memset(&in_tr, 0, sizeof(in_tr));
    in_tr.numberofpoints=(int)vctVertsSource.size();
    in_tr.numberofpointattributes = 1;
    in_tr.pointlist = (REAL *) std::malloc(in_tr.numberofpoints * 2 * sizeof(REAL));
    in_tr.pointattributelist = (REAL *) std::malloc(in_tr.numberofpoints * sizeof(REAL));
    in_tr.pointmarkerlist =(int *) NULL;

    in_tr.numberofcorners = 3; // триангуляция треугольников
    in_tr.numberofholes = 0; // дырок нет
    in_tr.holelist = (REAL *) NULL;
    in_tr.numberofregions = 0;
    in_tr.regionlist = (REAL *) NULL;
    if (segments){
        in_tr.numberofsegments = segments->size();
        in_tr.segmentlist = (int *) std::malloc(in_tr.numberofsegments * 2 * sizeof(int));
    } else {
        in_tr.numberofsegments = 0;
        in_tr.segmentlist =(int *) NULL;
    }
    in_tr.segmentmarkerlist=(int *) NULL;


    for ( int i = 0; i < vctVertsSource.size(); ++i ){
        //unsigned j = (i + 1) % vctVertsSource.size(); //следующая вершина или первая при i = vctVertsSource.size()-1
        // вершина
        in_tr.pointlist[i*2] = vctVertsSource[i].x;
        in_tr.pointlist[i*2 + 1] = vctVertsSource[i].y;
//        in_tr.pointattributelist[i]=vctVertsSource[i].z;
        // сегмент (контур)

    }

    for ( int i = 0; i < in_tr.numberofsegments; ++i ){
        in_tr.segmentlist[i*2] = (*segments)[i][0];
        in_tr.segmentlist[i*2+1] = (*segments)[i][1];
    }

    //////////////////////////////////////////////////////////////////////////
    // Инициализация вывода триангуляции

    triangulateio out_tr;
    memset(&out_tr, 0, sizeof(out_tr));
    out_tr.numberofcorners=3;
    out_tr.pointlist = (REAL *) NULL;
    out_tr.pointattributelist = (REAL *) NULL;
    out_tr.pointmarkerlist = (int *) NULL;
    out_tr.trianglelist = (int *) NULL;
    out_tr.triangleattributelist = (REAL *) NULL;
    out_tr.neighborlist = (int *) NULL;
    out_tr.segmentlist = (int *) NULL;
    out_tr.segmentmarkerlist = (int *) NULL;
    out_tr.edgelist = (int *) std::malloc(in_tr.numberofpoints * 2 * sizeof(int));
    out_tr.edgemarkerlist = (int *) NULL;

    //////////////////////////////////////////////////////////////////////////
    // Триангулируем

    QString str = QString("zpa%1q27Q");
    str.arg(ZoneBuilder::DISTANCE_BETWEEN_POINTS);


    if (segments){
        triangulate(str.toStdString().c_str(), &in_tr, &out_tr, 0); // zDQ
    }
    else
        triangulate("zpq", &in_tr, &out_tr, 0); //zq27eQ
    //////////////////////////////////////////////////////////////////////////
    bOk = (bool)out_tr.numberoftriangles && (bool)out_tr.numberofpoints;
    // Добаление треугольников
    verts.clear();
    for(int i = 0; i<out_tr.numberofpoints; ++i)
        verts.push_back(glm::vec2(out_tr.pointlist[i*2], out_tr.pointlist[i*2 + 1]/*, out_tr.pointattributelist[i]*/));
    edges.clear();
//    for(int i = 0; i < out_tr.numberofedges; ++i){
//        QVector <int> seg;
//        if(out_tr.edgelist != NULL)
//        {
//            seg.push_back(out_tr.edgelist[i*2]);
//            seg.push_back(out_tr.edgelist[i*2+1]);
//            edges.push_back(seg);
//        }
//    }
    faces.clear();
    int tmp = out_tr.numberoftriangles;
    for (int i = 0; i < out_tr.numberoftriangles; i++) {
        QVector < glm::vec2 > triaToAdd;
        QVector <int> face;
        //индексы 3-х вершин треугольника
        for (int deg = 0; deg < 3;deg++){
            face.push_back(out_tr.trianglelist[i * 3 + deg]);
            triaToAdd.push_back(verts[face[deg]]);
        }
        faces.push_back(face);
        indexes.push_back(triaToAdd);
    }

    //////////////////////////////////////////////////////////////////////////
    // !!! Перед выходом не забыть почистить все выделенные ресурсы !!!
    std::free(in_tr.pointlist);
    std::free(in_tr.segmentlist);
    std::free(in_tr.pointattributelist);

    std::free(out_tr.pointlist);
    std::free(out_tr.segmentlist);
    std::free(out_tr.trianglelist);
    std::free(out_tr.pointattributelist);
    //////////////////////////////////////////////////////////////////////////
    if ( !bOk )
        indexes.clear();
    return bOk;
}

QVector <QVector<glm::vec2>> Trianglator::getIndexes()
{
    return indexes;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

