#-------------------------------------------------
#
# Project created by QtCreator 2016-08-08T10:43:48
#
#-------------------------------------------------

QT += gui opengl concurrent network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

VERSION = 1.0.0

INCLUDEPATH += ./include

QMAKE_CXXFLAGS += -std=c++0x -U_STRICT_ANSI__

TARGET = Nita3Dnew
TEMPLATE = app

win32{
    LIBS += -lglew32 -lopengl32 -lGeographic
}
unix{
    LIBS += -lGLEW -fontconfig
#   LIBS += -L$$/usr/local/lib/ -lassimp
}

SOURCES += main.cpp\
        MainWindow.cpp \
    src/Shader/Shader.cpp \
    src/Shader/ShaderProgram.cpp \
    src/Model/Mesh.cpp \
    src/Model/Model.cpp \
    src/AirPort/Procedure.cpp \
    src/AirPort/Hall.cpp \
    src/AirPort/AirPort.cpp \
    src/Earth/EarthPart.cpp \
    src/Earth/Earth.cpp \
    src/RenderObject/VertexBuffer.cpp \
    src/RenderObject/VertexArray.cpp \
    src/RenderObject/Texture.cpp \
    src/Zones/ZoneBuilder.cpp \
    src/Zones/Zone.cpp \
    src/Zones/FlyZone.cpp \
    MainWidget.cpp \
    src/RenderObject/RenderData.cpp \
    src/Helper.cpp \
    src/GeoHelper.cpp \
    src/Camera.cpp \
    src/Ray.cpp \
    src/TreeWidgetModel.cpp \
    MainRenderModel.cpp \
    src/Converter/ZoneConverter.cpp \
    src/Converter/Converter.cpp \
    src/Parser/ZoneParser.cpp \
    src/Parser/Parser.cpp \
    src/AirPort/Turn.cpp \
    src/ViewOption.cpp \
    src/ColorFrame.cpp \
    src/Lamp.cpp \
    src/CubeMap.cpp \
    src/FrameBuffer.cpp \
    src/Barrier.cpp \
    src/Trajectory.cpp \
    src/triangle.cpp \
    src/triangulator.cpp \
    MainSettingsClass.cpp \
    AirPlane.cpp \
    CSVReader/csvreader.cpp \
    CSVReader/wp.cpp

HEADERS  += MainWindow.h \
    include/Model/Model.h \
    include/Model/Mesh.h \
    include/Shader/ShaderProgram.h \
    include/Shader/Shader.h \
    include/AirPort/WayPoint.h \
    include/AirPort/Procedure.h \
    include/AirPort/Hall.h \
    include/AirPort/AirPort.h \
    include/Earth/EarthPart.h \
    include/Earth/Earth.h \
    include/RenderObject/VertexBuffer.h \
    include/RenderObject/VertexArray.h \
    include/RenderObject/Texture.h \
    include/RenderObject/RenderObject.h \
    include/Zones/ZoneBuilder.h \
    include/Zones/Zone.h \
    include/Zones/FlyZone.h \
    MainWidget.h \
    include/RenderObject/RenderData.h \
    include/Helper.h \
    include/GeoHelper.h \
    include/Headers.h \
    include/Camera.h \
    include/Ray.h \
    MainRenderModel.h \
    include/Converter/ZoneConverter.h \
    include/Converter/Converter.h \
    include/Parser/ZoneParser.h \
    include/Parser/Parser.h \
    include/AirPort/Turn.h \
    include/ViewOption.h \
    include/ColorFrame.h \
    include/TreeWidgetModel.h \
    include/Lamp.h \
    include/CubeMap.h \
    include/FrameBuffer.h \
    include/Barrier.h \
    include/Trajectory.h \
    include/MySplashScreen.h \
    include/triangle.h \
    include/triangulator.h \
    MainSettingsClass.h \
    AirPlane.h \
    CSVReader/csvreader.h \
    CSVReader/wp.h

FORMS    += MainWindow.ui \
    ViewOption.ui

RESOURCES += \
    res.qrc \
    res/QSS/style.qrc

# GLEW Library
#win32{
#        DEFINES += GLEW_STATIC
#
#        GLEW_PATH = $$(NITAIMPORT)/glew-1.12.0/vs2013/
#        INCLUDEPATH += $$GLEW_PATH/include
#        DEPENDPATH += $$GLEW_PATH/include
#
#        contains(QMAKE_HOST.arch, x86_64):{
#                ARCH = x64
#        }else{
#                ARCH = Win32
#        }
#        CONFIG(debug, debug|release){
#                LIBS += -L$$GLEW_PATH/lib/Debug/$$ARCH
#                LIBS += -lglew32sd
#        }else{
#                LIBS += -L$$GLEW_PATH/lib/Release/$$ARCH
#                LIBS += -lglew32s
#        }
#}else{
#        LIBS += -lGLEW
#}

# GLM Library
#win32{
#        INCLUDEPATH += $$(NITAIMPORT)/glm/
#}

#unix: LIBS += -L$$PWD/../../../usr/lib64/ -lqjson

#INCLUDEPATH += $$PWD/../../../usr/include
#DEPENDPATH += $$PWD/../../../usr/include

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../Qt/Qt5.7.0/Tools/mingw530_32/lib/ -llibGeographic.dll

INCLUDEPATH += $$PWD/../../../../../../Qt/Qt5.7.0/Tools/mingw530_32/include
DEPENDPATH += $$PWD/../../../../../../Qt/Qt5.7.0/Tools/mingw530_32/include

SUBDIRS += \
    CSVReader/CSVReader.pro

DISTFILES += \
    CSVReader/csv.csv
