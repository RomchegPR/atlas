#include "MainWidget.h"
#include "include/GeoHelper.h"
#include "include/Helper.h"
#include <QSlider>
#include <QDateTime>
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <QPen>
#include <QTextCodec>
#include <boost/lexical_cast.hpp>
#include "MainSettingsClass.h"

#include "QNetworkRequest"
#include "QNetworkReply"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

MainWidget::MainWidget(QWidget *parent) :
    QGLWidget(parent),
    mIsMidButtonPressed(false),
    mIsLeftButtonPressed(false),
    mIsRightButtonPressed(false),
    mXOffset(0),
    mYOffset(0),
    quadVAO(0),
    mAntiBugFlag(false)

{
       setMouseTracking(true);
       //Timer
       connect(&mTimer, SIGNAL(timeout()), SLOT(update()));
       connect(&mTimer, SIGNAL(timeout()), SLOT(earthInertia()));
       connect(&mTimer2, SIGNAL(timeout()), SLOT(checkMouseCoord()));
       connect(&mTimer3, SIGNAL(timeout()), SLOT(updateAirPlanesCoord()));

       mTimer.start(12);
       mTimer2.start(50);
       mTimer3.start(10000);
       srand ( time(NULL) );

       mCamera = Camera::Instance();
       MainRenderModel = SingleMainRenderModel::Instance();
       SettingsClass = mainSettingsClass::Instance();

       mCursorOnObject = new CursorOnObject();

       manager = new QNetworkAccessManager(this);
       connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(replyFinished(QNetworkReply*)));

}

void MainWidget::replyFinished(QNetworkReply* reply){

    if(reply->error() == QNetworkReply::NoError) {

            QString strReply = (QString)reply->readAll();

            qDebug() << "updated";

            QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());

            QJsonObject jsonObject = jsonResponse.object();

            QJsonArray jsonArray = jsonObject["states"].toArray();

            for (int i = 0; i < jsonArray.size(); i++){

                QJsonArray jsonArray2 = jsonArray.at(i).toArray();

                myLan = GeoHelper::deg2rad(jsonArray2.at(6).toDouble());
                myLon = GeoHelper::deg2rad(jsonArray2.at(5).toDouble());
                myAlt = jsonArray2.at(7).toDouble();


                QString AirPlaneName = jsonArray2.at(1).toString();

                if ( MainRenderModel->mAirPlanes.contains(AirPlaneName) ){
                    MainRenderModel->mAirPlanes[AirPlaneName].addTrackPoint(myLan, myLon, myAlt);
                    MainRenderModel->mAirPlanes[AirPlaneName].setName(AirPlaneName);
                    MainRenderModel->mAirPlanes[AirPlaneName].calcLine();
                    MainRenderModel->mAirPlanes[AirPlaneName].update();

                } else {
                    AirPlane tmpAirPlane =  AirPlane();
                    tmpAirPlane.addTrackPoint(myLan, myLon, myAlt);
                    tmpAirPlane.setName(AirPlaneName);
                    tmpAirPlane.calcLine();
                    tmpAirPlane.init();

                    MainRenderModel->mAirPlanes.insert(AirPlaneName, tmpAirPlane);
                }

                mNameObj.insert(jsonArray2.at(1).toString());
            }

        } else {
            qDebug() << "ERROR";
        }


//    if(reply->error() == QNetworkReply::NoError) {

//            QString strReply = (QString)reply->readAll();

//            qDebug() << strReply;

//            QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());

//            QJsonObject jsonObject = jsonResponse.object();

//            QJsonArray jsonArray = jsonObject["flightTracks"].toArray();

//            for (int i = 0; i < jsonArray.size(); i++){

//                QJsonObject curAirPlane = jsonArray.at(i).toObject();

//                QJsonArray positions = curAirPlane["positions"].toArray();

//                QJsonObject lastPosition = positions.at(0).toObject();

//                myLan = GeoHelper::deg2rad(lastPosition["lat"].toDouble());
//                myLon = GeoHelper::deg2rad(lastPosition["lon"].toDouble());
//                myAlt = lastPosition["altitudeFt"].toDouble() * 0.3048;


//                QString AirPlaneName = curAirPlane["carrierFsCode"].toString() + curAirPlane["flightNumber"].toString();

//                if ( MainRenderModel->mAirPlanes.contains(AirPlaneName) ){
//                    MainRenderModel->mAirPlanes[AirPlaneName].addTrackPoint(myLan, myLon, myAlt);
//                    MainRenderModel->mAirPlanes[AirPlaneName].setName(AirPlaneName);
//                    MainRenderModel->mAirPlanes[AirPlaneName].calcLine();
//                    MainRenderModel->mAirPlanes[AirPlaneName].update();

//                } else {
//                    AirPlane tmpAirPlane =  AirPlane();
//                    tmpAirPlane.addTrackPoint(myLan, myLon, myAlt);
//                    tmpAirPlane.setName(AirPlaneName);
//                    tmpAirPlane.calcLine();
//                    tmpAirPlane.init();

//                    MainRenderModel->mAirPlanes.insert(AirPlaneName, tmpAirPlane);
//                }

//                mNameObj.insert(AirPlaneName);
//            }

//        } else {
//            qDebug() << "ERROR";
//        }

}

void MainWidget::updateAirPlanesCoord(){
    QUrl url("https://opensky-network.org/api/states/all");
//    QUrl url("https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/tracks/LED/arr?appId=adf8c840&appKey=7577901ae9de3b23fb491370546be9d1&includeFlightPlan=false&maxPositions=3&maxFlights=20");

//    manager->get(QNetworkRequest(url));
}

void MainWidget::keyPressEvent(QKeyEvent* event)
{
    int a = event->key();

    if(a == Qt::Key_1)
    {
        mAntiBugFlag ? mAntiBugFlag = false : mAntiBugFlag = true;
    }
    if(a == Qt::Key_2)
    {
        mCamera->setDefaultParameters();
    }

    if(a == Qt::Key_0)
    {
       mCamera->setCameraUp(glm::vec3(0,1,0));
    }

    QSlider* slider = parent()->findChild<QSlider* >(QString("CameraSpeedSlider"));

    mCamera->moveCameraWithKey(a, static_cast<float>(slider->value()));
}

void MainWidget::mouseMoveEvent(QMouseEvent *event)
{
    mCurMousePos = event->pos();

    if (this->isActiveWindow()){

        if(mIsLeftButtonPressed)
        {
            setCursor(Qt::ClosedHandCursor);

            mXOffset = event->x() - mLastX;
            mYOffset = event->y() - mLastY;

            mLastX = event->x();
            mLastY = event->y();

            mRay.createRay(mLastX, mLastY, mWidgetW, mWidgetH);

            glm::vec3 tmp1, tmp;
            if(glm::intersectRaySphere(mCamera->getEye()*glm::vec3(100),
                                       glm::normalize(mRay.mSecondPoint - mCamera->getEye()*glm::vec3(100)),
                                       glm::vec3(0,0,0),
                                       GeoHelper::MINOR_AXIS,
                                       tmp,
                                       tmp1))
            {
                mCamera->rotateCameraAroundEarth(tmp);
                mCamera->setLastPos(tmp);
            }
        }

        if (mIsMidButtonPressed)
        {
            mXOffset = event->x() - mLastX;
            mYOffset = event->y() - mLastY;

            mLastX = event->x();
            mLastY = event->y();

            if(fabs(mXOffset) > fabs(mYOffset))
            {
                this->setCursor(Qt::SizeHorCursor);
            }else{
                this->setCursor(Qt::SizeVerCursor);
            }

            mCamera->rotateWithMouseAroundPoint(mXOffset, mYOffset);
            mXOffset = 0;
            mYOffset = 0;
        }
        if(mIsRightButtonPressed)
        {
            mXOffset = event->x() - mLastX;
            mYOffset = event->y() - mLastY;

            mLastX = event->x();
            mLastY = event->y();

            mCamera->rotateWithMouseAroundObjectPoint(mXOffset, mYOffset);
        }
    }
}

void MainWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton){
        mIsLeftButtonPressed = false;
    }

    if (event->button() == Qt::MidButton){
        mIsMidButtonPressed = false;

    }

    if (event->button() == Qt::RightButton){
        mIsRightButtonPressed = false;
    }
}

void MainWidget::mousePressEvent(QMouseEvent *event)
{
    mXOffset = 0;
    mYOffset = 0;

    mLastX = event->x();
    mLastY = event->y();
    mCamera->stopAllMoves();

    if (event->button() == Qt::LeftButton)
    {
        setCursor(Qt::ClosedHandCursor);

        mIsLeftButtonPressed = true;
        glm::vec3 tmp, tmp1;

        mRay.createRay(mLastX, mLastY, mWidgetW, mWidgetH);
        if(glm::intersectRaySphere(mRay.mFirstPoint, glm::normalize(mRay.mSecondPoint - mRay.mFirstPoint), glm::vec3(0,0,0), GeoHelper::MINOR_AXIS, tmp, tmp1))
        {
            mCamera->setLastPos(tmp);
        }

    }

    if (event->button() == Qt::MidButton)
    {
        mIsMidButtonPressed = true;
        mRay.createRay(mCurMousePos.x(), mCurMousePos.y(), mWidgetW, mWidgetH);
        glm::vec3 pointOfIntersection, tmp1;
        if(glm::intersectRaySphere(mRay.mFirstPoint, glm::normalize(mRay.mSecondPoint - mRay.mFirstPoint), glm::vec3(0,0,0), (GeoHelper::MINOR_AXIS + GeoHelper::MAJOR_AXIS)/2 , pointOfIntersection, tmp1))
        {
            mCamera->setPointRotateAround(pointOfIntersection);
        }
    }

    if (event->button() == Qt::RightButton)
    {
        mIsRightButtonPressed = true;
    }
}


void MainWidget::wheelEvent(QWheelEvent *event)
{
    if(!mIsLeftButtonPressed && !mIsMidButtonPressed && !mIsRightButtonPressed)
    {
//        mRay.createRay(mCurMousePos.x(), mCurMousePos.y(), mWidgetW, mWidgetH);
        glm::vec3 pointOfIntersection, tmp1;
        if(glm::intersectRaySphere(mCamera->getCenter(), glm::normalize(mCamera->getEye() - mCamera->getCenter()), glm::vec3(0,0,0), (GeoHelper::MINOR_AXIS + GeoHelper::MAJOR_AXIS)/2 , pointOfIntersection, tmp1))
        {
            mCamera->zoom(event->delta());
        }
    }
}

void MainWidget::mouseDoubleClickEvent(QMouseEvent * event)
{
    if (event->button() == Qt::LeftButton)
    {

        QString ProcedureName = "";
        glm::vec3 tmpPointAround;

        for(QMap<AirPortType, AirPort>::Iterator it = MainRenderModel->mAirPort.begin(); it != MainRenderModel->mAirPort.end(); it++)
        {
            AirPort *temp = &it.value();

            ProcedureName = mRay.getPossibleProcedures();

             if (ProcedureName != "") {

                 if (temp->addCheckedProcedure(ProcedureName)){

                     changeStateClicledObject(ProcedureName);
                     emit procedureChoosed(mClickedObjects);
                     tmpPointAround = mRay.getClickedHall()->getCenter();
                }
             }
        }

        if (ProcedureName == "")
        {
            for (int i = 0; i < mRay.getPossibleZones().size(); i++)
            {
                MainRenderModel->mZoneBuilder.addCheckedZone(mRay.getPossibleZones()[i]);
                changeStateClicledObject(mRay.getPossibleZones()[i]);
                tmpPointAround = mRay.getClickedZones()->getCenter();
            }
        }
        tmpPointAround /= glm::vec3(100);
        mCamera->setPointObjectRotateAround(tmpPointAround);
    }
}

void MainWidget::changeStateClicledObject(QString objectName){
    if (mClickedObjects.contains(objectName)){
        mClickedObjects.remove(objectName);
    } else {
        mClickedObjects.insert(objectName);
    }
}

void MainWidget::earthInertia()
{
    if(!mIsLeftButtonPressed && !mIsMidButtonPressed && !mIsRightButtonPressed)
    {
        mCamera->rotateCameraAroundEarth(mXOffset, mYOffset);
        if(mXOffset > 0)
        {
            mXOffset--;
        }
        if(mXOffset < 0)
        {
            mXOffset++;
        }

        if(mYOffset > 0)
        {
            mYOffset--;
        }
        if(mYOffset < 0)
        {
            mYOffset++;
        }
    }
}

/*-----------------------------------------------

Name:	checkMouseCoord

Params: none

Result:	if mouse near the border rotate camera.

---------------------------------------------*/

void MainWidget::checkMouseCoord()
{
    if (this->isActiveWindow() && !mIsLeftButtonPressed && !mIsMidButtonPressed){
        mRay.createRay(mapFromGlobal(cursor().pos()).x(), mapFromGlobal(cursor().pos()).y(), mWidgetW, mWidgetH);
        mRay.checkClickObject(&mNameObj);

        bool ans = !mRay.getPossibleZones().empty();
        mObjectsCursorOn.clear();

        mCursorOnObject->isActive = false;
        if(!mIsMidButtonPressed){
            for (int i = 0; i < mRay.getPossibleZones().size(); i++)
            {
                mObjectsCursorOn.insert(mRay.getPossibleZones()[i]);
                mCursorOnObject->isActive = true;
                mCursorOnObject->mName = mRay.getClickedZones()->mName;
                mCursorOnObject->mType = ZoneType;

                mCursorOnObject->pZone = mRay.getClickedZones();
            }

           if (mRay.getPossibleProcedures() != "")
           {
               if (*mRay.getClickedHall()->getDrawMask() & (HALL | LINES)){
                   ans = true;
                   mObjectsCursorOn.insert(mRay.getPossibleProcedures());
                   mCursorOnObject->isActive = true;
                   mCursorOnObject->mType = HallType;
                   mCursorOnObject->mName = mRay.getPossibleProcedures();
                   mCursorOnObject->pHall = mRay.getClickedHall();
               }
           }

           if (mRay.getPossibleBarrier() != ""){
               ans = true;
               mObjectsCursorOn.insert(mRay.getPossibleBarrier());
               mCursorOnObject->isActive = true;
               mCursorOnObject->mType = BarrierType;

               mCursorOnObject->pBarrier = mRay.getClickedBarrier();
               mCursorOnObject->mName = mRay.getPossibleBarrier();
              }

           if (mRay.getPossibleAirPlane() != ""){
               ans = true;
               mObjectsCursorOn.insert(mRay.getPossibleAirPlane());
               mCursorOnObject->isActive = true;
               mCursorOnObject->mType = AirPlaneType;

               mCursorOnObject->pAirPlane = mRay.getClickedAirPlane();
               mCursorOnObject->mName = mRay.getPossibleAirPlane();
           }
       }


       if (!ans) mObjectsCursorOn.clear();

       if(!mIsLeftButtonPressed && !mIsMidButtonPressed && !mIsRightButtonPressed)
       {
           if (ans)
           {
               setCursor(Qt::PointingHandCursor);
           }
           else setCursor(Qt::OpenHandCursor);
       }
    }
}


/*-----------------------------------------------

Name:	sortObjects

Params: none

Result:	calc distance to all halls and save to map(mSorted).
        in map all distance sort!
        we sort all objects  only on XZ.

        and don't draw same objects 2 or more times.
        for this use Set of distances in 3D (XYZ) for
        halls.

---------------------------------------------*/

void MainWidget::sortObjects(){
    if (this->isActiveWindow()){
        mSortMap.clear();
        double distance;
        glm::vec3 tmp, tmp1;
        glm::vec3 direction;

        glm::vec3 curEye(glm::vec4(mCamera->getEye(),1.0f) * glm::inverse(MainRenderModel->mModelMatrix));

        for(QMap<AirPortType, AirPort>::Iterator it = MainRenderModel->mAirPort.begin(); it != MainRenderModel->mAirPort.end(); it++){
             AirPort *CurAirport = &it.value();

             for (int i = 0; i < CurAirport->mProcedures.size(); i++){
                 for (int j = 0; j < CurAirport->mProcedures[i].getCountHalls(); j++)
                 {
                    glm::vec3 CenterOfHall = CurAirport->mProcedures[i].getHallCenter(j);
                    if(glm::distance(glm::vec3(0), CenterOfHall) > glm::distance(glm::vec3(0),curEye))
                    {
                        direction = CenterOfHall - curEye;
                    }
                    else
                    {
                        direction = curEye - CenterOfHall;
                    }
                    // Check to don't draw Procedures under the horizon.
                    if(!glm::intersectRaySphere(CenterOfHall,
                                               glm::normalize(direction),
                                               glm::vec3(0,0,0),
                                               GeoHelper::MINOR_AXIS,
                                               tmp,
                                               tmp1))
                    {
                        distance = glm::distance(curEye, CenterOfHall);
                        mSortMap[distance].push_back(&CurAirport->mProcedures[i].mHalls[j]);
                        if (CurAirport->mProcedures[i].mTurns.size() > j)
                        mSortMap[distance].push_back(&CurAirport->mProcedures[i].mTurns[j]);
                    }

                 }
             }
        }

        for (int i = 0; i < MainRenderModel->mZoneBuilder.mZones.size(); i++)
        {
            glm::vec3 curZoneCenter = MainRenderModel->mZoneBuilder.mZones[i].getCenter();
    // Check to don't draw Zones under the horizon.
            if(glm::distance(glm::vec3(0), curZoneCenter) < glm::distance(glm::vec3(0),curEye))
            {
                    direction = curEye - curZoneCenter;
            }
            else
            {
                if (glm::distance(curEye, curZoneCenter) > 900000)
                {
                     direction = curEye - curZoneCenter;
                }
                else
                {
                    direction = curZoneCenter - curEye;
                }
            }
            if(!glm::intersectRaySphere(curZoneCenter,
                                       glm::normalize(direction),
                                       glm::vec3(0,0,0),
                                       GeoHelper::MINOR_AXIS,
                                       tmp,
                                       tmp1))
            {
                distance = glm::distance(curEye, curZoneCenter);
                mSortMap[distance].push_back(&MainRenderModel->mZoneBuilder.mZones[i]);
            }
        }
    }
}
/*-----------------------------------------------

Name:	DrawObj

Params: nameObj - name of object

Result:	need for clicking object.
        save names of object for draw in PainGL.

        it calls from mainwindow.cpp::onItemChanged
---------------------------------------------*/

void MainWidget::DrawObj(QString nameObj){
    if (mNameObj.contains(nameObj)) {
        mNameObj.remove(nameObj);
    }
    else {
        mNameObj.insert(nameObj);
    }
}

void MainWidget::rotationToObject(QString nameObj)
{
    for (QMap < double, QVector < RenderObject * > >::Iterator it = mSortMap.end() - 1; it != mSortMap.begin() - 1; it--){

        QVector < RenderObject * > *tmpObjects = &it.value();

        for (int i = 0; i < tmpObjects->size(); i++){

            RenderObject * tt = tmpObjects->at(i);

            Hall *hp = dynamic_cast< Hall* >( tt );
            Zone *zp = dynamic_cast< Zone* >( tt );

            QString hallName, zoneName;
            hallName = hp == NULL ? "" : hp->getParentProcedure()->getName();
            zoneName = zp == NULL ? "" : zp->getName();
            glm::vec3 tmpPoint;
            if (hp && nameObj == hp->getParentProcedure()->getName()) {
                tmpPoint = hp->getCenter();
                mCamera->startRotationToPoint(tmpPoint);

            } else if (zp && nameObj == zp->getName()) {
                tmpPoint = zp->getCenter();
                mCamera->startRotationToPoint(tmpPoint);


            }
        }
    }
}

void MainWidget::resizeGL(int w, int h){
    glViewport(0,0,w,h);
    mWidgetW = w;
    mWidgetH = h;

    MainRenderModel->mProjectionMatrix = glm::perspective(
                45.0f,         // Горизонтальное поле обзора в градусах. Обычно между 90° (очень широкое) и 30° (узкое)
                (float) w / h, // Отношение сторон.
                10.15f,        // Ближняя плоскость отсечения. Должна быть больше 0.
                // for world and camera
                300000.0f * 2 // Дальняя плоскость отсечения.
            );

    mFrameBuffer.bind();

    for (GLuint i = 0; i < 2; i++)
    {
        mColorBuffer[i].bind();
        mColorBuffer[i].setImage2D(Texture::RGB16, mWidgetW, mWidgetH, InputData::RGB, InputData::dtFloat, NULL, 0, 0);
    }
    // - Create and attach depth buffer (renderbuffer)

    glBindRenderbuffer(GL_RENDERBUFFER, mRBO);
    glRenderbufferStorage(GL_RENDERBUFFER,  GL_DEPTH24_STENCIL8, mWidgetW, mWidgetH);

    // - Finally check if framebuffer is complete
    mFrameBuffer.checkStatus();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Ping pong framebuffer for blurring

    for (GLuint i = 0; i < 2; i++)
    {

        mBloorFramBuffer[i].bind();
        mBloorColorBuffer[i].bind();
        mBloorColorBuffer[i].setImage2D(Texture::RGB16, mWidgetW, mWidgetH, InputData::RGB, InputData::dtFloat, NULL, 0, 0);
        mBloorFramBuffer[i].checkStatus();
    }
}

void MainWidget::initializeGL()
{
    glewInit();
    glGetError();
    glCheckError();
    checkMouseCoord();
    glewExperimental = GL_TRUE;

   // MainRenderModel->mShaders[BasicShader].create(":/Shaders/res/Shaders/Vertex.sh", ":/Shaders/res/Shaders/Fragment.sh"); // not used!
    MainRenderModel->mShaders[ModelsShader].create(":/Shaders/res/Shaders/Vertex3DModel.sh", ":/Shaders/res/Shaders/Fragment3DModel.sh");
    MainRenderModel->mShaders[CubeMapShader].create(":/Shaders/res/Shaders/VertexCubeMap.sh",":/Shaders/res/Shaders/FragmentCubeMap.sh");
    MainRenderModel->mShaders[LightShader].create(":/Shaders/res/Shaders/VertexLighting.sh",":/Shaders/res/Shaders/FragmentLighting.sh");
    MainRenderModel->mShaders[LampShader].create(":/Shaders/res/Shaders/VertexLamp.sh",":/Shaders/res/Shaders/FragmentLamp.sh");
    MainRenderModel->mShaders[BloomFinalShader].create(":/Shaders/res/Shaders/Bloom/bloom_final.vert",":/Shaders/res/Shaders/Bloom/bloom_final.frag");
    MainRenderModel->mShaders[BlurShader].create(":/Shaders/res/Shaders/Bloom/Blur.vert",":/Shaders/res/Shaders/Bloom/Blur.frag");
    MainRenderModel->mShaders[ScreenShader].create(":/Shaders/res/Shaders/Screen/screen.vert",":/Shaders/res/Shaders/Screen/screen.frag");

   // MainRenderModel->mShaders[BasicShader].link(); // not used!
    MainRenderModel->mShaders[ModelsShader].link();
    MainRenderModel->mShaders[CubeMapShader].link();
    MainRenderModel->mShaders[LightShader].link();
    MainRenderModel->mShaders[LampShader].link();
    MainRenderModel->mShaders[BloomFinalShader].link();
    MainRenderModel->mShaders[BlurShader].link();
    MainRenderModel->mShaders[ScreenShader].link();

    MainRenderModel->mLamp.init();

    /// NEW BLOOR

    MainRenderModel->mShaders[BloomFinalShader].bind();
    MainRenderModel->mShaders[BloomFinalShader].setUniformValue("scene", 0);
    MainRenderModel->mShaders[BloomFinalShader].setUniformValue("bloomBlur", 1);
    MainRenderModel->mShaders[BloomFinalShader].unbind();

    setBloorWeights(0.3270270270f, 0.1945945946f, 0.1516216216f,  0.0540540541f, 0.0562162162f);

    /// END NEW BLOOR

    // init halls and Zones


    QTime time;
    time.start();

    for(QMap<AirPortType, AirPort>::Iterator it = MainRenderModel->mAirPort.begin(); it != MainRenderModel->mAirPort.end(); it++){
         AirPort *temp = &it.value();

         temp->init();
         temp->setDrawMask(HALL | LINES | LINES_RAM);
    }

    MainRenderModel->mZoneBuilder.init();

    MainRenderModel->mModelMatrix = glm::scale(MainRenderModel->mModelMatrix, glm::vec3(0.01f, 0.01f, 0.01f));

    MainRenderModel->mEarth.setEarthRadius(GeoHelper::MINOR_AXIS);

    MainRenderModel->mEarth.createEarth();

    MainRenderModel->mEarth.init();

    Barrier nBar;

    Waypoint OBSTACLE;
    OBSTACLE.lat = 554911.0;
    OBSTACLE.lon = 0373642.6;
//    OBSTACLE.lat = 553730.0;
//    OBSTACLE.lon = 0371300.0;
    OBSTACLE.maxAlt = 700;
    OBSTACLE.mProcedureName = "Башня1";

    nBar.setAltitude(OBSTACLE.maxAlt);
    nBar.setPosition(OBSTACLE);
    nBar.setName(OBSTACLE.mProcedureName);
    nBar.init();

    MainRenderModel->mBarriers.push_back(nBar);

    Barrier nBar2;

    Waypoint OBSTACLE2;

//    OBSTACLE2.lat = 570821.0;
//    OBSTACLE2.lon = 0330641.0;
    OBSTACLE2.lat = 555930.0;
    OBSTACLE2.lon = 0372130.0;
    OBSTACLE2.maxAlt = 675;
    OBSTACLE2.mProcedureName = "Труба";

    nBar2.setAltitude(OBSTACLE2.maxAlt);
    nBar2.setPosition(OBSTACLE2);
    nBar2.setName(OBSTACLE2.mProcedureName);
    nBar2.init();

    MainRenderModel->mBarriers.push_back(nBar2);

    Barrier nBar3;
    Waypoint OBSTACLE3;
    OBSTACLE3.lat = 565435.0;
    OBSTACLE3.lon = 0333448.0;
//    OBSTACLE3.lat = 570000.0;
//    OBSTACLE3.lon = 0350000.0;
    OBSTACLE3.maxAlt = 647;
    OBSTACLE3.mProcedureName = "Башня2";

    nBar3.setAltitude(OBSTACLE3.maxAlt);
    nBar3.setPosition(OBSTACLE3);
    nBar3.setName(OBSTACLE3.mProcedureName);
    nBar3.init();

    MainRenderModel->mBarriers.push_back(nBar3);

    Barrier nBar4;
    Waypoint OBSTACLE4;
    OBSTACLE4.lat = 540830.0;
    OBSTACLE4.lon = 0373504.0;
//    OBSTACLE4.lat = 590000.0;
//    OBSTACLE4.lon = 0300000.0;
    OBSTACLE4.maxAlt = 601;
    OBSTACLE4.mProcedureName = "Башня3";

    nBar4.setAltitude(OBSTACLE4.maxAlt);
    nBar4.setPosition(OBSTACLE4);
    nBar4.setName(OBSTACLE4.mProcedureName);
    nBar4.init();

    MainRenderModel->mBarriers.push_back(nBar4);

    mNameObj.insert(OBSTACLE2.mProcedureName);
    mNameObj.insert(OBSTACLE.mProcedureName);
    mNameObj.insert(OBSTACLE3.mProcedureName);
    mNameObj.insert(OBSTACLE4.mProcedureName);

    mTrajectory.init(MainRenderModel->mModelMatrix, MainRenderModel->mViewMatrix, MainRenderModel->mProjectionMatrix);
//    MainRenderModel->mModel.push_back(Model());
//    MainRenderModel->mModel[0].init("/Boeing747/B-747.obj");

    // Projection for camera


    MainRenderModel->mProjectionMatrix = glm::perspective(
                45.0f,         // Горизонтальное поле обзора в градусах. Обычно между 90° (очень широкое) и 30° (узкое)
                (float) width() / height(), // Отношение сторон.
                10.15f,        // Ближняя плоскость отсечения. Должна быть больше 0.
                // for world and camera
                300000.0f * 2 // Дальняя плоскость отсечения.
            );

   MainRenderModel->mCubeMap.init();

   // Framebuffers

   mWidgetW = width();
   mWidgetH = height();

   mFrameBuffer.init(FrameBuffer::MODE_READ_DRAW);
   mFrameBuffer.create();
   mFrameBuffer.bind();

   //new FRAME

   // Set up floating point framebuffer to render scene to
   // - Create 2 floating point color buffers (1 for normal rendering, other for brightness treshold values)

   for (GLuint i = 0; i < 2; i++)
   {

      mColorBuffer[i].create(Texture::Texture_2D);
      mColorBuffer[i].bind();
      mColorBuffer[i].setImage2D(Texture::RGB16, mWidgetW, mWidgetH, InputData::RGB, InputData::dtFloat, NULL, 0, 0);
      mColorBuffer[i].setFilter(Texture::Linear, Texture::Linear);
      mColorBuffer[i].setWrap(Texture::ClampToEdge, Texture::ClampToEdge);

       // attach texture to framebuffer

      if (i == 0){
           mFrameBuffer.setTexture(FrameBuffer::COLOR0, mColorBuffer[i], 0);
      } else {
           mFrameBuffer.setTexture(FrameBuffer::COLOR1, mColorBuffer[i], 0);
      }
   }

       // - Create and attach depth buffer (renderbuffer)

   glGenRenderbuffers(1, &mRBO);
   glBindRenderbuffer(GL_RENDERBUFFER, mRBO);
   glRenderbufferStorage(GL_RENDERBUFFER,  GL_DEPTH24_STENCIL8, mWidgetW, mWidgetH);

   glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, mRBO);
   // - Tell OpenGL which color attachments we'll use (of this framebuffer) for rendering

   GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
   glDrawBuffers(2, attachments);

   // - Finally check if framebuffer is complete
   mFrameBuffer.checkStatus();

   glBindFramebuffer(GL_FRAMEBUFFER, 0);

   // Ping pong framebuffer for blurring

   for (GLuint i = 0; i < 2; i++)
   {

       mBloorFramBuffer[i].init(FrameBuffer::MODE_READ_DRAW);
       mBloorFramBuffer[i].create();
       mBloorFramBuffer[i].bind();

       mBloorColorBuffer[i].create(Texture::Texture_2D);
       mBloorColorBuffer[i].bind();
       mBloorColorBuffer[i].setImage2D(Texture::RGB16, mWidgetW, mWidgetH, InputData::RGB, InputData::dtFloat, NULL, 0, 0);
       mBloorColorBuffer[i].setFilter(Texture::Linear, Texture::Linear);
       mBloorColorBuffer[i].setWrap(Texture::ClampToEdge, Texture::ClampToEdge);

        // attach texture to framebuffer

       mBloorFramBuffer[i].setTexture(FrameBuffer::COLOR0, mBloorColorBuffer[i], 0);
       mBloorFramBuffer[i].checkStatus();
   }

   glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
   glBindTexture(GL_TEXTURE_2D, 0);


   emit initializeGLisComplete();
}

void MainWidget::paintEvent(QPaintEvent * event){

    Q_UNUSED(event)

    makeCurrent();

    QPainter* painter = new QPainter(this);

    painter->beginNativePainting( );

        painter->setRenderHint(QPainter::Antialiasing);

        paintGL();

        drawLegend(painter);


    painter->endNativePainting();

    delete painter;
}

void MainWidget::paintGL()
{

    /////////////////////////////////////////////////////
    // Bind to framebuffer and draw to color texture
    // as we normally would.
    // //////////////////////////////////////////////////

    // 1. Render scene into floating point framebuffer
    mFrameBuffer.bind();

    glEnable(GL_MULTISAMPLE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );


    MainRenderModel->mViewMatrix = mCamera->getLookAt();

    // Draw CUBEMAP

    glDepthMask(GL_FALSE);// Remember to turn depth writing off
    glLineWidth(1.0f);

//    MainRenderModel->mShaders[LightShader].bind();
//    MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);

//    MainRenderModel->mShaders[CubeMapShader].bind();

//    MainRenderModel->mShaders[CubeMapShader].setUniformMatrix("view", MainRenderModel->mViewMatrix);
//    MainRenderModel->mShaders[CubeMapShader].setUniformMatrix("projection", MainRenderModel->mProjectionMatrix);

//    MainRenderModel->mShaders[CubeMapShader].setUniformValue("skybox", 0);
//    MainRenderModel->mCubeMap.paint();

    sortObjects();

    glDisable(GL_BLEND);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LEQUAL);

    ///// DRAW EARTH

    MainRenderModel->mShaders[LightShader].bind();

    MainRenderModel->mShaders[LightShader].setUniformMatrix("model", MainRenderModel->mModelMatrix);
    MainRenderModel->mShaders[LightShader].setUniformMatrix("view", MainRenderModel->mViewMatrix);
    MainRenderModel->mShaders[LightShader].setUniformMatrix("projection", MainRenderModel->mProjectionMatrix);


    MainRenderModel->mCubeMap.paint();
        MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);
    MainRenderModel->mEarth.paint();


    glEnable(GL_BLEND);

    glEnable (GL_ALPHA_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//    for (int i = 0; i < MainRenderModel->mBarriers.size(); i++){

//        QString name = MainRenderModel->mBarriers[i].getName();

//        if (mNameObj.contains(name)) {

//            if (mObjectsCursorOn.contains(name)) {
//                 MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", true);
//            } else {
//                 MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);
//            }

//            MainRenderModel->mBarriers[i].paint();
//        }
//    }


//    for (QMap < QString,  AirPlane >::Iterator it = MainRenderModel->mAirPlanes.end() - 1; it != MainRenderModel->mAirPlanes.begin() - 1; it--){
//        AirPlane *tAirPlane = &it.value();

//        QString nameAirPlane = tAirPlane->getName();

//        if (mNameObj.contains(nameAirPlane)) {

//            if (mObjectsCursorOn.contains(nameAirPlane)) {
//                 MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", true);
//            } else {
//                 MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);
//            }
//        }

//        tAirPlane->paint();
//    }

    MainRenderModel->mShaders[LightShader].bind();
    MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", true);

    MainRenderModel->mLamp.update();

    // DRAW OBJECTS

    MainRenderModel->mShaders[LightShader].bind();
    MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);

    glDisable(GL_DEPTH_TEST);

    for (QMap < double, QVector < RenderObject * > >::Iterator it = mSortMap.end() - 1; it != mSortMap.begin() - 1; it--){

        QVector < RenderObject * > *tmpObjects = &it.value();

        for (int i = 0; i < tmpObjects->size(); i++){

            RenderObject * tt = tmpObjects->at(i);

            Hall *hp = dynamic_cast< Hall* >( tt );
            Zone *zp = dynamic_cast< Zone* >( tt );
            Turn *tp = dynamic_cast< Turn* >( tt );

            QString hallName, zoneName, turnName;
            hallName = hp == NULL ? "" : hp->getParentProcedure()->getName();
            zoneName = zp == NULL ? "" : zp->getName();
            turnName = tp == NULL ? "" : tp->getParentProcedure()->getName();

            if (hp && mNameObj.contains(hallName)) {

                if (mObjectsCursorOn.contains(hallName)) {
                     MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", true);
                } else {
                     MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);
                }

                hp->paint();

            } else if (zp && mNameObj.contains(zoneName)) {

                if (mObjectsCursorOn.contains(zoneName)) {
                     MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", true);
                } else {
                     MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);
                }

                zp->paint();

            } else if (tp && mNameObj.contains(turnName)) {

                if (mObjectsCursorOn.contains(turnName)) {
                     MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", true);
                } else {
                     MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);
                }

                tp->paint();

            }
        }
    }

    for(QMap<AirPortType, AirPort>::Iterator it = MainRenderModel->mAirPort.begin(); it != MainRenderModel->mAirPort.end(); it++){
         AirPort *CurAirport = &it.value();

         for (int i = 0; i < CurAirport->mProcedures.size(); i++){
             if (mNameObj.contains(CurAirport->mProcedures[i].getName()))
                CurAirport->mProcedures[i].paint();
         }
    }

    MainRenderModel->mShaders[LightShader].setUniformValue("isBloom", false);

    ///// DRAW MODELS

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

    for (int i = 0; i < MainRenderModel->mModel.size(); i++){
        MainRenderModel->mModel[i].Draw();
    }

    /////////////////////////////////////////////////////
    // Bind to default framebuffer again and draw the
    // quad plane with attched screen texture.
    // //////////////////////////////////////////////////

    glDisable(GL_MULTISAMPLE);
    glDisable(GL_BLEND);

    mTrajectory.draw();

    glDisable(GL_DEPTH_TEST);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//     2. Blur bright fragments w/ two-pass Gaussian Blur
    GLboolean horizontal = true, first_iteration = true;
    GLuint amount = 10;
    MainRenderModel->mShaders[BlurShader].bind();
    for (GLuint i = 0; i < amount; i++)
    {
        mBloorFramBuffer[horizontal].bind();

        MainRenderModel->mShaders[BlurShader].setUniformValue("horizontal", horizontal);
        glBindTexture(GL_TEXTURE_2D, first_iteration ? mColorBuffer[1].id() : mBloorColorBuffer[!horizontal].id());  // bind texture of other framebuffer (or scene if first iteration)

        RenderQuad();

        horizontal = !horizontal;
        if (first_iteration)
            first_iteration = false;
    }

    MainRenderModel->mShaders[BlurShader].unbind();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // 2. Now render floating point color buffer to 2D quad and tonemap HDR colors to default framebuffer's (clamped) color range
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    MainRenderModel->mShaders[BloomFinalShader].bind();

    glActiveTexture(GL_TEXTURE0);
    mColorBuffer[0].bind();
    glActiveTexture(GL_TEXTURE1);
    mBloorColorBuffer[!horizontal].bind();

    GLboolean bloom = true;

    MainRenderModel->mShaders[BloomFinalShader].setUniformValue("bloom", bloom);

    RenderQuad();

    MainRenderModel->mShaders[BloomFinalShader].unbind();

    mFrameBuffer.unbind();
    glBindBuffer(GL_ARRAY_BUFFER, 0);

//        MainRenderModel->mShaders[LightShader].bind();

}
void MainWidget::drawLegend( QPainter* painter )
{
    QPoint p = this->mapFromGlobal(QCursor::pos());

    if (p.x() > 0 &&  p.y() > 0 && p.x() < mWidgetW &&  p.y() < mWidgetH && this->isActiveWindow()){
        drawObjectLegend(painter);
    }

    drawFooter(painter);
}

void MainWidget::setBloorWeights(float a1, float a2, float a3, float a4, float a5){

    MainRenderModel->mShaders[BlurShader].bind();

    MainRenderModel->mShaders[BlurShader].setUniformValue("Weight.weight1", a1);
    MainRenderModel->mShaders[BlurShader].setUniformValue("Weight.weight2", a2);
    MainRenderModel->mShaders[BlurShader].setUniformValue("Weight.weight3", a3);
    MainRenderModel->mShaders[BlurShader].setUniformValue("Weight.weight4", a4);
    MainRenderModel->mShaders[BlurShader].setUniformValue("Weight.weight5", a5);

    MainRenderModel->mShaders[BlurShader].unbind();
}

glm::vec3 MainWidget::getScreenCoord(glm::vec3 objPos) {

      glm::vec4 viewport(0.0f, 0.0f, mWidgetW, mWidgetH);

//      glm::vec3 pos = glm::vec3(mCurMousePos.x(), mWidgetH-mCurMousePos.y(), 0.0f);

//      glm::vec3 unProj = glm::unProject(pos,
//                                        MainRenderModel->mViewMatrix * MainRenderModel->mModelMatrix,
//                                        MainRenderModel->mProjectionMatrix,
//                                        viewport);

      // Get screen coordinates from an object point

      glm::vec3 screenCoords = glm::project(
         objPos,
         MainRenderModel->mViewMatrix * MainRenderModel->mModelMatrix,
         MainRenderModel->mProjectionMatrix,
         viewport
      );

      screenCoords.y = mWidgetH - screenCoords.y;

      return screenCoords;
}


void MainWidget::drawFooter(QPainter* painter){
    painter->save();

    QFont font;
    font.setWeight(QFont::DemiBold);
    font.setPointSize( 10 );
    QFontMetrics metrics( font );

    QRect Rect = QRect(0, mWidgetH - metrics.height() * 2, mWidgetW , metrics.height() * 2);

    QBrush brush( QColor(155, 155, 155, 80) );
    painter->setBrush( brush );
    painter->drawRect( Rect );
    painter->setPen(QColor(Qt::white));
    painter->setFont( font );

    glm::vec3 eyeInOurWorld;
    eyeInOurWorld = mCamera->getEye() * glm::vec3(100);
    eyeInOurWorld = glm::vec3(-eyeInOurWorld.x, eyeInOurWorld.z, eyeInOurWorld.y);
    double height = GeoHelper::heightAboveSeaLevel(eyeInOurWorld);
    height /= SettingsClass->getHeightRate();
    height /= 1000;
    QString text1;
    mRay.createRay(mCurMousePos.x(), mCurMousePos.y(), mWidgetW, mWidgetH);

    glm::vec3 tmp1, IntersectPos;
    bool hasIntersect = glm::intersectRaySphere(mRay.mFirstPoint,
                                                glm::normalize(mRay.mSecondPoint - mRay.mFirstPoint),
                                                glm::vec3(0,0,0),
                                                GeoHelper::MINOR_AXIS,
                                                IntersectPos,
                                                tmp1);

    if (hasIntersect) {

        glm::vec2 tmpPoint = GeoHelper::cartesian2geo(glm::vec3(-IntersectPos.x, IntersectPos.z, IntersectPos.y));
        glm::vec2 polarCursorPos = GeoHelper::cartesian2geo(GeoHelper::geo2cartesian(glm::vec3(GeoHelper::rad2deg(tmpPoint.x), GeoHelper::rad2deg(tmpPoint.y), 0.0f)));

        polarCursorPos.x = GeoHelper::rad2deg(polarCursorPos.x);
        polarCursorPos.y = GeoHelper::rad2deg(polarCursorPos.y);

        QString NorS = polarCursorPos.x > 0 ? "N" : "S";
        QString EorW = polarCursorPos.y > 180 ? "W" : "E";

        polarCursorPos.y = polarCursorPos.y > 180 ? polarCursorPos.y - 180 : polarCursorPos.y;

        text1 = "Координаты курсора: " + GeoHelper::deg2dms(polarCursorPos.x)
                + NorS + " " + GeoHelper::deg2dms(polarCursorPos.y) +  EorW + "  Высота камеры:" + QString::number(height, 'g', 3) + " km";
    } else {

        text1 = "Координаты курсора: --- / ---  Высота камеры:" + QString::number(height);

    }

    int textW = metrics.width( text1 );
    QRect textRect = QRect(mWidgetW /2 - textW / 2, mWidgetH - metrics.height() * 1.5, textW , metrics.height() * 1.5);
    painter->drawText( textRect, text1 );

    drawLabels(painter);

    painter->restore();
}

void MainWidget::drawLabels(QPainter* painter){

     mShowedZoneLabels.clear();

    for(QMap<AirPortType, AirPort>::Iterator it = MainRenderModel->mAirPort.begin(); it != MainRenderModel->mAirPort.end(); it++){
         AirPort *CurAirport = &it.value();

         for (int i = 0; i < CurAirport->mProcedures.size(); i++){

             QString name = CurAirport->mProcedures[i].getName();

             bool temp1 = mClickedObjects.contains(name) || mObjectsCursorOn.contains(name);

             if (mNameObj.contains(name) && ( temp1 ) && mShowPoints ) {

                 for (int j = 0; j < CurAirport->mProcedures[i].mWayPoints.size(); j++){
                     Waypoint  *point = &CurAirport->mProcedures[i].mWayPoints[j];

                     int val = CurAirport->getHeightRate();

                     glm::vec3 pointCoordUp = Helper::CalcPointOnDistance(glm::vec3(point->x, point->y, point->z),
                                                                      glm::vec3(0,0,0),
                                                                      -CurAirport->getHeightRate()*((point->maxAlt + point->minAlt)/2));

                     glm::vec3 pointCoordDown = glm::vec3(point->x, point->y, point->z);

                     QString text =  "★\n" + point->mName;

                     if (point->mName == mCursorOnObject->pHall->mStartWaypoint.mName ||
                             point->mName == mCursorOnObject->pHall->mEndWaypoint.mName ){

                        text = "★\n" + point->mName;
                        QColor color(255,255,255, 225);

                        DrawLabelOnWorldPos(pointCoordDown,
                                     text,
                                     painter,
                                     true,
                                     color
                                     );

                        DrawLabelOnWorldPos(mCursorOnObject->pHall->StartUpPoint,
                                     QString::number(mCursorOnObject->pHall->getStartAltMax()),
                                     painter,
                                     true,
                                     color
                                     );

                        DrawLabelOnWorldPos(mCursorOnObject->pHall->StartDownPoint,
                                     QString::number(mCursorOnObject->pHall->getStartAltMin()),
                                     painter,
                                     true,
                                     color
                                     );

                        DrawLabelOnWorldPos(mCursorOnObject->pHall->EndUpPoint,
                                     QString::number(mCursorOnObject->pHall->getEndAltMax()),
                                     painter,
                                     true,
                                     color
                                     );

                        DrawLabelOnWorldPos(mCursorOnObject->pHall->EndDownPoint,
                                     QString::number(mCursorOnObject->pHall->getEndAltMin()),
                                     painter,
                                     true,
                                     color
                                     );

                    }

                 }
            }
        }
    }
}

void MainWidget::DrawLabelOnWorldPos(glm::vec3 pos, QString name, QPainter* painter, bool drawNecessarily, QColor color){


    glm::vec3 ScreenPos = getScreenCoord(pos);

    if (!(ScreenPos.x < 0 || ScreenPos.x > mWidgetW || ScreenPos.y < 0 || ScreenPos.y > mWidgetH /* || ScreenPos.z < 0.990*/)) {

        if ( mShowedZoneLabels.size() < 15) {
            mShowedZoneLabels.insert(name);
        }

        if (drawNecessarily || mShowedZoneLabels.contains(name)){

            glm::vec3 worldPos = glm::vec3(MainRenderModel->mModelMatrix * glm::vec4(pos, 1.0f));

            double distance = glm::length(glm::vec3(mCamera->getEye() - worldPos));

            painter->setPen(color);

            QFont font;
            font.setWeight(QFont::Bold);
            font.setOverline(true);

            int fontSize =  12 - ( distance/10000);
            if (fontSize <= 0) fontSize = 1;

            font.setPointSize( fontSize );
            QFontMetrics metrics( font );

            int width = metrics.width( name );
            QRect textRect = QRect(ScreenPos.x - width / 2, ScreenPos.y, width+5 , metrics.height() * 4);
            painter->drawText(textRect, Qt::AlignHCenter,name, new QRect());

            painter->setPen(QColor(255,255,255,255));
        }
    }
}

void MainWidget::drawObjectLegend(QPainter* painter){

    if (mNameObj.contains(mCursorOnObject->mName) && mCursorOnObject->isActive){
        painter->save();
        painter->setPen(QColor(Qt::white));
        QFont font;
        font.setPointSize( SettingsClass->getLegendTextSize() );
        QFontMetrics metrics( font );

        QString object;

        switch (mCursorOnObject->mType) {
            case ZoneType: object = "Зона"; break;
            case HallType: object = "Процедура"; break;
            case BarrierType: object = "Препядствие"; break;
            case AirPlaneType: object = "Самолет"; break;
        }

        QString line1 = "Объект (" + object + ") : " ;

        QString line2 = mCursorOnObject->mName.left(10);

        QString line3 = "Свойства:";

        std::string minHeight = "---";
        std::string maxHeight = "---";

        QString line4 = "";

        int textH;

        if (mCursorOnObject->mType == ZoneType)
        {

            QString maxHeight = mCursorOnObject->pZone->getAltMax() == 30000 ? "UNLIMITED" : QString::number(mCursorOnObject->pZone->getAltMax() ,'g', 6);

            line4 = "Высота: от " + QString::number(mCursorOnObject->pZone->getAltMin() ,'g', 6)
                    +" до " + maxHeight + " (м)";

            textH = metrics.height() * 4;

        }

        if (mCursorOnObject->mType == HallType){

            textH = metrics.height() * 2;

            QColor colorHeightText = QColor(222, 0, 0, 255);

        }

        if (mCursorOnObject->mType == BarrierType){
            line4 = "Высота: " + QString::number(mCursorOnObject->pBarrier->getAltitude() ,'g', 6) + " (м)";
            textH = metrics.height() * 4;
        }

        if (mCursorOnObject->mType == AirPlaneType){
            line4 = "Высота: " + QString::number(mCursorOnObject->pAirPlane->getAltitude() ,'g', 6) + " (м)";
            textH = metrics.height() * 4;
        }

        QString text = line1 + "\n" + line2 + "\n" + line3 + "\n" + line4;

        text = text.toUtf8();

        QRect textRect = QRect( mCurMousePos.x() + 100, mCurMousePos.y() - 100, 220, textH );
        painter->setFont( font );

        QRect boundingRect = QRect(QPoint(), textRect.size()*1.2);
        boundingRect.moveCenter( textRect.center() );

        painter->setPen(QColor(SettingsClass->getLegendFrameColor().x * 255.0f, SettingsClass->getLegendFrameColor().y * 255.0f, SettingsClass->getLegendFrameColor().z * 255.0f, SettingsClass->getObjectsTrancparency(TRANCPARENCY_LEGEND)*255.0f));
        painter->drawLine(mCurMousePos.x() + 80, mCurMousePos.y() - 80, mCurMousePos.x() + 72, mCurMousePos.y() - 80);
        painter->drawLine(mCurMousePos.x() + 72, mCurMousePos.y() - 80, mCurMousePos.x() + 5 , mCurMousePos.y() - 5);
        painter->drawRoundedRect( boundingRect, 5, 5 );
        painter->setPen(QColor(SettingsClass->getLegendTextColor().x * 255.0f, SettingsClass->getLegendTextColor().y * 255.0f, SettingsClass->getLegendTextColor().z*  255.0f , SettingsClass->getObjectsTrancparency(TRANCPARENCY_LEGEND)*255.0f));
        painter->drawText( textRect, text );

        painter->restore();
    }
    if (mIsMidButtonPressed && pointRotateAroundOnScreen == (glm::vec2(-1,-1))){

        pointRotateAroundOnScreen = getScreenCoord(mCamera->getPointRotateAround());
    }
    if (mIsMidButtonPressed) {
        painter->save();
        pointRotateAroundOnScreen = getScreenCoord(mCamera->getPointRotateAround());
        painter->drawEllipse(QPointF(pointRotateAroundOnScreen.x, pointRotateAroundOnScreen.y), 10, 10);
        painter->restore();
    }

}

void MainWidget::RenderQuad()
{
    if (quadVAO == 0)
    {
        GLfloat quadVertices[] = {
            // Positions        // Texture Coords
            -1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        // Setup plane VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

void MainWidget::setShowPoints(bool value){
    mShowPoints = value;
}

QSet < QString > * MainWidget::getClickedObjects(){
    return &mClickedObjects;
}

void MainWidget::clearClickedObjects(){

    for (QSet<QString>::iterator i = mClickedObjects.begin(); i != mClickedObjects.end(); i++) {
       QString element = *i;

       for(QMap<AirPortType, AirPort>::Iterator it = MainRenderModel->mAirPort.begin(); it != MainRenderModel->mAirPort.end(); it++){
            AirPort *temp = &it.value();

            temp->addCheckedProcedure(element);
       }

       MainRenderModel->mZoneBuilder.addCheckedZone(element);
    }

    mClickedObjects.clear();
}


MainWidget::~MainWidget(){
    mFrameBuffer.remove();
    mBloorFramBuffer[0].remove();
    mBloorFramBuffer[1].remove();
}
