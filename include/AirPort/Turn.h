#ifndef TURN_H
#define TURN_H

#include "include/Headers.h"
#include <glm/glm.hpp>
#include "include/Helper.h"
#include "include/RenderObject/RenderObject.h"

/********************************

Class:		Rounding

Purpose:	calculation FLY_BY, FLY_OVER

            for working need:
            1) Rounding(params)
            2) init

********************************/

class SingleMainRenderModel;
class mainSettingsClass;
class Procedure;

class Turn : public RenderObject
{

public:
    Turn                                                ( ) {}
    Turn                                                ( glm::vec3 start,
                                                          glm::vec3 middle,
                                                          glm::vec3 end,
                                                          double LTA,
                                                          TurnTypes type );

    void                    calcBasicTrajectory         ( );
    void                    calcAllRenderPoint          ( );

    void                    addLineVertex               ( QVector < glm::vec3 > *turn,
                                                         double Height,
                                                         int idOffset );
    void                    addLineVertex               ( QVector<glm::vec3> *turn,
                                                          RenderObjectType type,
                                                          double Height,
                                                          double HeightStep,
                                                          int idOffset);

    void                    init                        ( );
    void                    paint                       ( );

    glm::vec3               calcRoundPointOnDist        ( glm::vec3 start,
                                                         glm::vec3 end,
                                                         double lta );

    QVector <glm::vec3>     calcFlyBy                   ( glm::vec3 start,
                                                         glm::vec3 center,
                                                         glm::vec3 end );

    QVector <glm::vec3>     calcFlyOver                 ( Waypoint wp1,
                                                         Waypoint wp2,
                                                         Waypoint wp3 );

    void                    calcHeightsStart            ( );
    void                    calcHeightsEnd              ( );

    void                    calcTurnDiretion            ( );

    bool                    getIsNoTurn                 ( ) const;
    QVector<glm::vec3>      getRoundPoints              ( );
    glm::vec3               getStartPoint               ( );
    glm::vec3               getEndPoint                 ( );
    glm::vec3               getCenterOfRound            ( );
    QVector<glm::vec3>      getSmallRound               ( );
    QVector<glm::vec3>      getBigRound                 ( );
    double                  getEndHeight                ( );
    double                  getStartHeight              ( );
    float                   getHeightRate               ( ) const;
    Procedure *             getParentProcedure          ( );

    void                    setIsNoTurn                 ( bool value );
    void                    setEndHeight                ( double h );
    void                    setStartHeight              ( double h );
    void                    setRadius                   ( double r );
    void                    setHeight                   ( double h );
    void                    setDistCenter               ( double c );

    void                    setColor                    ( glm::vec3 color );
    void                    updateColor                 ( glm::vec4 color );

    void                    setWayPoints                ( Waypoint start,
                                                         Waypoint center,
                                                         Waypoint end );

    void                    setParentProcedure          ( Procedure *parent );

    void                    setHeightRate               ( const float value );
    void                    updateBuffers               ( );
    void                    updateColor                 (RenderData *curVAO,
                                                         elementsToDraw element);
    void                    SetStartPointForHeight      ( Waypoint point );


    double getDopHUpStart() const;
    void setDopHUpStart(double value);

    double getDopHDownStart() const;
    void setDopHDownStart(double value);

    double getStartHeightUp() const;
    void setStartHeightUp(double startHeightUp);

    double getStartHeightDown() const;
    void setStartHeightDown(double startHeightDown);

    double getDopHCenterStart() const;
    void setDopHCenterStart(double value);

    double getStartHeightCurUp() const;

    double getEndHeightCurUp() const;

    double getStartHeightCurDown() const;

    double getEndHeightCurDown() const;

private:

    double                  dist                        ( glm::vec3 a, glm::vec3 b );
    glm::vec3               calcCenter3D                ( );

    double                  mLTA;  // Linear turn anticipation
    double                  EPS;

    double                  mStartHeight;
    double                  mEndHeight;
    double                  mCenterHeight;

    double                  mStartHeightUp;
    double                  mEndHeightUp;
    double                  mStartHeightDown;
    double                  mEndHeightDown;

    double                  mDistCenter;
    double                  mRadiusTurn;

    QVector <glm::vec3>     mTurn;
    QVector <glm::vec3>     mSmallRound;
    QVector <glm::vec3>     mBigRound;

    bool                    isNoTurn;
public:
    bool                    isLeftTurn;
    double getWidth() const;
    void setWidth(double width);

    bool getIsLeftTurn() const;

private:

    glm::vec3               mCenterTurn;

    glm::vec3               mStartRoundPoint3D;
    glm::vec3               mEndRoundPoint3D;

    glm::vec3               mStart, mCenter, mEnd;

    Waypoint                mWaypointStart;
    Waypoint                mWaypointCenter;
    Waypoint                mWaypointEnd;
    Waypoint                mWaypointStartHeight;

    TurnTypes               mTurnType;

    glm::vec3               mColor;

    float                   mHeightRate;

    SingleMainRenderModel*  MainRenderModel;
    mainSettingsClass *     SettingsClass;

    Procedure*              pProcedure;
    double                  mWidth;

    ////

    double dopHUpStart;
    double dopHDownStart;
    double dopHUpEnd;
    double dopHDownEnd;
    double dopHCenterStart;


    double StartHeightCurUp;
    double StartHeightCurDown;
    double EndHeightCurUp;
    double EndHeightCurDown;

    double distFromEarthCenter;

};

#endif // ROUNDING_H
