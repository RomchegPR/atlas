#ifndef PROCEDURE_H
#define PROCEDURE_H

#include <QVector>
#include <include/AirPort/Hall.h>
#include <include/Headers.h>
#include "include/RenderObject/RenderObject.h"

/********************************

Class:		Procedure

Purpose:	contains all information for Procedure,
            calculation of Vertexes and other data
            for rendering

            1) addWayPoint - add all Air points
            2) init
            3) draw

********************************/

class Procedure : public RenderObject
{

public:
    Procedure                               ()          {}

    Procedure                               ( ProcedureType type,
                                              ProcedureMode mode,
                                              QString name );

                         // if need to build simple hall from point to point
    void                 addHall            ( Waypoint &start,
                                              Waypoint &center,
                                              Waypoint &end );

    Hall                 createHall         ( Waypoint &start,
                                              Waypoint &end );

    Hall                 createHall         ( Waypoint &start,
                                              Waypoint &end,
                                              Turn *StartTurn,
                                              Turn *EndTurn);

    void                 addWayPoint        ( Waypoint &point );

    //from RenderObject
    void                 init               ( );
    void                 initVAO            ( RenderData *VAO, glm::vec4 color );

    void                 paint              ( int index );
    void                 paint              ( );

    void                 calc               ( );

    void                 draw               ( );
    void                 draw               ( HallBitmask mask );

    void                 draw               ( int ind,
                                              GLbitfield mask );

    void                 clear              ( );


    Waypoint             vec2waypoint       ( glm::vec3 temp,
                                              double MinAlt,
                                              double MaxAlt,
                                              Waypoint &example );

    ProcedureType        getType            ( )          const;
    ProcedureMode        getMode            ( )          const;
    QString              getName            ( )          const;
    glm::vec3            getColorFill       ( )          const;
    glm::vec3            getColorFrame      ( )          const;
    bool                 getIsChecked       ( )          const;
    int                  getCountHalls      ( )          const;
    glm::vec3            getHallCenter      ( int i );
    float                getHeightRate      ( )          const;
    HallBitmask          getDrawMask        ( );
    ObjectStatus         getStatus          ( )          const;

    void                 setType            ( const ProcedureType &value );
    void                 setName            ( const QString &value );
    void                 setColorFill       ( const glm::vec3 &value );
    void                 setColorFrame      ( const glm::vec3 &value );
    void                 setMode            ( const ProcedureMode &value );
    void                 setIsChecked       ( bool value );
    void                 setDrawMask        ( HallBitmask value,
                                              bool fool );
    void                 setHeightRate      ( const float value );
    void                 setFillTrancparency(const float value);
    void                 setStatus          (const ObjectStatus &value);

private:

    ProcedureType               mType;               // STAR, SID, APRCH
    ProcedureMode               mMode;               // need in future for display diferent Modes
    QString                     mName;
    bool                        isChecked;           // is this procedure has been clicked

    glm::vec3                   mColorFill;          // color for sides
    glm::vec3                   mColorFrame;         // color for lines

    HallBitmask                 mDrawMask;           // mask of elementsToDraw, pointer to Airport.mDrawMask
    ObjectStatus                Status;

    QVector <Waypoint >         mWaypointsAllForTurns;
public:
    QVector <Hall>              mHalls;
    QVector <Waypoint>          mWayPoints;
    QVector <Turn>              mTurns;
    float                       mHeightRate;

};

#endif // PROCEDURE_H
