#ifndef HALL_H
#define HALL_H

#include "glm/glm.hpp"
#include "QMap"
#include "QVector"
#include "include/AirPort/WayPoint.h"
#include "include/AirPort/Turn.h"
#include "include/RenderObject/RenderObject.h"

class SingleMainRenderModel;
class Procedure;
class mainSettingsClass;

class Hall : public RenderObject
{

public:

    Hall();
    ~Hall();

    Waypoint               mStartWaypoint;
    Waypoint               mEndWaypoint;

    //from RenderObject
    void            paint                          ( );
    void            init                           ( );

    // functions for calculations and filling Vectors of Data
    void            setPoints                      ( Waypoint StartWayPoint,
                                                     Waypoint EndWayPoint,
                                                     bool isUpdate = false);

    void            buildHallFromPoints            ( Waypoint StartWayPoint,
                                                     Waypoint EndWayPoint,
                                                     bool isUpdate = false);

    void            buildLines                     ( );
    void            buildTrajectory                ( Waypoint StartWayPoint,
                                                     Waypoint EndWayPoint );

    void            buildMoreRam                   ( Waypoint StartWayPoint,
                                                     Waypoint EndWayPoint,
                                                     double r );

    Waypoint        findMiddle                     ( Waypoint StartWayPoint,
                                                     Waypoint EndWayPoint);

    void            init                           ( glm::vec3 &color );
    void            initVAO                        ( RenderData *VAO,
                                                     glm::vec4 color );

    void            draw                           ( HallBitmask elemToDraw );
    void            drawTriangles                  ( RenderData *curVAO,
                                                      elementsToDraw elem );
    void            drawLines                      ( RenderData *curVAO,
                                                      elementsToDraw elem );

    void            setTurns                        (Turn *StartTurn = NULL,
                                                    Turn *EndTurn = NULL,
                                                     bool isUpdate = false);

    void            setColor                       ( glm::vec4 value );

    void            setDrawMask                    ( HallBitmask *value );
    void            setHeightRate                  ( const float value );
    void            setSpeed                       ( float startSpeedm,
                                                     float endSpeed );
    float           setFillTrancparency             (float value);

    void            setParentProcedure             ( Procedure *parent );

    double          getAltMax                      ( );
    double          getAltMin                      ( );
    glm::vec3       getCenter                      ( );
    glm::vec3       *getPColor                     ( ) const;
    float           getHeightRate                  ( ) const;
    double          getStartSpeed                  ( );
    double          getEndSpeed                    ( );
    double          getStartAltMin                 ( );
    double          getEndAltMin                   ( );
    double          getStartAltMax                 ( );
    double          getEndAltMax                   ( );
    Procedure *     getParentProcedure             ( );
    HallBitmask *   getDrawMask                    ( );

    void            setStartPoint                  ( glm::vec3 point );
    void            setEndPoint                    ( glm::vec3 point );

    glm::vec3       getStartPoint                  ( );
    glm::vec3       getEndPoint                    ( );

    void            updateRenderPoints             ( );
    void            updateBuffers                  ( );
    void            updateColor                    ( RenderData *curVAO,
                                                     elementsToDraw element );

private:

    glm::vec3               a, a0, a1, a2, a3, b, b0, b1, b2, b3; // not use this coords!!!!

    double                  mAltMaxStart; // max value Height from start to the end
    double                  mAltMinStart; // min value Height from start to the end

    double                  mAltMaxEnd; // max value Height from start to the end
    double                  mAltMinEnd; // min value Height from start to the end


    double                  mStartSpeed;
    double                  mEndSpeed;

    glm::vec3               mCenterHall;
    glm::vec3               mStartHall;
    glm::vec3               mEndHall;

    glm::vec4               mColor;
    HallBitmask*            elemToDraw;

    SingleMainRenderModel*  MainRenderModel;
    mainSettingsClass *     SettingsClass;

    float                   mHeightRate;
    float                   mFillTrancparency;

    Procedure              *pProcedure;
    Turn                   *pStartTurn;
    Turn                   *pEndTurn;

    double                  mWidth;

public:
    glm::vec3 StartDownPoint;
    glm::vec3 EndDownPoint;
    glm::vec3 StartUpPoint;
    glm::vec3 EndUpPoint;
    double getWidth() const;
    void setWidth(double width);
};

#endif // HALL_H
