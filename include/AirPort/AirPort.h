#ifndef AIRPORT_H
#define AIRPORT_H

#include <QMap>
#include <QString>
#include <QStringList>
#include "include/AirPort/WayPoint.h"
#include "glm/glm.hpp"
#include "include/AirPort/Procedure.h"
#include "include/Ray.h"

/********************************

Class:		AirPort

Purpose:	Airport is main class which contains
            Procedures and colors for them.


            This class is required for reading
            the input data for the procedures
            and common management of them
            (drawing, color change, choosing etc.)

            to correct working:
            1) fillData
            2) init
            3) draw

********************************/

#define InputAirportData QMap < QString, QVector < QVector < Waypoint > > >

class AirPort
{
public:

    AirPort();

    void                fillData                ( InputAirportData &airPoints );

    void                init                    ( );

    void                draw                    ( );
    void                draw                    ( QString name );
    void                draw                    ( QString name,
                                                  int id );

    QString             checkClickProcedure     ( Ray &ray,
                                                  QSet < QString > &NameObj );

    bool                checkTriangle           ( Hall Hall,
                                                  int i1,
                                                  int i2,
                                                  int i3,
                                                  Ray &ray );

    bool                addCheckedProcedure     ( QString name );

    void                clear                   ();

    void                setName                 ( const QString &value );
    void                setDrawMask             ( const HallBitmask &value );
    void                setHeightRate           ( const float value );
    void                setFillTrancparency     ( float val );

    QString             getName                 ( ) const;
    HallBitmask         getDrawMask             ( ) const;
    QStringList         getProcedureNames       ( ) const;
    float               getHeightRate           ( ) const;
    Hall*               getCursorOnHall         ( ) ;
    float               getFillTrancparency     ( );

private:
    QString                                 mName;
    QMap <ProcedureType, glm::vec3 >        mProcedureColors;         // Example - STAR = yellow
    HallBitmask                             mDrawMask;                // needs to saving elementsToDraw (Headers.h)
    glm::vec3                               mColorForChecked;         // color for procedure, that clicked
    Hall                                    mCursorOnHall;

    static float                            TRANCPARENCY_HALL;


public:
    QVector <Procedure >                    mProcedures;
    float                                   mHeightRate;


};

#endif // AIRPORT_H
