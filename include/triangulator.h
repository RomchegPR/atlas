#ifndef TRIANGULATOR_H
#define TRIANGULATOR_H

#include <include/triangle.h>
#include <qvector.h>
#include <glm/glm.hpp>
typedef QList<int> ILine;
typedef QList<ILine> SILine;

class Trianglator
{
private:
    QVector < QVector < glm::vec2 > > indexes;
    QVector < glm::vec2 > verts;
    QVector < QVector <int> > faces;
    QVector < QVector <int> > edges;
public:
    // Функция трианлуляции полигона
    bool DoTriangulate( QVector < glm::vec2 > &vctVertsSource, QVector < QVector <int> >  *segments = NULL);
    QVector < QVector < glm::vec2 > > getIndexes();
    // Внутренние функции
    QVector<QVector<int> > getFaces() const;
    QVector<glm::vec2> getVerts() const;
};

#endif // TRIANGULATOR_H
