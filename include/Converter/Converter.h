#ifndef CONVERTER_H
#define CONVERTER_H

#include "include/Headers.h"
#include "include/Parser/Parser.h"
#include "include/GeoHelper.h"

class Converter
{
private:
    QMap <QString, QVector<ProcedureData > > mProcedures; // Конвертированные данные
    QMap <QString, QVector<ProcedureData > > mOriginalProcedures; // Исходные данные
    QString mFileName;
private:
    void convertAlt(QString type); // Конвертирование высот (на вход подается тип процедуры)
    void convertWP(QStringList& wp); // Конвертирование координат
    QStringList splitAlt(QString& alt); // Разделение строки с высотами на 2 строки
    QStringList splitWP(QString& wp); // Разделение строки с координатами на 2 строки
    double calculateGradient(QString type, int procIdx, int pointIdx, QString alt);
public:
    Converter();
    Converter(Parser &parser);
    virtual ~Converter();
    void convert(); // Основной метод, в котором происходит конверирование данных
    void print(QString type);
    void fill(QMap <QString, QVector<ProcedureData > > &procedures); // Заполнение данных
    void print(QString type, int k);
    void clear();
    QMap <QString, QVector<ProcedureData > >& getPoints();
    QMap <QString, QVector<QVector <Waypoint> > > getStruct();
};

#endif // CONVERTER_H
