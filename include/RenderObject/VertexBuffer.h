#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include <GL/glew.h>
#include <QVector>

/**
  @class VertexBuffer

  @brief Обертка для OpenGL Vertex Buffer Object
  */
class VertexBuffer
{
public:
    enum Type{
        VertexArray			= GL_ARRAY_BUFFER,
        ElementArray		= GL_ELEMENT_ARRAY_BUFFER,
        PixelPack			= GL_PIXEL_PACK_BUFFER,
        PixelUnpack			= GL_PIXEL_UNPACK_BUFFER
    };

    enum UsagePattern{
        StreamDraw          = GL_STREAM_DRAW,
        StreamRead          = GL_STREAM_READ,
        StreamCopy          = GL_STREAM_COPY,
        StaticDraw          = GL_STATIC_DRAW,
        StaticRead          = GL_STATIC_READ,
        StaticCopy          = GL_STATIC_COPY,
        DynamicDraw         = GL_DYNAMIC_DRAW,
        DynamicRead         = GL_DYNAMIC_READ,
        DynamicCopy         = GL_DYNAMIC_COPY
    };

    enum AccessMode{
        ReadOnly			= GL_READ_ONLY,
        WriteOnly			= GL_WRITE_ONLY,
        ReadWrite			= GL_READ_WRITE
    };

    VertexBuffer( Type type = VertexArray, UsagePattern usage = StaticDraw );
    virtual ~VertexBuffer();

    //Инициализатор, назначение как конструктора
    void init(Type type, UsagePattern usage);

    /// @brief Создает новый буфер
    /// @return true если создание прошло успешно. Иcпользуйте glGetError чтобы получить подробности в случае неудачи.
    bool create( );

    bool isCreated();

    /// Удаляет буфер
    void remove();

    /// Возвращает OPenGL идентификатор буфера
    GLuint getID();

    /// Сделать текущий буфер активным
    void bind();

    /// Сбросить активность буфера
    void unbind();

    /// @brief Установить значение буфера
    /// @param size размер буфера в байтах
    /// @param ptr путь к данным
    void setData( ptrdiff_t size, const void* ptr );

    template <typename T>
    /// @brief Установить значение буфера из вектора данных
    /// @param dataVector вектор данных
    void setData(const QVector<T> &dataVector );

    template <typename T>
    /// @brief Установить значение буфера из вектора данных
    /// @param dataVector вектор данных
    void setData(const std::vector<T> &dataVector );

    /// Очищает данные буфера
    void clear();

    /// @brief Обновляет фрагмент данных буфера
    /// @param offset Смещение в данных буферного объекта, с которого данные будут обновлены.
    /// @param size Размер заменяемых данных
    /// @param ptr Указататель на новые данные которые нужно поместить в буфере
    void setSubData( ptrdiff_t offset, ptrdiff_t size, const void* ptr );


    /// @brief запрашивает содержимое буферного объекта
    /// @param offset Смещение в данных буферного объекта, с которого необходимо получить данные.
    /// @param size Размер запрашиваемых данных
    /// @param ptr Указатель на область в памяти, куда будут помещены данные
    void getSubData( ptrdiff_t offset, ptrdiff_t size, void* ptr );

    /// @brief Предоставляет доступ к чтению или изменению данных буферного объекта
    /// @param access Тип доступа к данным. Может принимать одно из трёх значений: GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE
    /// @return Указатель на адресное пространство на стороне клиента, связанное с данными буферного объекта, при успешном вызове; или NULL — при неуспешном вызове.
    void* map( AccessMode access );

    /// @brief Освобождает данные буферного объекта.
    bool unmap();

    /// @brief Размер буфера
    int size( );

private:
    GLuint mId;
    Type mTarget;
    UsagePattern mUsagePattern;
    bool mOk;

};

template <typename T>
void VertexBuffer::setData(const QVector<T> &dataVector )
{
    glBufferData(mTarget,
        dataVector.size()*sizeof(T),
        dataVector.constData(),
        mUsagePattern
    );
}

template <typename T>
void VertexBuffer::setData(const std::vector<T> &dataVector )
{
    glBufferData(mTarget,
        dataVector.size()*sizeof(T),
        dataVector.constData(),
        mUsagePattern
    );
}

#endif // VERTEXBUFFER_H
