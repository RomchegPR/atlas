#ifndef TEXTURE_H
#define TEXTURE_H

#include "GL/glew.h"
#include <QImage>

namespace InputData{
    enum Format{
        Red				= GL_RED,
        RG				= GL_RG,
        RGB				= GL_RGB,
        BGR				= GL_BGR,
        RGBA			= GL_RGBA,
        BGRA			= GL_BGRA,
        Red_Int			= GL_RED_INTEGER,
        RG_Int			= GL_RG_INTEGER,
        RGB_Int			= GL_RGB_INTEGER,
        BGR_Int			= GL_BGR_INTEGER,
        RGBA_Int		= GL_RGBA_INTEGER,
        BGRA_Int		= GL_BGRA_INTEGER,
        StencilIndex	= GL_STENCIL_INDEX,
        DepthComponent	= GL_DEPTH_COMPONENT,
        DepthStencil	= GL_DEPTH_STENCIL
    };

    enum Type{
        dtUnsignedByte				= GL_UNSIGNED_BYTE,
        dtByte						= GL_BYTE,
        dtUnsignedShort				= GL_UNSIGNED_SHORT,
        dtShort						= GL_SHORT,
        dtUnsignedInt				= GL_UNSIGNED_INT,
        dtInt						= GL_INT,
        dtFloat						= GL_FLOAT,
        dtUnsignedByte332			= GL_UNSIGNED_BYTE_3_3_2,
        dtUnsignedByte233Rev		= GL_UNSIGNED_BYTE_2_3_3_REV,
        dtUnsignedShort565			= GL_UNSIGNED_SHORT_5_6_5,
        dtUnsignedShort565Rev		= GL_UNSIGNED_SHORT_5_6_5_REV,
        dtUnsignedShort4444			= GL_UNSIGNED_SHORT_4_4_4_4,
        dtUnsignedShort4444Rev		= GL_UNSIGNED_SHORT_4_4_4_4_REV,
        dtUnsignedShort5551			= GL_UNSIGNED_SHORT_5_5_5_1,
        dtUnsignedShort1555Rev		= GL_UNSIGNED_SHORT_1_5_5_5_REV,
        dtUnsignedInt8888			= GL_UNSIGNED_INT_8_8_8_8,
        dtUnsignedInt8888Rev		= GL_UNSIGNED_INT_8_8_8_8_REV,
        dtUnsignedInt_10_10_10_2	= GL_UNSIGNED_INT_10_10_10_2,
        dtUnsignedInt_2_10_10_10Rev	= GL_UNSIGNED_INT_2_10_10_10_REV
    };
}

class Texture
{
public:
    enum WrapMode{
        ClampToEdge		= GL_CLAMP_TO_EDGE,
        ClampToBorder	= GL_CLAMP_TO_BORDER,
        Repeat			= GL_REPEAT,
        MirroredRepeat	= GL_MIRRORED_REPEAT
    };

    enum FilterMode{
        Nearest					= GL_NEAREST,
        Linear					= GL_LINEAR,
        NearestMipMapNearest	= GL_NEAREST_MIPMAP_NEAREST,
        LinearMipMapNearest		= GL_LINEAR_MIPMAP_NEAREST,
        LinearMipMapLinear		= GL_LINEAR_MIPMAP_LINEAR
    };

    enum Target{
        Texture_1D      = GL_TEXTURE_1D,
        Texture_2D      = GL_TEXTURE_2D,
        Texture_3D      = GL_TEXTURE_3D,
        Texture_CubeMap = GL_TEXTURE_CUBE_MAP
    };

    enum TextureFormat{
        Red				= GL_RED,
        RGB				= GL_RGB,
        RGB16           = GL_RGB16F,
        RGBA			= GL_RGBA,
        Depth			= GL_DEPTH_COMPONENT
    };

    enum CubeMapSide{
        PositiveX		= GL_TEXTURE_CUBE_MAP_POSITIVE_X,
        NegativeX		= GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
        PositiveY		= GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
        NegativeY		= GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
        PositiveZ		= GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
        NegativeZ		= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,

        Rigth			= PositiveX,
        Left			= NegativeX,
        Top				= PositiveY,
        Bottom			= NegativeY,
        Front			= PositiveZ,
        Back			= NegativeZ
    };

            Texture();
    virtual ~Texture();

    GLuint	id				( ) const;

    bool	create			( Target target );

    Target	target			( );
    void	bind			( );
    void	unbind			( );
    static
    void	unbind			( Target target );

    void	setImage2D		( const QImage &img );
    void	setImage2D		( TextureFormat		format,
                              int				width,
                              int				height,
                              InputData::Format	inputFormat,
                              InputData::Type	inputType,
                              uchar*			data,
                              int				miplevel = 0,
                              int				border = 0		);
    void	setImage1D		( TextureFormat		format,
                              int				width,
                              InputData::Format	inputFormat,
                              InputData::Type	inputType,
                              uchar*			data,
                              int				miplevel = 0
                            );

    void	setCubeMap		( CubeMapSide		side,
                              TextureFormat		format,
                              int				width,
                              int				height,
                              InputData::Format	inputFormat,
                              InputData::Type	inputType,
                              uchar*			data,
                              int				miplevel = 0,
                              int				border = 0		);

    void	setFilter		( FilterMode min, FilterMode mag );
    void	setWrap			( WrapMode s );
    void	setWrap			( WrapMode s, WrapMode t );
    void	setWrap			( WrapMode s, WrapMode t, WrapMode r );
    void	setBaseLevel	( int level );
    void	setMaxLevel		( int level );
    void	generateMipmaps	( );
    void	copyTexImage2D	( TextureFormat format,
                              int x,
                              int y,
                              int width,
                              int height,
                              int miplevel = 0
                            );

    void	remove		( );

protected:
    GLuint mID;
    Target mTarget;

};

#endif // TEXTURE_H
