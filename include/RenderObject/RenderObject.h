#ifndef RENDEROBJECT_H
#define RENDEROBJECT_H

#include <QMap>
#include "include/RenderObject/RenderData.h"

/********************************

Class:		RenderObject

Purpose:	wrapper class for RenderData,
            use for rendered objects

********************************/

class RenderObject
{

public:

    enum RenderObjectType {

        BasicType,
        LinesType,

        HallObjectType,
        HallLinesObjectType,
        HallLinesTrajectoryObjectType,
        HallLinesRamObjectType,
        HallGroundLinesProjectionObjectType,
        HallGroundLinesObjectType

    };

    // Initialize Buffers
    virtual void        init        ( ) {}

    //GL functions switching on/off
    virtual void        paint       ( ) {}

    virtual void        clear       ( ) {
        for(QMap<RenderObjectType, RenderData >::Iterator it = mRenderData.end() - 1; it != mRenderData.begin() - 1; it--){
                it.value().clear();
        }
    }

public:

    QMap < RenderObjectType,  RenderData >         mRenderData;
};

#endif // RENDEROBJECT_H
