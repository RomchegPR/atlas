#ifndef VERTEXARRAY_H
#define VERTEXARRAY_H

#include <GL/glew.h>

class VertexArray
{
public:
    VertexArray( );
    virtual ~VertexArray();

    bool	create( );
    bool	isCreated( );

    GLuint	getID();

    void	remove();

    void	bind( );
    void	unbind();

private:
    GLuint mID;

};

#endif // VERTEXARRAY_H
