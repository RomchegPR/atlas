#ifndef RENDERDATA_H
#define RENDERDATA_H

#include "include/RenderObject/VertexArray.h"
#include "include/RenderObject/VertexBuffer.h"
#include <QVector>
#include "glm/glm.hpp"
#include "include/RenderObject/Texture.h"
#include "iostream"

/********************************

Class:		RenderData

Purpose:	contains Vertex, Buffers and etc for
            ONE Hall or Turn of procedure.

********************************/

class RenderData
{

public:
    RenderData() {};

    VertexArray                         mVAO;

    VertexBuffer                        mElementBuffer;
    VertexBuffer                        mVertexBuffer;
    VertexBuffer                        mColorBuffer;
    VertexBuffer                        mTextureBuffer;
    VertexBuffer                        mHeightVecBuffer;

    QVector < glm::vec3 >               mVertex;
    QVector < GLuint >                  mIndices;
    QVector < glm::vec3 >               mNormal;
    QVector < glm::vec2 >               mTextureIndex;

    Texture                             mTexture;

    void clear(){

        mVAO.bind();

        mElementBuffer.remove();
        mVertexBuffer.remove();
        mColorBuffer.remove();
        mTextureBuffer.remove();

        mVertex.clear();
        mIndices.clear();
        mTextureIndex.clear();
        mNormal.clear();

        mTexture.remove();

        mVAO.remove();
    }
};

static GLenum glCheckError_(const char *file, int line)
{
    GLenum errorCode;
    while ((errorCode = glGetError()) != GL_NO_ERROR)
    {
        /*
        * Flag                                Code 	Description
        *
        * GL_NO_ERROR                         0     No user error reported since last call to glGetError.
        * GL_INVALID_ENUM                     1280 	Set when an enumeration parameter is not legal.
        * GL_INVALID_VALUE                    1281 	Set when a value parameter is not legal.
        * GL_INVALID_OPERATION                1282 	Set when the state for a command is not legal for its given parameters.
        * GL_STACK_OVERFLOW                   1283 	Set when a stack pushing operation causes a stack overflow.
        * GL_STACK_UNDERFLOW                  1284 	Set when a stack popping operation occurs while the stack is at its lowest point.
        * GL_OUT_OF_MEMORY                    1285 	Set when a memory allocation operation cannot allocate (enough) memory.
        * GL_INVALID_FRAMEBUFFER_OPERATION    1286 	Set when reading or writing to a framebuffer that is not complete.
        *
        */

        std::string error;
        switch (errorCode)
        {
            case GL_INVALID_ENUM:                  error = "INVALID_ENUM"; break;
            case GL_INVALID_VALUE:                 error = "INVALID_VALUE"; break;
            case GL_INVALID_OPERATION:             error = "INVALID_OPERATION"; break;
            case GL_STACK_OVERFLOW:                error = "STACK_OVERFLOW"; break;
            case GL_STACK_UNDERFLOW:               error = "STACK_UNDERFLOW"; break;
            case GL_OUT_OF_MEMORY:                 error = "OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION: error = "INVALID_FRAMEBUFFER_OPERATION"; break;
        }
        std::cout << error << " | " << file << " (" << line << ")" << std::endl;
    }
    return errorCode;
}

#define glCheckError() glCheckError_(__FILE__, __LINE__)

#endif // RENDERDATA_H
