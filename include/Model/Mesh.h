#ifndef MESH_H
#define MESH_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include "QString"
#include "include/Shader/ShaderProgram.h"
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "include/RenderObject/RenderObject.h"

class SingleMainRenderModel;

struct Vertex {

    glm::vec3       Position;
    glm::vec3       Normal;
    glm::vec2       TexCoords;

};

struct TextureData {

    GLuint          id;
    string          type;
    QString         path;

};

class Mesh : public RenderObject {

public:

    Mesh                    ( vector<Vertex> vertices,
                             vector<GLuint> indices,
                             vector<TextureData> textures );

    void        init        ();
    void        paint       ();

    vector < Vertex >          vertices;
    vector < GLuint >          indices;
    vector < TextureData >     textures;

private:

    SingleMainRenderModel* MainRenderModel;
};

#endif // MESH_H
