#ifndef MODEL_H
#define MODEL_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessery OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <QImage>

//#include <assimp/Importer.hpp>
//#include <assimp/scene.h>
//#include <assimp/postprocess.h>

#include "include/Model/Mesh.h"

#include <QMouseEvent>

class SingleMainRenderModel;

class Model
{
public:
    Model(){
    }
    void            init                    ( GLchar* path );

    void            Draw                    ( );

    glm::mat4       getMModelMatrix         ( ) const;
    void            setMModelMatrix         ( const glm::mat4 &value );

    void            rotateWithMouse         ( QMouseEvent &e,
                                              GLfloat &LastX,
                                              GLfloat &LastY );

private:
    /*  Model Data  */

    /*  Functions   */
    // Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
    void                        loadModel               ( string path );

    // Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
//    void                        processNode             ( aiNode* node,
//                                                          const aiScene* scene );

//    Mesh                        processMesh             ( aiMesh* mesh,
//                                                          const aiScene* scene );

//    // Checks all material textures of a given type and loads the textures if they're not loaded yet.
//    // The required info is returned as a Texture struct.
//    vector<TextureData>         loadMaterialTextures    ( aiMaterial* mat,
//                                                          aiTextureType type,
//                                                          string typeName );

    GLint                       TextureFromFile         ( const char* path,
                                                          string directory );

    vector < Mesh >             meshes;
    string                      directory;
    vector < TextureData >      textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.


    glm::mat4   mModelMatrix;

    float mYaw, mPitch;

    SingleMainRenderModel* MainRenderModel;
};

#endif // MODEL_H
