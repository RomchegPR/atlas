#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <GL/glew.h>

class Texture;

class FrameBuffer
{
public:


    enum AttachmentPoint {
        COLOR0 = GL_COLOR_ATTACHMENT0,
        COLOR1 = GL_COLOR_ATTACHMENT1,
        COLOR2 = GL_COLOR_ATTACHMENT2,
        COLOR3 = GL_COLOR_ATTACHMENT3,
        COLOR4 = GL_COLOR_ATTACHMENT4,
        COLOR5 = GL_COLOR_ATTACHMENT5,
        COLOR6 = GL_COLOR_ATTACHMENT6,
        COLOR7 = GL_COLOR_ATTACHMENT7,
        COLOR8 = GL_COLOR_ATTACHMENT8,
        COLOR9 = GL_COLOR_ATTACHMENT9,
        COLOR10 = GL_COLOR_ATTACHMENT10,
        COLOR11 = GL_COLOR_ATTACHMENT11,
        COLOR12 = GL_COLOR_ATTACHMENT12,
        COLOR13 = GL_COLOR_ATTACHMENT13,
        COLOR14 = GL_COLOR_ATTACHMENT14,
        COLOR15 = GL_COLOR_ATTACHMENT15,
        DEPTH = GL_DEPTH_ATTACHMENT,
        STENCIL = GL_STENCIL_ATTACHMENT,
        DEPTH_AND_STENCIL = GL_DEPTH_STENCIL_ATTACHMENT
      };

    enum Mode {
        MODE_READ = GL_READ_FRAMEBUFFER,
        MODE_DRAW = GL_DRAW_FRAMEBUFFER,
        MODE_READ_DRAW = GL_FRAMEBUFFER
      };

    FrameBuffer();

    bool create();

    void init(Mode);

    bool bind();

    bool unbind();

    void remove();

    void setTexture(AttachmentPoint attachment, Texture texture, GLuint mipMapLevel);

    void checkStatus();

private:

    GLuint mId;
    AttachmentPoint mAttachment;
    Mode mMode;
    bool mOk;

};

#endif // FRAMEBUFFER_H
