#ifndef MYSPLASHSCREEN_H
#define MYSPLASHSCREEN_H

#include <QtWidgets>


class MySplashScreen:public QSplashScreen
{
  Q_OBJECT
  public:
    MySplashScreen():movie(QCoreApplication::applicationDirPath () + "/res/Prop(small).gif")
    {
      setPixmap(QPixmap::fromImage(QImage(QCoreApplication::applicationDirPath ()  + "/res/Prop(small).gif")));
      movie.start();
      connect(&movie,SIGNAL(updated(QRect)),this,SLOT(frameUpdate()));
    }

    ~MySplashScreen()
    {}

    void paintEvent(QPaintEvent* event)
    {

      QPainter painter(this);
      painter.drawPixmap(movie.frameRect(), movie.currentPixmap());

    }

  private slots:
    void frameUpdate()
    {
      setPixmap(movie.currentPixmap());
      update();
    }

  private:
    QMovie movie;

};

#endif // MYSPLASHSCREEN_H
