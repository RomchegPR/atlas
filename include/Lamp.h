#ifndef LAMP_H
#define LAMP_H

#include "RenderObject/RenderObject.h"

class SingleMainRenderModel;
class Camera;
class mainSettingsClass;

class Lamp : public RenderObject
{
public:
    Lamp();

    void    init            ( );
    void    paint           ( );
    void    update          ( );

    void    setLightPos     ( glm::vec3 );

private:

    GLuint      VBO;
    GLuint      lightVAO;

    glm::vec3   lightPos;

    SingleMainRenderModel * MainRenderModel;
    Camera *                mCamera;
    mainSettingsClass *     SettingsClass;
};

#endif // LIGHT_H
