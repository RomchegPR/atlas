#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <QString>

class Shader
{
public:
	enum Type{
		Vertex			= GL_VERTEX_SHADER,
		Fragment		= GL_FRAGMENT_SHADER,
		Pixel			= Fragment,
		Geometry		= GL_GEOMETRY_SHADER,
		TessControl		= GL_TESS_CONTROL_SHADER,
		TessEvaluation	= GL_TESS_EVALUATION_SHADER
	};

	Shader( Type type );
	virtual ~Shader( );

	bool	create				( );
    bool	create				( QString fileName );
	void	remove				( );
    bool	compileSourceFile	( QString fileName );
	bool	isCompiled			( ) const;
	Type	type				( ) const;
	GLuint	id					( ) const;

private:
	GLuint	mID;
	Type	mType;
	bool	mCompiled;
};

#endif // SHADER_H
