#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include <list>
#include <memory>
#include <GL/glew.h>
#include "Shader.h"
#include <boost/shared_ptr.hpp>
#include "glm/matrix.hpp"
#include "list"
#include <vector>

class QMatrix4x4;

/**
@class ShaderProgram

Класс-обертка инкапсулирующий идентификатор шейдерной программы и функции OpenGL для работы с шейдерами GLSL

@brief Класс иcпользуемый для создания и компиляции шейдеров GLSL
 */
class ShaderProgram
{
public:
	ShaderProgram				( );
	virtual ~ShaderProgram		( );

	GLuint	programId			( );
	bool	create				( );
    bool	create				( QString vertexShaderSourceFile, QString fragmentShaderSourceFile );
	void	remove				( );
	void	addShader			( Shader* shader);
    void	detachShader		( Shader* shader );
    void    detachShader        ( );
	bool	link				( );
	bool	isLinked			( );
	void	bind				( );
	void	unbind				( );
	GLint	uniformLocation		( const char* name );
	GLint	attributeLocation	( const char* name );
	void	bindAttribLocation	( const char* name, GLuint location );

	void	setUniformValue		( const char* name, int value );
	void	setUniformValue		( const char* name, float value );
    void	setUniformArrayfv	( const char* name, int size, const float* data );
    void    setUniformArrayf    ( const char* name, int size, std::vector <float> *data );
    void    setUniformArray3f   ( const char* name, glm::vec3 data);
	void	setUniformMatrix	( const char* name, const QMatrix4x4& m );
    void    setUniformMatrix    ( const char* name, const glm::mat4 m );

private:
	GLuint mId;
	/// true если шейдерная программа собрана и готова к использованию
	bool mLinked;

    typedef std::list< boost::shared_ptr<Shader> > ShaderList;
	/// Список внутренних шейдеров создаваемых функцией create, должны быть удалены при удалении программы
	ShaderList mPrivateShaders;
};

#endif // SHADERPROGRAM_H
