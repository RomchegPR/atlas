#ifndef COLORFRAME_H
#define COLORFRAME_H

#include <QFrame>
#include <include/Headers.h>
#include <glm/glm.hpp>

class ColorFrame : public QFrame
{
    Q_OBJECT
public:
    explicit ColorFrame(QObject *parent = 0);
    virtual ~ColorFrame();
    void setColor(glm::vec3 color);
    glm::vec3 getColor();
    void setType(QString type);

signals:
    void ChangeColorFromViewOption(glm::vec3 color, QString type);

protected:
    void mousePressEvent(QMouseEvent *event);

private:
    glm::vec3 mColor;
    QString mType;
};

#endif // COLORFRAME_H
