#ifndef ZONEPARSER_H
#define ZONEPARSER_H
#include "include/Headers.h"

class ZoneParser
{
private:
    QString mFileName;
    QMap<QString, QVector<QMap<QString, QString> > > mOriginalZones;

public:
    ZoneParser();
    virtual ~ZoneParser();
    explicit ZoneParser(QString fileName);
    QMap<QString, QVector<QMap<QString, QString> > >& getZones();
    void readData(QString fileName = "");
    void setFileName(QString fileName);
};

#endif // ZONEPARSER_H
