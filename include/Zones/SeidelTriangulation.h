#ifndef SEIDELTRIANGULATION_H
#define SEIDELTRIANGULATION_H
#include <glm/glm.hpp>
#include <QVector>
#include <GL/glew.h>

struct MonoPolygon
{
    QVector < glm::vec3 >   mPoints;

    GLboolean               mRsweep;
    GLboolean               mLsweep;

    MonoPolygon             (QVector <glm::vec3> points,
                             GLboolean lSweep,
                             GLboolean rSweep);

    MonoPolygon             ()  {}
};

struct sortedPoint
{
    glm::vec3               mP;
    GLint                   index;

    sortedPoint             ()  {};

    sortedPoint             (glm::vec3 Point,
                             GLuint index)
    {
        this->mP = Point;
        this->index = index;
    }
};

void        triangulation           (QVector < glm::vec2 > points,
                                     GLdouble altMin,
                                     GLdouble altMax,
                                     QVector < glm::vec3 > &VertexData);

void        seidelTriangulation     (QVector < glm::vec3 > &points,
                                     GLdouble altMin,
                                     GLdouble altMax,
                                     QVector < glm::vec3 > &vertexData);

bool        pointInPolygon          (QVector < glm::vec3 > a,
                                     glm::vec3 b);

void        fragmentationPolygon    (QVector < glm::vec2 > &originalPolygon,
                                     QVector < MonoPolygon > &MonoPolygons);

bool        IsReflex                (glm::vec2 &p1,
                                     glm::vec2 &p2,
                                     glm::vec2 &p3);

void        setCoorditanes          (QVector < glm::vec3 > &VertexData,
                                     glm::vec3 a,
                                     glm::vec3 b,
                                     glm::vec3 c,
                                     GLdouble altMin,
                                     GLdouble altMax);

#endif // SEIDELTRIANGULATION_H
