#ifndef ZONECONVERTER_H
#define ZONECONVERTER_H
#include "ZoneParser.h"
#include "FlyZone.h"
#include "include/GeoHelper.h"

class ZoneConverter
{
private:
    QMap<QString, QVector<QMap<QString, QString> > > mOriginalZones; // Исходные данные (полученные из парсера)
    QMap<QString, QVector<FlyZone> > mConvertedZones; // Конвертированные данные

public:
    ZoneConverter(ZoneParser& ZP);
    ZoneConverter();
    virtual ~ZoneConverter();
	// Для таких конструкций используйте typedef:
	// typedef QMap<QString, QVector<QMap<QString, QString> > > MyMadnesTemplate;
    void fill(QMap<QString, QVector<QMap<QString, QString> > > &zones); // Заполнение данных и конвертирование исходных данных
    static QList<double> convertAlt(QString& alt); // Конверитрование высот
    static void convertPoints(QVector<ZonePoint> &points, QMap<int, double> &radius);
    void clear();
    void print() const;
    void printZone(QString& name) const;
    void PrintMix(QString& name) const;
    QMap<QString, QVector<FlyZone> >& getZones(); // Такой метод возвращает КОПИЮ структуры, подозреваю что структура не маленькая
};

#endif // ZONECONVERTER_H
