#ifndef FLYZONE_H
#define FLYZONE_H

#include <QVector>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <QString>
#include <QMap>

enum TypesOfPoints
{
    SIMPLE_POINT = 0, CLOCKWISE_ARC_POINT = 1, COUNTER_CLOCKWISE_ARC_POINT = -1, SIMPLE_POINT_ADDED_FOR_TRIANGULATION = 2
};

struct ZonePoint
{
    double          mLatitude;
    double          mLongitude;

    TypesOfPoints   mType;

    ZonePoint       ();
    ZonePoint       (glm::vec3 v, TypesOfPoints type);
    ZonePoint       (glm::vec2 v, TypesOfPoints type);
    bool operator ==(ZonePoint &a)
    {
        return (mLatitude == a.mLatitude) && (mLongitude == a.mLongitude) && (mType == a.mType);
    }

    bool operator !=(ZonePoint &a)
    {
        return !(*this == a);
    }
};

struct FlyZone
{
    QString                     mName;
    QString                     mDescription;

    QVector < ZonePoint >       mPointsGeographical;
    double                      mAltMin;
    double                      mAltMax;

    QMap< int, double >         mRadius; // Map используется для хранения радиусов точек. (В смешанных зонах, могут быть несколько дуг)
};

#endif
