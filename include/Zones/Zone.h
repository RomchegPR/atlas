﻿#ifndef ZONE_H
#define ZONE_H
#include <include/Zones/FlyZone.h>
#include <GL/glew.h>
#include "include/RenderObject/RenderObject.h"
#include "include/Headers.h"
#include "include/Triangulator.h"

class SingleMainRenderModel;
class mainSettingsClass;

class Zone : public RenderObject
{
private:
    glm::vec3               mCentrZone; //Центральная точка для правильной сортировки
    glm::vec4               mColor;

    TypesForZones           mType;      //Определяет тип зоны. RESTRICTED - Зона ограничения, PRIVATE - Запретная зона
    ObjectStatus            Status;
    Trianglator             mTriangulator;

    SingleMainRenderModel   *MainRenderModel;
    mainSettingsClass *     SettingsClass;

    FlyZone                 mInputFlyZone; //It is needed for change scale

public:
    QString                 mName;
    GLdouble                mAltMin, mAltMax;   //Для каждой зоны MAX и MIN высоты
    bool                    mBloor;

    QVector < glm::vec3 >   mPointsGeocentric;            //Для того чтобы не изменять исходный массив (используется для триагуляции)
    QVector < glm::vec2 >   mPointsMercator;

    void                    paint();
    void                    init();
    void                    clear();

private:
    void                    reverseVector               (QVector < glm::vec3 > &vector);            // разворачивает вектор
    void                    reverseVector               (QVector <glm::vec2> &vector);
    bool                    clockWise                   (glm::vec2 a, glm::vec2 b, glm::vec2 c);    // По часовой или против
    GLdouble                triangleArea                (glm::vec2 a, glm::vec2 b, glm::vec2 c);    // Расчитывает направленную площадь треугольника (если возвращает +, то точки стоят против часовой стрелки)
    void                    buildMorePoints             ();
    void                    addPointsInMiddle           (GLint index);
    void                    updatePointsForUpsideAndDownside();
    void                    updateColor                 (RenderData *curVAO,
                                                         elementsToDraw element);
public:
    void                    setRates                    (double newWIDTH_RATE,double newHEIGHT_RATE);

    Zone();
    Zone(FlyZone flyZone);
    virtual                 ~Zone();

    //� v ассчитаные данные для отрисовки
    void                    calcCenter                  ();                     //Функция для расчета центральной точки
    void                    setPoints                   ();                     //Функция для расчета всех точек
    void                    setPointsForSide            ();                 //Для расчета координат боковых граней
    void                    updatePointsForSide         ();
    void                    setPointsForSideLinesType   ();
    void                    setPointsForSideBasicType   ();
    void                    setPointsForUpsideAndDownside();
    void                    concertInputPoints          ();
    void                    convertPointFromMercatorToGeocentric();
    void                    updateBuffers               ();
    void                    calcNormals                 ();
    void                    updatePointsWithNewAlt      ();

    // Функции для работы с данными
    double                  getAltMax                   ();
    double                  getAltMin                   ();
    TypesForZones           getType                     ();
    glm::vec3               getCenter                   ();
    QString                 getTypeName                 ();
    QString                 getName                     ();
    FlyZone                 getFlyZone                  ();
    double                  getPerimeter                ();
    ObjectStatus            getStatus                   () const;
    void                    setName                     (QString name);
    void                    setAltitude                 ();
    void                    setTriangelesForRendering   (glm::vec2 &mercatorPointA, glm::vec2 &mercatorPointB, glm::vec2 &mercatorPointC);
    void                    setType                     (TypesForZones type);
    void                    setColor                    (glm::vec4 color);
    void                    setHeightRate               (float value);
    void                    setFlyZone                  (FlyZone &flyZone);
    void                    setStatus                   (const ObjectStatus &value);

    glm::vec3               convertPoint                (glm::vec2 &point, GLdouble &alt);
    void                    drawTriangle                (QVector < glm::vec3 > vecs);
    glm::vec3               convertToWorld              (glm::vec3 &inputPoint);
};
#endif // ZONE_H
