#ifndef ZONEBUILDER_H
#define ZONEBUILDER_H

#include "include/Headers.h"
#include "include/Zones/Zone.h"
#include "FlyZone.h"
#include <QObject>
#include "include/RenderObject/RenderObject.h"


class ZoneBuilder
{

private:
    glm::vec4                                   mColorForFrame, mColorForZone;
    GLdouble                                    mMaxHeightForDraw, mMinHeightForDraw;

public:
    QVector < Zone > mZones;

    ZoneBuilder();
    virtual             ~ZoneBuilder            ( );
    void                fillData                ( QMap < QString, QVector < FlyZone> > &FlyZones );
    void                init                    ( );
    void                draw                    ( int i );

    QVector < Zone > *  getZones                ( );

    QStringList         getNames                ( );
    GLdouble            getMaxHeightForDraw     ( );
    glm::vec3           getColor                ( GLint id );
    GLdouble            getMinHeightForDraw     ( );

    void                setmMaxHeightForDraw    ( GLdouble height );
    void                setColor                ( GLint id,
                                                  glm::vec3 color );

    void                setMinHeightForDraw     ( GLdouble height );

    void                addCheckedZone          ( QString ZoneName );

    void                setHeightRate           ();
    void                sortZonesBySize         ();

    const static int                            DISTANCE_BETWEEN_POINTS;

    QSet < QString >                            mCheckedZones; // names of Zones, that had been ckicked
    QMap < GLint, glm::vec3 >                   mColorForZones; // Изменяется цвет в зависимости от типа зоны 1 - запретные, 0 - ограничения
    QVector < Zone* >                           mZonesSortedbySize;

public slots:
    void chengedWAR(double value);
};

#endif // ZONEBUILDER_H
