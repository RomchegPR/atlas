#ifndef VIEWOPTION_H
#define VIEWOPTION_H

#include <QWidget>
#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace Ui {
class ViewOption;
}

class mainSettingsClass;
class Camera;
class ViewOption : public QWidget
{
    Q_OBJECT
public:
    explicit ViewOption(Ui::MainWindow* mainWindow);
    ~ViewOption();

private slots:

    void on_EarthTexture_clicked();

    void on_HeighmapTexture_clicked();

    void on_useSettingsButton_clicked();

    void on_OkButton_clicked();



    void on_SliderTrancparencyZones_valueChanged(int value);

    void on_SliderTrancparencyProcedures_valueChanged(int value);

    void on_SliderTrancparencyProceduresLines_valueChanged(int value);

    void on_SliderTrancparencyZonesLines_valueChanged(int value);

    void on_SliderProcedureLineWidthBase_valueChanged(int value);

    void on_SliderProcedureLineWidthTrajectory_valueChanged(int value);

    void on_SliderProcedureLineWidthRams_valueChanged(int value);

    void on_SliderZoneLineWidthTrajectory_valueChanged(int value);


    void on_SliderLightDistKoef_valueChanged(int value);

    void on_SliderLightAmbient_valueChanged(int value);

    void on_SliderLightDiffuse_valueChanged(int value);

    void on_SliderLightSpecular_valueChanged(int value);

    void on_SliderMaterialShine_valueChanged(int value);

    void on_SliderBarrierTrancparency_valueChanged(int value);

    void on_SliderTextSize_valueChanged(int value);

    void on_SliderLegendTrancparency_valueChanged(int value);

    void on_setDefaultButton_clicked();

    void on_EarthGridCheckBox_clicked(bool checked);

    void on_SpinBoxTextSize_valueChanged(int arg1);

    void on_spinBoxLegendTrancparency_valueChanged(int arg1);

    void on_spinBoxLightDistKoef_valueChanged(int arg1);

    void on_doubleSpinBoxLightAmbient_valueChanged(double arg1);

    void on_doubleSpinBoxLightDiffuse_valueChanged(double arg1);

    void on_doubleSpinBoxLightSpecular_valueChanged(double arg1);

    void on_doubleSpinBoxMaterialShine_valueChanged(double arg1);

    void on_spinBoxTrancparencyProcedures_valueChanged(int arg1);

    void on_spinBoxTrancparencyProceduresLines_valueChanged(int arg1);

    void on_spinBoxProcedureLineWidthBase_valueChanged(int arg1);

    void on_spinBoxProcedureLineWidthTrajectory_valueChanged(int arg1);

    void on_spinBoxProcedureLineWidthRams_valueChanged(int arg1);

    void on_spinBoxTrancparencyZones_valueChanged(int arg1);

    void on_spinBoxTrancparencyZonesLines_valueChanged(int arg1);

    void on_spinBoxZoneLineWidthTrajectory_valueChanged(int arg1);

    void on_spinBoxBarrierTrancparency_valueChanged(int arg1);

    void on_spinBoxBarrierSize_valueChanged(int arg1);

private:

    void setDefault();

signals:
    void signalChangeWRL(double );

private:
    Ui::ViewOption *ui;

    SingleMainRenderModel *     mainRenderModel;
    mainSettingsClass *         SettingsClass;
    Camera*                     mCamera;

    bool isFilePathChanged;
};

#endif // VIEWOPTION_H
