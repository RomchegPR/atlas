#ifndef PARSER_H
#define PARSER_H

#include "include/Headers.h"
#include "include/AirPort/WayPoint.h"

using namespace std;

class Parser
{
    QMap <QString, QVector<ProcedureData > > mProcedures; // Распарсенные данные
    QMap <QString, QVector<glm::vec2> > mPoints;
    QString mFileName;
public:
    Parser();
    Parser(QString fileName); // А зачем вы пренебрегаете родителем в этом конструкторе?
    virtual ~Parser();
    void readData(const QString flieName = "");
    QMap <QString, QVector<ProcedureData > >& getPoints();
    void setFileName(QString fileName);
};

#endif // PARSER_H
