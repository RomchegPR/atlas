#ifndef HELPER_H
#define HELPER_H
#include <glm/glm.hpp>
#include <QVector>
#include "GL/glew.h"
#include <include/Headers.h>
#include "include/AirPort/WayPoint.h"

class Helper
{

public:
    struct Line3D{
        double a;
        double b;
        double c;

        double y1;
        double x1;
        double z1;

        Line3D( glm::vec3 start,
                glm::vec3 end);
    };

    Helper(){}
    static double dist      ( glm::vec3 a,
                              glm::vec3 b );

    static double calcLUR   ( Waypoint wp1,
                              Waypoint wp2,
                              Waypoint wp3,
                              QString type );

    static double calcLUR   ( Waypoint wp1,
                              Waypoint wp2,
                              Waypoint wp3,
                              ProcedureType type,
                              double r );

    static double calcR     ( Waypoint wp,
                              double roll = 0 );

    static double calcTAS   ( Waypoint &wp );

    static glm::vec3 CalcPointOnDistance( glm::vec3 start,
                                          glm::vec3 end,
                                          double distance );

    double static DegreeToRadians       ( double degree );
    double static Km_HTokt_h            ( double km_h );

    static double   determinantMat3     ( QVector <glm::vec3> matr ) ;
    static void     debugVec3           ( glm::vec3 tmp );

};


#endif //HELPER_H
