#ifndef HEADERS_H
#define HEADERS_H

#include <GL/glew.h>
#include <QString>
#include <QDebug>
#include "glm/common.hpp"

//for hall
#define RNAV1 1852.0f

#define LINE_WIDTH_RAM 0.15f
static double LINE = 1.0f;
#define LINE_WIDTH 1.0f

enum LinesType{
    LINE_BASE,
    LINE_TRAJECTORY,
    LINE_RAM,
    LINE_ZONE_FRAME
};

enum ProcedureType{
    STAR,
    APRCH,
    DEPARTURE,
    AIRWAY
};

enum ObjectStatus{
    CHECKED,
    UNCHECKED
};

enum ProcedureMode{
    Example
};

enum ObjectType {
    ZoneType,
    HallType,
    BarrierType,
    AirPlaneType
};

enum AirPortType {
    SWO,
    VKO,
    LED,
    AIRWAYS
};

// need to /1000

enum TypesForZones
{
    RESTRICTED,
    PRIVATE,
    RESPONSIBLE
};


enum Trancparency{
    TRANCPARENCY_LINE                = 25,
    TRANCPARENCY_LINE_RAM            = 5,
    TRANCPARENCY_LINE_TRAJECTORY     = 1,
    TRANCPARENCY_LINE_GROUND         = 2,
    TRANCPARENCY_HALL                = 10,
    TRANCPARENCY_ZAPRET_ZONES        = 15,
    TRANCPARENCY_FRAME_ZONES         = 1,
    TRANCPARENCY_GROUND              = 1,
    TRANCPARENCY_FILL_ZONE           = 12,
    TRANCPARENCY_FRAME_ZONE          = 31,
    TRANCPARENCY_LEGEND              = 122
};

enum elementsToDraw{
    HALL                             = 1,
    LINES                            = 2,
    LINES_TRAJECTORY                 = 4,
    LINES_RAM                        = 8,
    LINES_GROUND                     = 16,
    LINES_PROJECTION                 = 32
};

enum TurnTypes{
    FLY_OVER,
    FLY_BY
};

enum ButtonPressedType
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
    STOP
};

typedef unsigned int HallBitmask;


#endif // HEADERS_H
