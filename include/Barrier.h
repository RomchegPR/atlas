#ifndef BARRIER_H
#define BARRIER_H

#include "include/RenderObject/RenderObject.h"
#include "include/AirPort/WayPoint.h"
#include <glm/glm.hpp>

class SingleMainRenderModel;
class mainSettingsClass;

class Barrier : public RenderObject
{
public:

    Barrier();

    void            init                    ();
    void            paint                   ();
    void            clear                   ();

    void            updateColor             ();
    void            update                  ();

    void            calc                    ();
    void            calcNormal              ();

    QString         getName                 () const;
    double          getAltitude             () const;
    glm::vec3       getPosition             () const;
    glm::vec4       getColor                () const;
    glm::vec4       getLightColor           () const;
    float           getHeightRate           () const;
    glm::vec3       getCenter               ();


    void            setName                 ( const QString &value );
    void            setPosition             ( const glm::vec3 &value );
    void            setPosition             ( const Waypoint &value );
    void            setPositionGeo          ( glm::vec3 pos );
    void            setAltitude             ( const double value );
    void            setColor                ( const glm::vec4 &value );
    void            setLightColor           ( const glm::vec4 &value );
    void            setHeightRate           ( float value );

private:

    QString         mName;
    glm::vec3       mPosition;
    glm::vec3       mHeightPoint;
    double          mAltitude;
    glm::vec4       mColor;
    glm::vec4       mLightColor;

    float           mHeightRate;

    double          mCubeSize;

    QVector < glm::vec3 >           mHeightVec;

    SingleMainRenderModel*          MainRenderModel;
    mainSettingsClass *             SettingsClass;


};

#endif // BARRIER_H
