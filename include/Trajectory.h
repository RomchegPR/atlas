#ifndef TRAJECTORY_H
#define TRAJECTORY_H

#include <include/Headers.h>
#include <glm/glm.hpp>

class Trajectory
{
public:

private:
    glm::mat4*              mModel;
    glm::mat4*              mView;
    glm::mat4*              mProj;


    GLuint mVAO, mVBO[2], mShaderProgram, mVertexShader, mFragmentShader, mProjID, mViewID, mModelID;
public:
    QVector < glm::vec4 >   mColors;
    QVector < glm::vec3 >   mCoords;
public:
    Trajectory();
    void print();
    void draw();
    void init(glm::mat4 model, glm::mat4 view, glm::mat4 proj);
    void addNewPoint(glm::vec3 point);
};

#endif // TRAJECTORY_H
