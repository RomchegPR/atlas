#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <QMouseEvent>
#include <include/Headers.h>
#include <include/Ray.h>
#include <QTimer>

/********************************

Class:		Camera

Purpose:	Camera of our world.


********************************/

class QMouseEvent;
class mainSettingsClass;

class Camera : public QObject
{
    Q_OBJECT
private:
    const static int    SMOOTH_ROTATION;
    const static int    SPEED_ROTATION;

    glm::vec3       mEye;       // position of Eye
    glm::vec3       mCenter;    // where the camera looks
    glm::vec3       mCameraUp;  // normal to the camera
    GLdouble        mAngleVerticalCamera;
    GLdouble        mAngleCameraUpNorth;
    GLfloat         mPartOfAngleVertical, mPartOfAngleNorth, mPartofAngleToPointY, mPartofAngleToPointZ;
    glm::vec3       mPointRotateAround;
    glm::vec3       mPointObjectRotateAround;
    glm::vec3       mNorthPole;
    glm::mat4       mAllAxes;

    GLfloat         mCameraSpeed;   // speed of moving
    glm::vec3       mLastPos;
    QTimer          mActionTimer;

    GLboolean       mFlagRotationVertical, mFlagRotationToTheNorth, mFlagRotationToPoint;
    ButtonPressedType mFlagRotationAroundEarth;
    mainSettingsClass *SettingsClass;

public:
    glm::vec3       mAxisRotationAroundEarthX, mAxisRotationAroundEarthY, mAxisRotationAroundEarthZ;                      // Axes rotation around Earth
    glm::vec3       mAxisRotationAroundPointOnEarthX, mAxisRotationAroundPointOnEarthY, mAxisRotationAroundPointOnEarthZ; // Axes rotation around point on the surface of Earth
    GLfloat         mZoomSensitivity, mInclineSensitivity, mSensitivityRotationAroundPointOnEarth, mSensitivityRotationAroundEarth;

private:
    Camera          (QObject *parent = NULL);
    void            rotateCameraAroundEarthAxis(glm::vec3 &Axis1, glm::vec3 &Axis2, glm::vec3 &secondPoint, glm::vec3 &normalizedFirstPoint, glm::vec3 normalizedSecondPoint);
    void            rotateCameraAroundEarthAxis(glm::mat4 RotationAroundAxis);
    GLboolean       isPointInEllipse(GLfloat bigAxle, GLfloat smallAxle, glm::vec3 checkPoint);
    void            rotateAroundPointVertical(GLdouble angle);
    virtual ~Camera         ();

public:

    static Camera* Instance();

    void            rotateWithMouseAroundPoint(GLint &, GLint &);
    void            rotateWithMouseAroundObjectPoint(GLint &xOffset, GLint &yOffset);
    void            checkCameraPosition();
    void            checkAngleBetweenEarthAndCamera(GLfloat &angle);
    void            moveCameraWithKey(int key, float speed);
    void            zoom(int value);
    void            rotateCameraAroundEarth(GLint xOffset, GLint yOffset);
    void            rotateCameraAroundEarth(glm::vec3 &firstPoint);
    void            setDefaultParameters();

    glm::mat4       getLookAt();
    glm::vec3       getEye();
    glm::vec3       getCenter();
    glm::vec3       getCameraUp();
    glm::vec3       getPointRotateAround();
    glm::vec3       getLastPos();
    GLdouble        getVerticalCameraAngle();
    glm::vec3       getAxisRotationAroundEarthZ();

    void            setCameraSpeed(GLfloat x);
    void            setCenter(glm::vec3 center);
    void            setEye(glm::vec3 newEyePosition);
    void            setCameraUp(glm::vec3 newCameraUp);
    void            setPointOfView(glm::vec3 EarthCenter, double Radius);
    void            setPointRotateAround(glm::vec3 &newPoint);
    void            setPointObjectRotateAround(glm::vec3 &newPoint);
    void            setLastPos(glm::vec3 &value);
    static          void setSPEED_MOVING(float value);
    static          void setSPEED_ROTATION(int value);
    static          void setSMOOTH_ROTATION(int value);
    void            setVerticalCameraAngle(const GLdouble &value);
    void            rotateCameraUpRight();
    void            startRotationToTheNorthPole();
    void            startRotationToVerticalView();
    void            startRotationToPoint(glm::vec3 &point);
    void            startRotationAroundEarth(ButtonPressedType button);
    void            rotationToTheNorthPole();
    void            rotationToVerticalView();
    void            rotationToPoint();
    void            stopAllMoves();

public slots:
    void            allRotationWithTimer();
};

#endif // CAMERA_H
