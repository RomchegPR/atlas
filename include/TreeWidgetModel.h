#ifndef TREEWIDGETMODEL_H
#define TREEWIDGETMODEL_H
#include <QTreeWidget>
#include "include/Headers.h"

class AirPort;
class FlyZone;
class SingleMainRenderModel;

class TreeWidgetModel
{
public:
    TreeWidgetModel();


    void                    linkWidget          ( QTreeWidget *Widget );
    void                    fillWithData        ( QTreeWidget *pWidget );

    void                    addChild            (QTreeWidgetItem *parent,
                                                 QTreeWidgetItem *child );

    QTreeWidgetItem *       createChild         ( QString Name1,
                                                  QString Name2,
                                                  QString toolTip );


    void                    itemChange          ( QTreeWidgetItem* item );

    void                    addAirPort          (AirPortType airPortType,
                                                 QTreeWidget *pWidget);


    SingleMainRenderModel *     mainRenderModel;

    QTreeWidget *               pWidget;


};

#endif // TREEWIDGETMODEL_H
