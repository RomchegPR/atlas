#ifndef RAY_H
#define RAY_H

#include <glm/glm.hpp>
#include <QVector>
#include "GL/glew.h"
#include "include/Zones/ZoneBuilder.h"
#include "include/AirPort/Hall.h"
#include "include/Barrier.h"

class SingleMainRenderModel;
class AirPlane;

class Ray
{
public:
    glm::vec3               mFirstPoint;
    glm::vec3               mSecondPoint;

private:
    QVector < QString >     mPossibleZone;
    Zone*                   mClickedZone;

    QString                 mPossibleProcedure;
    Hall*                   mClickedHall;

    QString                 mPossibleBarrier;
    Barrier*                mClickedBarrier;

    QString                 mPossibleAirPlane;
    AirPlane*               mClickedAirPlane;

    SingleMainRenderModel* MainRenderModel;

private:
    GLboolean              checkProcedureTriangle(Hall hall, int i1, int i2, int i3);
    GLboolean              hasIntersection(int i1, int i2, int i3, glm::vec3& point, QVector <glm::vec3> &restrictedData);

public:
    Ray();

    void                    printPoints();

    //Методы для работы с лучом

    float                   planeDistance(glm::vec3 normal, glm::vec3 point);
    GLboolean               intersectedPlane(QVector <glm::vec3> &poly, glm::vec3 &normal, float &originDistance);
    double                  angleBetweenVectors(glm::vec3 &vector1, glm::vec3 &vector2);
    glm::vec3               intersectionPoint(glm::vec3 normal, double distance);
    GLboolean               insidePolygon(glm::vec3 vintersection, QVector <glm::vec3> poly, long verticeCount);
    GLboolean               intersectedPolygon(QVector <glm::vec3> &poly, int verticeCount);
    void                    createRay(GLuint mouseX, GLuint mouseY, GLint widgetW, GLint widgetH);
    void                    checkClickObject( QSet < QString > *NameObj);

    QVector < QString >     getPossibleZones();
    QString                 getPossibleProcedures();
    QString                 getPossibleBarrier();
    QString                 getPossibleAirPlane();

    Zone*                   getClickedZones();
    Hall*                   getClickedHall();
    Barrier*                getClickedBarrier();
    AirPlane*               getClickedAirPlane();


};

#endif // RAY_H
