#ifndef EARTHPART_H
#define EARTHPART_H

#include <glm/glm.hpp>
#include <GL/glew.h>
#include "include/RenderObject/RenderObject.h"
#include <QtCore/QThread>

class SingleMainRenderModel;
class mainSettingsClass;

class EarthPart : public RenderObject{

public:

    EarthPart();

    void            init                ( );
    void            paint               ( );
    void            clear               ( );

    void            calc                ( );

    void            setEarthRenderData  ( RenderData *data );
    void            setCenter           ( glm::vec3 *center );
    void            setRadiusEarth      ( double *value );
    void            setRateCount        ( int count );
    void            setImage            ( QString path );
    void            setModel            ( glm::mat4 *model );
    void            setPartCoords       ( float startLat,
                                          float startLon,
                                          float finishLat,
                                          float finishLon );
    void            setLevel            ( float level );
    void            setVisible          ( bool value );

    glm::vec3       getPartCenter       ();

public:

    float           mLongit;
    float           mLatit;
    float           mFinishLongit;
    float           mFinishLatit;

    QString         PathToImage;


    bool getIsTextureFound() const;
    void setIsTextureFound(bool value);

private:

    double*         mRadiusEarth;
    int             mRateCount;
    glm::vec3*      mEarthCenter;
    RenderData*     pEarthRenderData;


    glm::mat4*      pModelMatrix;

    glm::vec3       mCenter;

    float           mLevel;
    bool            isVisible;
    bool            isTextureFound;

    Texture          mHeightMap;

    SingleMainRenderModel*  MainRenderData;
    mainSettingsClass *     SettingsClass;

};


#endif // EARTHPART_H
