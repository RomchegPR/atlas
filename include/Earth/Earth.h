#ifndef EARTH_H
#define EARTH_H

#include <glm/glm.hpp>
#include <GL/glew.h>
#include "QString"
#include "include/Camera.h"
#include "EarthPart.h"
#include "include/Shader/ShaderProgram.h"
#include "include/Ray.h"
#include <QStringList>
#include <QThread>
using namespace std;

class SingleMainRenderModel;
class mainSettingsClass;

/********************************

Class:		Earth

Purpose:	to coorect usage need to:

            1) link shader (setShader)
            2) set ViewMatrix (setViewMatrix), ProjectionMatrix
            3) set Earth Image;
            4) createEarth()
            5) init
            6) draw

********************************/

#define MAX_ALTITUDO 8848
#define MIN_ALTITUDO 0

class Earth : public RenderObject
{

public:

    Earth();

    void                    createEarth                 ( );
    void                    createEarthPart             ( float Startlat,
                                                          float Startlon,
                                                          float Endlat,
                                                          float Endlon,
                                                          QString PathToImage,
                                                          float level );

    void                    checkEarthPart              ( );
    void                    hideEarthPart               ( QString PathToImage );

    void                    init                        ( );
    void                    paint                       ( );

    bool                    intersectWithRay            ( Ray &Ray );

    void                    clear                       ( );

    void                    updateEarthTextures         ( );
    void                    preloadParts                ( );

    void                    setCamera                   ( Camera &camera );

    void                    setEarthRadius              ( const int &radius );
    void                    setEarthCenter              ( const glm::vec3 &center );
    void                    setColor                    ( glm::vec4 color );
    void                    setHeightRate               ( float rate );

    double                  getRadius                   ( );
    glm::vec3               getCenter                   ( );
    glm::vec3               getIntersectPointWithRay    ( );

private:

    int                     mRateCount;                 // number of Parts
    int                     mDistToReDraw;
    double                  mRadiusEarth;
    glm::vec3               mEarthCenter;
    glm::vec3               mLastCameraPos;

//    RenderObject            mRenderObject;
    glm::vec4               mColor;

    QVector <EarthPart>     mEarthParts;

    glm::vec3               mIntersectedPoint;          // for Mouse middle button  - rotation

    SingleMainRenderModel*  MainRenderData;
    Camera*                 mCamera;
    mainSettingsClass *     SettingsClass;

    EarthPart               moscowPart;

    QStringList             mEarthPartsFiles;
    QString                 mEarthPartsDir;

    Texture                 heightmap, heightmap1;
    float                   mHeightRate;
    bool                    mAllPartsVisible;

};

#endif // EARTH_H
