#ifndef GEOHELPER_H
#define GEOHELPER_H
#include "Headers.h"
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <QString>
#include <GeographicLib/Geocentric.hpp>
#include <GeographicLib/TransverseMercator.hpp>
#include <GeographicLib/Gnomonic.hpp>
#include <GeographicLib/Geodesic.hpp>

class GeoHelper{
public:
    static const float FT_2_MT;
    static const float MT_2_FT;
    static const float DEG_2_RAD;
    static const float RAD_2_DEG;
    static const float MAJOR_AXIS;
    static const float MINOR_AXIS;
    static const float FLATTENING;
    static const float ECCENTRICITY;
    static const float MINUTE_2_SECONDS;
    static const float HOUR_2_SECONDS;
    static const float FLATTENING_INV;
    static const float FLATTEINING_INV_SQUARE;
    static const float LON_0;
    static const float LAT_0;
    static const GeographicLib::Geocentric GEOCENTRIC;
    static const GeographicLib::TransverseMercator MERCATOR;
    static const GeographicLib::Geodesic GEODESIC;
    static const GeographicLib::Gnomonic GNOMONIC;


private:

public:
    GeoHelper();
    virtual ~GeoHelper();
    static bool compare(double x, double y);
    static float dms2deg(float degrees, float minutes, float seconds);
    static float dms2deg(const QString& value);
    static float deg2rad(float degree);
    static float deg2rad(const QString& degree);
    static float rad2deg(float rad);
    static float dms2rad(float degree);
    static float dms2rad(float degrees, float minutes, float seconds);
    static float dms2rad(const QString& value);
    static float dms2deg(float degree);
    static QString deg2dms(float degrees);
    static float ft2metres(float val);
    static float ft2metres(QString& val);
    static glm::vec3 geo2cartesian(QString& lat, QString& lon, bool isRad = false);
    static glm::vec3 geo2cartesian(glm::vec2 point, bool isRad = false);
    static glm::vec3 geo2cartesian(glm::vec3 point, bool isRad = false);
    static glm::vec3 geo2cartesianGL(glm::vec3 point, bool isRad);
    static glm::vec2 cartesian2geo(glm::vec3 point);
    static double heightAboveSeaLevel(glm::vec3 &point);
    static glm::vec2 geo2mercator(const glm::vec2 &point);
    static glm::vec2 mercator2geo(const glm::vec2 &point);
    static glm::vec3 mercator2cartesian(const glm::vec2 &point);
    static glm::vec3 mercator2cartesian(const glm::vec2 &point, float alt);
    static glm::vec2 gnomonic2geo(const glm::vec2 &point);
    static glm::vec2 geo2gnomonic(const glm::vec2 &point);
    static glm::vec2 directionProblem(glm::vec2 point, double angle, double dist);
    static QString rad2dms(QString value);
    static QString formatStringDMS(QString value);
    static float inverseProblem(double x1, double y1, double x2, double y2, double &azimuth);

private:
    static void sphereDirect(double pt1[], double azi, double dist, double pt2[]);
    static void sphereInverse(glm::vec2 &pt1, glm::vec2 &pt2, double &azi, double &dist);
    static float cartToSpher(glm::vec3 &pt1, glm::vec2 &pt2);
    static void rotate(glm::vec3 &pt, double a, int i);
    static void spherToCart(glm::vec3 &pt1, glm::vec2 &pt2);
    static float geocentic2geodetic(glm::vec2 point);
    static float radiusEarth(float lat);
    static glm::vec3 currentRadius(float val);

};

#endif // GEOHELPER_H
