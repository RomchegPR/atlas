#ifndef CUBEMAP_H
#define CUBEMAP_H
#include "RenderObject/RenderObject.h"
#include <vector>

class SingleMainRenderModel;

class CubeMap : public RenderObject
{
private:

    double                              mSize;

    std::vector<QString>                mFaces;
    SingleMainRenderModel *             MainRenderModel;


    void        loadCubemap ( );

public:
    CubeMap                 ( );

    void        init        ( );
    void        paint       ( );
    void        clear       ( );
};

#endif // CUBEMAP_H
