#include "AirPlane.h"
#include "include/Helper.h"
#include "include/GeoHelper.h"
#include "MainRenderModel.h"
#include <MainSettingsClass.h>

AirPlane::AirPlane()
{
    Color = glm::vec4(1, 0.5f, 0, 1);
    Size = 800.0f;
    TrackSize = 5;

    MainRenderModel = SingleMainRenderModel::Instance();
    SettingsClass = mainSettingsClass::Instance();

    CubeVertexes << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3( -Size,  Size, -Size)
                 << glm::vec3( -Size, -Size, -Size)

                 << glm::vec3( -Size, -Size,  Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3( -Size,  Size,  Size)
                 << glm::vec3( -Size, -Size,  Size)

                 << glm::vec3( -Size,  Size,  Size)
                 << glm::vec3( -Size,  Size, -Size)
                 << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3( -Size, -Size,  Size)
                 << glm::vec3( -Size,  Size,  Size)

                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)

                 << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3( -Size, -Size,  Size)
                 << glm::vec3( -Size, -Size, -Size)

                 << glm::vec3( -Size,  Size, -Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3( -Size,  Size,  Size)
                 << glm::vec3( -Size,  Size, -Size);

    Size = Size / 2;

    CubeVertexes2Step << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3( -Size,  Size, -Size)
                 << glm::vec3( -Size, -Size, -Size)

                 << glm::vec3( -Size, -Size,  Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3( -Size,  Size,  Size)
                 << glm::vec3( -Size, -Size,  Size)

                 << glm::vec3( -Size,  Size,  Size)
                 << glm::vec3( -Size,  Size, -Size)
                 << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3( -Size, -Size,  Size)
                 << glm::vec3( -Size,  Size,  Size)

                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)

                 << glm::vec3( -Size, -Size, -Size)
                 << glm::vec3(  Size, -Size, -Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3(  Size, -Size,  Size)
                 << glm::vec3( -Size, -Size,  Size)
                 << glm::vec3( -Size, -Size, -Size)

                 << glm::vec3( -Size,  Size, -Size)
                 << glm::vec3(  Size,  Size, -Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3(  Size,  Size,  Size)
                 << glm::vec3( -Size,  Size,  Size)
                 << glm::vec3( -Size,  Size, -Size);
}

void AirPlane::addTrackPoint(glm::vec2 geoPos, double alt){

    if (Track.size() > 0 && isSamePoint(geoPos, Track.last().PositionGeo))
        return;

    TrackPoint newPoint;
    glm::vec3 HeightPoint = GeoHelper::geo2cartesianGL(glm::vec3(geoPos, SettingsClass->getHeightRate() * alt), true);

    newPoint.PositionGeo = geoPos;
    newPoint.Position = HeightPoint;
    newPoint.Altitude = alt;


    Track.push_back(newPoint);

    if (Track.size() > TrackSize ){
                Track.pop_front();
    }
}

bool AirPlane::isSamePoint(glm::vec2 geoPos1, glm::vec2 geoPos2){
    return (geoPos1.x == geoPos2.x && geoPos1.y == geoPos2.y);
}

void AirPlane::addTrackPoint(double lan, double lon, double alt){
    addTrackPoint(glm::vec2(lan, lon), alt);
}

void AirPlane::calcLine(){

    glm::vec3 HeightPoint = Track.last().Position;
    glm::vec3 GroundPoint = GeoHelper::geo2cartesianGL(glm::vec3(Track.last().PositionGeo, 0), true);

    if (mRenderData[LinesType].mVertex.size() == 0){
        mRenderData[LinesType].mVertex.push_back(HeightPoint);
        mRenderData[LinesType].mVertex.push_back(GroundPoint);
        mRenderData[LinesType].mIndices.push_back(0); mRenderData[LinesType].mIndices.push_back(1);
    } else {
        mRenderData[LinesType].mVertex[0] = HeightPoint;
        mRenderData[LinesType].mVertex[1] = GroundPoint;
    }
}

void AirPlane::init(){

    mRenderData[LinesType].mVAO.create();
    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mElementBuffer.init(VertexBuffer::ElementArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mElementBuffer.create();
    mRenderData[LinesType].mElementBuffer.bind();
    mRenderData[LinesType].mElementBuffer.setData(mRenderData[LinesType].mIndices.size() * sizeof(GLuint), &mRenderData[LinesType].mIndices.front());

    mRenderData[LinesType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mVertexBuffer.create();
    mRenderData[LinesType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[LinesType].mColorBuffer.create();
    mRenderData[LinesType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    mRenderData[LinesType].mColorBuffer.setData(sizeof(glm::vec4), &Color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    mRenderData[LinesType].mVAO.unbind();

/////////////////////////////////////////////////////////////////////////////////////////////////////////

    QVector < glm::vec3 > trackCubes;

    for (int i = 0; i < Track.size() * 36; i++){
        trackCubes.push_back(glm::vec3(0));
    }

    for (int i = 0; i < Track.size(); i++){

        glm::mat4 translate = glm::translate(glm::mat4(1.f), Track.at(i).Position );

        for (int i = 0; i < CubeVertexes.size(); i++){
            trackCubes[i] =  glm::vec3(translate * glm::vec4(CubeVertexes[i], 1.0f));
        }
    }

    mRenderData[BasicType].mVertex = trackCubes;

    calcNormal();

    mRenderData[BasicType].mVAO.create();
    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*) 0);

    mRenderData[BasicType].mColorBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mColorBuffer.create();
    mRenderData[BasicType].mColorBuffer.bind();
    glEnableVertexAttribArray(1);
    mRenderData[BasicType].mColorBuffer.setData(sizeof(glm::vec4), &Color);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
    glVertexAttribDivisor(1, 2);

    mRenderData[BasicType].mVertexBuffer.init(VertexBuffer::VertexArray, VertexBuffer::StaticDraw);
    mRenderData[BasicType].mVertexBuffer.create();
    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(3);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mNormal.size() * sizeof(glm::vec3), &mRenderData[BasicType].mNormal.front());
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[BasicType].mVAO.unbind();
}

void AirPlane::calcNormal(){
    for (int i = 0 ; i < mRenderData[BasicType].mVertex.size(); i++){
        mRenderData[BasicType].mNormal.push_back(glm::vec3(0));
    }

    for ( int i = 0 ; i < mRenderData[BasicType].mVertex.size() ; i += 3) {

        int Index0 = i;
        int Index1 = i + 1;
        int Index2 = i + 2;

        glm::vec3 v1 = mRenderData[BasicType].mVertex[Index1] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 v2 = mRenderData[BasicType].mVertex[Index2] - mRenderData[BasicType].mVertex[Index0];
        glm::vec3 Normal = glm::cross(v1,v2);

        glm::normalize(Normal);

        mRenderData[BasicType].mNormal[Index0] += Normal;
        mRenderData[BasicType].mNormal[Index1] += Normal;
        mRenderData[BasicType].mNormal[Index2] += Normal;
    }
}

void AirPlane::update(){

    mRenderData[LinesType].mVAO.bind();

    mRenderData[LinesType].mElementBuffer.bind();
    mRenderData[LinesType].mElementBuffer.setData(mRenderData[LinesType].mIndices.size() * sizeof(GLuint), &mRenderData[LinesType].mIndices.front());

    mRenderData[LinesType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[LinesType].mVertexBuffer.setData(mRenderData[LinesType].mVertex.size() * sizeof(glm::vec3), &mRenderData[LinesType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    mRenderData[LinesType].mVAO.unbind();

/////////////////////////////////////////////////////////////////////////////////////////////////////////

    QVector < glm::vec3 > trackCubes;

    for (int i = 0; i < Track.size() * 36; i++){
        trackCubes.push_back(glm::vec3(0));
    }

    for (int i = 0; i < Track.size(); i++){

        glm::mat4 translate = glm::translate(glm::mat4(1.f), Track.at(i).Position );

        for (int j = 0; j < CubeVertexes.size(); j++){
            if (i == Track.size() - 1){
                trackCubes[i*CubeVertexes.size() + j] = glm::vec3(translate * glm::vec4(CubeVertexes[j], 1.0f));
            } else {
                trackCubes[i*CubeVertexes2Step.size() + j] = glm::vec3(translate * glm::vec4(CubeVertexes2Step[j], 1.0f));
            }
        }
    }

    mRenderData[BasicType].mVertex.clear();
    mRenderData[BasicType].mVertex = trackCubes;

//    calcNormal();

    mRenderData[BasicType].mVAO.bind();

    mRenderData[BasicType].mVertexBuffer.bind();
    glEnableVertexAttribArray(0);
    mRenderData[BasicType].mVertexBuffer.setData(mRenderData[BasicType].mVertex.size() * sizeof(glm::vec3), &mRenderData[BasicType].mVertex.front());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*) 0);

    mRenderData[BasicType].mVAO.unbind();
}

void AirPlane::paint(){

    mRenderData[BasicType].mVAO.bind();
    glDrawArrays(GL_TRIANGLES, 0, 36 * TrackSize);
    mRenderData[BasicType].mVAO.unbind();

    mRenderData[LinesType].mVAO.bind();
    glDrawElements(GL_LINES, mRenderData[LinesType].mIndices.size(), GL_UNSIGNED_INT, 0);
    mRenderData[LinesType].mVAO.unbind();

}

void AirPlane::clear()
{

}

QString AirPlane::getName() const
{
    return Name;
}

void AirPlane::setName(const QString &value)
{
    Name = value;
}

double AirPlane::getCourse() const
{
    return Course;
}

void AirPlane::setCourse(double value)
{
    Course = value;
}

double  AirPlane::getAltitude() const
{
    return Track.last().Altitude;
}

double AirPlane::getSize() const
{
    return Size;
}

void AirPlane::setSize(double value)
{
    Size = value;
}

glm::vec4 AirPlane::getColor() const
{
    return Color;
}

void AirPlane::setColor(const glm::vec4 &value)
{
    Color = value;
}

glm::vec3 AirPlane::getCurCoord3D() const{
    return Track.last().Position;
}
