#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "include/ViewOption.h"
#include "include/Parser/Parser.h"
#include "include/Converter/Converter.h"
#include "include/Parser/ZoneParser.h"
#include "include/Converter/ZoneConverter.h"
#include <QTextCodec>
#include <QDebug>
#include "MainSettingsClass.h"
#include <QtConcurrent>
#include "CSVReader/csvreader.h"
#include "QPropertyAnimation"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mViewOptionWindow(NULL)
{

}

MainWidget *MainWindow::getMainWidget( ){
    return ui->VisualWidget;
}

void MainWindow::init(){
    QTime time;
    time.start();
    ui->setupUi(this);

    ui->treeWidget->setSortingEnabled(true);
    ui->treeWidget->setRootIsDecorated(true);

    ui->VisualWidget->setFocus();

    ui->CameraSpeedSlider->setMaximum(20);
    ui->CameraSpeedSlider->setMinimum(1);
    ui->CameraSpeedSlider->setValue(10);

    ui->ScaleSliderH->setMaximum(15);
    ui->ScaleSliderH->setMinimum(1);
    ui->ScaleSliderH->setValue(10);

    ui->label_5->setText(QString::number(ui->ScaleSliderH->value()));
    ui->CurrentObjectoptions->hide();

   // mOnClickWindow = new ChooseObjectOnClick();

    connect(ui->VisualWidget, SIGNAL(initializeGLisComplete()), this, SLOT(MainWidgetInitializeGLisComplete()));
    connect(ui->VisualWidget, SIGNAL(procedureChoosed(QSet < QString >)), this, SLOT(choosedProcedure(QSet < QString >)));

    QFile widgetStyle;
    widgetStyle.setFileName(":/qdarkstyle/style.qss");
    widgetStyle.open(QFile::ReadOnly);
    mStyleQSS = widgetStyle.readAll();

    this->setStyleSheet(mStyleQSS);

    SettingsClass = mainSettingsClass::Instance();
    mainRenderModel->mTextureEarthFileName = QCoreApplication::applicationDirPath () + "/res/Earth.bmp";
    mainRenderModel->mTextureHeightMapFileName = QCoreApplication::applicationDirPath () + "/res/grayEarth21.bmp";

    mCamera = Camera::Instance();
    isShowOnlyChoosed = false;
}

void MainWindow::fillData(){

    CSVReader csvReader;

    QString fileName = QCoreApplication::applicationDirPath () + "/CSVReader/csv.csv";

    qDebug() << fileName;
    QVector < QVector<Waypoint> > WPs = csvReader.readWayPoints(fileName);

    mainRenderModel->mAirPort[SWO];
    mainRenderModel->mAirPort[SWO] = AirPort();
    mainRenderModel->mAirPort[SWO].setName("SWO");

    Parser mParser;
    QString dir = QCoreApplication::applicationDirPath();
    mParser.readData(dir + "/res/svo1");
    Converter mConverter;
    mConverter.fill(mParser.getPoints());
    mConverter.convert();

    QMap <QString, QVector<QVector <Waypoint> > > airPoints;
    airPoints = mConverter.getStruct();

    mainRenderModel->mAirPort[SWO].fillData(airPoints);

//    mainRenderModel->mAirPort[AIRWAYS];
//    mainRenderModel->mAirPort[AIRWAYS] = AirPort();
//    mainRenderModel->mAirPort[AIRWAYS].setName("AIRWAY");

//    QMap<QString, QVector<QVector<Waypoint> > > tt;

//    tt["SID"].append(WPs);

//    mainRenderModel->mAirPort[AIRWAYS].fillData(tt);





//    mParserVKO.readData(dir + "/res/vko");
//    mConverterVKO.fill(mParserVKO.getPoints());
//    mConverterVKO.convert();
//    airPoints = mConverterVKO.getStruct();
//    mainRenderModel->mAirPort[VKO].fillData(airPoints);




    mainRenderModel->mAirPort[LED];
    mainRenderModel->mAirPort[LED] = AirPort();
    mainRenderModel->mAirPort[LED].setName("LED");


    Parser mParserLED;
    Converter mConverterLED;
    mParserLED.readData(dir + "/res/led");
    mConverterLED.fill(mParserLED.getPoints());
    mConverterLED.convert();
    airPoints = mConverterLED.getStruct();
    mainRenderModel->mAirPort[LED].fillData(airPoints);


//     Zapret Zones

    ZoneParser parser(dir + "/res/zonesSvo");
    parser.readData();
    ZoneConverter converter(parser);

    mainRenderModel->mZoneBuilder.fillData(converter.getZones());

    mTreeWidgetModel->fillWithData(ui->treeWidget);

}

void MainWindow::addAirport(QString filePath){
    QString name = filePath.right(filePath.lastIndexOf("/"));

    qDebug() << name;

    AirPortType airPortType;

    if (name == "led") {
        airPortType = LED;
    } else if (name == "svo1") {
        airPortType = SWO;
    }

    mainRenderModel->mAirPort[airPortType];
    mainRenderModel->mAirPort[airPortType] = AirPort();
    mainRenderModel->mAirPort[airPortType].setName(name);

    Parser mParser;
    mParser.readData(filePath);
    Converter mConverter;
    mConverter.fill(mParser.getPoints());
    mConverter.convert();

    QMap <QString, QVector<QVector <Waypoint> > > airPoints;
    airPoints = mConverter.getStruct();
    mainRenderModel->mAirPort[airPortType].fillData(airPoints);

    mTreeWidgetModel->addAirPort(airPortType, ui->treeWidget);
}

void MainWindow::MainWidgetInitializeGLisComplete(){

    QStringList StringsForSearch;

    for(QMap<AirPortType, AirPort>::Iterator it =  mainRenderModel->mAirPort.begin(); it != mainRenderModel->mAirPort.end(); it++){
         AirPort *temp = &it.value();

         QStringList StringsForSearchAirport;

         StringsForSearchAirport = temp->getProcedureNames();

         for (int i = 0; i < StringsForSearchAirport.size(); i++){
             StringsForSearch.push_back(StringsForSearchAirport[i]);
         }

    }

    QStringList StringsForSearchZones;
    StringsForSearchZones = mainRenderModel->mZoneBuilder.getNames();

    for (int i = 0; i < StringsForSearchZones.size(); i++){
        StringsForSearch.push_back(StringsForSearchZones[i]);
    }

    mStringSearch = new QCompleter(StringsForSearch, this);
    mStringSearch->setCaseSensitivity(Qt::CaseInsensitive);
    ui->SearchLine->setCompleter(mStringSearch);

    mStringSearch->popup()->setObjectName("MyCompleterList");

    QString qss = QString("QAbstractItemView[objectName=") + QString("MyCompleterList") + QString("] {")
            + "background-color: #232629;"
            + "border-style: solid;"
            + "border: 1px solid #76797C;"
            + "border-radius: 2px;"
            + "color: #eff0f1;}";

    mStringSearch->popup()->setStyleSheet(qss);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_treeWidget_itemChanged(QTreeWidgetItem *item, int column)
{
    mTreeWidgetModel->itemChange(item);

    ui->VisualWidget->DrawObj(item->text(0).toUtf8());
//    ui->VisualWidget->setFocus();
}

void MainWindow::on_SearchLine_returnPressed(){
    ui->VisualWidget->setFocus();
}

void MainWindow::on_FindButton_clicked()
{
    QString SearchText = ui->SearchLine->text();

    ui->VisualWidget->rotationToObject(SearchText);

    QList < QTreeWidgetItem* > clist = ui->treeWidget->findItems(QString ( SearchText ), Qt::MatchContains|Qt::MatchRecursive, 0);

    foreach(QTreeWidgetItem* item, clist)
    {
        item->setCheckState(0, Qt::Checked);
    }

    ui->VisualWidget->setFocus();

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (mViewOptionWindow != NULL) {
        mViewOptionWindow->close();
    }
}

void MainWindow::on_ScaleSliderH_sliderReleased()
{
    float rate = ui->ScaleSliderH->value();
    SettingsClass->setHeightRate(rate);

    ui->label_5->setText(QString::number((int)rate));

    for(QMap<AirPortType, AirPort>::Iterator it =  mainRenderModel->mAirPort.begin(); it != mainRenderModel->mAirPort.end(); it++){

        AirPort *temp = &it.value();

        temp->setHeightRate(rate);

    }
    mainRenderModel->mZoneBuilder.setHeightRate();

    for (int i = 0; i < mainRenderModel->mBarriers.size(); i++){
        mainRenderModel->mBarriers[i].setHeightRate(rate);
    }

//    mainRenderModel->mEarth.setHeightRate(rate);

    ui->VisualWidget->setFocus();
}

void MainWindow::choosedProcedure(QSet < QString > setName){
    if (setName.size() == 0) {
        ui->CurrentObjectoptions->hide();
    } else
    {

        mChoosedProcedures = setName;

        HallBitmask mask;

        for(QMap<AirPortType, AirPort>::Iterator it =  mainRenderModel->mAirPort.begin(); it != mainRenderModel->mAirPort.end(); it++){
             AirPort *temp = &it.value();

             for (int i = 0; i < temp->mProcedures.size(); i++){

                 if (setName.contains(temp->mProcedures[i].getName())) {
                    mask = temp->mProcedures[i].getDrawMask();
                    break;
                 }
             }
        }

        if (mask && HALL){
            ui->FillCheckBoxCurrent->setChecked(mask & HALL);
        }

        if (mask && LINES){
            ui->RamCheckBoxCurrent->setChecked(mask & LINES);
        }

        if (mask && LINES_TRAJECTORY){
            ui->TrajectoryCurrent->setChecked(mask & LINES_TRAJECTORY);
        }

        if (mask && LINES_GROUND){
            ui->ProjectionCheckBoxCurrent->setChecked(mask & LINES_GROUND);
        }

        if (mask && LINES_PROJECTION){
            ui->NormalCheckBoxCurren->setChecked(mask & LINES_PROJECTION);
        }

        ui->CurrentObjectoptions->show();
//        ui->ChoosedObjectName->setText(name.left(10));
    }
}

//airports

void MainWindow::on_FillCheckBox_clicked()
{
    setDrawMaskForAirport(HALL);

    ui->FillCheckBoxCurrent->setChecked(ui->FillCheckBox->isChecked());

    updateChecksForCurrent();

}

void MainWindow::on_NormalCheckBox_clicked()
{
    setDrawMaskForAirport(LINES_PROJECTION);

    ui->NormalCheckBoxCurren->setChecked(ui->NormalCheckBox->isChecked());

    updateChecksForCurrent();

}

void MainWindow::on_ProjectionCheckBox_clicked()
{
    setDrawMaskForAirport(LINES_GROUND);

    ui->ProjectionCheckBoxCurrent->setChecked(ui->ProjectionCheckBox->isChecked());

    updateChecksForCurrent();

}

void MainWindow::on_RamCheckBox_clicked()
{
    setDrawMaskForAirport(LINES);

    ui->RamCheckBoxCurrent->setChecked(ui->RamCheckBox->isChecked());

    updateChecksForCurrent();

}

void MainWindow::on_pointsCheckBox_clicked()
{
    ui->VisualWidget->setShowPoints(ui->pointsCheckBox->isChecked());
    updateChecksForCurrent();
}


void MainWindow::on_Trajectory_clicked()
{
    setDrawMaskForAirport(LINES_TRAJECTORY);

    ui->TrajectoryCurrent->setChecked(ui->Trajectory->isChecked());

    updateChecksForCurrent();

}

//currents

void MainWindow::updateChecksForCurrent(){
    ui->FillCheckBoxCurrent->setChecked(ui->FillCheckBox->isChecked());
    ui->ProjectionCheckBoxCurrent->setChecked(ui->ProjectionCheckBox->isChecked());
    ui->NormalCheckBoxCurren->setChecked(ui->NormalCheckBox->isChecked());
    ui->RamCheckBoxCurrent->setChecked(ui->RamCheckBox->isChecked());
    ui->TrajectoryCurrent->setChecked(ui->Trajectory->isChecked());
}

void MainWindow::on_FillCheckBoxCurrent_clicked()
{
    setDrawMaskForProcedure(HALL);
}

void MainWindow::on_ProjectionCheckBoxCurrent_clicked()
{
    setDrawMaskForProcedure(LINES_GROUND);
}

void MainWindow::on_NormalCheckBoxCurren_clicked()
{
    setDrawMaskForProcedure(LINES_PROJECTION);
}

void MainWindow::on_RamCheckBoxCurrent_clicked()
{
    setDrawMaskForProcedure(LINES);
}

void MainWindow::on_TrajectoryCurrent_clicked()
{
    setDrawMaskForProcedure(LINES_TRAJECTORY);
}

void MainWindow::setDrawMaskForProcedure(elementsToDraw elem){

    for(QMap<AirPortType, AirPort>::Iterator it =  mainRenderModel->mAirPort.begin(); it != mainRenderModel->mAirPort.end(); it++){
         AirPort *temp = &it.value();

         for (int i = 0; i < temp->mProcedures.size(); i++){
             if (mChoosedProcedures.contains(temp->mProcedures[i].getName())){
                temp->mProcedures[i].setDrawMask(elem, false);
             }
         }
    }

    ui->VisualWidget->setFocus();
}

void MainWindow::setDrawMaskForAirport(elementsToDraw elem){

    for(QMap<AirPortType, AirPort>::Iterator it =  mainRenderModel->mAirPort.begin(); it != mainRenderModel->mAirPort.end(); it++){
         AirPort *temp = &it.value();

         temp->setDrawMask(elem);
    }

    ui->VisualWidget->setFocus();
}

void MainWindow::on_ShowOnlyChooesdButton_clicked()
{
    ui->CurrentObjectoptions->show();
    QList<QTreeWidgetItem*> items = ui->treeWidget->findItems("", Qt::MatchContains | Qt::MatchRecursive);

    if (isShowOnlyChoosed) {
        ui->ShowOnlyChooesdButton->setText("Только выбранные");
        ui->CurrentObjectoptions->hide();

        for (int j = 0; j < items.size(); j++){
            if (mChoosedInTreeViewMenuItems.contains(items[j]->text(0))){
                items[j]->setCheckState(0, Qt::Checked);
            }

            if (mChoosedInTreeViewMenuItems.contains(items[j]->text(0)) && !ui->VisualWidget->getClickedObjects()->contains(items[j]->text(0)) ){
                if (items.at(j)->checkState(0)){
                    on_treeWidget_itemChanged(items[j], 0);
                }
            }
        }

        mChoosedInTreeViewMenuItems.clear();


    } else{
        ui->ShowOnlyChooesdButton->setText("Все");

        for (int j = 0; j < items.size(); j++){

            if (items[j]->checkState(0) == Qt::Checked){
                mChoosedInTreeViewMenuItems.push_back(items[j]->text(0));
            }

            if (ui->VisualWidget->getClickedObjects()->contains(items[j]->text(0))){
                items[j]->setCheckState(0, Qt::Checked);

            }else {
                items[j]->setCheckState(0, Qt::Unchecked);
            }
        }

        for (int j = 0; j < items.size(); j++){

            if (!ui->VisualWidget->getClickedObjects()->contains(items[j]->text(0))){
                if (items.at(j)->checkState(0)){
                    on_treeWidget_itemChanged(items[j], 0);
                }
            }

        }

    }

    isShowOnlyChoosed = 1 - isShowOnlyChoosed;
    ui->VisualWidget->setFocus();
}

void MainWindow::on_ViewSettings_triggered()
{
    mViewOptionWindow = new ViewOption(ui);
    mViewOptionWindow->setStyleSheet(mStyleQSS);
    mViewOptionWindow->setWindowFlags(Qt::WindowStaysOnTopHint);

    mViewOptionWindow->show();
    mViewOptionWindow->activateWindow();
}

void MainWindow::on_saveAsImage_triggered()
{
    QString file = QFileDialog::getSaveFileName(this, "Save as...", "name", "PNG (*.png);; BMP (*.bmp);;TIFF (*.tiff *.tif);; JPEG (*.jpg *.jpeg)");
    ui->VisualWidget->grabFrameBuffer(true).save(file);
}

void MainWindow::on_resetChoosedButton_clicked()
{
    ui->VisualWidget->clearClickedObjects();
}

void MainWindow::on_ButtonVerticalView_clicked()
{
    mCamera->startRotationToVerticalView();
    ui->VisualWidget->setFocus();
}

void MainWindow::on_ButtonLookToNorth_clicked()
{
    mCamera->startRotationToTheNorthPole();
    ui->VisualWidget->setFocus();
}

void MainWindow::on_ExitButton_triggered()
{
    QApplication::quit();
}

void MainWindow::on_CameraSpeedSlider_sliderReleased()
{
    float rate = ui->CameraSpeedSlider->maximum() - ui->CameraSpeedSlider->value() + 1;
    SettingsClass->setCameraSpeed(rate);
}

void MainWindow::on_ButtonCameraLeft_pressed()
{
    mCamera->startRotationAroundEarth(LEFT);
}

void MainWindow::on_ButtonCameraUp_pressed()
{
    mCamera->startRotationAroundEarth(UP);
}

void MainWindow::on_ButtonCameraDown_pressed()
{
    mCamera->startRotationAroundEarth(DOWN);
}

void MainWindow::on_ButtonCameraRight_pressed()
{
    mCamera->startRotationAroundEarth(RIGHT);
}


void MainWindow::on_ButtonCameraLeft_released()
{
    mCamera->stopAllMoves();
}

void MainWindow::on_ButtonCameraUp_released()
{
    mCamera->stopAllMoves();
}

void MainWindow::on_ButtonCameraRight_released()
{
    mCamera->stopAllMoves();
}

void MainWindow::on_ButtonCameraDown_released()
{
    mCamera->stopAllMoves();
}

void MainWindow::on_AirportImport_triggered()
{
    QString str = QFileDialog::getOpenFileName(0, "Open Dialog", "");
    addAirport(str);
}

//void MainWindow::on_pushButton_2_clicked()
//{
//    QPropertyAnimation* animation = new QPropertyAnimation(ui->toolBox, "geometry");
//    animation->setDuration(1000);
//    animation->setEasingCurve(QEasingCurve::OutBounce);
//    animation->setStartValue(QRect(x(), y(), 300, 300));

//    qDebug() << size().width() << size().height() << x() << y();
//    animation->setEndValue(QRect(x(), y(), 0, 0));
//    animation->start();
//}

void MainWindow::on_pushButton_clicked()
{
    QPropertyAnimation* animation = new QPropertyAnimation(ui->treeWidget, "geometry");
    animation->setDuration(1000);
    animation->setEasingCurve(QEasingCurve::OutBounce);
    animation->setStartValue(QRect(x(), y(), 0, 0));

    qDebug() << size().width() << size().height() << x() << y();
    animation->setEndValue(QRect(x(), y(), 300, 300));
    animation->start();
    ui->treeWidget->setVisible(true);
}
