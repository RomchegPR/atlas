#include "MainSettingsClass.h"
#include "MainRenderModel.h"

mainSettingsClass mainSettingsClass::instance;
bool mainSettingsClass::isCreated;

mainSettingsClass::mainSettingsClass()
{
    setDefault();
}

void mainSettingsClass::setDefault(){
    ProceduresColors[STAR] = glm::vec3(1.0f, 1.0f, 0.0f);
    ProceduresColors[DEPARTURE] = glm::vec3(0.8f, 0.8f, 0.8f);
    ProceduresColors[APRCH] = glm::vec3(0.0f, 1.0f, 0.0f);

    ZonesColors[PRIVATE] = glm::vec3(1.0, 0.0, 0.0);
    ZonesColors[RESTRICTED] = glm::vec3(1.0, 0.64, 0.29);
    ZonesColors[RESPONSIBLE] = glm::vec3(0.0, 0.0, 1.0);

    CheckedObjectColor = glm::vec3(1.0f, 0.0f, 1.0f);

    ObjectsTrancparency[TRANCPARENCY_HALL] = 0.1f;
    ObjectsTrancparency[TRANCPARENCY_LINE] = 0.25f;
    ObjectsTrancparency[TRANCPARENCY_FILL_ZONE] = 0.1f;
    ObjectsTrancparency[TRANCPARENCY_FRAME_ZONE] = 0.3f;
    ObjectsTrancparency[TRANCPARENCY_LEGEND] = 1.0f;

    LinesWidth[LINE_BASE] = 1.0f;
    LinesWidth[LINE_TRAJECTORY] = 1.0f;
    LinesWidth[LINE_RAM] = 0.15f;
    LinesWidth[LINE_ZONE_FRAME] = 0.15f;

    LightSettings.Position = glm::vec3(-27414.400 * 1.3, 54807.400 * 1.3, 17859.900 * 1.3);
    LightSettings.lightDir = glm::vec3(0.3f, -1.3f, 0.3f);

    LightSettings.lightAmbient = 0.8f;
    LightSettings.lightDiffuse = 0.9f;
    LightSettings.lightSpecular = 0.3f;

    LightSettings.lightShineConstant = 1.0f;
    LightSettings.lightShineLinear = 0.000000003f;
    LightSettings.lightShineQuadratic = 0.00000000001f;

    LightSettings.materialDiffuse = 0.5f;
    LightSettings.materialShine = 64.0f;
    LightSettings.DistKoef = 1.3f;

    BarrierColor = glm::vec3(1.0f, 0.0f, 0.0f);
    BarrierTrancparency = 1.0f;

    LegendTextSize = 9;
    LegendTextColor = glm::vec3(1.0f, 1.0f, 1.0f);
    LegendFrameColor = glm::vec3(1.0f, 1.0f, 1.0f);

    isDrawEarthGrid = true;
    HeightRate = 10;

    CameraSpeed = 1;
}

bool mainSettingsClass::getIsDrawEarthGrid() const
{
    return isDrawEarthGrid;
}

void mainSettingsClass::setIsDrawEarthGrid(bool value)
{
    isDrawEarthGrid = value;
}

glm::vec3 mainSettingsClass::getCheckedObjectColor() const
{
    return CheckedObjectColor;
}

void mainSettingsClass::setCheckedObjectColor(const glm::vec3 &value)
{
    CheckedObjectColor = value;
}

int mainSettingsClass::getHeightRate() const
{
    return HeightRate;
}

void mainSettingsClass::setHeightRate(int value)
{
    HeightRate = value;
}

float mainSettingsClass::getObjectsTrancparency(Trancparency type) const
{
    return ObjectsTrancparency[type];
}

void mainSettingsClass::setObjectsTrancparency(Trancparency type, float value)
{
    ObjectsTrancparency[type] = value/100.0f;
}

glm::vec3 mainSettingsClass::getProceduresColors(ProcedureType type) const
{
    return ProceduresColors[type];
}

void mainSettingsClass::setProceduresColors(ProcedureType type, glm::vec3 value)
{
    ProceduresColors[type] = value;
}

glm::vec3 mainSettingsClass::getZonesColors(TypesForZones type) const
{
    return ZonesColors[type];
}

void mainSettingsClass::setZonesColors(TypesForZones type, glm::vec3 value)
{
    ZonesColors[type] = value;
}


float mainSettingsClass::getLinesWidth(LinesType type) const
{
    return LinesWidth[type];
}

void mainSettingsClass::setLinesWidth(LinesType type, float value)
{
    LinesWidth[type] = value / 20.0f;
}

Light* mainSettingsClass::getLight(){
    return &LightSettings;
}

float mainSettingsClass::getBarrierTrancparency() const
{
    return BarrierTrancparency;
}

void mainSettingsClass::setBarrierTrancparency(float value)
{
    BarrierTrancparency = value;
}

glm::vec3 mainSettingsClass::getBarrierColor() const
{
    return BarrierColor;
}

void mainSettingsClass::setBarrierColor(const glm::vec3 &value)
{
    BarrierColor = value;
}

float mainSettingsClass::getBarrierSize() const
{
    return BarrierSize;
}

void mainSettingsClass::setBarrierSize(float value)
{
    BarrierSize = value;
}

int mainSettingsClass::getLegendTextSize() const
{
    return LegendTextSize;
}

void mainSettingsClass::setLegendTextSize(int value)
{
    LegendTextSize = value;
}

glm::vec3 mainSettingsClass::getLegendTextColor() const
{
    return LegendTextColor;
}

void mainSettingsClass::setLegendTextColor(const glm::vec3 &value)
{
    LegendTextColor = value ;
}

glm::vec3 mainSettingsClass::getLegendFrameColor() const
{
    return LegendFrameColor;
}

void mainSettingsClass::setLegendFrameColor(const glm::vec3 &value)
{
    LegendFrameColor = value;
}


float Light::getDistKoef() const
{
    return DistKoef;
}

void Light::setDistKoef(float value)
{
    DistKoef = value;
}

float Light::getLightAmbient() const
{
    return lightAmbient;
}

void Light::setLightAmbient(float value)
{
    lightAmbient = value;
}

float Light::getLightDiffuse() const
{
    return lightDiffuse;
}

void Light::setLightDiffuse(float value)
{
    lightDiffuse = value;
}

float Light::getLightSpecular() const
{
    return lightSpecular;
}

void Light::setLightSpecular(float value)
{
    lightSpecular = value;
}

glm::vec3 Light::getLightDir() const
{
    return lightDir;
}

void Light::setLightDir(const glm::vec3 &value)
{
    lightDir = value;
}

float Light::getLightShineConstant() const
{
    return lightShineConstant;
}

void Light::setLightShineConstant(float value)
{
    lightShineConstant = value;
}

float Light::getLightShineLinear() const
{
    return lightShineLinear;
}

void Light::setLightShineLinear(float value)
{
    lightShineLinear = value;
}

float Light::getLightShineQuadratic() const
{
    return lightShineQuadratic;
}

void Light::setLightShineQuadratic(float value)
{
    lightShineQuadratic = value;
}

float Light::getMaterialDiffuse() const
{
    return materialDiffuse;
}

void Light::setMaterialDiffuse(float value)
{
    materialDiffuse = value;
}

float Light::getMaterialShine() const
{
    return materialShine;
}

void Light::setMaterialShine(float value)
{
    materialShine = value;
}

glm::vec3 Light::getPosition() const
{
    return Position;
}

void Light::setPosition(const glm::vec3 &value)
{
    Position = value;
}

QString mainSettingsClass::getApplicationDirPath() const
{
    return ApplicationDirPath;
}

void mainSettingsClass::setApplicationDirPath(const QString &value)
{
    ApplicationDirPath = value;
}

int mainSettingsClass::getCameraSpeed() const
{
    return CameraSpeed;
}

void mainSettingsClass::setCameraSpeed(int value)
{
    CameraSpeed = value;
}
