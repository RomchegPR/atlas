#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 normal;
layout (location = 5) in vec3 heightVec;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;
out vec4 ourColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform sampler2D heightmap;
uniform bool useHeightMap;
uniform bool useHeightVec;
uniform float heightRate;

uniform int minHeight = 0;
uniform int maxHeight = 8848;

void main()
{
	float posMinHeight = minHeight / 8848.0 ;
    float posMaxHeight = maxHeight / 8848.0 ;

    float h = (posMaxHeight - posMinHeight)*texture2D(heightmap, texCoords).r + posMinHeight;

    vec3 position1 = vec3(position.x * h, position.y * h, position.z * h);
    vec3 normalPosition = normalize(position);
    h = h*heightRate;
    vec3 addposition = vec3(normalPosition.x * h,normalPosition.y * h, normalPosition.z * h);

    if (useHeightVec) {
        vec3 addposition1 = vec3(heightVec.x * heightRate, heightVec.y * heightRate, heightVec.z * heightRate);
        gl_Position = projection * view *  model * vec4(position + addposition1, 1.0f);
    } else  if (useHeightMap){
        gl_Position = projection * view *  model * vec4(position + addposition, 1.0f);
    } else {
        gl_Position = projection * view *  model * vec4(position, 1.0f);
    }


    
    FragPos = vec3(model * vec4(position, 1.0f));
    Normal = mat3(transpose(inverse(model))) * normal;

    ourColor = color;
    TexCoords = texCoords;
}
