#version 330 core
out vec4 FragColor;
in vec2 TexCoords;

uniform sampler2D image;
uniform bool horizontal;

struct WeightStruct {
    float weight1;
    float weight2;
    float weight3;
    float weight4;
    float weight5;
};

uniform WeightStruct Weight;

void main()
{             

    float weight[5] = float[] (1.0, 0.0, 0.0, 0.0, 0.0);

    weight[0] = Weight.weight1;
    weight[1] = Weight.weight2;
    weight[2] = Weight.weight3;
    weight[3] = Weight.weight4;
    weight[4] = Weight.weight5;

     vec2 tex_offset = 1.0 / textureSize(image, 0); // gets size of single texel
     vec3 result = texture(image, TexCoords).rgb * weight[0];
     if(horizontal)
     {
         for(int i = 1; i < 5; ++i)
         {
            result += texture(image, TexCoords + vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
            result += texture(image, TexCoords - vec2(tex_offset.x * i, 0.0)).rgb * weight[i];
         }
     }
     else
     {
         for(int i = 1; i < 5; ++i)
         {
             result += texture(image, TexCoords + vec2(0.0, tex_offset.y * i)).rgb * weight[i];
             result += texture(image, TexCoords - vec2(0.0, tex_offset.y * i)).rgb * weight[i];
         }
     }
     FragColor = vec4(result, 1.0);
}
