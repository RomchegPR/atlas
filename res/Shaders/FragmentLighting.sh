#version 330 core

layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

//out vec4 color;

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;
in vec4 ourColor;

uniform vec3 viewPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

struct Light {
    vec3 position;

    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
};

struct Material {
    sampler2D diffuse;

    vec3      ambient;
    vec3      specular;
    float     shininess;
    vec3      pos;
};

uniform Light light;
uniform Material material;

uniform bool isBloom;



void main()
{
    vec3 ambient;
    vec3 diffuse;
    if (TexCoords.x != 0) {
       ambient  = light.ambient * vec3(texture(material.diffuse, TexCoords));
    }
    else {
        ambient = light.ambient * vec3(ourColor.x, ourColor.y, ourColor.z) ;
    }
        // Diffuse
        vec3 norm = normalize(Normal);
        vec3 lightDir = normalize(light.position - FragPos);

        float diff = max(dot(norm, lightDir), 0.0);

    if (TexCoords.x != 0){
        diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));
    } else {
        diffuse = light.diffuse * diff * vec3(ourColor.x, ourColor.y, ourColor.z) ;
    }
        // Specular
        vec3 viewDir = normalize(viewPos - FragPos);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
        vec3 specular = light.specular * (spec * material.specular);

        float distance    = length(light.position - FragPos);
        float attenuation = 1.0f / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

        ambient  *= attenuation;
        diffuse  *= attenuation;
        specular *= attenuation;

        vec3 result = ambient + diffuse + specular;

        float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));

	//	float h = texture2D(heightmap, TexCoords).r;
	//	FragColor = vec4(h,h,h,1.0);
		
        FragColor = vec4(result, ourColor.a);

        if (isBloom){
            BrightColor = vec4(result, ourColor.a);

        }
}
