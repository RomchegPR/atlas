#version 330 core
in vec4 ourColor;
in vec2 Texcoord;

out vec4 color;

uniform sampler2D uiSampler;

void main()
{
    vec4 texel = texture(uiSampler, Texcoord);
    if(texel.a < 0.5) discard;
        color = texel;
    if (Texcoord.x != 0)
        color = ourColor * texture(uiSampler, Texcoord);
    else color = ourColor;
}
