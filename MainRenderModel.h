#ifndef MAINRENDERMODEL_H
#define MAINRENDERMODEL_H

#include "include/AirPort/AirPort.h"
#include "include/Earth/Earth.h"
#include "include/Model/Model.h"
#include "include/Zones/ZoneBuilder.h"
#include "include/Shader/ShaderProgram.h"
#include "include/Lamp.h"
#include "include/CubeMap.h"
#include "include/Barrier.h"
#include "AirPlane.h"

#include <QVector>
#include <QMap>

 enum ShaderType {
     LightShader,
     LampShader,

     BasicShader,
     ModelsShader,
     CubeMapShader,

     BloomFinalShader,
     BlurShader,

     ScreenShader
 };

class SingleMainRenderModel
{
    static SingleMainRenderModel instance;
    static bool isCreated;


    SingleMainRenderModel() {};
    ~SingleMainRenderModel() {};

public:
    static SingleMainRenderModel* Instance(){

        if (isCreated) {
            return &instance;
        } else {
            isCreated = true;
            instance = SingleMainRenderModel();
            return &instance;
        }
    }


public:

    static QMap < AirPortType, AirPort >           mAirPort;
    static QMap < QString,  AirPlane >             mAirPlanes;
    static ZoneBuilder                             mZoneBuilder;
    static Earth                                   mEarth;
    static Earth                                   mEarth2;
    static QVector < Model >                       mModel;
    static Lamp                                    mLamp;
    static CubeMap                                 mCubeMap;
    static QVector < Barrier >                     mBarriers;

    static QMap < ShaderType , ShaderProgram >     mShaders;

    glm::mat4                                      mProjectionMatrix;
    glm::mat4                                      mModelMatrix;
    glm::mat4                                      mViewMatrix;

    static QString                                 mTextureEarthFileName;
    static QString                                 mTextureHeightMapFileName;

};

#endif // MAINRENDERMODEL_H
