#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qtreewidget.h"
#include "include/Headers.h"
#include <qcompleter.h>
#include "QFileDialog"
#include "include/TreeWidgetModel.h"
#include "MainRenderModel.h"

class MainWidget;
class mainSettingsClass;
class Camera;

namespace Ui {
class MainWindow;

}

class ViewOption;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void fillData                               ( );
    MainWidget *getMainWidget                   ( );
    void init                                   ( );

private slots:
    void MainWidgetInitializeGLisComplete       ( );
    void on_SearchLine_returnPressed            ( );
    void on_FillCheckBox_clicked                ( );
    void on_NormalCheckBox_clicked              ( );
    void on_ProjectionCheckBox_clicked          ( );
    void on_RamCheckBox_clicked                 ( );
    void on_FindButton_clicked                  ( );

    void on_treeWidget_itemChanged              ( QTreeWidgetItem *item,
                                                  int column );

    void closeEvent                             ( QCloseEvent *event );
    void on_ScaleSliderH_sliderReleased();
    void choosedProcedure                       (QSet < QString > setName);
    void on_Trajectory_clicked();
    void on_FillCheckBoxCurrent_clicked();
    void on_ProjectionCheckBoxCurrent_clicked();
    void on_NormalCheckBoxCurren_clicked();
    void on_RamCheckBoxCurrent_clicked();
    void on_TrajectoryCurrent_clicked();
    void updateChecksForCurrent();
    void on_pointsCheckBox_clicked();
    void on_ShowOnlyChooesdButton_clicked();
    void on_ViewSettings_triggered();
    void on_saveAsImage_triggered();
    void on_ButtonVerticalView_clicked();
    void on_ButtonLookToNorth_clicked();
    void on_resetChoosedButton_clicked();

    void on_ExitButton_triggered();

    void on_CameraSpeedSlider_sliderReleased();

    void on_ButtonCameraLeft_pressed();

    void on_ButtonCameraUp_pressed();

    void on_ButtonCameraDown_pressed();

    void on_ButtonCameraRight_pressed();

    void on_ButtonCameraLeft_released();

    void on_ButtonCameraUp_released();

    void on_ButtonCameraRight_released();

    void on_ButtonCameraDown_released();

    void on_AirportImport_triggered();



    void on_pushButton_clicked();

signals:
    void ChangeRate                             ( float value,
                                                  char type );

private:

    void addAirport(QString filePath);

    Ui::MainWindow *            ui;
    SingleMainRenderModel *     mainRenderModel;
    mainSettingsClass *         SettingsClass;

    QCompleter *                mStringSearch;

    TreeWidgetModel *           mTreeWidgetModel;
    ViewOption *                mViewOptionWindow;

    void setDrawMaskForProcedure                (elementsToDraw elem);
    void setDrawMaskForAirport                  (elementsToDraw elem);

    QSet < QString >            mChoosedProcedures;
    QString                     mStyleQSS;
    Camera *                    mCamera;
    QList<QTreeWidgetItem*>     mSaveChoosedItems;
    bool                        isShowOnlyChoosed;
    QVector<QString>            mChoosedInTreeViewMenuItems;

};

#endif // MAINWINDOW_H

