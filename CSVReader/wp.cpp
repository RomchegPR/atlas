#include "CSVReader/wp.h"

QString WP::getID() const
{
    return ID;
}

void WP::setID(const QString &value)
{
    ID = value;
}

QString WP::getLat() const
{
    return Lat;
}

void WP::setLat(const QString &value)
{
    Lat = value;
}

QString WP::getLon() const
{
    return Lon;
}

void WP::setLon(const QString &value)
{
    Lon = value;
}

QString WP::getDesriptior() const
{
    return Desriptior;
}

void WP::setDesriptior(const QString &value)
{
    Desriptior = value;
}

QString WP::getAD() const
{
    return AD;
}

void WP::setAD(const QString &value)
{
    AD = value;
}

QString WP::toString() const
{
    QString str;

    str += "Id : ";
    str += this->getID();
    str += "\n";

    str += "Lat : ";
    str += this->getLat();
    str += "\n";

    str += "Lon : ";
    str += this->getLon();
    str += "\n";

    str += "Desriptior : ";
    str += this->getDesriptior();
    str += "\n";

    str += "AD : ";
    str += this->getAD();
    str += "\n";

    return str;

}

WP::WP()
{

}
