#ifndef WP_H
#define WP_H
#include <QString>

class WP
{
private:
    QString ID;
    QString Lat;
    QString Lon;
    QString Desriptior;
    QString AD;
public:
    WP();
    QString getID() const;
    void setID(const QString &value);
    QString getLat() const;
    void setLat(const QString &value);
    QString getLon() const;
    void setLon(const QString &value);
    QString getDesriptior() const;
    void setDesriptior(const QString &value);
    QString getAD() const;
    void setAD(const QString &value);
    QString toString() const;
};

#endif // WP_H
