#ifndef CSVREADER_H
#define CSVREADER_H

#include <QFile>
#include <QStringList>
#include <QDebug>
#include "CSVReader/wp.h"
#include "include/AirPort/WayPoint.h"

class CSVReader
{
public:
    CSVReader() {}
    ~CSVReader();
    QList<WP> readWPs(QString fileName);
    QVector <QVector <Waypoint> > readWayPoints(QString fileName);
private:
    QList<QList<QString>> readFile(QString fileName);
    int flToMetres(QString h);
};

#endif // CSVREADER_H
