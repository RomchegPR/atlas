#include "CSVReader/csvreader.h"
#include "include/GeoHelper.h"

CSVReader::~CSVReader()
{

}

QList<QList<QString>> CSVReader::readFile(QString fileName)
{
    QFile file(fileName);
    QList<QList<QString>> data;
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Всё плохо, всё сломалось, беги к программистам с этой ошибкой -> " << file.errorString();
    } else {
        while (!file.atEnd()) {
            QString line = file.readLine();
            data.append(line.split(';'));
        }
    }
    return data;
}


QList<WP> CSVReader::readWPs(QString fileName)
{
    QList<QList<QString>> data = this->readFile(fileName);
    QList<WP> WPs;
    for(QList<QString> i : data){
        WP wp;
        wp.setID(i.at(0));
        wp.setLat(i.at(1));
        wp.setLon(i.at(2));
        wp.setDesriptior(i.at(3));
        wp.setAD(i.at(4));
        WPs.append(wp);
    }
    return WPs;
}

int CSVReader::flToMetres(QString h){
    double x;
    if (h.contains("FL")){
        h.remove("FL");
        //h.remove(0);
        x = h.toDouble()*100*0.3048;
        int y = int(x) % 50;
        if (y > 25){
            x += 50 - y;
        } else {
            x -= y;
        }
    } else {
        h.remove("m");
        x = h.toDouble();
    }
    return int(x);
}

QVector<QVector<Waypoint>> CSVReader::readWayPoints(QString fileName)
{
    QVector <QVector <Waypoint> > Procs;
    QVector<Waypoint> Waypoints;
    QList<QList<QString>> data = this->readFile(fileName);
    Waypoint wp;
    QString lastProcName = "-";

    for(QList<QString> i : data){
        if (i.at(0).contains("procName")){
            continue;
        }
        Waypoint wp;
        if (!i.at(0).isEmpty()){
            if (lastProcName != "-"){
                QVector <Waypoint> temp;
                temp = Waypoints;
                Procs.append(temp);
                Waypoints.clear();
            }

            lastProcName = i.at(0);
        }
        wp.mProcedureName = lastProcName;
        wp.mName = (i.at(1));
        wp.minAlt = this->flToMetres(i.at(2));
        wp.maxAlt = this->flToMetres(i.at(3));
        wp.lat = i.at(4).toDouble();
        wp.lon = i.at(5).toDouble();

        glm::vec3 point = GeoHelper::geo2cartesian(glm::vec3 (wp.lon, wp.lat, 0), false);
        point = glm::vec3(-point.x, point.z, point.y);
        wp.x = point.x;
        wp.y = point.y;
        wp.z = point.z;

        wp.type = AIRWAY;
        Waypoints.append(wp);

    }
    return Procs;
}


