#ifndef AIRPLANE_H
#define AIRPLANE_H

#include "include/RenderObject/RenderObject.h"
#include "include/AirPort/WayPoint.h"
#include <glm/glm.hpp>
#include <QQueue>

class SingleMainRenderModel;
class mainSettingsClass;

class AirPlane : public RenderObject
{

    struct TrackPoint{
        glm::vec2   PositionGeo;
        glm::vec3   Position;
        double      Altitude;

    };

public:
    AirPlane();

    void            init                    ();
    void            paint                   ();
    void            clear                   ();

    void            addTrackPoint           (glm::vec2 geoPos,
                                             double alt);
    void            addTrackPoint           (double lan,
                                             double lon,
                                             double alt);

    void            update                  ();

    void            calcLine                ();
    void            calcNormal              ();

    bool            isSamePoint             (glm::vec2 geoPos1,
                                             glm::vec2 geoPos2);

    void            setPositionGeo          ( glm::vec3 pos );

    QString         getName                 () const;
    double          getCourse               () const;
    double          getAltitude             () const;
    double          getSize                 () const;
    glm::vec4       getColor                () const;
    glm::vec3       getCurCoord3D           () const;

    void            setName                 (const QString &value);
    void            setCourse               (double value);
    void            setSize                 (double value);
    void            setColor                (const glm::vec4 &value);

private:

    QVector < glm::vec3 >           CubeVertexes;
    QVector < glm::vec3 >           CubeVertexes2Step;

    QQueue <TrackPoint>             Track; // pos.x - lon, pos.y = lat, pos.z = alt
    int                             TrackSize;

    QString                         Name;
    double                          Course;

    double                          Size;
    glm::vec4                       Color;

    SingleMainRenderModel*          MainRenderModel;
    mainSettingsClass *             SettingsClass;

};

#endif // AIRPLANE_H
