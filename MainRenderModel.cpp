#include "MainRenderModel.h"

bool SingleMainRenderModel::isCreated = false;
SingleMainRenderModel SingleMainRenderModel::instance;

QMap < AirPortType, AirPort >           SingleMainRenderModel::mAirPort;
QMap < QString,  AirPlane >             SingleMainRenderModel::mAirPlanes;
ZoneBuilder                             SingleMainRenderModel::mZoneBuilder;
Earth                                   SingleMainRenderModel::mEarth;
Earth                                   SingleMainRenderModel::mEarth2;
QVector < Model >                       SingleMainRenderModel::mModel;
Lamp                                    SingleMainRenderModel::mLamp;
CubeMap                                 SingleMainRenderModel::mCubeMap;
QVector < Barrier >                     SingleMainRenderModel::mBarriers;

QMap  < ShaderType , ShaderProgram >    SingleMainRenderModel::mShaders;

QString                                 SingleMainRenderModel::mTextureEarthFileName;
QString                                 SingleMainRenderModel::mTextureHeightMapFileName;
