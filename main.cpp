#include "MainWindow.h"
#include <QApplication>
#include <QtConcurrent>
#include <QDesktopWidget>

//#include <QGraphicsView>

//#include <QGLWidget>

#include <QPushButton>

//#include <QGraphicsScene>
#include <QVBoxLayout>
//#include <qjson/parser.h>
//#include <QWebView>
#include "include/MySplashScreen.h"
#include "MainSettingsClass.h"
#include <algorithm>
#include <cstdlib>
#include <iostream>

MainWindow* initWindow(){
    return new MainWindow();
}

int main(int argc, char *argv[])
{

    vector<int> srcVec;
    for (int val = 0; val < 10; val++)
    {
        srcVec.push_back(val);
    }

    vector<double> destVec;

    transform(srcVec.begin(), srcVec.end(),
              back_inserter(destVec), [] (int _n) -> double
    {
        if (_n < 5)
            return _n + 1.0;
        else if (_n % 2 == 0)
            return _n / 2.0;
        else
            return _n * _n;
    });

    ostream_iterator<double> outIt(cout, " ");
    copy(destVec.begin(), destVec.end(), outIt);
    cout << endl;


    QApplication a(argc, argv);
    mainSettingsClass *SettingsClass = mainSettingsClass::Instance();
    SettingsClass->setApplicationDirPath(QCoreApplication::applicationDirPath ());

    MySplashScreen splashScreeen;

    splashScreeen.show();
    a.processEvents();

    MainWindow w;

    QTime time;
    time.start();
    while(time.elapsed() < 1000){
        a.processEvents();
    }


    w.init();
    w.fillData();


    QDesktopWidget desktop;
    QRect rect = desktop.availableGeometry(desktop.primaryScreen());
    QPoint center = rect.center();
    center.setX(center.x() - w.width()/2);
    center.setY(center.y() - w.height()/2);


    w.move(center);

    w.show();

    splashScreeen.finish(&w);

    return a.exec();
}
