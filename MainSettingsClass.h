#ifndef MAINSETTINGSCLASS_H
#define MAINSETTINGSCLASS_H

#include "glm/common.hpp"
#include <QMap>
#include <include/Headers.h>

class SingleMainRenderModel;

struct Light {

    glm::vec3 Position;

    float lightAmbient;
    float lightDiffuse;
    float lightSpecular;

    glm::vec3 lightDir;

    float lightShineConstant;
    float lightShineLinear;
    float lightShineQuadratic;

    float materialDiffuse;
    float materialShine;

    float DistKoef;


public:
    glm::vec3 getPosition() const;
    glm::vec3 getLightDir() const;

    float getDistKoef() const;
    float getLightAmbient() const;
    float getLightDiffuse() const;
    float getLightSpecular() const;

    float getLightShineConstant() const;
    float getLightShineLinear() const;
    float getLightShineQuadratic() const;
    float getMaterialDiffuse() const;
    float getMaterialShine() const;

    void setPosition(const glm::vec3 &value);

    void setLightAmbient(float value);
    void setLightDiffuse(float value);
    void setLightSpecular(float value);

    void setLightDir(const glm::vec3 &value);

    void setLightShineConstant(float value);
    void setLightShineLinear(float value);
    void setLightShineQuadratic(float value);

    void setMaterialDiffuse(float value);
    void setMaterialShine(float value);

    void setDistKoef(float value);
};

class mainSettingsClass
{
private:

    static mainSettingsClass instance;
    static bool isCreated;

    mainSettingsClass          ( );
    ~mainSettingsClass         ( )  {}

public:

    static mainSettingsClass* Instance(){

        if (isCreated) {
            return &instance;
        } else {
            isCreated = true;
            instance = mainSettingsClass();
            return &instance;
        }
    }

    float   getObjectsTrancparency        (Trancparency type ) const;
    void    setObjectsTrancparency        (Trancparency type, float value );

    glm::vec3 getProceduresColors(ProcedureType type) const;
    void setProceduresColors(ProcedureType type, glm::vec3 value);

    glm::vec3 getZonesColors(TypesForZones type) const;
    void setZonesColors(TypesForZones type, glm::vec3 value);

    float getLinesWidth(LinesType type) const;
    void setLinesWidth(LinesType type, float value);

    Light* getLight();

    float getBarrierTrancparency() const;
    void setBarrierTrancparency(float value);

    glm::vec3 getBarrierColor() const;
    void setBarrierColor(const glm::vec3 &value);

    float getBarrierSize() const;
    void setBarrierSize(float value);

    int getLegendTextSize() const;
    void setLegendTextSize(int value);

    glm::vec3 getLegendTextColor() const;
    void setLegendTextColor(const glm::vec3 &value);

    glm::vec3 getLegendFrameColor() const;
    void setLegendFrameColor(const glm::vec3 &value);

    void setDefault();

    bool getIsDrawEarthGrid() const;
    void setIsDrawEarthGrid(bool value);

    glm::vec3 getCheckedObjectColor() const;
    void setCheckedObjectColor(const glm::vec3 &value);

    int getHeightRate() const;
    void setHeightRate(int value);

    QString getApplicationDirPath() const;
    void setApplicationDirPath(const QString &value);

    int getCameraSpeed() const;
    void setCameraSpeed(int value);

private:
    //    float                               Trancparency_Hall = 0.1f;
    QMap < Trancparency, float >        ObjectsTrancparency;
    QMap < ProcedureType, glm::vec3 >   ProceduresColors;
    QMap < TypesForZones, glm::vec3 >   ZonesColors;

    glm::vec3                           CheckedObjectColor;

    QMap < LinesType, float >           LinesWidth;

    Light                               LightSettings;

    glm::vec3                           BarrierColor;
    float                               BarrierTrancparency;
    float                               BarrierSize;

    int                                 LegendTextSize;
    glm::vec3                           LegendTextColor;
    glm::vec3                           LegendFrameColor;

    bool                                isDrawEarthGrid;
    int                                 HeightRate;
    int                                 CameraSpeed;
    QString                             ApplicationDirPath;

};

#endif // MAINSETTINGSCLASS_H
