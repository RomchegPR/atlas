#ifndef MainWidget_H
#define MainWidget_H

#include <include/Headers.h>
#include <set>
#include "include/Shader/ShaderProgram.h"
#include "MainRenderModel.h"

#include <QTimer>
#include <QGLWidget>

#include "include/FrameBuffer.h"
#include "include/Barrier.h"

#include "include/Trajectory.h"

#include <QLabel>
#include "QNetworkAccessManager"

#define SortType QMap < double, QVector < RenderObject * > >

class mainSettingsClass;
/********************************

Class:		MainWidget

Purpose:	Main Class that do everything)

            parse data from file, convert into format, that we use.
            fill, initialize and draw Halls and Zones, using HallBuilder
            and ZoneBuilder (this classes use data from converter).
            draw CubeMap and draw ground.

        *    to switch on/of camera           - press @right Mouse button@
        *    to choose nearest hall to camera - press @left Mouse button@
        *    to faster/ slower camera speed   - press @wheel Mouse button@

        *    to switch on/of projection      - press 1 , 2 , 3 , 4 key
        *    to switch on/of halls
        *    to switch on/of rams
        *    to switch on/of trajectory
        *    to watch from sky and rotate to North     - press Space and Ctrl

        *    to build/ unbuild hall   - press F5/F4  key

        *    buttons - show on/off Star/Aprch/Sid

            frame auto update every 34 msec

********************************/
class Camera;

struct CursorOnObject{
    QString     mName;

    ObjectType  mType;
    bool        isActive;

    Hall*       pHall;
    Zone*       pZone;
    Barrier*    pBarrier;
    AirPlane*   pAirPlane;

};

class MainWidget : public QGLWidget
{
    Q_OBJECT

public:

    MainWidget                      (QWidget *parent = 0);
    virtual ~MainWidget();

    // reaction to some actions froom user)
    void                            keyPressEvent               (QKeyEvent *);
    void                            mouseMoveEvent              (QMouseEvent *e);
    void                            wheelEvent                  (QWheelEvent *event);
    void                            mousePressEvent             (QMouseEvent *event);
    void                            mouseDoubleClickEvent       (QMouseEvent * event);
    void                            mouseReleaseEvent           (QMouseEvent *event);

    void                            lookFromAir                 ();
    void                            DrawObj                     (QString s);
    void                            rotationToObject            (QString nameObj);

public:
    void                            paintGL                     ();
    void                            initializeGL                ();
    void                            resizeGL                    (int w, int h);

protected slots:
    void                            checkMouseCoord             ();
    void                            earthInertia                ();
    //void                            replyFinished               (QNetworkReply* reply);

signals:
    void                            initializeGLisComplete      ();
    void                            procedureChoosed            (QSet < QString > setNames);


private:
    void                            sortObjects                 ();
    GLuint                          generateAttachmentTexture   (GLboolean depth, GLboolean stencil);
    void                            RenderQuad                  ();

    void                            paintEvent                  ( QPaintEvent * );
    void                            drawLegend                  ( QPainter* painter );
    void                            drawObjectLegend            ( QPainter* painter );
    void                            drawFooter                  ( QPainter* painter );

    void                            drawLabels                  ( QPainter* painter );
    void                            DrawLabelOnWorldPos         ( glm::vec3 pos,
                                                                  QString name,
                                                                  QPainter* painter,
                                                                  bool drawNecessarily,
                                                                  QColor color);

    glm::vec3                       getScreenCoord               ( glm::vec3 );


    void                            setBloorWeights             ( float a1,
                                                                  float a2,
                                                                  float a3,
                                                                  float a4,
                                                                  float a5 );
    void                            changeStateClicledObject    ( QString objectName );

public:

    QSet < QString >                mNameObj; //names of obj to draw
    void                            setShowPoints               (bool value);
    QSet < QString > *              getClickedObjects           ( );
    void                            setVerticalView             (GLboolean value);
    void                            setLookToNorth              (GLboolean value);
    void                            clearClickedObjects         ( );

private:

    QSet < QString >                mClickedObjects;
    QSet < QString >                mObjectsCursorOn;

    CursorOnObject*                 mCursorOnObject;

    //for moving

    QTimer                          mTimer, mTimer2, mTimer3;  //mTimer - updateGL        mTimer2 - checkMouseCoord

    GLint                           mXOffset;
    GLint                           mYOffset;   //Differnce between last and current mouse coordinates

    GLfloat                         mLastX;
    GLfloat                         mLastY;     //last position of mouse'
    QPoint                          mCurMousePos;

    GLboolean                       mIsMidButtonPressed;
    GLboolean                       mIsLeftButtonPressed;
    GLboolean                       mIsRightButtonPressed;
    GLboolean                       mVerticalView;
    GLboolean                       mLookToNorth;

    GLboolean                       mAntiBugFlag;

    GLuint                          mWidgetW;
    GLuint                          mWidgetH;

    Ray                             mRay;

    SingleMainRenderModel*          MainRenderModel;
    Camera*                         mCamera;
    mainSettingsClass *             SettingsClass;


    QPoint                          mStartRect;
    QPoint                          mEndRect;

    GLuint                          quadVAO, quadVBO;
    GLuint                          mRBO;

    FrameBuffer                     mFrameBuffer;
    FrameBuffer                     mBloorFramBuffer[2];

    Texture                         mColorBuffer[2];
    Texture                         mBloorColorBuffer[2];

    SortType                        mSortMap;

    QVector < Barrier >             mBarriers;

    Trajectory                      mTrajectory;
    QSet < QString >                mShowedZoneLabels;
    bool                            mShowPoints;


    glm::vec2 pointRotateAroundOnScreen;

    QNetworkAccessManager           *manager;
private slots:
    void replyFinished(QNetworkReply* reply);
    void updateAirPlanesCoord();
private:
    double myLan, myLon, myAlt;

};

#endif // MainWidget_H
